// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  gatewayES: 'https://search-aoess-pagomundo-lwdjgvtyhactzouiqag34pjkoi.us-east-1.es.amazonaws.com',
  gatewayAPI: 'https://api.pmi-americas.com',
  defaultCurrency: 'USD',
  watchTime: 30000,
  arrayIdExtendedUserPesosMex: [9806,9808,15699],
  arrayIdExtendedUserEuro: [479],
  isProd: false,
};

/*
 *
 * arrayIdExtendedUserEuro #27416 ID: 3198, fullName: Yokebed Flores Torres, email: yoke@iml.ad HARDCODE EURO
 * URL REMOTE 'http://10.104.5.172:8080' LOCAL
 * URL LOCAL  'http://127.0.0.1:8080'
 *
 * URL OUTSIDE:
 * gatewayAPI: 'http://test-pagomundoapi.aconcaguasf.com.ar'
 * gatewayES: 'http://test-pagomundoes.aconcaguasf.com.ar'
 *
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
