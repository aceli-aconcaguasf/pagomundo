export const environment = {
  production: true,
  gatewayES: 'https://search-aoess-pagomundo-lwdjgvtyhactzouiqag34pjkoi.us-east-1.es.amazonaws.com',
  gatewayAPI: 'https://api.pmi-americas.com',
  defaultCurrency: 'USD',
  arrayIdExtendedUserEuro: [479],
  arrayIdExtendedUserPesosMex: [9806,9808,15699],
  watchTime: 30000,
  isProd: true
};
