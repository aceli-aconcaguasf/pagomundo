import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {PagesComponent} from './pages/pages.component';
import {LoginGuard} from './services/service.index';
import {LandingComponent} from './landing/landing.component';
import {AppComponent} from './app.component';
import {pagesRoutes} from './pages/pages.routes';
import {NgModule} from '@angular/core';


const appRoutingModule: Routes = [
  {path: 'account/reset/finish', component: AppComponent},
  {path: 'account/change-email-confirmation', component: AppComponent},
  {path: 'createAccount/:key', component: LandingComponent},
  {path: 'changeEmail/:key', component: LandingComponent},
  {path: 'login', component: LoginComponent},
  {
    path: '',
    component: PagesComponent,
    canActivate: [LoginGuard],
    children: pagesRoutes
  },
  {path: '', component: LoginComponent},
  {path: '**', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutingModule)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
