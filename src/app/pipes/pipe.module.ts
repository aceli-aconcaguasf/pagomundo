import {NgModule} from '@angular/core';
import {FormatCardNumberPipe, SafeImagePipe} from './pipe.index';


@NgModule({
  declarations: [
    FormatCardNumberPipe,
    SafeImagePipe
  ],
  imports: [],
  exports: [
    FormatCardNumberPipe,
    SafeImagePipe
  ]
})

export class PipeModule {
}
