import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatCardNumber'
})
export class FormatCardNumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      if (value.length < 19) {
        // tslint:disable-next-line:variable-name
        value = value.replace(/-/g, '');
        const arrNumber = value.match(/.{1,4}/g);
        if (arrNumber.length === 1) {
          return `${arrNumber[0]}`;
        } else if (arrNumber.length === 2) {
          return `${arrNumber[0]}-${arrNumber[1]}`;
        } else if (arrNumber.length === 3) {
          return `${arrNumber[0]}-${arrNumber[1]}-${arrNumber[2]}`;
        } else if (arrNumber.length === 4) {
          return `${arrNumber[0]}-${arrNumber[1]}-${arrNumber[2]}-${arrNumber[3]}`;
        }
      } else {
        value =  value.replace(/-/g, '');
        const arrNumber = value.match(/.{1,4}/g);
        return  `${arrNumber[0]}-${arrNumber[1]}-${arrNumber[2]}-${arrNumber[3]}`;
      }
    } else {
      return null;
    }

  }

}
