import {Injectable} from '@angular/core';

@Injectable()
export class TokenService {

  TOKEN = '';

  constructor() {
  }

  saveToken(token: string) {
    this.TOKEN = token;
  }

  getToken(): string {
    if (this.TOKEN && this.TOKEN !== '') {
      return this.TOKEN;
    } else {
      return null;
    }
  }
}
