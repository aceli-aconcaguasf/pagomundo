import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';

@Injectable({ providedIn: 'root' })
export class RequestService {

  constructor(private http: HttpClient, private tokenService: TokenService) {
  }

  private static getESHeaders(): HttpHeaders {
    const headers = {
      'Content-type': 'application/json',
      'Authorization' : 'Basic cm9vdDp9ZzJxSi1VLyNnU11wMz1Y'
    };
    return new HttpHeaders(headers);
  }

  public esPostRequest(uri: string, data: any, parameter?: any) {
    return this.http.post(`${environment.gatewayES}${uri}`, data, { headers: RequestService.getESHeaders(), params: parameter });
  }

  public esPutRequest(uri: string, data: any, parameter?: any) {
    return this.http.put(`${environment.gatewayES}${uri}`, data, { headers: RequestService.getESHeaders(), params: parameter });
  }


  public getRequest(uri: string, retrieveHeaders = false) {
    if (retrieveHeaders) {
      return this.http.get(`${environment.gatewayAPI}${uri}`, { headers: this.getHeaders(), observe: 'response' });
    }
    else {
      return this.http.get(`${environment.gatewayAPI}${uri}`, { headers: this.getHeaders() });
    }
  }

  public putRequest(uri: string, data: any, parameter?: any) {
    return this.http.put(`${environment.gatewayAPI}${uri}`, data, { headers: this.getHeaders(), params: parameter });
  }

  public postRequest(uri: string, data: any, parameter?: any) {
    return this.http.post(`${environment.gatewayAPI}${uri}`, data, { headers: this.getHeaders(), params: parameter });
  }

  public postImageRequest(uri: string, data: any, parameter?: any) {
    return this.http.post(`${environment.gatewayAPI}${uri}`, data, { headers: this.getImageHeaders(), params: parameter });
  }

  private getImageHeaders(): HttpHeaders {
    const headers = {
      Authorization: `Bearer ${this.tokenService.getToken()}`,
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data; boundary=---',
      'Cache-Control': 'no-cache',
    };
    return new HttpHeaders(headers);
  }

  private getHeaders(): HttpHeaders {
    const headers = {
      Authorization: `Bearer ${this.tokenService.getToken()}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
    };
    return new HttpHeaders(headers);
  }
}
