import {Injectable} from '@angular/core';
import {RequestService} from '../request.service';
import {Observable} from 'rxjs';


@Injectable({providedIn: 'root'})

export class BankService {

  private bankAccountUri = '/api/bank-accounts';
  private fintechRegisterClient = '/api/bank/fintech/register-client';
  private generateCodePath = '/api/bank/generate-code';
  private authenticateCodePath = '/api/bank/authenticate-code';
  private transactionGroupsCodePath = '/api/transaction-groups-ripple';

  constructor(public requestService: RequestService) {
  }

  updateBankAccount(bankAccount: any): Observable<any> {
    return this.requestService.putRequest(this.bankAccountUri, JSON.stringify(bankAccount));
  }

  bankIntegrationFinitech(bodyBank): Observable<any> {
    return this.requestService.postRequest(this.fintechRegisterClient, JSON.stringify(bodyBank));
  }

  generateCode(bodyGenerate): Observable<any> {
    return this.requestService.postRequest(this.generateCodePath, JSON.stringify(bodyGenerate));
  }

  authenticateCode(bodyGenerate): Observable<any> {
    return this.requestService.postRequest(this.authenticateCodePath, JSON.stringify(bodyGenerate));
  }

  transactionGroups(bodyGenerate): Observable<any> {
    return this.requestService.postRequest(this.transactionGroupsCodePath, JSON.stringify(bodyGenerate));
  }
}
