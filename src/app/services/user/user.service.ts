import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {RequestService} from '../request.service';
import {ExtendedUser} from '../../config/interfaces';


@Injectable()
export class UserService {

  URI = '/api/account';
  extendedUsersURI = '/api/extended-users';
  extendedUsersSearchURI = '/api/_search/extended-users';
  multiURI = '/api/_multi';
  searchURI = '/api/_search';
  extUserFundURI = '/api/extended-user-funds';

  accountBalance = new BehaviorSubject<number>(0);
  logoImage = new BehaviorSubject<any>({});
  menuCollapse = new BehaviorSubject<boolean>(false);

  constructor(public requestService: RequestService) {
  }

  accountResetPassword(newPassword: string, key: string): Observable<any> {
    const uriMethod = `${this.URI}/reset-password/finish`;
    const body = {
      key,
      newPassword
    };
    return this.requestService.postRequest(uriMethod, JSON.stringify(body));
  }

  authLogin(username: string, password: string): Observable<any> {
    const authURI = `/api/authenticate`;
    const body = {
      username,
      password,
      rememberMe: true
    };
    return this.requestService.postRequest(authURI, JSON.stringify(body));
  }

  changeLanguage(userId:number, language:string): Observable<any> {

    const languageURI = `/api/users/${userId}/langKey/${language}`;
    return this.requestService.postRequest(languageURI, null);
  }

  getAccount(): Observable<any> {
    const accountURI = '/api/account';
    return this.requestService.getRequest(accountURI);
  }

  getExtendedUserByUserId(id: number): Observable<any> {
    const URI = `${this.searchURI}/extended-users?query=user.id:${id}`;
    return this.requestService.getRequest(URI);
  }

  getExtendedUserById(id: number): Observable<any> {
    const URI = `${this.extendedUsersURI}/${id}`;
    return this.requestService.getRequest(URI);
  }

  getSearchExtendedUserById(id: number): Observable<any> {
    const URI = `${this.extendedUsersSearchURI}?query=id:${id}`;
    return this.requestService.getRequest(URI);
  }

  accountResetPasswordInit(email: string): Observable<any> {
    const uriMethod = `${this.URI}/reset-password/init`;
    return this.requestService.postRequest(uriMethod, email);
  }

  accountChangePassword(currentPassword: string, newPassword: string): Observable<any> {
    const uriMethod = `${this.URI}/change-password`;
    const body = {
      currentPassword,
      newPassword
    };
    return this.requestService.postRequest(uriMethod, JSON.stringify(body));
  }

  createMassiveUsers(body: any): Observable<any> {
    const userURI = `${this.multiURI}/users`;
    return this.requestService.postRequest(userURI, JSON.stringify(body));
  }

  createExtendedUser(extendedUser: any): Observable<any> {
    return this.requestService.postRequest(this.extendedUsersURI, JSON.stringify(extendedUser));
  }

  updateExtendedUser(extendedUser: any): Observable<any> {
    return this.requestService.putRequest(this.extendedUsersURI, JSON.stringify(extendedUser));
  }

  updateAccountBalanceObservable(newValue: number) {
    this.accountBalance.next(newValue);
  }

  refreshReSellerLogoObservable(extendedUser) {
    this.logoImage.next(extendedUser);
  }

  updateMenuCollapse(newValue: boolean) {
    this.menuCollapse.next(newValue);
  }

  verifyKey(key: string): Observable<any> {
    const URI = `/api/account/verify-key?key=${key}`;
    return this.requestService.getRequest(URI);
  }

  setFundsMerchant(id: number, value: number) {
    const body = {
      extendedUser: {
        id
      },
      value
    };
    return this.requestService.postRequest(this.extUserFundURI, JSON.stringify(body));
  }

  processUser(bankId: number, extendedUsersIds: number[]): Observable<any> {
    const URI = '/api/extended-user-groups';
    const body = {
      bank: {
        id: bankId
      },
      extendedUsers: extendedUsersIds
    };
    return this.requestService.postRequest(URI, JSON.stringify(body));
  }

  changeUserStatus(extendedUser: ExtendedUser) {
    const body = {
      id: extendedUser.id,
      status: extendedUser.status,
      statusReason: extendedUser.statusReason,
      cardNumber: extendedUser.cardNumber ? extendedUser.cardNumber : null,
    };
    return this.requestService.putRequest(this.extendedUsersURI, JSON.stringify(body));
  }

  changeMassiveUserStatus(extendedUsers): Observable<any> {
    const extUserURI = `${this.multiURI}/extended-users`;
    const body = {
      extendedUsers,
    };
    return this.requestService.putRequest(extUserURI, JSON.stringify(body));
  }

  getBasicProfile(): Observable<any> {
    const accountBalanceURI = '/api/basic-profile';
    return this.requestService.getRequest(accountBalanceURI);
  }

  requestFunds(amount: number): Observable<any> {
    const body = {
      value: amount
    };
    return this.requestService.postRequest(this.extUserFundURI, JSON.stringify(body));
  }

  requestWithdrawals(amount: number): Observable<any> {
    const body = {
      value: amount
    };
    return this.requestService.postRequest(this.extUserFundURI, JSON.stringify(body));
  }

  updateFunds(fund: any): Observable<any> {
    return this.requestService.putRequest(this.extUserFundURI, JSON.stringify(fund));
  }

  associateMassiveMerchantToReSeller(extendedUserRelations): Observable<any> {
    const body = {
      extendedUserRelations
    };
    return this.requestService.postRequest(`${this.multiURI}/extended-user-relations`, JSON.stringify(body));
  }

  removeMerchantReSellerAssociation(extendedUserRelations): Observable<any> {
    const body = {
      extendedUserRelations
    };
    return this.requestService.putRequest(`${this.multiURI}/extended-user-relations`, JSON.stringify(body));
  }

  reSendEmail(email): Observable<any> {
    return this.requestService.postRequest(`${this.URI}/resend-creation-email`, email);
  }

  changeInvitedUserEmail(body): Observable<any> {
    return this.requestService.postRequest(`${this.URI}/change-email`, JSON.stringify(body));
  }

  updateLogoReSeller(extendedUserId, file): Observable<any> {
    const fileData = new FormData();
    fileData.append('file', file);
    fileData.append('name', file.name);
    fileData.append('extendedUserId', extendedUserId);

    return this.requestService.postImageRequest(`/api/_upload/reseller-logo`, fileData);
  }

  changeExtendedUserEmail(currentEmail, newEmail): Observable<any> {
    const body = {
      currentEmail,
      newEmail
    };

    return this.requestService.postRequest(`${this.URI}/change-email-notification`, JSON.stringify(body));
  }

  changeExtendedUserEmailValidation(key): Observable<any> {
    return this.requestService.postRequest(`${this.URI}/change-email-confirmation`, key);
  }

  updateExtendedUserRelationNickname(extendedUserRelation): Observable<any> {
      return this.requestService.putRequest(`/api/extended-user-relations`, JSON.stringify(extendedUserRelation));
      // return this.requestService.putRequest(`${this.multiURI}/extended-user-relations`, JSON.stringify(extendedUserRelation));
  }
}
