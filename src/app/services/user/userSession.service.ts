import {Injectable} from '@angular/core';
import {ExtendedUser, User} from '../../config/interfaces';
import {Router} from '@angular/router';
import {ROLES} from '../../config/enums';
import {environment} from '../../../environments/environment';

@Injectable()
export class UserSessionService {

  user: User;
  extendedUser: ExtendedUser;
  role = '';
  userStatus = '';
  isMobile = false;

  constructor(private router: Router) {
  }

  saveUserSession(user: User, extendedUser: ExtendedUser) {

    this.user = user;
    this.role = user.authorities[0];
    if (extendedUser && extendedUser.status) {
      this.extendedUser = extendedUser;
      this.userStatus = extendedUser.status;
    } else {
      this.userStatus = 'USERSTATELESS';
    }
  }

  saveExtendedUser(extendedUser: ExtendedUser) {
    this.extendedUser = extendedUser;
    this.userStatus = extendedUser.status;
  }

  isLoggedIn(): boolean {
    return !!this.user;
  }

  isNotDelete(): boolean {
    return this.loadUserStatus() !== 'CANCELLED';
  }

  logout() {
    this.router.navigate(['/login']).then(() => {
      window.localStorage.clear();
      window.sessionStorage.clear();
      this.user = null;
      this.extendedUser = null;
      this.role = '';
      this.userStatus = '';
      window.localStorage.setItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss", "0" );
    });
  }

  isDisabled(): boolean {
    return this.loadUserStatus() === 'REJECTED';
  }

  onlyProfileView(): boolean {
    return this.loadUserStatus() === 'USERSTATELESS'
      || this.loadUserStatus() === 'CREATED'
      || this.loadUserStatus() === 'FAILED'
      || this.loadUserStatus() === 'IN_PROCESS';
  }

  loadUser(): User {
    if (this.user) {
      return this.user;
    } else {
      return null;
    }
  }

  loadExtendedUser(): ExtendedUser {
    if (this.extendedUser) {
      return this.extendedUser;
    } else {
      return null;
    }
  }

  loadRole(): string {
    if (this.role && this.role !== '') {
      return this.role;
    } else {
      return null;
    }
  }

  setUserStatus(status) {
    this.userStatus = status;
  }

  loadUserStatus(): string {
    if (this.userStatus && this.userStatus !== '') {
      return this.userStatus;
    } else {
      return null;
    }
  }

  saveIsMobile(is: boolean) {
    this.isMobile = is;
  }

  loadIsMobile(): boolean {
    return this.isMobile;
  }

  haveStatus(): boolean {
    return this.loadUserStatus() !== 'USERSTATELESS';
  }

  isSuperOrAdmin(): boolean {
    return this.role === ROLES.SUPER_ADMIN || this.role === ROLES.ADMIN;
  }

  isSuperAdmin(): boolean {
    return this.role === ROLES.SUPER_ADMIN;
  }

  isAdmin(): boolean {
    return this.role === ROLES.ADMIN;
  }

  isReSeller(): boolean {
    return this.role === ROLES.RESELLER;
  }

  isMerchant(): boolean {
    return this.role === ROLES.MERCHANT;
  }

  isPayee(): boolean {
    return this.role === ROLES.PAYEE;
  }

  useEuroCoin(): boolean {
    return environment.arrayIdExtendedUserEuro.includes(this.loadExtendedUser().id);
  }

  usePesosMexicanCoin(userID): boolean {
    return environment.arrayIdExtendedUserPesosMex.includes(userID);
  }
}
