import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';

import {
  DataFinderService,
  RequestService,
  TokenService,
  UserService,
  UserSessionService,
  PaymentService,
  BankService,
  NotificationService,
  UserOnlyProfileViewGuard,
  ChartService,
  ErrorHandlerService,
  ReportService
} from './service.index';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [],
  providers: [
    DataFinderService,
    RequestService,
    TokenService,
    UserService,
    UserSessionService,
    UserOnlyProfileViewGuard,
    BankService,
    NotificationService,
    PaymentService,
    ChartService,
    ErrorHandlerService,
    ReportService
  ],
})
export class ServiceModule {
}
