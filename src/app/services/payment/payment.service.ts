import { Injectable } from '@angular/core';
import { RequestService } from '../request.service';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })

export class PaymentService {

  private transactionsUri = '/api/transactions';
  private multiTransactionUri = '/api/_multi/transactions/';
  private transactionsGroupUri = '/api/transaction-groups';
  private transactionsQuote = '/api/transactionQuote';

  constructor(public requestService: RequestService) {
  }

  createPayment(payeesToPay: any): Observable<any> {
    const payments = [];
    for (const payee of payeesToPay) {
      const payment = {
        amountBeforeCommission: payee.amount,
        description: payee.description,
        payee: { id: payee.extendedUserId },
      };
      payments.push(payment);
    }
    return this.requestService.postRequest(this.multiTransactionUri, JSON.stringify({ transactions: payments }));
  }
 
  processTransactionsQuote(payments: any, idBank: any): Observable<any> {
    const url = this.transactionsQuote + '/' + idBank;
    return this.requestService.postRequest(url, JSON.stringify(payments));
  }

  processPayment(paymentsToProcess: any, bankAccountId: number, exchangeRate: number, directPayment: boolean): Observable<any> {
    const transactions = [];
    const bankAccount = { id: bankAccountId };
    for (const payment of paymentsToProcess) {
      const id = payment.id;
      const transaction = { id };
      transactions.push(transaction);
    }

    const transactionGroup = {
      bankAccount,
      exchangeRate,
      transactions,
      directPayment
    };
    return this.requestService.postRequest(this.transactionsGroupUri, JSON.stringify(transactionGroup));
  }

  updatePaymentNote(id: number, notes: string): Observable<any> {
    const body = {
      id,
      notes
    };
    return this.requestService.putRequest(this.transactionsUri, JSON.stringify(body));
  }

  changePaymentStatus(payment: any, extendedUserId: number, role: string, actionType?: string): Observable<any> {
    let body;
    console.log(actionType)
    if (role === 'ADMIN') {
      body = {
        id: payment.id,
        admin: {
          id: extendedUserId,
        },
        status: '',
        statusReason: payment.statusReason
      };
      if (actionType === 'REJECTED') {
        body.status = 'REJECTED';
      } else if (actionType === 'REVERTED') {
        body.status = 'REVERTED';
      }
      else {
        body.status = 'ACCEPTED';
      }
    } else {
      body = {
        id: payment.id,
        merchant: {
          id: extendedUserId,
        },
        status: 'CANCELLED',
        statusReason: payment.statusReason
      };
    }
    console.log(body)
    return this.requestService.putRequest(this.transactionsUri, JSON.stringify(body));
  }

  changeMassivePaymentStatus(payments: any[], statusReason: string, status: string): Observable<any> {
    const transactions = [];

    for (const pay of payments) {
      const transaction = {
        id: pay.id,
        status,
        statusReason
      };
      transactions.push(transaction);
    }
    const body = {
      transactions
    };
    return this.requestService.putRequest(this.multiTransactionUri, JSON.stringify(body));
  }
}
