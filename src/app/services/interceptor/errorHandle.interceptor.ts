import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ChartService, ErrorHandlerService} from '../service.index';
import {environment} from '../../../environments/environment.prod';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root'})
export class ErrorHandleInterceptor implements HttpInterceptor {


  constructor(private chartService: ChartService,
              private router: Router,
               private errorHandlerService: ErrorHandlerService,
              private translate: TranslateService) {
                this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
  }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        catchError(error => this.handleError(error, req))
      );
  }

  handleError(error: HttpErrorResponse, req: HttpRequest<any>) {
    const detail: any = error;
    // let errorMessageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AN_UNEXPECTED_ERROR');
    let errorMessageTranslate = "An unexpected error occurred";
    let errorExpiredTokenTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SESSION_EXPIRED');
    try {
      if ( error.error && error.error.error && error.error.error.caused_by.reason
        && error.error.error.caused_by.reason.includes('Fielddata is disabled')) {
        const idChart = +req.params.get('idChart');
        this.chartService.setFieldDataTrue(idChart);
      } else if (error.url === "http://localhost:4200/assets/i18n/null.json") {
      } 
      else if (error.status == 401 && error.error.detail == "Full authentication is required to access this resource") {
        this.errorHandlerService.setMessageError(errorExpiredTokenTranslate.value);
        setTimeout(() => { 
          this.router.navigate(['/login']).then(); }, 2000);

      } else {
        if (error.error) {
          this.handleMessageErrorEnvironment(error.error);
        } else {
          this.errorHandlerService.setMessageError(errorMessageTranslate);
        }
      }
    } catch (err) {
    }
    return throwError(error);
  }

  handleMessageErrorEnvironment(error) {
    // let errorMessageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AN_UNEXPECTED_ERROR');
    let errorMessageTranslate = "An unexpected error occurred";
    if (environment.isProd) {
      this.handleMessageError(error);
    } else {
      if (error.detail) {
        this.handleMessageError(error);
      } else {
        this.errorHandlerService.setMessageError(errorMessageTranslate);
      }
    }
  }

  handleMessageError(error) {
    // let errorMessageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AN_UNEXPECTED_ERROR');
    let errorMessageTranslate = "An unexpected error occurred";
    switch (error.entityName) {
      case 'User':
        this.handleUserMessageError(error);
        break;
      case 'ExtendedUser':
        this.handleExtendedUserMessageError(error);
        break;
      case 'NotificationReceiver':
        this.handleNotificationReceiverMessageError(error);
        break;
      case 'Transaction':
      case 'Notification':
      case 'ExtendedUserFunds':
        this.errorHandlerService.setMessageError(error.detail);
        break;
      default:
        this.errorHandlerService.setMessageError(errorMessageTranslate);
        break;
    }
  }

  handleUserMessageError(error) {
    // let errorMessageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AN_UNEXPECTED_ERROR');
    let errorMessageTranslate = "An unexpected error occurred";
    const errorCode = error.errorCode.split('.')[1];
    let msgError = null;
    let msg: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.INVALID_CREDENTIAL');
    switch (errorCode) {
      case '001':
      case '002':
      case '003':
      case '005':
      case '006':
      case '007': // Enlace no encontrad
        msgError =  error.detail;
        break;
      case '008':
        msgError = msg.value;
        break;
      case '011':
        msgError = { mss: msgError = error.detail, static: true};
        break;
      default: // enlace expirado
        msgError = errorMessageTranslate;
        break;
    }
    this.errorHandlerService.setMessageError(msgError);
  }

  handleExtendedUserMessageError(error) {
    // let errorMessageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AN_UNEXPECTED_ERROR');
    let errorMessageTranslate = "An unexpected error occurred";
    const errorCode = error.errorCode.split('.')[1];
    let msgError = '';
    let msg: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_NOT_FOUND');

    switch (errorCode) {
      case '001':
      case '002':
        msgError = msg.value;
        break;
      default:
        msgError = errorMessageTranslate;
        break;
    }
    this.errorHandlerService.setMessageError(msgError);
  }

  handleNotificationReceiverMessageError(error) {
    // let errorMessageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AN_UNEXPECTED_ERROR');
    let errorMessageTranslate = "An unexpected error occurred";
    const errorCode = error.errorCode.split('.')[1];
    let msgError = '';
    let msg: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.NOTIFICATION_NOT_FOUND');
    switch (errorCode) {
      case '001':
      case '002':
        msgError = msg.value;
        break;
      default:
        msgError = errorMessageTranslate;
        break;
    }
    this.errorHandlerService.setMessageError(msgError);
  }

}
