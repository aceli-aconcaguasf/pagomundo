import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {LoaderService} from '../loader/loader.service';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
  constructor(private loaderService: LoaderService) {
  }

  static removeParam(key, sourceURL) {
    let rtn = sourceURL.split('?')[0];
    let param;
    let paramsArr = [];
    const queryString = (sourceURL.indexOf('?') !== -1) ? sourceURL.split('?')[1] : '';
    if (queryString !== '') {
      paramsArr = queryString.split('&');
      for (let i = paramsArr.length - 1; i >= 0; i -= 1) {
        param = paramsArr[i].split('=')[0];
        if (param === key) {
          paramsArr.splice(i, 1);
        }
      }
      rtn = rtn + '?' + paramsArr.join('&');
    }
    return rtn;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.params.get('withoutLoader') && req.params.get('findPost')) {
      let cloneReq = req.clone();
      const url1 = LoaderInterceptor.removeParam('withoutLoader', req.urlWithParams);
      const url2 = LoaderInterceptor.removeParam('findPost', url1);
      const httpRequest = new HttpRequest( req.method as any, url2, req.body, {headers: req.headers});
      cloneReq = Object.assign(cloneReq, httpRequest);

      return next.handle(cloneReq);

    } else if (req.params.get('withoutLoader')) {
      let cloneReq = req.clone();
      const url = req.urlWithParams.split('?');
      const httpRequest = new HttpRequest( req.method as any, url[0], req.body, {headers: req.headers});
      cloneReq = Object.assign(cloneReq, httpRequest);

      return next.handle(cloneReq);

    } else if (req.method === 'POST' || req.method === 'PUT') {
      const urlSplit = req.url.split('/');
       if ( urlSplit[urlSplit.length - 1] !== 'citiesbycountry'){ 
         this.loaderService.show();
       }
      return next.handle(req).pipe(
        finalize(() => {
          this.loaderService.hide();
        })
      );
    } else {
      return next.handle(req);
    }
  }
}
