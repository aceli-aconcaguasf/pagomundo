import {EventEmitter, Injectable} from '@angular/core';
import {RequestService} from './request.service';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {STATUS, TYPE_ID_TYPE} from '../config/enums';

@Injectable({providedIn: 'root'})
export class DataFinderService {

  idBank$ = new EventEmitter<number>();

  constructor(public requestService: RequestService, private http: HttpClient) {
  }

  public getCountriesForPayeeAndName(name: string): Observable<any> {
    const uri = `/api/_search/countries?query=forPayee:true AND name:*${name}*`;
    return this.requestService.getRequest(uri);
  }

  public getCountriesForSelect(): Observable<any> {
    const uri = '/api/_search/countries?query=forPayee:true';
    return this.requestService.getRequest(uri);
  }

  public getMerchantForCompany(company: string): Observable<any> {
    const uri = `/api/_search/extended-users?query=role:ROLE_MERCHANT AND company:${company}`;
    return this.requestService.getRequest(uri);
  }

  public getMerchantOfReSellerForCompoany(idUserReSeller, company: string): Observable<any> {
    const uri = `/api/_search/extended-user-relations?query=userRelated.id:${idUserReSeller}` +
                ` AND status:${STATUS.ACCEPTED} AND extendedUser.company:*${company}*`;
    return this.requestService.getRequest(uri);
  }

  public getCountriesForName(name: string): Observable<any> {
    const uri = `/api/_search/countries?query=name:*${name}*`;
    return this.requestService.getRequest(uri);
  }

  public getCitiesByCountryIdAndName(countryId, name): Observable<any> {
    const uri = `/api/_search/cities?query=country.id:${countryId} AND name:*${name}*`;
    return this.requestService.getRequest(uri);
  }

  public getBanksById(bankId): Observable<any> {
    const uri = `/api/_search/banks?query=id:${bankId}`;
    return this.requestService.getRequest(uri);
  }

  public getBankAccountsByBankId(bankId): Observable<any> {
    const uri = '/api/_search/bank-accounts?query=bank.id:' + bankId;
    return this.requestService.getRequest(uri);
  }

  public getAllBankAccounts(countryId): Observable<any> {
    const uri = `/api/_search/bank-accounts?query=bank.country.id:${countryId}`;
    return this.requestService.getRequest(uri);
  }

  public getCurrenciesByCountryId(countryId): Observable<any> {
    const uri = '/api/_search/currencies?query=country.id:' + countryId;
    return this.requestService.getRequest(uri);
  }

  public getIdTypesByCountryIdAndType(countryId, type): Observable<any> {
    countryId = parseInt( countryId );
    let typeIdType;

    if (type === 'NATURAL') {

      switch (countryId) {
        case 1: // ID 1 = COLOMBIA
        case 66: // ID 66 = ARGENTINA
        case 74: // BRAZIL
        case 77: // BRAZIL
        case 101: // PERÚ
        case 115: // VENEZUELA
          typeIdType = TYPE_ID_TYPE.NATURAL;
          break;
        case 2: // ID 2 = MEXICO
          typeIdType = TYPE_ID_TYPE.BOTH;
          break;
      }
    }
    else {

      switch (countryId) {
        case 1: // ID 1 = COLOMBIA
        case 74: // BRAZIL
        case 77: // CHILE
        case 66: // ID 66 = ARGENTINA
        case 101: // PERÚ
        case 115: // VENEZUELA
          typeIdType = TYPE_ID_TYPE.LEGAL;
          break;
        case 2: // ID 2 = MEXICO
          typeIdType = TYPE_ID_TYPE.BOTH;
          break;
      }
    }
    const uri = countryId !== 66 && countryId !== 101 && countryId !== 115 && countryId !== 77 ? `/api/_search/id-types?query=country.id:${countryId} AND naturalOrLegal:${typeIdType}` : `/api/_search/id-types?query=country.id:${countryId}`;
    return this.requestService.getRequest(uri);
  }

  public _search(
    uri: string, query: string, sort: string, order: string,
    size?: number, page?: number): Observable<any> {
    let requestUrl = `${uri}`;
    requestUrl += query ? `?query=${query}` : `?query=*`;
    requestUrl += page ? `&page=${page}` : ``;
    requestUrl += size ? `&size=${size}` : ``;
    requestUrl += sort ? `&sort=${sort}` : ``;
    requestUrl += order ? `,${order}` : ``;

    return this.requestService.getRequest(requestUrl);
  }

	public _getBank( uri: string, page?: number ): Observable<any> {
		let requestUrl = `${uri}`;
		requestUrl += `&page=${page}&size=100`;
		return this.requestService.getRequest(requestUrl);
	}

  public _find(uri: string, query: string, sort: string, order: string, size?: number, page?: number,
               withoutLoader?: boolean): Observable<any> {

    let requestUrl = `${uri}`;
    requestUrl += page ? `&page=${page}` : ``;
    requestUrl += size ? `&size=${size}` : ``;
    requestUrl += sort ? `&sort=${sort}` : ``;
    requestUrl += order ? `,${order}` : ``;

    const body = {
      query
    };

    return withoutLoader
      ? this.requestService.postRequest(requestUrl, JSON.stringify(body), {withoutLoader: true, findPost: true})
      : this.requestService.postRequest(requestUrl, JSON.stringify(body));

  }

  public getOptionsJSON(): Observable<any> {
    return this.http.get('./assets/data/menuOptions.json');
  }
}
