import {Injectable} from '@angular/core';
import {RequestService} from '../request.service';
import {Observable, Subject} from 'rxjs';
import {UserSessionService} from '../user/userSession.service';
import {ROLES} from '../../config/enums';

@Injectable({providedIn: 'root'})

export class ReportService {

  private transactionReportUri = '/api/_report/transactions';
  private extendedUserReportUri = '/api/_report/extended-users';
  private extendedUserRelationReportUri = '/api/_report/extended-user-relations';

  alreadyLoadNotificationReport = new Subject<false>();
  constructor(private requestService: RequestService,
              private userSessionService: UserSessionService) {
  }

  setNotificationReportValue(value) {
    this.alreadyLoadNotificationReport.next(value);
  }

   // TRANSACTIONS REPORT
  requestTransactionReports(idChart: number, title: string, date: string, idUser: number, idExtendedUser: number): Observable<any> {
    // EXAMPLE DATE [2020-08-01 TO 2020-08-18]

    /*
    * ID CHART === TITLE CHART === ROLES
    * ID = 1  === Completed payments monetary amount === ALL
    * ID = 2  === Number of payments === ALL
    * ID = 3  === Payment statuses === Payee
    * ID = 4  === Merchants added === Admin(Super)
    * ID = 5  === Commissions Earned === Reseller
    * ID = 6  === Merchants added === Reseller
    * ID = 7  === Payees added ==== Merchant : Admin(Super)
    * ID = 8  === Payees added country === Merchant
    * ID = 9  === Payees added status : Users added status === Merchant : Admin(Super)
    * ID = 10  === Complete Payments Monetary by Country === Merchant : Reseller : Admin(Super)
    */

    let uri = '';
    switch (idChart) {
      case 1 :
      case 10 :
        switch (this.userSessionService.loadRole()) {
          case ROLES.SUPER_ADMIN:
          case ROLES.ADMIN:
            uri = `${this.transactionReportUri}?query=status:accepted AND lastUpdated:${date}&chartTitle=${title}`;
            break;
          case ROLES.RESELLER:
            uri = `${this.transactionReportUri}?` +
              `query=reseller.id:${idExtendedUser} AND status:accepted AND lastUpdated:${date}&chartTitle=${title}`;
            break;
          case ROLES.MERCHANT:
            uri = `${this.transactionReportUri}?` +
              `query=merchant.id:${idExtendedUser} AND status:accepted AND lastUpdated:${date}&chartTitle=${title}`;
            break;
          case ROLES.PAYEE:
            uri = `${this.transactionReportUri}?` +
              `query=payee.id:${idExtendedUser} AND status:accepted AND lastUpdated:${date}&chartTitle=${title}`;
            break;
        }
        break;
      case 2 :
      case 3 :
        switch (this.userSessionService.loadRole()) {
          case ROLES.SUPER_ADMIN:
          case ROLES.ADMIN:
            uri = `${this.transactionReportUri}?query=lastUpdated:${date}&chartTitle=${title}`;
            break;
          case ROLES.RESELLER:
            uri = `${this.transactionReportUri}?` +
              `query=reseller.id:${idExtendedUser} AND lastUpdated:${date}&chartTitle=${title}`;
            break;
          case ROLES.MERCHANT:
            uri = `${this.transactionReportUri}?` +
              `query=merchant.id:${idExtendedUser} AND lastUpdated:${date}&chartTitle=${title}`;
            break;
          case ROLES.PAYEE:
            uri = `${this.transactionReportUri}?` +
              `query=payee.id:${idExtendedUser} AND lastUpdated:${date}&chartTitle=${title}`;
            break;
        }
        break;
      case 4 :
        uri = `${this.extendedUserReportUri}?query=role:role_merchant AND dateCreated:${date}&chartTitle=${title}`;
        break;
      case 5 :
        uri = `${this.transactionReportUri}?` +
              `query=reseller.id:${idExtendedUser} AND status:accepted AND lastUpdated:${date}&chartTitle=${title}`;
        break;
      case 6 :
        uri = `${this.extendedUserRelationReportUri}?` +
              `query=userRelated.id:${idUser} AND status:accepted AND dateRelated:${date}&chartTitle=${title}`;
        break;
      case 7 :
        this.userSessionService.isSuperOrAdmin()
          ? uri = `${this.extendedUserReportUri}?query=role:role_payee AND dateCreated:${date}&chartTitle=${title}`
          : uri = `${this.extendedUserRelationReportUri}?query=userRelated.id:${idUser} AND status:accepted` +
                  ` AND extendedUser.role:role_payee AND dateRelated:${date}&chartTitle=${title}`;
        break;
      case 8 :
        uri = `${this.extendedUserRelationReportUri}?query=userRelated.id:${idUser} AND status:accepted` +
              ` AND extendedUser.role:role_payee AND dateRelated:${date}&chartTitle=${title}`;
        break;
      case 9 :
        this.userSessionService.isSuperOrAdmin()
          ? uri = `${this.extendedUserReportUri}?query=(role:role_payee OR role:role_merchant) AND dateCreated:${date}&chartTitle=${title}`
          : uri = `${this.extendedUserRelationReportUri}?query=userRelated.id:${idUser}` +
                  ` AND status:accepted AND extendedUser.role:role_payee AND dateRelated:${date}&chartTitle=${title}`;
        break;
      default :
        break;
    }

    return this.requestService.getRequest(uri);
  }
}
