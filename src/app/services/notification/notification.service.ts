import {Injectable} from '@angular/core';
import {RequestService} from '../request.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {DataFinderService} from '../data-finder.service';
import {UserSessionService} from '../user/userSession.service';
import {STATUS} from '../../config/enums';

@Injectable({providedIn: 'root'})
export class NotificationService {

  private notificationReceiversUri = '/api/notification-receivers';
  private searchNotificationReceiversUri = '/api/_search/notification-receivers';
  private findNotificationReceiversUri = '/api/_find/notification-receivers?';
  private notificationUri = '/api/notifications';

  status = STATUS;

  newIssues = new BehaviorSubject<boolean>(false);
  newMessages = new BehaviorSubject<boolean>(false);
  issueSubject = new BehaviorSubject<boolean>(false);

  constructor(public requestService: RequestService,
              private userSessionService: UserSessionService,
              private dataFinder: DataFinderService) {
  }

  changeNewNotificationValue(type: string, value: boolean) {
    type === 'ISSUE' ? this.newIssues.next(value) : this.newMessages.next(value);
  }

  createNotificationReceiver(notificationReceiver): Observable<any> {
    return this.requestService.postRequest(this.notificationReceiversUri, notificationReceiver);
  }

  updateNotificationReceiver(notificationReceiver): Observable<any> {
    return this.requestService.putRequest(this.notificationReceiversUri, notificationReceiver);
  }

  updateNotificationReceiverWithoutLoader(notificationReceiver): Observable<any> {
    return this.requestService.putRequest(this.notificationReceiversUri, notificationReceiver, {withoutLoader: true});
  }

  updateNotification(notification): Observable<any> {
    return this.requestService.putRequest(this.notificationUri, notification);
  }

  getAllNotificationReceiverOfMessage(notificationId, page?): Observable<any> {
    const query = `notification.id:${notificationId} AND receiver.id:*`;
    return page ?
      this.dataFinder._search(this.searchNotificationReceiversUri, query, 'dateCreated', 'desc', 20, page)
      : this.dataFinder._search(this.searchNotificationReceiversUri, query, 'dateCreated', 'desc', 20);
  }

  getAllNotificationSentOfMessage(notificationId, page?): Observable<any> {
    const query = `notification.id:${notificationId} AND sender.id:*`;
    return page ?
      this.dataFinder._search(this.searchNotificationReceiversUri, query, 'dateCreated', 'desc', 20, page)
      : this.dataFinder._search(this.searchNotificationReceiversUri, query, 'dateCreated', 'desc', 20);
  }

  getAllCreatedMessageOfUser(extendedUser, withoutContent?: boolean): Observable<any> {
    const query = 'status:CREATED AND (notification.subCategory:MESSAGE OR ' +
      'notification.subCategory:TRANSACTION_REJECTED OR notification.subCategory:TRANSACTION_ACCEPTED OR ' +
      'notification.subCategory:TRANSACTION_CREATED OR notification.subCategory:EXTENDED_USER_CHANGE_PAYMENT_METHOD OR ' +
      'notification.subCategory:EXTENDED_USER_ACCEPTED OR notification.subCategory:EXTENDED_USER_CREATED OR ' +
      `notification.subCategory:FUND_ACCEPTED OR notification.subCategory:FUND_CREATED OR ` +
      `notification.subCategory:FUND_REJECTED OR notification.subCategory:WITHDRAWAL_ACCEPTED OR ` +
      `notification.subCategory:WITHDRAWAL_CREATED OR notification.subCategory:WITHDRAWAL_REJECTED)` +
      ` AND receiver.id:${extendedUser.id}`;
    return withoutContent ?
      this.dataFinder._find(this.findNotificationReceiversUri, query, 'dateCreated', 'desc', 1, 0, true)
      : this.dataFinder._find(this.findNotificationReceiversUri, query, 'dateCreated', 'desc', 1, 0, true);
  }

  getAllCreatedIssueOfUser(extendedUser, withoutContent?: boolean): Observable<any> {
    const query = `status:CREATED AND notification.subCategory:ISSUE_CREATED AND receiver.id:${extendedUser.id}`;
    return withoutContent ?
      this.dataFinder._search(this.searchNotificationReceiversUri, query, 'dateCreated', 'desc', 1)
      : this.dataFinder._search(this.searchNotificationReceiversUri, query, 'dateCreated', 'desc');
  }

  getDownloadReportNotification(extendedUser): Observable<any> {
    const query = `status:${this.status.CREATED} AND notification.subCategory:DOWNLOADED_FILE AND sender.id:${extendedUser.id}`;
    return this.dataFinder._search(this.searchNotificationReceiversUri, query, 'dateCreated', 'desc', 1);
  }


}
