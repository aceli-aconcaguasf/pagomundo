import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class ErrorHandlerService {
  errorMessage = new Subject<string>();

  setMessageError(msgError) {
    this.errorMessage.next(msgError);
  }
}