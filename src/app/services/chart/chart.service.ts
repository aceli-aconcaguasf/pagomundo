import {Injectable} from '@angular/core';
import {RequestService} from '../request.service';
import {Observable, Subject} from 'rxjs';
import {ROLES} from '../../config/enums';

@Injectable({providedIn: 'root'})

export class ChartService {

  private transactionUri = '/transaction/_search';
  private extendedUserUri = '/extendeduser/_search';
  private extendedUserRelationUri = '/extendeduserrelation/_search';
  private mappingUriTransaction = '/transaction/_mapping/transaction';
  private mappingUriRelatedUser = '/extendeduserrelation/_mapping/extendeduserrelation';
  private mappingUriExtendedUser = '/extendeduser/_mapping/extendeduser';

  refreshPieComponent = new Subject<any>();
  refreshBarComponent = new Subject<any>();

  constructor(public requestService: RequestService) {
  }

  static setQueryToTransactionBody(body: any, rolAndId: string, id: number, monetary?: boolean) {
    Object.defineProperty(body, 'query', {value: {}, enumerable: true});
    Object.defineProperty(body.query, 'bool', {value: {}, enumerable: true});
    Object.defineProperty(body.query.bool, 'filter', {value: [], enumerable: true});
    body.query.bool.filter.push({term: {}});
    Object.defineProperty(body.query.bool.filter[0].term, rolAndId, {value: id, enumerable: true});
    if (monetary) {
      body.query.bool.filter.push({term: {}});
      Object.defineProperty(body.query.bool.filter[1].term, 'status', {value: 'accepted', enumerable: true});
    }
  }

  static setQueryToMoTransactionMonetaryAmountBody(body: any) {
    Object.defineProperty(body, 'query', {value: {}, enumerable: true});
    Object.defineProperty(body.query, 'bool', {value: {}, enumerable: true});
    Object.defineProperty(body.query.bool, 'filter', {value: [], enumerable: true});
    body.query.bool.filter.push({term: {}});
    Object.defineProperty(body.query.bool.filter[0].term, 'status', {value: 'accepted', enumerable: true});

  }

  setFieldDataTrue(idChart: number) {
    this.setTransactionPayeeCountryFieldDataTrue().subscribe(() => {
      this.setTransactionCurrencyFieldDataTrue().subscribe(() => {
        this.setTransactionStatusFieldDataTrue().subscribe(() => {
          this.setUserStatusFieldDataTrueRelatedUser().subscribe(() => {
            this.setUserRoleFieldDataTrueRelatedUser().subscribe(() => {
              this.setUserRoleFieldDataTrueExtendedUser().subscribe(() => {
                this.setUserStatusFieldDataTrueExtendedUser().subscribe(() => {
                  const result = {
                    value: true,
                    idChart
                  };
                  this.refreshPieComponent.next(result);
                  this.refreshBarComponent.next(result);
                });
              });
            });
          });
        });
      });
    });
  }

  setTransactionStatusFieldDataTrue(): Observable<any> {
    const body = {
      properties: {
        status: {
          type: 'text',
          fielddata: true
        }
      }
    };

    return this.requestService.esPutRequest(this.mappingUriTransaction, JSON.stringify(body), {withoutLoader: true});
  }

  setTransactionCurrencyFieldDataTrue(): Observable<any> {
    const body = {
      properties: {
        'currency.country.name': {
          type: 'text',
          fielddata: true
        }
      }
    };

    return this.requestService.esPutRequest(this.mappingUriTransaction, JSON.stringify(body), {withoutLoader: true});
  }

  setTransactionPayeeCountryFieldDataTrue(): Observable<any> {
    const body = {
      properties: {
        'payee.postalCountry.name': {
          type: 'text',
          fielddata: true
        }
      }
    };

    return this.requestService.esPutRequest(this.mappingUriTransaction, JSON.stringify(body), {withoutLoader: true});
  }

  setUserStatusFieldDataTrueRelatedUser(): Observable<any> {
    const body = {
      properties: {
        'extendedUser.status': {
          type: 'text',
          fielddata: true
        }
      }
    };

    return this.requestService.esPutRequest(this.mappingUriRelatedUser, JSON.stringify(body), {withoutLoader: true});
  }

  setUserRoleFieldDataTrueRelatedUser(): Observable<any> {
    const body = {
      properties: {
        'extendedUser.role': {
          type: 'text',
          fielddata: true
        }
      }
    };

    return this.requestService.esPutRequest(this.mappingUriRelatedUser, JSON.stringify(body), {withoutLoader: true});
  }

  setUserStatusFieldDataTrueExtendedUser(): Observable<any> {
    const body = {
      properties: {
        status: {
          type: 'text',
          fielddata: true
        }
      }
    };

    return this.requestService.esPutRequest(this.mappingUriExtendedUser, JSON.stringify(body), {withoutLoader: true});
  }

  setUserRoleFieldDataTrueExtendedUser(): Observable<any> {
    const body = {
      properties: {
        role: {
          type: 'text',
          fielddata: true
        }
      }
    };

    return this.requestService.esPutRequest(this.mappingUriExtendedUser, JSON.stringify(body), {withoutLoader: true});
  }

  // ALL VIEW (Pie Chart)
  searchTransactionStatus(extendedUserId: number, gte: string, lte: string, role: string, idChart: number): Observable<any> {
    const body = {
      aggs: {
        filterByDate: {
          filter: {
            range: {
              lastUpdated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            status: {
              terms: {
                field: 'status'
              }
            }
          }
        }
      },
      size: 0
    };

    switch (role) {
      case ROLES.PAYEE:
        ChartService.setQueryToTransactionBody(body, 'payee.id', extendedUserId);
        break;
      case ROLES.MERCHANT:
        ChartService.setQueryToTransactionBody(body, 'merchant.id', extendedUserId);
        break;
      case ROLES.RESELLER:
        ChartService.setQueryToTransactionBody(body, 'reseller.id', extendedUserId);
        break;
      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        break;
    }

    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }

  // ALL VIEW (Bar Chart)
  searchTransactionReceived(extUserId: number, gte: string, lte: string, interval: string,
                            role: string, idChart: number, monetary?: boolean, commissionReseller?: boolean): Observable<any> {

    const body = {
      aggs: {
        filterByDate: {
          filter: {
            range: {
              lastUpdated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            dateCreated_date_histogram: {
              date_histogram: {
                field: 'lastUpdated',
                interval,
                format: 'yyyy-MM-dd',
                keyed: true
              },
              aggs: {
                stats_field: {
                  stats: {
                    field: ''
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    switch (role) {
      case ROLES.PAYEE:
        ChartService.setQueryToTransactionBody(body, 'payee.id', extUserId, monetary);
        body.aggs.filterByDate.aggs.dateCreated_date_histogram.aggs.stats_field.stats.field = 'amountLocalCurrency';
        break;
      case ROLES.MERCHANT:
        ChartService.setQueryToTransactionBody(body, 'merchant.id', extUserId, monetary);
        body.aggs.filterByDate.aggs.dateCreated_date_histogram.aggs.stats_field.stats.field = 'amountBeforeCommission';
        break;
      case ROLES.RESELLER:
        ChartService.setQueryToTransactionBody(body, 'reseller.id', extUserId, monetary);
        commissionReseller
          ? body.aggs.filterByDate.aggs.dateCreated_date_histogram.aggs.stats_field.stats.field = 'resellerCommission'
          : body.aggs.filterByDate.aggs.dateCreated_date_histogram.aggs.stats_field.stats.field = 'amountBeforeCommission';
        break;
      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        if (monetary) { ChartService.setQueryToMoTransactionMonetaryAmountBody(body); }
        body.aggs.filterByDate.aggs.dateCreated_date_histogram.aggs.stats_field.stats.field = 'amountAfterCommission';
    }

    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }

  // RESELLER AND MERCHANT VIEW (Bar Chart)
  searchUserRelatedCount(userId: number, roleToSearch: string,
                         gte: string, lte: string, interval: string, idChart: number): Observable<any> {

    const body = {
      query: {
        bool: {
          filter: [
            {term: {'userRelated.id': userId }},
            {term: {status: 'accepted' }},
            {term: {'extendedUser.role': roleToSearch }},
          ]
        }
      },
      aggs: {
        relations: {
          filter: {
            range: {
              dateRelated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            dateCreated_date_histogram: {
              date_histogram: {
                field: 'dateRelated',
                interval,
                format: 'yyyy-MM-dd',
                keyed: true
              },
              aggs: {
                count: {
                  value_count: {
                    field: 'extendedUser.id'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.extendedUserRelationUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }

  // MERCHANT VIEW (Pie Chart)
  searchUserRelatedStatus(userId: number, gte: string, lte: string, idChart: number): Observable<any> {

    const body = {
      query: {
        bool: {
          filter: [
            {term: {'userRelated.id': userId }},
            {term: {status: 'accepted' }},
            {term: {'extendedUser.role': 'role_payee' }},
          ]
        }
      },
      aggs: {
        filters: {
          filter: {
            range: {
              dateRelated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            status: {
              terms: {
                field: 'extendedUser.status'
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.extendedUserRelationUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }

  // MERCHANT VIEW (Pie Chart)
  searchUserRelatedCountry(userId: number, gte: string, lte: string, idChart: number): Observable<any> {

    const body = {
      query: {
        bool: {
          filter: [
            {term: {'userRelated.id': userId }},
            {term: {status: 'accepted' }},
            {term: {'extendedUser.role': 'role_payee' }},
          ]
        }
      },
      aggs: {
        filters: {
          filter: {
            range: {
              dateRelated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            country: {
              terms: {
                field: 'extendedUser.postalCountry.id'
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.extendedUserRelationUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }

  // ADMIN VIEW (Pie Chart)
  searchUserStatus(gte: string, lte: string, idChart: number): Observable<any> {
    const body = {
      query: {
        bool: {
          must: {
            bool : {
              should: [
                { match: { role: 'role_payee' }},
                { match: { role: 'role_merchant' }}
              ]
            }
          }
        }
      },
      aggs: {
        filters: {
          filter: {
            range: {
              dateCreated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            status: {
              terms: {
                field: 'status'
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.extendedUserUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }

  // ADMIN VIEW (Bar Chart)
  searchUserCount(role: string, gte: string, lte: string, interval: string, idChart: number): Observable<any> {

    const body = {
      query: {
        bool: {
          filter: [
            {
              term: {
                role
              }
            }
          ]
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              dateCreated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            dateCreated_date_histogram: {
              date_histogram: {
                field: 'dateCreated',
                interval,
                format: 'yyyy-MM-dd',
                keyed: true
              },
              aggs: {
                count: {
                  value_count: {
                    field: 'role'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.extendedUserUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }


  // DASHBOARD for cards
  searchMonthlyTransactionStatus(gte: string) {
    const body = {
      query: {
        bool: {
          filter: [
            {term: {status: 'accepted'}}
          ]
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              dateCreated: {
                gte,
                lte: 'now'
              }
            }
          },
          aggs: {
            dateCreated_date_histogram: {
              date_histogram: {
                field: 'dateCreated',
                interval: '1M',
                format: 'yyyy-MM',
                keyed: true
              },
              aggs: {
                count: {
                  value_count: {
                    field: 'status'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };
    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true});
  }

  searchMonthlyTransactionMonetary(extendedUser, gte: string, commissionReseller?: boolean): Observable<any> {

    const body = {
      query: {
        bool: {
          filter: [
            {term: {status: 'accepted'}}
          ]
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              lastUpdated: {
                gte,
                lte: 'now'
              }
            }
          },
          aggs: {
            lastUpdated_date_histogram: {
              date_histogram: {
                field: 'lastUpdated',
                interval: '1M',
                format: 'yyyy-MM',
                keyed: true
              },
              aggs: {
                stats_field: {
                  stats: {
                    field: 'amountAfterCommission'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    switch (extendedUser.role) {
      case ROLES.PAYEE:
        ChartService.setQueryToTransactionBody(body, 'payee.id', extendedUser.id, true);
        body.aggs.filterByDate.aggs.lastUpdated_date_histogram.aggs.stats_field.stats.field = 'amountLocalCurrency';
        break;
      case ROLES.MERCHANT:
        ChartService.setQueryToTransactionBody(body, 'merchant.id', extendedUser.id, true);
        break;
      case ROLES.RESELLER:
        ChartService.setQueryToTransactionBody(body, 'reseller.id', extendedUser.id, true);
        commissionReseller
          ? body.aggs.filterByDate.aggs.lastUpdated_date_histogram.aggs.stats_field.stats.field = 'resellerCommission'
          : body.aggs.filterByDate.aggs.lastUpdated_date_histogram.aggs.stats_field.stats.field = 'amountAfterCommission';
        break;
      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        ChartService.setQueryToMoTransactionMonetaryAmountBody(body);
    }

    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true});
  }

  searchMonthlyPayeeCount(onlyStatusAccepted: boolean, gte: string): Observable<any> {
    const body = {
      query: {
        bool: {
          filter: []
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              dateCreated: {
                gte,
                lte: 'now'
              }
            }
          },
          aggs: {
            dateCreated_date_histogram: {
              date_histogram: {
                field: 'dateCreated',
                interval: '1M',
                format: 'yyyy-MM',
                keyed: true
              },
              aggs: {
                count: {
                  value_count: {
                    field: 'role'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    body.query.bool.filter.push({term: {}});
    Object.defineProperty(body.query.bool.filter[0].term, 'role', {value: 'role_payee', enumerable: true});
    if (onlyStatusAccepted) {
      body.query.bool.filter.push({term: {}});
      Object.defineProperty(body.query.bool.filter[1].term, 'status', {value: 'accepted', enumerable: true});
    }

    return this.requestService.esPostRequest(this.extendedUserUri, JSON.stringify(body), {withoutLoader: true});
  }

  searchMonthlyMerchantPayeeCount(merchantUserId: number, gte: string): Observable<any> {
    const body = {
      query: {
        bool: {
          filter: [
            {term: {'userRelated.id': merchantUserId }},
            {term: {status: 'accepted' }},
            {term: {'extendedUser.role': 'role_payee' }},
          ]
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              dateRelated: {
                gte,
                lte: 'now'
              }
            }
          },
          aggs: {
            dateCreated_date_histogram: {
              date_histogram: {
                field: 'dateRelated',
                interval: '1M',
                format: 'yyyy-MM',
                keyed: true
              },
              aggs: {
                count: {
                  value_count: {
                    field: 'extendedUser.id'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.extendedUserRelationUri, JSON.stringify(body), {withoutLoader: true});
  }

  searchMonthlyCountryTransaction(gte: string, lte: string): Observable<any> {
    const body = {
      query: {
        bool: {
          filter: [
            {term: {status: 'accepted'}}
          ]
        }
      },
      aggs: {
        filters: {
          filter: {
            range: {
              dateCreated: {
                gte,
                lte,
              }
            }
          },
          aggs: {
            count: {
              terms: {
                field: 'currency.country.name'
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true});

  }

  searchMonthlyMethodTransaction(gte: string): Observable<any> {
    const body = {
      query: {
        bool: {
          filter: [
            {term: {status: 'accepted'}}
          ]
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              dateCreated: {
                gte,
                lte: 'now'
              }
            }
          },
          aggs: {
            dateCreated_date_histogram: {
              date_histogram: {
                field: 'dateCreated',
                interval: '1M',
                format: 'yyyy-MM',
                keyed: true
              },
              aggs: {
                directPayment: {
                  terms: {
                    field: 'transactionGroup.directPayment'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true});

  }

  // ALL VIEW (less payee) (Pie Chart) DASHBOARD
  searchTransactionMonetaryByCountry(extendedUserId: number, gte: string, lte: string,
                                     role: string, idChart: number): Observable<any> {
    const body = {
      query: {
        bool: {
          filter: []
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              lastUpdated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            by_territory: {
              terms: {
                field: 'payee.postalCountry.name'
              },
              aggs: {
                total_amountBeforeCommission: {
                  sum: {
                    field: 'amountAfterCommission'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    body.query.bool.filter.push({term: {}});
    Object.defineProperty(body.query.bool.filter[0].term, 'status', {value: 'accepted', enumerable: true});

    switch (role) {
      case ROLES.MERCHANT:
        body.query.bool.filter.push({term: {}});
        Object.defineProperty(body.query.bool.filter[1].term, 'merchant.id', {value: extendedUserId, enumerable: true});
        break;
      case ROLES.RESELLER:
        body.query.bool.filter.push({term: {}});
        Object.defineProperty(body.query.bool.filter[1].term, 'reseller.id', {value: extendedUserId, enumerable: true});
        break;
      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        break;
    }

    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }

  // PAYEE (Pie Chart) DASHBOARD
  searchTransactionMonetaryByMerchant(extendedUserId: number, gte: string, lte: string, role: string, idChart: number): Observable<any> {
    const body = {
      query: {
        bool: {
          filter: [
            {term: {'payee.id': extendedUserId}},
            {term: {status: 'accepted'}}
          ]
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              lastUpdated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            by_merchant: {
              terms: {
                field: 'merchant.id'
              },
              aggs: {
                total_amountBeforeCommission: {
                  sum: {
                    field: 'amountLocalCurrency'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }

  // ADMIN (Bar Chart) DASHBOARD
  searchCompleteTransactionMonetaryReceived(extUserId: number, gte: string, lte: string, interval: string,
                                            role: string, idChart: number): Observable<any> {

    const body = {
      query: {
        bool: {
          filter: [
            {term: {status: 'accepted'}}
          ]
        }
      },
      aggs: {
        filterByDate: {
          filter: {
            range: {
              lastUpdated: {
                gte,
                lte
              }
            }
          },
          aggs: {
            dateCreated_date_histogram: {
              date_histogram: {
                field: 'lastUpdated',
                interval,
                format: 'yyyy-MM-dd',
                keyed: true
              },
              aggs: {
                stats_field: {
                  stats: {
                    field: 'amountAfterCommission'
                  }
                }
              }
            }
          }
        }
      },
      size: 0
    };

    return this.requestService.esPostRequest(this.transactionUri, JSON.stringify(body), {withoutLoader: true, idChart});
  }
}
