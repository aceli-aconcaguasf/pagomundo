import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  public langCurrent = localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" );


  constructor( private translate: TranslateService ){
    this.translate.setDefaultLang(this.langCurrent);
  }

  public cambiarLenguaje(lang) {
    this.langCurrent = lang;
    this.translate.use(lang);
  }
}