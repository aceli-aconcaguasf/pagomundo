import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserSessionService} from '../user/userSession.service';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(private router: Router,
              public userSessionService: UserSessionService) {
  }

  canActivate(): boolean {

    if (this.userSessionService.isLoggedIn() && this.userSessionService.isNotDelete()) {

      return true;
    }
    else {
      this.router.navigate(['/login']).then();
      return false;
    }
  }
}