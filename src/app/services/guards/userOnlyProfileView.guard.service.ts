import { Injectable } from '@angular/core';
import { ActivatedRoute, CanActivate, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { UserSessionService } from '../user/userSession.service';

@Injectable()
export class UserOnlyProfileViewGuard implements CanActivate {

  constructor(private router: Router,
    private route: ActivatedRoute,
    public userSessionService: UserSessionService) {

  }
  pathSuperAdmin = ['setting', 'withdrawal'];
  pathGeneral = ['dashboard', 'payment', 'relatedUsers', 'notification', 'statistics','login','mobileMessage',
'profile','messageConfirmation'];
  canActivate() {
    let windowLocationHref = window.location.href;
    let path = String(windowLocationHref).split('/');
    let componentPath = path[3];
    // console.log(path[3]);
    if (this.userSessionService.onlyProfileView()) {
      this.router.navigate(['/profile']).then();
      return false;
    } else {
      if ((this.userSessionService.isSuperAdmin() && this.pathSuperAdmin.find(path => path === componentPath))) {
        return true;
      } else if ((this.userSessionService.isAdmin() ||
        this.userSessionService.isSuperAdmin() ||
        this.userSessionService.isMerchant()) && componentPath === 'fund') {
        return true;
      }
      else if (this.pathGeneral.find(path => path === componentPath)) {
        return true;
      } else {
        return false;
      }
    }
  }
}
