import {Component, OnDestroy, ChangeDetectorRef, OnInit} from '@angular/core';
import {DataFinderService} from '../../services/data-finder.service';
import {UserService} from '../../services/user/user.service';
import {Router} from '@angular/router';
import {ExtendedUser, User} from '../../config/interfaces';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserSessionService} from '../../services/user/userSession.service';
import {NotificationService} from '../../services/service.index';
import {TooltipPosition} from '@angular/material/tooltip';
import {ROLES, STATUSES_SIDE_BAR, STATUSES_COLOR} from '../../config/enums';
import {environment} from '../../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import * as getLanguage from '../../app.action';
export enum MenuOptions {
  DASHBOARD= 'DASHBOARD',
  PAYMENTS= 'PAYMENTS',
  USERS = 'USERS',
  NOTIFICATIONS = 'NOTIFICATIONS',
  FUNDING = 'FUNDING',
  WITHDRAWAL = 'WITHDRAWAL',
  STATISTICS = 'STATISTICS',
  SETTINGS = 'SETTINGS',
}

declare var $: any;

@Component({
  selector: 'app-side-bar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SideBarComponent implements OnInit, OnDestroy {

  environment = environment;
  langCurrent:string = "en";
  options: any[] = [];
  user: User;
  extendedUser: ExtendedUser;
  role: string;
  userImg: string;
  logoImg: string;
  userEmail: string;
  menuCollapse = false;
  textSizeRatio = 3;
  sidebarReSellerColor = false;
  statuses = STATUSES_SIDE_BAR;
  statusesColor = STATUSES_COLOR;
  positionOptions: TooltipPosition =  'above';
  openMobileBar = false;

  newIssues: BehaviorSubject<boolean> = this.notificationService.newIssues;
  newMessages: BehaviorSubject<boolean> = this.notificationService.newMessages;
  issueSubject: BehaviorSubject<boolean> = this.notificationService.issueSubject;
  accountBalance: BehaviorSubject<number> = this.userService.accountBalance;
  logoImage: BehaviorSubject<any> = this.userService.logoImage;

  watcher: any;
  accountBalanceWatcher: any;
  keyLanguage$:Observable<any> = this.store.select('language');
  changeClass:number = 0;
  bandPm14m3r1c4sCh4ng3Cl4ss:string;
  constructor(private dataFinder: DataFinderService,
              private router: Router,
              private notificationService: NotificationService,
              public userSessionService: UserSessionService,
              private userService: UserService,
              public translate: TranslateService,
              private store: Store<language>) {
                let userSessionLocal = JSON.parse( localStorage.getItem( "Pm1-4m3r1c4s-us3r" ) );
                this.keyLanguage$.subscribe(item => {
                  const language:any = item
                  this.translate.use(language.key);
                  this.langCurrent = language.key;
                  this.bandPm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
                  if( this.bandPm14m3r1c4sCh4ng3Cl4ss == "1" ){
                    this.changeClass = 1;
                  }
                  else{
                    this.changeClass = 0;
                  }
                })
    this.user = this.userSessionService.loadUser();
    this.extendedUser = this.userSessionService.loadExtendedUser();
    this.getLogoReSellerImage();
    this.role = this.userSessionService.loadRole();
    this.dataFinder.getOptionsJSON().subscribe(
      data => ( this.options = data)
    );
    this.userEmail = this.user.email;
    if (this.userSessionService.loadExtendedUser()) {
      this.userService.updateAccountBalanceObservable(this.userSessionService.loadExtendedUser().balance);
      this.watchForNewNotification();
    }
    this.translate.use(this.user.langKey);
    this.langCurrent = this.user.langKey;
  }

  selectLangueage(){
    window.localStorage.setItem( "Pm1-4m3r1c4s-l4ngu4g3", this.langCurrent );
    this.store.dispatch(new getLanguage.LanguageKey(this.langCurrent));
    this.userService.changeLanguage( this.user.id, this.langCurrent ).subscribe();
  }

  canSeeConnection(): boolean {
    return this.user.id === 5;
  }

  initUserForRole() {
    if (this.userSessionService.isReSeller()) {
      this.updateAccountBalance();
    } else if (this.userSessionService.isMerchant()) {
      this.updateAccountBalance();
    }
    this.refreshReSellerLogoImage();
  }

  ngOnInit() {
    this.initUserForRole();
    this.user.imageUrl ? this.userImg = this.user.imageUrl : this.userImg = null;
    this.accountBalance = this.userService.accountBalance;
    this.setTextSizeRatio();

    this._selectFunction();
  }

  //Funciones de select
  _selectFunction(){

    let allOptions = $("ul").children('li:not(.init)');
    allOptions.removeClass('selected');
    $( ".select-" + this.langCurrent ).addClass('selected');
    let countryLang = "";
    if( this.langCurrent == "en" ){

      countryLang = "English";
    }
    else if( this.langCurrent == "es" ){

      countryLang = "Español";
    }
    if( this.langCurrent == "pt" ){

      countryLang = "Português";
    }

    $("ul").children('li.init').html( '<div class="row"><div class="col-md-4"><img class="bandera-imagen" src="./assets/flag/24/'+ this.langCurrent +'.png"></div><div class="col-md-6"><label class="countryLang">' + countryLang + '</label></div><div class="col-md-2"><i class="fas fa-angle-down"></i></div></div>' );


    $("ul").on("click", ".init", function( e ) {

      e.stopPropagation();

      $( "li.init" ).attr("data-value",1);
      $(this).closest("ul").children('li:not(.init)').toggle();
    });

    $("ul").on("click", "li:not(.init)", function() {
        allOptions.removeClass('selected');
        $(this).addClass('selected');
        $("ul").children('.init').html($(this).html());
        allOptions.toggle();
        $( ".init" ).attr("data-value",0);
    });

    $( "html" ).click(function(){

      let selectInit = $( "li.init" ).attr("data-value");
      if( selectInit == "1" ){

        $( ".init" ).attr("data-value",0);
        allOptions.toggle();
      }
    });
  }

  setTextSizeRatio() {
    if (this.userSessionService.haveStatus()) {
      this.textSizeRatio =  this.extendedUser.fullName.split(' ').length <= 3
        ? 3 : this.extendedUser.fullName.split(' ').length - 1;
    } else {
      const arrayName = this.user.firstName.concat(' ' + this.user.lastName);
      this.textSizeRatio =  arrayName.split(' ').length <= 3
        ? 3 : arrayName.split(' ').length - 1;
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.watcher);
    clearTimeout(this.accountBalanceWatcher);
    this.newIssues.next(false);
    this.newMessages.next(false);
  }

  logout() {
    this.userSessionService.logout();
  }

  seeUserProfile() {
    this.router.navigate(['/profile']).then();
    this.openMobileBar = false;
  }

  menuCollapseOnClickHandler() {
    this.menuCollapse = !this.menuCollapse;
    this.userService.updateMenuCollapse(this.menuCollapse);
  }

  seeMenuOption(option): boolean {
    let canSee;
    switch (option) {
      case MenuOptions.DASHBOARD:
      case MenuOptions.PAYMENTS:
        canSee = true;
        break;
      case MenuOptions.USERS:
      case MenuOptions.STATISTICS:
      case MenuOptions.NOTIFICATIONS:
        canSee = !this.userSessionService.isDisabled();
        break;
      case MenuOptions.FUNDING:
        canSee = this.role === ROLES.MERCHANT || this.isSuperOrAdmin();
        break;
      case MenuOptions.WITHDRAWAL:
        canSee = this.userSessionService.isSuperAdmin() || this.userSessionService.isReSeller();
        break;
      case MenuOptions.SETTINGS:
        canSee = this.userSessionService.isSuperAdmin();
        break;
    }
    return canSee;
  }

  watchForNewNotification() {
    const isSuperOrAdmin = this.role === ROLES.SUPER_ADMIN || this.role === ROLES.ADMIN;
    this.notificationService.getAllCreatedMessageOfUser(this.extendedUser, true).subscribe(
      message => {
        if (message.totalElements > 0) {
          this.notificationService.changeNewNotificationValue('MESSAGE', true);
        }
      }
    );

    if (isSuperOrAdmin) {
      this.notificationService.getAllCreatedIssueOfUser(this.extendedUser, true).subscribe(
        issue => {
          if (issue.totalElements > 0) {
            this.notificationService.changeNewNotificationValue('ISSUE', true);
          }
        }
      );
    }

    this.watcher = setTimeout(() => {
      this.watchForNewNotification();
    }, environment.watchTime);
    // OnDestroy we clear the setTimeout

  }

  handleClickNewNotificationButton(type) {
    this.issueSubject.next( type === 'ISSUE');
    this.notificationService.changeNewNotificationValue(type === 'ISSUE' ? 'ISSUE' : 'MESSAGE', false);
    this.router.navigate(['/notification']).then();
  }

  handleMenusOption(option) {
    this.openMobileBar = false;

    if (option.value === MenuOptions.NOTIFICATIONS) {
      this.issueSubject.next( false);
      return;
    }

    if (option.value === MenuOptions.WITHDRAWAL && this.userSessionService.isReSeller()) {
      this.userService.getBasicProfile().subscribe(
        extendedUser => {
          this.userSessionService.setUserStatus(extendedUser.status);
          this.userService.updateAccountBalanceObservable(extendedUser.balance);
          return;
        });
    }
  }

  updateAccountBalance() {
    this.accountBalanceWatcher = setInterval(() => {
      this.userService.getBasicProfile().subscribe(
        extendedUser => {
          this.userSessionService.setUserStatus(extendedUser.status);
          this.userService.updateAccountBalanceObservable(extendedUser.balance);
          this.extendedUser.balance = extendedUser.balance;
          this.userSessionService.saveExtendedUser(this.extendedUser);
        });
    }, 60000);
  }

  isSuperOrAdmin(): boolean {
    return this.role === ROLES.SUPER_ADMIN || this.role === ROLES.ADMIN;
  }

  setColorAccountBalance(): boolean {
    if (this.userSessionService.isReSeller() && this.accountBalance.value < 500) {
      return true;
    } else {
      return this.userSessionService.isMerchant() && this.accountBalance.value < 100;
    }
  }

  getLogoReSellerImage() {
    setTimeout(() => {
      if (this.userSessionService.haveStatus() && this.extendedUser.imageAddressUrl) {
        const imagePath = this.extendedUser.imageAddressUrl.split('/');
        this.logoImg = `assets/fileBucket/logo/${imagePath[2]}?${performance.now()}`;
        this.sidebarReSellerColor = true;
      } else {
        this.sidebarReSellerColor = false;
        this.logoImg = this.bandPm14m3r1c4sCh4ng3Cl4ss === '1' ? 'assets/img/logo-pago-mundo.png' : 'assets/img/logo.svg';
      }
    }, 0);

  }

  refreshReSellerLogoImage() {
    this.logoImage.subscribe(extendedUser => {
      if (extendedUser) {
        if (Object.keys(extendedUser).length > 0) {
          this.extendedUser = extendedUser;
          this.userSessionService.saveExtendedUser(this.extendedUser);
          this.getLogoReSellerImage();
        }
      }
    });
  }

  toggleMobileBar() {
    this.openMobileBar = !this.openMobileBar;
  }
}
