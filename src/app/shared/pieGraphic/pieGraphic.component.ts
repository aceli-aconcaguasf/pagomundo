import {Component, Input, AfterViewInit, ChangeDetectorRef, Output, EventEmitter} from '@angular/core';
import {ChartType, ChartOptions} from 'chart.js';
import {SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip} from 'ng2-charts';
import {ChartService, ReportService, UserService} from 'src/app/services/service.index';
import {Subject, BehaviorSubject} from 'rxjs';
import * as moment from 'moment';
import {ExtendedUser} from 'src/app/config/interfaces';
import {CHART_TYPE, CHART_SUB_TYPE, ROLES} from '../../config/enums';

enum STATUS_NAME {
  CREATED = 'created',
  IN_PROCESS = 'in_process',
  ACCEPTED = 'accepted',
  REJECTED = 'rejected',
  CANCELLED = 'cancelled',
  FAILED = 'failed'
}

enum TRANSACTION_STATUSES {
  CREATED = 'Pending',
  IN_PROCESS = 'In process',
  ACCEPTED = 'Completed',
  REJECTED = 'Rejected',
  CANCELLED = 'Cancelled',
  FAILED = 'Failed'
}

enum USER_STATUSES {
  CREATED = 'Pending',
  IN_PROCESS = 'Processing',
  ACCEPTED = 'Active',
  REJECTED = 'Disabled',
  CANCELLED = 'Deleted',
  FAILED = 'Failed'
}

@Component({
  selector: 'app-pie-graphic',
  templateUrl: './pieGraphic.component.html',
  styleUrls: ['./pieGraphic.component.scss']
})
export class PieGraphicComponent implements AfterViewInit {

  @Input() idChart: number;
  @Input() chartType: string;
  @Input() chartSubType: string;
  @Input() extendedUser: ExtendedUser;
  @Input() title: string;
  @Input() role: string;
  @Input() isDashboard: boolean;
  @Input() showIntervals: boolean;
  @Input() legendToLeft: boolean;

  @Output() public showRequestTransactionMsg = new EventEmitter<boolean>();

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;

  noData = false;
  showSpinner = false;
  errorData = false;
  failedAttempts = 0;

  now = moment().format('YYYY-MM-DD');
  year = moment().startOf('year').format('YYYY-MM-DD');
  month = moment().startOf('month').format('YYYY-MM');

  firstDayOfCurrentYear = moment().startOf('year').format('YYYY-MM-DD');
  period = moment().format('YYYY');
  lastDayOfCurrentYear = moment().endOf('year').format('YYYY-MM-DD');
  gte = this.firstDayOfCurrentYear;
  lte = this.lastDayOfCurrentYear;
  stringYear = moment().format('YYYY');
  firstDayOfCurrentMonth = moment().startOf('month').format('YYYY-MM-DD');
  startOfMonth = this.firstDayOfCurrentMonth;
  endOfMonth = moment().endOf('month').format('YYYY-MM-DD');
  stringMonth = moment(this.firstDayOfCurrentMonth).format('MMM - YYYY');
  semester: string;
  firstDayOfFirstSemester = moment(this.year).startOf('year').format('YYYY-MM-DD');
  lastMonthOfFirstSemester = moment(this.firstDayOfFirstSemester).add(5, 'month');
  lastDayOfFirstSemester = moment(this.lastMonthOfFirstSemester).endOf('month').format('YYYY-MM-DD');
  checked: boolean;
  value: number;
  periodFilterSelected = 4;
  options = [
    {name: '15d', value: 1, checked: false},
    {name: '1M', value: 2, checked: false},
    {name: '6M', value: 3, checked: false},
    {name: '12M', value: 4, checked: true}
  ];

  // Range date picker
  bsValue = new Date();
  bsRangeValue: Date[];
  maxDate = new Date();

  // Pie
  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public chartColors: any[] = [{
    backgroundColor: ['#88A3D2', '#7BD8DD', '#B4CC8E', '#E2EC98', '#FCEC8A', '#F8D372',
      '#F6BA84', '#F79882', '#F37C8F', '#D17D9F', '#B188C0', '#998AC1']
  }];
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    }
  };

  arrayStatus = ['accepted', 'rejected', 'created', 'in_process', 'cancelled'];
  refreshPieComponent: Subject<any> = this.chartService.refreshPieComponent;

  constructor(private chartService: ChartService,
              private reportService: ReportService,
              private cdRef: ChangeDetectorRef,
              private userService: UserService) {

    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
    this.maxDate.setDate(this.maxDate.getDate() + 7);
    this.bsRangeValue = [this.bsValue, this.maxDate];

    if (this.legendToLeft) {
      this.pieChartOptions = {
        responsive: true,
        legend: {
          position: 'left',
        }
      };
    }

    // To re render when field data error curred
    this.refreshPieComponent.subscribe(result => {
      if (result.idChart === this.idChart) {
        if (this.failedAttempts !== 1) {
          this.failedAttempts++;
          this.chartType === CHART_TYPE.TRANSACTION ? this.getTransaction() : this.getUserStatus();
        } else {
          this.showSpinner = false;
          this.errorData = true;
        }
      }
    });
  }

  ngAfterViewInit() {
    this.chartType === CHART_TYPE.TRANSACTION ? this.getTransaction() : this.getUserStatus();
    this.cdRef.detectChanges();
  }

  getUserStatus() {
    this.role === ROLES.MERCHANT
      ? this.chartSubType === CHART_SUB_TYPE.PAYEE_STATUS
      ? this.getExtendedUserRelatedStatus() : this.getExtendedUserRelatedCountry()
      : this.getExtendedUserStatus();
  }

  getTransaction() {
    if (this.chartSubType === CHART_SUB_TYPE.MONETARY_BY_COUNTRY) {
      this.getTransactionMonetaryByCountry();
    } else if (this.chartSubType === CHART_SUB_TYPE.MONETARY_BY_MERCHANT) {
      this.getTransactionMonetaryByMerchant();
    } else {
      this.getTransactionStatus();
    }
  }

  // ALL
  getTransactionStatus() {
    this.errorData = false;
    this.showSpinner = true;
    this.chartService.searchTransactionStatus(this.extendedUser.id, this.gte, this.lte, this.extendedUser.role, this.idChart).subscribe(
      data => {
        this.showSpinner = false;
        try {
          this.pieChartLabels = [];
          this.pieChartData = [];
          const buckets = data.aggregations.filterByDate.status.buckets;
          this.arrayStatus.forEach(status => {
            let nameStatus;
            const elem = buckets.find(obj => obj.key === status);
            switch (status) {
              case STATUS_NAME.ACCEPTED:
                nameStatus = TRANSACTION_STATUSES.ACCEPTED;
                break;
              case STATUS_NAME.REJECTED:
                nameStatus = TRANSACTION_STATUSES.REJECTED;
                break;
              case STATUS_NAME.CREATED:
                nameStatus = TRANSACTION_STATUSES.CREATED;
                break;
              case STATUS_NAME.IN_PROCESS:
                nameStatus = TRANSACTION_STATUSES.IN_PROCESS;
                break;
              case STATUS_NAME.CANCELLED:
                nameStatus = TRANSACTION_STATUSES.CANCELLED;
                break;
              case STATUS_NAME.FAILED:
                nameStatus = TRANSACTION_STATUSES.FAILED;
                break;
            }
            if (elem) {
              this.pieChartLabels.push(nameStatus);
              this.pieChartData.push(elem.doc_count);
            }
          });

        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
        this.pieChartData.length === 0 ? this.noData = true : this.noData = false;
      });
  }

  // ALL
  getTransactionMonetaryByCountry() {
    this.errorData = false;
    this.showSpinner = true;
    let callMethod;

    if (this.isDashboard) {
      const month =  moment().startOf('month').format('YYYY-MM');
      callMethod = this.chartService.searchTransactionMonetaryByCountry(this.extendedUser.id,
        month, 'now', this.extendedUser.role, this.idChart);
    } else {
      callMethod = this.chartService.searchTransactionMonetaryByCountry(this.extendedUser.id,
        this.gte, this.lte, this.extendedUser.role, this.idChart);
    }

    callMethod.subscribe(data => {
      this.showSpinner = false;
      try {
        this.pieChartLabels = [];
        this.pieChartData = [];
        const buckets = data.aggregations.filterByDate.by_territory.buckets;

        buckets.forEach(elem => {
          this.pieChartLabels.push(elem.key);
          this.pieChartData.push(Math.trunc(elem.total_amountBeforeCommission.value));
        });
      } catch (err) {
        console.error('CATCH_ERROR');
        console.error(err);
      }
      this.pieChartData.length === 0 ? this.noData = true : this.noData = false;
    });
  }

  // PAYEE
  getTransactionMonetaryByMerchant() {
    this.errorData = false;
    this.noData = false;
    this.showSpinner = true;
    const month =  moment().startOf('month').format('YYYY-MM');

    this.chartService.searchTransactionMonetaryByMerchant(this.extendedUser.id,
      month, 'now', this.extendedUser.role, this.idChart).subscribe(data => {
      try {
        this.pieChartLabels = [];
        this.pieChartData = [];
        const buckets = data.aggregations.filterByDate.by_merchant.buckets;

        if (buckets.length === 0) {
          this.noData = true;
          this.showSpinner = false;
        }

        let bucketsCount = 0;
        buckets.forEach(elem => {
          bucketsCount++;
          this.userService.getSearchExtendedUserById(elem.key).subscribe(extendedUser => {
            this.pieChartLabels.push(extendedUser.content[0].company);
            this.pieChartData.push(Math.trunc(elem.total_amountBeforeCommission.value));
            if (buckets.length === bucketsCount) { this.showSpinner = false; }
          });
        });

      } catch (err) {
        console.error('CATCH_ERROR');
        console.error(err);
        this.showSpinner = false;
        this.errorData = true;
      }
    });
  }

  // MERCHANT
  getExtendedUserRelatedStatus() {
    this.errorData = false;
    this.showSpinner = true;
    this.chartService.searchUserRelatedStatus(this.extendedUser.user.id, this.gte, this.lte, this.idChart).subscribe(
      data => {
        this.showSpinner = false;
        try {
          this.pieChartLabels = [];
          this.pieChartData = [];
          const buckets = data.aggregations.filters.status.buckets;
          buckets.forEach(status => {
            let nameStatus;
            switch (status.key) {
              case STATUS_NAME.ACCEPTED:
                nameStatus = USER_STATUSES.ACCEPTED;
                break;
              case STATUS_NAME.CANCELLED:
                nameStatus = USER_STATUSES.CANCELLED;
                break;
              case STATUS_NAME.CREATED:
                nameStatus = USER_STATUSES.CREATED;
                break;
              case STATUS_NAME.IN_PROCESS:
                nameStatus = USER_STATUSES.IN_PROCESS;
                break;
              case STATUS_NAME.REJECTED:
                nameStatus = USER_STATUSES.REJECTED;
                break;
              case STATUS_NAME.FAILED:
                nameStatus = USER_STATUSES.FAILED;
                break;
            }
            this.pieChartLabels.push(nameStatus);
            this.pieChartData.push(status.doc_count);
          });
        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
        this.pieChartData.length === 0 ? this.noData = true : this.noData = false;
      });
  }

  // MERCHANT
  getExtendedUserRelatedCountry() {
    this.errorData = false;
    this.showSpinner = true;
    this.chartService.searchUserRelatedCountry(this.extendedUser.user.id, this.gte, this.lte, this.idChart).subscribe(
      data => {
        this.showSpinner = false;
        try {
          this.pieChartLabels = [];
          this.pieChartData = [];
          const buckets = data.aggregations.filters.country.buckets;

          buckets.forEach(country => {
            let nameStatus;
            switch (country.key) {
              case 1:
                nameStatus = 'Colombia';
                break;
              case 2:
                nameStatus = 'México';
                break;
              case 66:
                nameStatus = 'Argentina';
                break;
              case 74:
                nameStatus = 'Brazil';
                break;
              default:
                nameStatus = 'Other';
                break;
            }
            this.pieChartLabels.push(nameStatus);
            this.pieChartData.push(country.doc_count);
          });
        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
        this.pieChartData.length === 0 ? this.noData = true : this.noData = false;
      });
  }

  // ADMIN
  getExtendedUserStatus() {
    this.errorData = false;
    this.showSpinner = true;
    this.chartService.searchUserStatus(this.gte, this.lte, this.idChart).subscribe(
      data => {
        this.showSpinner = false;
        try {
          this.pieChartLabels = [];
          this.pieChartData = [];
          const buckets = data.aggregations.filters.status.buckets;
          buckets.forEach(status => {
            let nameStatus;
            switch (status.key) {
              case STATUS_NAME.ACCEPTED:
                nameStatus = USER_STATUSES.ACCEPTED;
                break;
              case STATUS_NAME.CANCELLED:
                nameStatus = USER_STATUSES.CANCELLED;
                break;
              case STATUS_NAME.CREATED:
                nameStatus = USER_STATUSES.CREATED;
                break;
              case STATUS_NAME.IN_PROCESS:
                nameStatus = USER_STATUSES.IN_PROCESS;
                break;
              case STATUS_NAME.REJECTED:
                nameStatus = USER_STATUSES.REJECTED;
                break;
              case STATUS_NAME.FAILED:
                nameStatus = USER_STATUSES.FAILED;
                break;
            }
            this.pieChartLabels.push(nameStatus);
            this.pieChartData.push(status.doc_count);
          });
        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
        this.pieChartData.length === 0 ? this.noData = true : this.noData = false;
      });
  }

  requestReportTransaction() {
    const date = `[${this.gte} TO ${this.lte}]`;

    this.reportService.requestTransactionReports(this.idChart, this.title, date,
      this.extendedUser.user.id, this.extendedUser.id).subscribe(
      () => {
        this.showRequestTransactionMsg.emit(true);
      });
  }

  // -------------------------

  initPeriodFilter() {
    switch (this.periodFilterSelected) {
      case 1:
        this.getLast15Days();
        break;
      case 2:
        this.getFirstDayOfAMonth();
        break;
      case 3:
        this.getFirstDayOfSixMonthAgo();
        break;
      case 4:
        this.getFirstDayOfAYear();
        break;
    }
    this.chartType === CHART_TYPE.TRANSACTION ? this.getTransaction() : this.getUserStatus();
  }

  getLast15Days() {
    this.gte = moment().subtract(14, 'days').format('YYYY-MM-DD'); // fifteenDaysBackConst;
    this.lte = this.now;
    this.period = `from ${this.gte}`;
  }

  getFirstDayOfAMonth() {
    this.firstDayOfCurrentMonth = moment().startOf('month').format('YYYY-MM-DD');
    this.endOfMonth = moment().endOf('month').format('YYYY-MM-DD');
    this.gte = this.firstDayOfCurrentMonth; // firstDayOfTheMonthConst;
    this.lte = this.endOfMonth;
    this.period = this.stringMonth;
  }

  getFirstDayOfSixMonthAgo() {
    this.year = moment().startOf('year').format('YYYY-MM-DD');
    this.gte = this.firstDayOfFirstSemester;
    this.lte = this.lastDayOfFirstSemester;
    this.setSemiannualPeriod();
  }

  getFirstDayOfAYear() {
    this.year = moment().startOf('year').format('YYYY-MM-DD');
    this.firstDayOfCurrentYear = moment(this.year).startOf('year').format('YYYY-MM-DD');
    this.lastDayOfCurrentYear = moment(this.year).endOf('year').format('YYYY-MM-DD');
    this.gte = this.firstDayOfCurrentYear;
    this.lte = this.lastDayOfCurrentYear;
    this.period = this.stringYear;
  }

  setSemiannualPeriod() {
    if (moment(this.gte).format('MM') === '01') {
      this.semester = '1st semester';
    } else {
      this.semester = '2nd semester';
    }
    this.period = `${this.semester} ${moment(this.year).format('YYYY')}`;
  }

  setPeriodBefore() {
    switch (this.periodFilterSelected) {
      case 1:
        this.gte = moment(this.gte).subtract(15, 'day').format('YYYY-MM-DD');
        this.lte = moment(this.lte).subtract('15', 'day').format('YYYY-MM-DD');
        this.period = `from ${this.gte}`;
        break;

      case 2:
        this.firstDayOfCurrentMonth = moment(this.firstDayOfCurrentMonth).subtract(1, 'month').format('YYYY-MM-DD');
        this.period = moment(this.firstDayOfCurrentMonth).format('MMM - YYYY');
        this.startOfMonth = moment(this.firstDayOfCurrentMonth).startOf('month').format('YYYY-MM-DD');
        this.endOfMonth = moment(this.firstDayOfCurrentMonth).endOf('month').format('YYYY-MM-DD');
        this.gte = this.startOfMonth;
        this.lte = this.endOfMonth;
        break;

      case 3:
        this.firstDayOfFirstSemester = moment(this.firstDayOfFirstSemester).subtract(6, 'month').format('YYYY-MM-DD');
        this.lastDayOfFirstSemester = moment(this.lastDayOfFirstSemester).subtract(6, 'month').format('YYYY-MM-DD');
        this.gte = this.firstDayOfFirstSemester;
        this.lte = this.lastDayOfFirstSemester;
        this.year = moment(this.firstDayOfFirstSemester).format('YYYY-MM-DD');
        this.setSemiannualPeriod();
        break;

      case 4:
        this.year = moment(this.year).subtract(1, 'year').format('YYYY-MM-DD');
        this.period = moment(this.year).format('YYYY');
        this.firstDayOfCurrentYear = moment(this.year).startOf('year').format('YYYY-MM-DD');
        this.lastDayOfCurrentYear = moment(this.year).endOf('year').format('YYYY-MM-DD');
        this.gte = this.firstDayOfCurrentYear;
        this.lte = this.lastDayOfCurrentYear;
        break;
      default:
        this.period = null;
    }
    this.chartType === CHART_TYPE.TRANSACTION ? this.getTransaction() : this.getUserStatus();
  }

  setPeriodAfter() {
    switch (this.periodFilterSelected) {
      case 1:
        this.gte = moment(this.gte).add(15, 'day').format('YYYY-MM-DD');
        this.lte = moment(this.lte).add('15', 'day').format('YYYY-MM-DD');
        this.period = `from ${this.gte}`;
        break;
      case 2:
        let establecidaMes = moment(this.month).format('YYYY-MM');
        let actualMes = moment().format('YYYY-MM');

        if (establecidaMes < actualMes) { // condicionamos el mes/año para que sea solo igual o menor
        this.firstDayOfCurrentMonth = moment(this.firstDayOfCurrentMonth).add(1, 'month').format('YYYY-MM-DD');
        this.period = moment(this.firstDayOfCurrentMonth).format('MMM - YYYY');
        this.startOfMonth = moment(this.firstDayOfCurrentMonth).startOf('month').format('YYYY-MM-DD');
        this.endOfMonth = moment(this.firstDayOfCurrentMonth).endOf('month').format('YYYY-MM-DD');
        this.gte = this.startOfMonth;
        this.lte = this.endOfMonth;
        }
        break;
      case 3:
        let establecidaSem = +moment(this.year).format('YYYY');
        let actualSem = +moment().format('YYYY');

        if (establecidaSem < actualSem) { // condicionamos el año para que sea solo igual o menor
        this.firstDayOfFirstSemester = moment(this.firstDayOfFirstSemester).add(6, 'month').format('YYYY-MM-DD');
        this.lastDayOfFirstSemester = moment(this.lastDayOfFirstSemester).add(6, 'month').format('YYYY-MM-DD');
        this.gte = this.firstDayOfFirstSemester;
        this.lte = this.lastDayOfFirstSemester;
        this.year = moment(this.firstDayOfFirstSemester).format('YYYY-MM-DD');
        this.setSemiannualPeriod();
        }
        break;
      case 4:
        let establecida = +moment(this.year).format('YYYY');
        let actual = +moment().format('YYYY');

        if (establecida < actual) { // condicionamos el año para que sea solo igual o menor
          this.year = moment(this.year).add(1, 'year').format('YYYY-MM-DD');
          this.period = moment(this.year).format('YYYY');
          this.firstDayOfCurrentYear = moment(this.year).startOf('year').format('YYYY-MM-DD');
          this.lastDayOfCurrentYear = moment(this.year).endOf('year').format('YYYY-MM-DD');
          this.gte = this.firstDayOfCurrentYear;
          this.lte = this.lastDayOfCurrentYear;
        }

        break;
      default:
        this.period = null;
    }
    this.chartType === CHART_TYPE.TRANSACTION ? this.getTransaction() : this.getUserStatus();
  }

}
