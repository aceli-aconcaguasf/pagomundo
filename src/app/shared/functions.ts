import {FormControl} from '@angular/forms';

export function noWhitespaceValidator(control: FormControl) {
  const isWhitespace = (control.value || '').trim().length === 0;
  return !isWhitespace ? null : {whitespace: true};
}

export function determinateMaxDate(): Date {
  const today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1; // January is 0!
  const yyyy = today.getFullYear();
  if (dd < 10) {
    dd = +('0' + dd);
  }
  if (mm < 10) {
    mm = +('0' + mm);
  }

  return new Date(`${yyyy}-${mm}-${dd}`);
}
