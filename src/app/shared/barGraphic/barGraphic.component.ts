import {AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import {ChartService, ReportService, UserService} from 'src/app/services/service.index';
import {ExtendedUser} from 'src/app/config/interfaces';
import * as moment from 'moment';
import {BehaviorSubject, Subject} from 'rxjs';
import {CHART_TYPE, CHART_SUB_TYPE, ROLES} from '../../config/enums';

@Component({
  selector: 'app-bar-graphic',
  templateUrl: './barGraphic.component.html',
  styleUrls: ['./barGraphic.component.scss']
})
export class BarGraphicComponent implements AfterViewInit {

  @Input() idChart: number;
  @Input() extendedUser: ExtendedUser;
  @Input() chartSubType: string;
  @Input() chartType: string;
  @Input() role: string;
  @Input() roleFilter: string;
  @Input() title: string;
  @Input() isDashboard: boolean;
  @Input() showIntervals: boolean;

  @Output() public showRequestTransactionMsg = new EventEmitter<boolean>();

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;

  noData = false;
  showSpinner = false;
  errorData = false;
  failedAttempts = 0;

  startOfYear = moment().startOf('year').format('YYYY-MM-DD');
  endOfYear = moment().endOf('year').format('YYYY-MM-DD');
  gte = this.startOfYear;
  lte = this.endOfYear;

  now = moment().format('YYYY-MM-DD');
  year = moment().startOf('year').format('YYYY-MM-DD');
  period = moment(this.year).format('YYYY');
  stringYear = moment(this.year).format('YYYY');
  month = moment().startOf('month').format('YYYY-MM-DD');
  startOfMonth = this.month;
  endOfMonth = moment().endOf('month').format('YYYY-MM-DD');
  stringMonth = moment(this.month).format('MMM - YYYY');
  firstDayOfFirstSemester = moment(this.year).startOf('year').format('YYYY-MM-DD');
  lastMonthOfFirstSemester = moment(this.firstDayOfFirstSemester).add(5, 'month');
  lastDayOfFirstSemester = moment(this.lastMonthOfFirstSemester).endOf('month').format('YYYY-MM-DD');

  semester: string;
  checked: boolean;
  value: number;
  periodFilterSelected = 4;
  interval = '1M';

  options = [
    {name: '15d', value: 1, checked: false},
    {name: '1M', value: 2, checked: false},
    {name: '6M', value: 3, checked: false},
    {name: '12M', value: 4, checked: true}
  ];

  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartData: ChartDataSets[] = [
    {data: []},
  ];
  public barChartColors: Color[] = [
    {backgroundColor: '#88A3D2'}
  ];

  refreshBarComponent: Subject<any> = this.chartService.refreshBarComponent;

  constructor(private chartService: ChartService,
              private reportService: ReportService,
              private cdRef: ChangeDetectorRef,
              private userService: UserService) {

    // To re render when field data error curred
    this.refreshBarComponent.subscribe(result => {
      if (result.idChart === this.idChart) {
        if (this.failedAttempts !== 1) {
          this.failedAttempts++;
          this.chartType === CHART_TYPE.TRANSACTION ? this.getTransactionReceived() : this.getUserStats();
        } else {
          this.showSpinner = false;
          this.errorData = true;
        }
      }
    });
  }

  ngAfterViewInit() {
    this.chartType === CHART_TYPE.TRANSACTION ? this.getTransactionReceived() : this.getUserStats();
    this.cdRef.detectChanges();
  }

  getUserStats() {
    this.role === ROLES.RESELLER || this.role === ROLES.MERCHANT ? this.getExtendedUserRelatedStats() : this.getExtendedUserStats();
  }

  // ALL
  getTransactionReceived() {
    switch (this.chartSubType) {
      case CHART_SUB_TYPE.NUMBER_PAYMENT:
        this.getNumberOfTransactionReceived();
        break;
      case CHART_SUB_TYPE.MONETARY_AMOUNT_PAYMENT:
      case CHART_SUB_TYPE.COMMISSION_EARN:
        this.getAmountTransactionReceived();
        break;
      default:
        break;
    }
  }

  // ALL
  getNumberOfTransactionReceived() {
    this.errorData = false;
    this.showSpinner = true;
    this.chartService.searchTransactionReceived(this.extendedUser.id,
      this.gte, this.lte, this.interval, this.extendedUser.role, this.idChart, false).subscribe(
      data => {
        this.showSpinner = false;
        try {
          this.barChartLabels = [''];
          const barData = [0];
          this.barChartData = [{data: []}];
          const buckets = data.aggregations.filterByDate.dateCreated_date_histogram.buckets;
          const bucketsKeys = Object.keys(buckets);
          bucketsKeys.length === 0 ? this.noData = true : this.noData = false;
          bucketsKeys.forEach(key => {
            this.barChartLabels.push(buckets[key].key_as_string);
            barData.push(buckets[key].doc_count);
          });
          this.barChartLabels.push('');
          barData.push(0);
          this.barChartData = [{data: barData}];
        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
      }
    );
  }

  // ALL
  getAmountTransactionReceived() {
    this.errorData = false;
    this.showSpinner = true;
    let searchTransactionReceivedCall;

    if (this.chartSubType === CHART_SUB_TYPE.COMMISSION_EARN) {

      // COMMISSION_EARN
      searchTransactionReceivedCall = this.chartService.searchTransactionReceived(this.extendedUser.id,
        this.gte, this.lte, this.interval, this.extendedUser.role, this.idChart, true, true);

    } else {

      // MONETARY_AMOUNT_PAYMENT
      if (this.isDashboard) {
        const month = moment().startOf('month').format('YYYY-MM-DD');
        searchTransactionReceivedCall = this.chartService.searchCompleteTransactionMonetaryReceived(this.extendedUser.id,
          month, 'now', this.interval, this.extendedUser.role, this.idChart);
      } else {
        searchTransactionReceivedCall = this.chartService.searchTransactionReceived(this.extendedUser.id,
          this.gte, this.lte, this.interval, this.extendedUser.role, this.idChart, true);
      }
    }

    searchTransactionReceivedCall.subscribe(
      data => {
        this.showSpinner = false;
        try {
          this.barChartLabels = [''];
          const barData = [0];
          this.barChartData = [{data: []}];
          const buckets = data.aggregations.filterByDate.dateCreated_date_histogram.buckets;
          const bucketsKeys = Object.keys(buckets);
          bucketsKeys.length === 0 ? this.noData = true : this.noData = false;
          bucketsKeys.forEach(key => {
            this.barChartLabels.push(buckets[key].key_as_string);
            barData.push(Math.trunc(buckets[key].stats_field.sum));
          });
          this.barChartLabels.push('');
          barData.push(0);
          this.barChartData = [{data: barData}];
        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
      }
    );
  }

  // Reseller & MERCHANT
  getExtendedUserRelatedStats() {
    this.errorData = false;
    this.showSpinner = true;
    let rolToSearch;
    this.role === ROLES.RESELLER ? rolToSearch = 'role_merchant' : rolToSearch = 'role_payee';
    this.chartService.searchUserRelatedCount(this.extendedUser.user.id, rolToSearch,
      this.gte, this.lte, this.interval, this.idChart).subscribe(
      data => {

        this.showSpinner = false;
        try {
          this.barChartLabels = [''];
          const barData = [0];

          this.barChartData = [{data: []}];
          const buckets = data.aggregations.relations.dateCreated_date_histogram.buckets;
          const bucketsKeys = Object.keys(buckets);
          bucketsKeys.length === 0 ? this.noData = true : this.noData = false;

          bucketsKeys.forEach(key => {
            this.barChartLabels.push(buckets[key].key_as_string);
            barData.push(buckets[key].doc_count);
          });

          this.barChartLabels.push('');
          barData.push(0);

          this.barChartData = [{data: barData}];

        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
      });
  }

  // ADMIN
  getExtendedUserStats() {
    this.errorData = false;
    this.showSpinner = true;
    this.chartService.searchUserCount(this.roleFilter, this.gte, this.lte, this.interval, this.idChart).subscribe(
      data => {
        this.showSpinner = false;
        try {
          this.barChartLabels = [''];
          const barData = [0];

          this.barChartData = [{data: []}];
          const buckets = data.aggregations.filterByDate.dateCreated_date_histogram.buckets;
          const bucketsKeys = Object.keys(buckets);
          bucketsKeys.length === 0 ? this.noData = true : this.noData = false;

          bucketsKeys.forEach(key => {
            this.barChartLabels.push(buckets[key].key_as_string);
            barData.push(buckets[key].doc_count);
          });

          this.barChartLabels.push('');
          barData.push(0);

          this.barChartData = [{data: barData}];

        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
      });
  }

  requestReportTransaction() {
    const date = `[${this.gte} TO ${this.lte}]`;

    this.reportService.requestTransactionReports(this.idChart, this.title, date,
      this.extendedUser.user.id, this.extendedUser.id).subscribe(
      () => {
        this.showRequestTransactionMsg.emit(true);
     });
  }

  // ------------------------------------------

  initPeriodFilter() {
    switch (this.periodFilterSelected) {
      case 1:
        this.getLast15Days();
        break;
      case 2:
        this.getFirstDayOfAMonth();
        break;
      case 3:
        this.getFirstDayOfSixMonthAgo();
        break;
      case 4:
        this.getFirstDayOfAYear();
        break;
    }
    this.chartType === CHART_TYPE.TRANSACTION ? this.getTransactionReceived() : this.getUserStats();


  }

  // Change Intervals

  getLast15Days() {
    this.gte = moment().subtract(14, 'days').format('YYYY-MM-DD'); // fifteenDaysBackConst
    this.lte = this.now;
    this.interval = '1d';
    this.period = `from ${this.gte}`;
  }

  getFirstDayOfAMonth() {
    this.month = moment().startOf('month').format('YYYY-MM-DD');
    this.endOfMonth = moment().endOf('month').format('YYYY-MM-DD');
    this.gte = this.month; // firstDayOfTheMonthConst
    this.lte = this.endOfMonth;
    this.interval = '1d';
    this.period = this.stringMonth;
  }

  getFirstDayOfSixMonthAgo() {
    this.year = moment().startOf('year').format('YYYY-MM-DD');
    this.gte = this.firstDayOfFirstSemester;
    this.lte = this.lastDayOfFirstSemester;
    this.interval = '1M';
    this.setSemiannualPeriod();
  }

  getFirstDayOfAYear() {
    this.year = moment().startOf('year').format('YYYY-MM-DD');
    this.startOfYear = moment(this.year).startOf('year').format('YYYY-MM-DD');
    this.endOfYear = moment(this.year).endOf('year').format('YYYY-MM-DD');
    this.gte = this.startOfYear;
    this.lte = this.endOfYear;
    this.interval = '1M';
    this.period = this.stringYear;
  }

  setSemiannualPeriod() {
    if (moment(this.gte).format('MM') === '01') {
      this.semester = '1st semester';
    } else {
      this.semester = '2nd semester';
    }
    this.period = `${this.semester} ${moment(this.year).format('YYYY')}`;

  }

  // Change Period

  setPeriodBefore() {
    switch (this.periodFilterSelected) {
      case 1:
        this.gte = moment(this.gte).subtract(15, 'day').format('YYYY-MM-DD');
        this.lte = moment(this.lte).subtract(15, 'day').format('YYYY-MM-DD');
        this.period = `from ${this.gte}`;
        break;

      case 2:
        this.month = moment(this.month).subtract(1, 'month').format('YYYY-MM-DD');
        this.period = moment(this.month).format('MMM - YYYY');
        this.startOfMonth = moment(this.month).startOf('month').format('YYYY-MM-DD');
        this.endOfMonth = moment(this.month).endOf('month').format('YYYY-MM-DD');
        this.gte = this.startOfMonth;
        this.lte = this.endOfMonth;
        break;

      case 3:
        this.firstDayOfFirstSemester = moment(this.firstDayOfFirstSemester).subtract(6, 'month').format('YYYY-MM-DD');
        this.lastDayOfFirstSemester = moment(this.lastDayOfFirstSemester).subtract(6, 'month').format('YYYY-MM-DD');
        this.gte = this.firstDayOfFirstSemester;
        this.lte = this.lastDayOfFirstSemester;
        this.year = moment(this.firstDayOfFirstSemester).format('YYYY-MM-DD');
        this.setSemiannualPeriod();
        break;

      case 4:
        this.year = moment(this.year).subtract(1, 'year').format('YYYY-MM-DD');
        this.period = moment(this.year).format('YYYY');
        this.startOfYear = moment(this.year).startOf('year').format('YYYY-MM-DD');
        this.endOfYear = moment(this.year).endOf('year').format('YYYY-MM-DD');
        this.gte = this.startOfYear;
        this.lte = this.endOfYear;
        break;
      default:
        this.period = null;
    }
    this.chartType === CHART_TYPE.TRANSACTION ? this.getTransactionReceived() : this.getUserStats();
  }

  setPeriodAfter() {
    switch (this.periodFilterSelected) {
      case 1:
        let añoActualQuincena = moment().format('YYYY');
        let mesActualQuincena = moment().format('MM');
        let diaActualQuincena = moment().format('DD');
        let añoperiodoQuincena = moment(this.lte).add(15, 'day').format('YYYY');
        let mesperiodoQuincenaInicio = moment(this.gte).add(15, 'day').format('MM');
        let diaperiodoQuincenaInicio = moment(this.gte).add(15, 'day').format('DD');

                                

        if (+añoActualQuincena === +añoperiodoQuincena) {
          if (+mesActualQuincena === +mesperiodoQuincenaInicio) {
            if (+diaActualQuincena >= +diaperiodoQuincenaInicio) {
              this.gte = moment(this.gte).add(15, 'day').format('YYYY-MM-DD');
              this.lte = moment(this.lte).add(15, 'day').format('YYYY-MM-DD');
              this.period = `from ${this.gte}`; 
            }

          } else if (+mesActualQuincena > +mesperiodoQuincenaInicio) {
            this.gte = moment(this.gte).add(15, 'day').format('YYYY-MM-DD');
            this.lte = moment(this.lte).add(15, 'day').format('YYYY-MM-DD');
            this.period = `from ${this.gte}`;
          }

        } else if (+añoActualQuincena > +añoperiodoQuincena) {
          this.gte = moment(this.gte).add(15, 'day').format('YYYY-MM-DD');
          this.lte = moment(this.lte).add(15, 'day').format('YYYY-MM-DD');
          this.period = `from ${this.gte}`;
        } 

        break;
      case 2:
        let añoActualmensual = moment().format('YYYY');
        let mesActualmensual = moment().format('MM');
        let añoperiodomensual = moment(this.month).add(1, 'month').format('YYYY');
        let mesperiodomensual = moment(this.month).add(1, 'month').format('MM');
                                

        if (+añoActualmensual === +añoperiodomensual) {
          if (+mesActualmensual === +mesperiodomensual) {
              this.month = moment(this.month).add(1, 'month').format('YYYY-MM-DD');
              this.period = moment(this.month).format('MMM - YYYY');
              this.startOfMonth = moment(this.month).startOf('month').format('YYYY-MM-DD');
              this.endOfMonth = moment(this.month).endOf('month').format('YYYY-MM-DD');
              this.gte = this.startOfMonth;
              this.lte = this.endOfMonth;  

          } else if (+mesActualmensual > +mesperiodomensual) {
            this.month = moment(this.month).add(1, 'month').format('YYYY-MM-DD');
            this.period = moment(this.month).format('MMM - YYYY');
            this.startOfMonth = moment(this.month).startOf('month').format('YYYY-MM-DD');
            this.endOfMonth = moment(this.month).endOf('month').format('YYYY-MM-DD');
            this.gte = this.startOfMonth;
            this.lte = this.endOfMonth; 
          }

        } else if (+añoActualmensual > +añoperiodomensual) {
          this.month = moment(this.month).add(1, 'month').format('YYYY-MM-DD');
          this.period = moment(this.month).format('MMM - YYYY');
          this.startOfMonth = moment(this.month).startOf('month').format('YYYY-MM-DD');
          this.endOfMonth = moment(this.month).endOf('month').format('YYYY-MM-DD');
          this.gte = this.startOfMonth;
          this.lte = this.endOfMonth; 
        } 
        break;
      case 3:
        let añoActualSemestre = moment().format('YYYY');
        let mesActualSemestre = moment().format('MM');
        let diaActualSemestre = moment().format('DD');
        let añoperiodoSemestre = moment(this.lastDayOfFirstSemester).add(6, 'month').format('YYYY');
        let mesperiodoSemestre = moment(this.lastDayOfFirstSemester).add(6, 'month').format('MM');
        let diaperiodoSemestre = moment(this.lastDayOfFirstSemester).add(6, 'month').format('DD');

        if (+añoActualSemestre === +añoperiodoSemestre) {
          if (+mesActualSemestre === +mesperiodoSemestre) {
            if (+diaActualSemestre < +diaperiodoSemestre ) {
              this.firstDayOfFirstSemester = moment(this.firstDayOfFirstSemester).add(6, 'month').format('YYYY-MM-DD');
              this.lastDayOfFirstSemester = moment(this.lastDayOfFirstSemester).add(6, 'month').format('YYYY-MM-DD');
                this.gte = this.firstDayOfFirstSemester;
                this.lte = this.lastDayOfFirstSemester;
                this.year = moment(this.firstDayOfFirstSemester).format('YYYY-MM-DD');
                this.setSemiannualPeriod();  
            }

          } else if (+mesActualSemestre < +mesperiodoSemestre || +mesperiodoSemestre === 6) {
            this.firstDayOfFirstSemester = moment(this.firstDayOfFirstSemester).add(6, 'month').format('YYYY-MM-DD');
            this.lastDayOfFirstSemester = moment(this.lastDayOfFirstSemester).add(6, 'month').format('YYYY-MM-DD');
              this.gte = this.firstDayOfFirstSemester;
              this.lte = this.lastDayOfFirstSemester;
              this.year = moment(this.firstDayOfFirstSemester).format('YYYY-MM-DD');
              this.setSemiannualPeriod();  
          }

        } else if (+añoActualSemestre > +añoperiodoSemestre) {
          this.firstDayOfFirstSemester = moment(this.firstDayOfFirstSemester).add(6, 'month').format('YYYY-MM-DD');
          this.lastDayOfFirstSemester = moment(this.lastDayOfFirstSemester).add(6, 'month').format('YYYY-MM-DD');
            this.gte = this.firstDayOfFirstSemester;
            this.lte = this.lastDayOfFirstSemester;
            this.year = moment(this.firstDayOfFirstSemester).format('YYYY-MM-DD');
            this.setSemiannualPeriod();  
        }      
        
        break;
      case 4:
        let establecida = +moment(this.year).format('YYYY');
        let actual = +moment().format('YYYY');

        if (establecida < actual) { // condicionamos el año para que sea solo igual o menor
        this.year = moment(this.year).add(1, 'year').format('YYYY-MM-DD');
        this.period = moment(this.year).format('YYYY');
        this.startOfYear = moment(this.year).startOf('year').format('YYYY-MM-DD');
        this.endOfYear = moment(this.year).endOf('year').format('YYYY-MM-DD');
        this.gte = this.startOfYear;
        this.lte = this.endOfYear;
        }
        break;
      default:
        this.period = null;
    }
    this.chartType === CHART_TYPE.TRANSACTION ? this.getTransactionReceived() : this.getUserStats();
  }

}
