import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-drowbox-calendar',
  templateUrl: './drowbox-calendar.component.html',
  styleUrls: ['./drowbox-calendar.component.scss']
})
export class DrowboxCalendarComponent implements OnInit {

  constructor(    private translate: TranslateService) {
    this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
  }
  @Output() dateBirth = new EventEmitter<Date>();
  public day: string;
  public month: string;
  public year: string;
  public yearArray = [];
  ngOnInit() {
    this.setYear();
  }

  setYear(){
    var d = new Date();
    var n = d.getFullYear();
    // var select = document.getElementById("ano");
    for(var i = n; i >= 1930; i--) {
      this.yearArray.push(i);
        // var opc = document.createElement("option");
        // opc.text = i;
        // opc.value = i;
        // select.add(opc)
    }
  }

  getDayBirth() {
    const dateString = this.day + "/" + this.month + "/" + this.year
    const date = new Date(dateString);
    this.dateBirth.emit(date);
  }
}
