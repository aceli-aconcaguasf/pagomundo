import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrowboxCalendarComponent } from './drowbox-calendar.component';

describe('DrowboxCalendarComponent', () => {
  let component: DrowboxCalendarComponent;
  let fixture: ComponentFixture<DrowboxCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrowboxCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrowboxCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
