import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrowBoxComponent } from './drow-box.component';

describe('DrowBoxComponent', () => {
  let component: DrowBoxComponent;
  let fixture: ComponentFixture<DrowBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrowBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrowBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
