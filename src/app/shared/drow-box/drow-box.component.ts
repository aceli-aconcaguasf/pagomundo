import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DROWBOX_TYPE } from 'src/app/config/enums';
import { Subject } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { RequestService } from 'src/app/services/request.service';
@Component({
  selector: 'app-drow-box',
  templateUrl: './drow-box.component.html',
  styleUrls: ['./drow-box.component.scss']
})
export class DrowBoxComponent implements OnInit {
  // CITY
  @Input() changeCountry: Subject<any>;
  @Input() readonly: boolean;
  @Input() invalidInput: boolean;
  @Input() noElementSelect: boolean;
  @Input() inputForm: FormGroup;
  @Input() inputFormControlName: string;
  @Input() countryId: number;
  @Input() type: string;
  @Output() elementChange = new EventEmitter<any>();
  typeAutocomplete = DROWBOX_TYPE;
  data = [];
  valueDrawBox = null;
  newCityName = null;
  newCityInput = false;
  constructor(public requestService: RequestService) { }
  get drowbox() { return this.inputForm.get(this.inputFormControlName); }
  ngOnInit(): void {    
    
    this.getCities();
    if (this.drowbox.value && this.drowbox.value.id) {
      this.drowbox.setValue(this.drowbox.value.id);
      }

    if (this.drowbox.value) {
      this.valueDrawBox = this.drowbox.value;
    }

    if (this.changeCountry) {
      this.changeCountry.subscribe(event => {
        this.countryId = event;
        if (this.type === DROWBOX_TYPE.CITY) {
          this.getCities();
        }
      });
    }
  }

  getCities() {
    const url = '/api/cities/bycountry?idCountry='+this.countryId;
    this.requestService.getRequest(url).subscribe(req => {
      const request: any = req;
      this.data = request;
    });
  }

  onChangeElement() {
    if (this.drowbox.value.id) {
      this.newCityInput = false;
      this.elementChange.emit(this.drowbox.value);
    } else if (this.drowbox.value === 0){
      this.newCityInput = true;
      this.elementChange.emit({
        id: 0,
        nameNewCity: this.newCityName
      });
    }
  }
}
