import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild, ViewRef} from '@angular/core';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatPaginator, MatSort} from '@angular/material';
import {ExtendedUser} from '../../config/interfaces';
import {DataFinderService, NotificationService, UserSessionService} from 'src/app/services/service.index';
import {TooltipPosition} from '@angular/material/tooltip';
import {ISSUES_STATUS_NAME, ROLES, STATUS, STATUSES_COLOR} from '../../config/enums';
import {TranslateService} from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnChanges {

  @Input() type: string;
  @Input() subType: string;
  @Input() roleOption: string;
  @Input() sessionRole: string;
  @Input() filterQuery: string;
  @Input() isReceiver: boolean;
  @Input() isDashboard: boolean;
  @Input() collapse: boolean;
  @Input() extendedUser: ExtendedUser;

  @Output() public loadNotification = new EventEmitter<any>();
  @Output() public addRecipientToList = new EventEmitter<any>();
  @Output() public addAllRelatedUserRecipient = new EventEmitter<any>();
  @Output() public callAllUser = new EventEmitter<boolean>();
  @Output() public showMessage = new EventEmitter<any>();
  @Output() public showStatusChangeModal = new EventEmitter<any>();
  @Output() public deleteMessageNotification = new EventEmitter<any>();

  roles = ROLES;
  issueStatusName = ISSUES_STATUS_NAME;
  issueStatusColor = STATUSES_COLOR;
  issueStatus = STATUS;

  uri = '';
  query = '';

  recipientList = [];

  // Table Variable
  totalElements = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  dataTable: any = [];
  pageSize = 10;
  isComponentInit = false;

  positionOptions: TooltipPosition =  'above';

  errorMessage: string;
  keyLanguage$: Observable<any> = this.store.select('language');
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private dataFinder: DataFinderService,
              private notificationService: NotificationService,
              private cdRef: ChangeDetectorRef,
              public userSessionService: UserSessionService,
              private translate: TranslateService,
              private store: Store<language>) {
    this.keyLanguage$.subscribe(item => {
      const language: any = item
      this.translate.use(language.key); 
      let errorMessageTranslate:any = this.translate.get( 'USER.ERROR_GETTING_USERS' );
      let error2MessageTranslate:any = this.translate.get( 'USER.ERROR_GETTING_NOTIFICATION' );
  
      this.errorMessage = this.type === 'USER' ? errorMessageTranslate.value : error2MessageTranslate.value;
      setTimeout(() => {
        this.setColumnNames();
      }, 500);
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.collapse && this.isComponentInit) { return; } // Do not refresh table when expanding filter
    if (changes.roleOption) {
      this.resetTableVariable();
    }
    this.isComponentInit = true;
    this.initTable();
  }

  initTable() {
    this.setColumnNames();
    setTimeout(() => {
      this.loadDataTable();
    }, 0);
  }

  loadDataTable() {
    this.query = this.setUriQuery();
    let mergeMethod;
    let switchMapMethod;

    if (this.isDashboard) {
      mergeMethod = merge(this.sort.sortChange);
      switchMapMethod = switchMap(() => {
        this.isLoadingResults = true;
        return this.dataFinder._search(this.uri, this.query, this.sort.active,
          this.sort.direction, 5);
      });

    } else {

      if (this.type === 'USER') {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        this.paginator.pageIndex = 0;
        mergeMethod = merge(this.sort.sortChange, this.paginator.page);
        switchMapMethod = switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        });

        // NOTIFICATION
      } else {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        this.paginator.pageIndex = 0;
        mergeMethod = merge(this.sort.sortChange, this.paginator.page);
        switchMapMethod = switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._find(this.uri, this.query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex, true);
        });
      }
    }

    mergeMethod.pipe(
        startWith({}),
        switchMapMethod,
        map((data: any) => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.totalElements = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataTable = data;
        if (this.type === 'USER') { this.checkElementSelected(); }
      });

    if (!(this.cdRef as ViewRef).destroyed) {
      this.cdRef.detectChanges();
    }
  }

  setAllCheckboxMark(value: boolean) {
    setTimeout(() => {
      if (this.type === 'USER') {
        (document.getElementById('allCheck') as HTMLInputElement).checked = value;
      }
    }, 0);
  }

  setColumnNames() {

    let dateMessageTranslate:any = this.translate.get( 'REPORTS_MESSAGES.DATE_CREATED' );
    let fromMessageTranslate:any = this.translate.get( 'DATA_TABLE.FROM' );
    let subjectMessageTranslate:any = this.translate.get( 'DATA_TABLE.SUBJECT' );
    let statusMessageTranslate:any = this.translate.get( 'DATA_TABLE.STATUS' );
    let responsibleMessageTranslate:any = this.translate.get( 'DATA_TABLE.RESPONSIBL' );
    let lastUpdateMessageTranslate:any = this.translate.get( 'DATA_TABLE.LAST_UPDATE' );
    let fullNameMessageTranslate:any = this.translate.get( 'REPORTS_MESSAGES.NAME' );
    let loginMessageTranslate:any = this.translate.get( 'TEXT.LOGIN_NAME' );
    let emailMessageTranslate:any = this.translate.get( 'TEXT.EMAIL' );

    switch (this.type) {
      case 'NOTIFICATION':
        switch (this.subType) {
          case 'MESSAGE':
            if (this.isReceiver) {
              this.columnNames = {
                dateCreated: dateMessageTranslate.value, from: fromMessageTranslate.value, subject: subjectMessageTranslate.value, action: ''
              };
            } else {
              this.columnNames = {
                dateCreated: dateMessageTranslate.value, subject: subjectMessageTranslate.value, action: ''
              };
            }
            break;
          case 'ISSUE':
            if (this.isReceiver) {
              if (this.isDashboard) {
                this.columnNames = {
                  lastUpdated: lastUpdateMessageTranslate.value, from: fromMessageTranslate.value, subject: subjectMessageTranslate.value,
                  status: statusMessageTranslate.value,
                };
              } else {
                this.columnNames = {
                  id: 'ID', lastUpdated: lastUpdateMessageTranslate.value, from: fromMessageTranslate.value, subject: subjectMessageTranslate.value,
                  status: statusMessageTranslate.value, responsible: responsibleMessageTranslate.value, action: '',
                };
              }
            } else {
              if (this.isDashboard) {
                this.columnNames = {
                  lastUpdated: lastUpdateMessageTranslate.value, subject: subjectMessageTranslate.value, status: statusMessageTranslate.value, responsible: responsibleMessageTranslate.value,
                };
              } else {
                this.columnNames = {
                  id: 'ID', lastUpdated: lastUpdateMessageTranslate.value, subject: subjectMessageTranslate.value, status: statusMessageTranslate.value, responsible: responsibleMessageTranslate.value,
                };
              }
            }
            break;
          default:
            break;
        }
        break;
      case 'USER':
        if ((this.userSessionService.isSuperOrAdmin() || this.userSessionService.isReSeller()) &&
          (this.roleOption === ROLES.RESELLER || this.roleOption === ROLES.MERCHANT || this.roleOption === ROLES.PAYEE)) {
          this.columnNames = {
            check: '', fullName: fullNameMessageTranslate.value,  loginName: loginMessageTranslate.value + ' | ' + emailMessageTranslate.value, action: ''
          };
        } else {
          this.columnNames = {
            check: '', fullName: fullNameMessageTranslate.value,  loginName: loginMessageTranslate.value + ' | ' + emailMessageTranslate.value
          };
        }

        break;
      default:
        break;
    }
    this.columns = Object.keys(this.columnNames);
  }

  setUriQuery(): string {
    let query = '';

    // console.log("**** TYPE:" + this.type);
    // console.log("**** subType:" + this.subType);
    // console.log("**** isReceiver:" + this.isReceiver);
    // console.log("**** isDashboard:" + this.isDashboard);
    // console.log("**** sessionRole:" + this.sessionRole);

    switch (this.type) {
      case 'NOTIFICATION':
        this.uri = '/api/_find/notification-receivers?';
        switch (this.subType) {
          case 'MESSAGE':
            query = '(status:CREATED OR status:IN_PROCESS OR status:ACCEPTED) AND (notification.subCategory:MESSAGE OR ' +
              'notification.subCategory:TRANSACTION_REJECTED OR notification.subCategory:TRANSACTION_ACCEPTED OR ' +
              'notification.subCategory:TRANSACTION_CREATED OR notification.subCategory:EXTENDED_USER_CHANG_PAYMENT_METHOD OR ' +
              'notification.subCategory:EXTENDED_USER_ACCEPTED OR notification.subCategory:EXTENDED_USER_CREATED OR ' +
              'notification.subCategory:FUND_ACCEPTED OR notification.subCategory:FUND_CREATED OR ' +
              'notification.subCategory:FUND_REJECTED OR notification.subCategory:WITHDRAWAL_ACCEPTED OR ' +
              'notification.subCategory:WITHDRAWAL_CREATED OR notification.subCategory:WITHDRAWAL_REJECTED)';
            if (this.isReceiver) {
              query += ` AND receiver.id:${this.extendedUser.id}`;
              query = `((${query}) OR ` +
                `((status:CREATED OR status:IN_PROCESS OR status:ACCEPTED)` +
                ` AND notification.subCategory:DOWNLOADED_FILE AND sender.id:${this.extendedUser.id}))`;
            } else {
              query += ` AND sender.id:${this.extendedUser.id}`;
            }
            break;
          case 'ISSUE':
            if (this.isDashboard) {
              this.uri = '/api/_search/notification-receivers';
              query = this.userSessionService.isSuperOrAdmin()
                      ? 'notification.status:CREATED AND notification.subCategory:ISSUE_CREATED'
                      : 'notification.subCategory:ISSUE_CREATED';
              query += this.isReceiver ? ` AND receiver.id:${this.extendedUser.id}` : ` AND sender.id:${this.extendedUser.id}`;
            } else {
              query = 'notification.subCategory:ISSUE_CREATED';
              query += this.isReceiver ? ` AND receiver.id:${this.extendedUser.id}` : ` AND sender.id:${this.extendedUser.id}`;
            }
            break;
          default: break;
        }
        break;
      case 'USER':
        switch (this.sessionRole) {
          case ROLES.SUPER_ADMIN:
          case ROLES.ADMIN:
            this.uri = '/api/_search/extended-users';
            query = `role:${this.roleOption} AND (status:${STATUS.ACCEPTED} OR status:${STATUS.REJECTED})`;
            break;
          case ROLES.RESELLER:
            switch (this.roleOption) {
              case ROLES.MERCHANT:
                this.uri = '/api/_search/extended-user-relations';
                query = `userRelated.id:${this.extendedUser.user.id} AND status:${STATUS.ACCEPTED}
                         AND (extendedUser.status:${STATUS.ACCEPTED} OR extendedUser.status:${STATUS.REJECTED})`;
                break;
              case ROLES.PAYEE:
                this.uri = '/api/_search/payees-related-to-reseller';
                query = `extendedUser.id:${this.extendedUser.id}`;
                break;
              default:
                break;
            }
            break;
          case ROLES.MERCHANT:
            this.uri = '/api/_search/extended-user-relations';
            query = `userRelated.id:${this.extendedUser.user.id} AND extendedUser.role:ROLE_PAYEE
                     AND (extendedUser.status:${STATUS.ACCEPTED} OR extendedUser.status:${STATUS.REJECTED})`;
            break;
          case ROLES.PAYEE:
            this.uri = '/api/_search/extended-user-relations';
            query = `userRelated.id:${this.extendedUser.user.id}
                     AND (extendedUser.status:${STATUS.ACCEPTED} OR extendedUser.status:${STATUS.REJECTED})`;
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
    if (this.filterQuery) {
      query += ` AND ${this.filterQuery}`;
    }
    return query;
  }

  sortTable(): string {
    let value;
    if (this.type === 'USER') {
      if (this.userSessionService.isSuperOrAdmin()) {
        value = 'lastUpdated';
      } else if (this.userSessionService.isReSeller()) {
        if ( this.roleOption === ROLES.MERCHANT) {
          value = 'extendedUser.lastUpdated';
        } else {
          value = 'id';
        }
      } else {
        value = 'extendedUser.lastUpdated';
      }
    } else {
      this.subType === 'ISSUE' ? value = 'notification.lastUpdated' : value = 'dateCreated';
    }
    return value;
  }

  addAllElementFromTable() {
    const selectAll = this.hasCheckTotalElements();
    if (selectAll) { return false; }

    if ((document.getElementById('allCheck') as HTMLInputElement).checked) {
      this.addAllElements();
    } else {
      this.removeAllElements();
    }
    this.addRecipientToList.emit(this.recipientList);
  }

  addAllElements() {
    this.dataTable.forEach(elem => {
      const id  = this.userSessionService.isSuperOrAdmin()
        ? elem.id
        : (this.userSessionService.isReSeller()
          ? (this.roleOption === ROLES.MERCHANT ? elem.extendedUser.id : elem.id)
          : elem.extendedUser.id);
      let exist = false;
      for (const recipient of this.recipientList) {
        if (id === recipient.id) { exist = true; }
      }
      if (!exist) {
        const recipient = this.userSessionService.isSuperOrAdmin()
          ? elem
          : (this.userSessionService.isReSeller()
            ? (this.roleOption === ROLES.MERCHANT ? elem.extendedUser : elem)
            : elem.extendedUser);
        this.recipientList.push(recipient);
        (document.getElementById(id) as HTMLInputElement).checked = true;
      }
    });
  }

  removeAllElements() {
    this.dataTable.forEach(elem => {
      const id  = this.userSessionService.isSuperOrAdmin()
        ? elem.id
        : (this.userSessionService.isReSeller()
          ? (this.roleOption === ROLES.MERCHANT ? elem.extendedUser.id : elem.id)
          : elem.extendedUser.id);
      for (const recipient of this.recipientList) {
        if (id === recipient.id) {
          const index = this.recipientList.indexOf(recipient);
          this.recipientList.splice(index, 1);
          (document.getElementById(id) as HTMLInputElement).checked = false;
        }
      }
    });
  }

  hasCheckTotalElements(): boolean {
    if ((this.dataTable.length === this.totalElements) && !this.filterQuery) {
      if ((document.getElementById('allCheck') as HTMLInputElement).checked) {
        this.addAllElements();
        this.callAllUser.emit(true);
        this.dataTable.forEach(elem => {
          const id  = this.userSessionService.isSuperOrAdmin()
            ? elem.id
            : (this.userSessionService.isReSeller()
              ? (this.roleOption === ROLES.MERCHANT ? elem.extendedUser.id : elem.id)
              : elem.extendedUser.id);
          (document.getElementById(id) as HTMLInputElement).checked = true;
        });
      } else {
        this.callAllUser.emit(false);
        this.removeAllElements();
        this.unCheckAllElementTable();
      }
      return true;
    } else if ((this.recipientList.length === this.totalElements) && !this.filterQuery) {
      this.callAllUser.emit(true);
    } else {
      return false;
    }
  }

  addCheckElement(recipientUser: any) {
    if ((document.getElementById(recipientUser.id) as HTMLInputElement).checked) {
      this.recipientList.push(recipientUser);
    } else {
      for (const recipient of this.recipientList) {
        if (recipient.id === recipientUser.id) {
          const index = this.recipientList.indexOf(recipient);
          this.recipientList.splice(index, 1);
        }
      }
    }
    (document.getElementById('allCheck') as HTMLInputElement).checked = this.isAllSelected();
    this.addRecipientToList.emit(this.recipientList);
    if (this.recipientList.length === this.totalElements) {
      this.hasCheckTotalElements();
    }
  }

  unCheckElement(recipientUser: any) {
    if (document.getElementById(recipientUser.id)) {
      (document.getElementById(recipientUser.id) as HTMLInputElement).checked = false;
    }
    const index = this.recipientList.indexOf(recipientUser);
    if (index !== -1) {
      this.recipientList.splice(index, 1);
    }
    (document.getElementById('allCheck') as HTMLInputElement).checked = this.isAllSelected();
    this.addRecipientToList.emit(this.recipientList);
  }

  isAllSelected(): boolean {
    if (this.recipientList.length === 0) {
      return false;
    } else {
      let cont = 0;
      for (const recipient of this.recipientList) {
        for (const element of this.dataTable) {
          const id  = this.userSessionService.isSuperOrAdmin()
              ? element.id
              : (this.userSessionService.isReSeller()
                ? (this.roleOption === ROLES.MERCHANT ? element.extendedUser.id : element.id)
                : element.extendedUser.id);
          if (recipient.id === id) {
            cont++;
            break;
          }
        }
      }
      return cont === this.dataTable.length;
    }
  }

  seeNotification(notification) {
    if (this.type === 'NOTIFICATION') {
      this.loadNotification.emit(notification);
    }
  }

  addAllRelatedUser(relatedUser) {
    const userId = relatedUser.extendedUser ? relatedUser.extendedUser.user.id : relatedUser.user.id;
    const userRelated = relatedUser.extendedUser ? relatedUser.extendedUser : relatedUser;
    const uri = '/api/_search/extended-user-relations';
    const query = this.userSessionService.isSuperOrAdmin()
          ? `userRelated.id:${userId} AND status:${STATUS.ACCEPTED} AND extendedUser:*`
          : `userRelated.id:${userId} AND status:${STATUS.ACCEPTED} AND extendedUser.role:${ROLES.PAYEE}`;
    this.dataFinder._search(uri, query, 'extendedUser.lastUpdated' , 'desc').subscribe(data => {
      if (data.totalElements > 0) {
        let userFrom;
        switch (this.roleOption) {
          case ROLES.RESELLER:
            userFrom = ROLES.RESELLER;
            break;

          case ROLES.MERCHANT:
            userFrom = ROLES.MERCHANT;
            break;

          case ROLES.PAYEE:
            userFrom = ROLES.PAYEE;
            break;
        }
        const relatedUserRecipient = {
          relatedUser: userRelated,
          userFrom,
          totalRelatedUser: data.totalElements
        };
        this.addAllRelatedUserRecipient.emit(relatedUserRecipient);
      }
      else {

        let withoutMessageTranslate:any = this.translate.get( 'TEXT.WITHOUT' );

        let text;
        switch (this.roleOption) {
          case ROLES.RESELLER:
            text = 'Reseller ' + withoutMessageTranslate.value + ' merchants';
            break;
          case ROLES.MERCHANT:
            text = 'Merchant ' + withoutMessageTranslate.value + ' payees';
            break;
          case ROLES.PAYEE:
            text = 'Payee ' + withoutMessageTranslate.value + ' merchants';
            break;
        }
        const msg = {
          type: 'error',
          text,
        };
        this.showMessage.emit(msg);
      }
    });
  }

  resetTableVariable() {
    this.recipientList = [];
    if (document.getElementById('allCheck')) {
      (document.getElementById('allCheck') as HTMLInputElement).checked = false;
    }
    this.addRecipientToList.emit(this.recipientList);
  }

  checkElementSelected() {
    let cont = 0;
    for (const tableElement of this.dataTable) {
      const id  = this.userSessionService.isSuperOrAdmin()
        ? tableElement.id
        : (this.userSessionService.isReSeller()
          ? (this.roleOption === ROLES.MERCHANT ? tableElement.extendedUser.id : tableElement.id)
          : tableElement.extendedUser.id);
      for (const recipientElement of this.recipientList) {
        if (id === recipientElement.id) {
          cont++;
          setTimeout(() => {
            (document.getElementById(id) as HTMLInputElement).checked = true;
          },  0);
        }
      }
    }
    cont === 10 ? this.setAllCheckboxMark(true) : this.setAllCheckboxMark(false);
  }

  unCheckAllElementTable() {
    this.setAllCheckboxMark(false);
    this.dataTable.forEach(elem => {
      const id  = this.userSessionService.isSuperOrAdmin()
        ? elem.id
        : (this.userSessionService.isReSeller()
          ? (this.roleOption === ROLES.MERCHANT ? elem.extendedUser.id : elem.id)
          : elem.extendedUser.id);
      (document.getElementById(id) as HTMLInputElement).checked = false;
    });
  }

  clearRecipientList() {
    this.recipientList = [];
    this.unCheckAllElementTable();
  }

  seeStatusChangeModalButtonHandler(notification) {
    this.showStatusChangeModal.emit(notification);
  }

  deleteMessage(event, notification) {
    event.stopPropagation();
    this.deleteMessageNotification.emit(notification);
  }

  setValueForFullNameHandler(row): string {
    let messageTranslate:any = this.translate.get( 'TEXT.PROFILE_NOT_CONFIRMED' );

    switch (this.sessionRole) {
      case ROLES.SUPER_ADMIN:
      case ROLES.ADMIN:
        if (this.roleOption === ROLES.MERCHANT || this.roleOption === ROLES.RESELLER) {
          return row.fullName;
        } else {
          if ( row.user && row.user.payeeAsCompany) {
            return row.company ? row.company : messageTranslate.value;
          } else {
            return row.fullName;
          }
        }

      case ROLES.RESELLER:
        if (this.roleOption === ROLES.MERCHANT) {
          return row.extendedUser && row.extendedUser.fullName ? row.extendedUser.fullName : '';
        } else {
          if (row.user && row.user.payeeAsCompany) {
            return row.company ? row.company : messageTranslate.value;
          } else {
            return row.fullName;
          }
        }

      case ROLES.MERCHANT:
        if (row.extendedUser.user.payeeAsCompany) {
          return row.extendedUser.company ? row.extendedUser.company : messageTranslate.value;
        } else {
          return row.extendedUser.fullName;
        }

      case ROLES.PAYEE:
        return row.extendedUser.fullName;
    }
  }

  setValueForLoginNameHandler(row) {

    switch (this.sessionRole) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        return row.email;

      case ROLES.RESELLER:
        if (this.roleOption === ROLES.MERCHANT) {
          return row.extendedUser && row.extendedUser.email ? row.extendedUser.email : '';
        } else {
          return row.email;
        }

      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        return row.extendedUser.email;

    }

  }

  setValueForCheckboxHandler(row): any {

    switch (this.sessionRole) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        return row;

      case ROLES.RESELLER:
        if (this.roleOption === ROLES.MERCHANT) {
          return row.extendedUser ;
        } else {
          return row;
        }

      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        return row.extendedUser;

    }
  }

  setValueForToolTipHandler(row): string {

    switch (this.sessionRole) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
      case ROLES.RESELLER:
        return row.user && row.user.payeeAsCompany ? 'Business Accounts' : 'Payee';

      case ROLES.MERCHANT:
        return row.extendedUser.user.payeeAsCompany ? 'Business Accounts' : 'Payee';

    }
  }

  setValueForIconTypeHandler(row): string {

    switch (this.sessionRole) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
      case ROLES.RESELLER:
        return row.user && row.user.payeeAsCompany ? 'home' : 'user';

      case ROLES.MERCHANT:
        return row.extendedUser.user.payeeAsCompany ? 'home' : 'user';

    }
  }

  setMatTooltipLabelHandler() {
    switch (this.roleOption) {
      case ROLES.RESELLER:
        return 'All reseller merchants';

      case ROLES.MERCHANT:
        return 'All merchant payees';

      case ROLES.PAYEE:
        return 'All payee merchants';
    }
  }
}