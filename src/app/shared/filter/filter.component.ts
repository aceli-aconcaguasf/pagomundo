import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {map, startWith} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {DataFinderService} from '../../services/data-finder.service';
import {ISSUES_STATUS_NAME} from '../../config/enums';
import {BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import * as moment from 'moment';
import {UserSessionService} from '../../services/user/userSession.service';

@Component({
  selector: 'app-filter-component',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})

export class FilterComponent implements OnChanges {

  @Input() tableFilterType: string;

  // NOTIFICATION tableFilterType variables
  @Input() isReceiver: boolean;
  @Input() isIssue: boolean;

  @Output() public applyFilterToTable = new EventEmitter<string>();
  @Output() public setCollapse = new EventEmitter<boolean>();

  mssgEmpty = false;


  keys = Object.keys;
  issuesStatus = ISSUES_STATUS_NAME;

  collapse = false;
  showApplyFilter = false;

  countryForm: FormGroup;
  countries = [];
  countrySelected: any;
  filteredCountries: Observable<string[]> | any;

  idArrayFilter = [];
  fromArrayFilter = [];
  statusArrayFilter = [];
  responsibleArrayFilter = [];
  countryArrayFilter = [];
  dateEqual = [];
  dateIsBefore = [];
  dateIsAfter = [];
  dateBetween = [];

  filterName = '';
  fromInput = '';
  idInput = '';
  statusInput = '';
  responsibleInput = '';

  maxDateBS = new Date();
  dateBeforeBS = '';
  dateFilter = '';
  dateBefore = '';
  dateAfter = '';
  showDataBetween = false;
  disabledEditAfterDate = true;
  datePickerConfig: Partial<BsDatepickerConfig>;
  changeClass:number;
  constructor(private dataFinder: DataFinderService,
              public userSessionService: UserSessionService) {
    this.initConfigDatePicker();
    this.countryForm = new FormGroup({
      country: new FormControl('', Validators.required),
    });
    this.initCountryFilter();
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  get CountryFilter() { return this.countryForm.get('country'); }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.isIssue || changes.isReceiver) {
      this.resetVariable();
      this.cleanFilter();
      this.setFilterQuery();
    }
  }

  initCountryFilter() {
    this.filteredCountries = this.CountryFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => this._countryFilter(value))
      );
  }

  private _countryFilter(value: string): string[] {
    if (value) {
      if (value.length >= 2) {
        this.loadCountry(value);
      } else {
        this.countries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.countries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadCountry(name: string) {
    this.dataFinder.getCountriesForName(name).subscribe(data => {
      this.countries = data.content;
    });
  }

  displayFnPayeeCountry(country): string {
    return country ? country.name : country;
  }

  onChangeCountry() {
    if (this.CountryFilter.value.id) {
      this.countrySelected = this.CountryFilter.value;
    } else {
      this.countrySelected = null;
    }
  }

  initConfigDatePicker() {
    this.maxDateBS.setDate(this.maxDateBS.getDate());
    this.datePickerConfig = Object.assign({},
      { dateInputFormat: 'YYYY-MM-DD',
        dateInputFormatSend: 'YYYY-MM-DD',
      });
  }

  applyFilter() {
    this.mssgEmpty = false;
    switch (this.filterName) {

      case 'country':
        if (this.countrySelected) {
          let notDuplicated = true;
          for (const country of this.countryArrayFilter) {
            if (country.id === this.countrySelected.id) {
              notDuplicated = false;
            }
          }
          if (notDuplicated) {
            this.countryArrayFilter.push(this.countrySelected);
          }
        } else {
          this.mssgEmpty = true;
          return;
        }
        break;

      case 'id':
        if(this.idInput !== '') {
          const indexId = this.idArrayFilter.indexOf(this.idInput);
          if (indexId === -1) {
            this.idArrayFilter.push(this.idInput);
          }
        } else{
          this.mssgEmpty = true;

        }
        break;

      case 'from':
        if (this.fromInput !== ''){
        const indexName = this.fromArrayFilter.indexOf(this.fromInput);
        if (indexName === -1) {
          this.fromArrayFilter.push(this.fromInput);
        } } else{
          this.mssgEmpty = true;

        }
        break;

      case 'status':
        if (this.statusInput !== '') {
          const indexStatus = this.statusArrayFilter.indexOf(this.statusInput);
          if (indexStatus === -1) {
            this.statusArrayFilter.push(this.statusInput);
          }
        } else{
          this.mssgEmpty = true;

        }
        break;

      case 'responsible':
        const indexResponsible = this.responsibleArrayFilter.indexOf(this.responsibleInput);
        if (indexResponsible === -1) {
          this.responsibleArrayFilter.push(this.responsibleInput);
        }
        break;

      case 'date':
        if(this.dateIsBefore.length === 0) {
          this.mssgEmpty = true;
          return
        }
        switch (this.dateFilter) {
          case 'dateEqual':
            if (this.dateBefore !== '') {
              const indexEqual = this.dateEqual.indexOf(this.dateBefore);
              if (indexEqual === -1) {
                this.dateEqual.push(this.dateBefore);
              }
              this.dateEqual[0] = moment(this.dateEqual[0]).format('YYYY-MM-DD');
            }
            break;

          case 'dateIsBefore':
            if (this.dateBefore !== '' && this.dateIsBefore.length === 0) {
              this.dateIsBefore.push(this.dateBefore);

              this.dateIsBefore[0] = moment(this.dateIsBefore[0]).format('YYYY-MM-DD');
              this.dateBefore = moment(this.dateBefore).format('YYYY-MM-DD');
            }
            break;

          case 'dateIsAfter':
            if (this.dateBefore !== '' && this.dateIsAfter.length === 0) {
              this.dateIsAfter.push(this.dateBefore);

              this.dateIsAfter[0] = moment(this.dateIsAfter[0]).format('YYYY-MM-DD');
            }
            break;

          case 'dateBetween':
            if (this.dateBefore !== '' && this.dateAfter !== '') {

              this.dateBefore = moment(this.dateBefore).format('YYYY-MM-DD');
              this.dateAfter = moment(this.dateAfter).format('YYYY-MM-DD');

              const date = `${this.dateBefore} TO ${this.dateAfter}`;
              const indexBeforeBet = this.dateBetween.indexOf(date);
              if (indexBeforeBet === -1) {
                this.dateBetween.push(date);
              }
            }
            break;
          default:
            break;
        }
        break;

      default:
        break;
    }
    this.setFilterQuery();
    this.resetVariable();
  }

  setFilterQuery() {
    let query = '';

    // COUNTRY
    if (this.countryArrayFilter.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.countryArrayFilter.length === 1) {
        query += `notification.sender.postalCountry.id:${this.countryArrayFilter[0].id}`;
      } else {
        for (let index = 0; index < this.countryArrayFilter.length; index++) {
          if (index === 0) {
            query += `( notification.sender.postalCountry.id:${this.countryArrayFilter[index].id} OR `;
          } else {
            if (this.fromArrayFilter.length - 1 !== index) {
              query += `notification.sender.postalCountry.id:${this.countryArrayFilter[index].id} OR `;
            } else {
              query += `notification.sender.postalCountry.id:${this.countryArrayFilter[index].id} )`;
            }
          }
        }
      }
    }
    // END COUNTRY

    // ID
    if (this.idArrayFilter.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.idArrayFilter.length === 1) {
        query += `notification.id:${this.idArrayFilter[0]}`;
      } else {
        for (let index = 0; index < this.idArrayFilter.length; index++) {
          if (index === 0) {
            query += `( notification.id:${this.idArrayFilter[index]} OR `;
          } else {
            if (this.idArrayFilter.length - 1 !== index) {
              query += `notification.id:${this.idArrayFilter[index]} OR `;
            } else {
              query += `notification.id:${this.idArrayFilter[index]} )`;
            }
          }
        }
      }
    }
    // END ID


    // NAME
    if (this.fromArrayFilter.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.fromArrayFilter.length === 1) {
        query += `notification.sender.fullName:*${this.fromArrayFilter[0]}*`;
      } else {
        for (let index = 0; index < this.fromArrayFilter.length; index++) {
          if (index === 0) {
            query += `( notification.sender.fullName:*${this.fromArrayFilter[index]}* OR `;
          } else {
            if (this.fromArrayFilter.length - 1 !== index) {
              query += `notification.sender.fullName:*${this.fromArrayFilter[index]}* OR `;
            } else {
              query += `notification.sender.fullName:*${this.fromArrayFilter[index]}* )`;
            }
          }
        }
      }
    }
    // END NAME


    // RESPONSIBLE
    if (this.responsibleArrayFilter.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.responsibleArrayFilter.length === 1) {
        query += `notification.responsible.fullName:*${this.responsibleArrayFilter[0]}*`;
      } else {
        for (let index = 0; index < this.responsibleArrayFilter.length; index++) {
          if (index === 0) {
            query += `( notification.responsible.fullName:*${this.responsibleArrayFilter[index]}* OR `;
          } else {
            if (this.responsibleArrayFilter.length - 1 !== index) {
              query += `notification.responsible.fullName:*${this.responsibleArrayFilter[index]}* OR `;
            } else {
              query += `notification.responsible.fullName:*${this.responsibleArrayFilter[index]}* )`;
            }
          }
        }
      }
    }
    // END RESPONSIBLE


    // STATUS
    if (this.statusArrayFilter.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.statusArrayFilter.length === 1) {
        query += `notification.status:${this.statusArrayFilter[0]}`;
      } else {
        for (let index = 0; index < this.statusArrayFilter.length; index++) {
          if (index === 0) {
            query += `( notification.status:${this.statusArrayFilter[index]} OR `;
          } else {
            if (this.statusArrayFilter.length - 1 !== index) {
              query += `notification.status:${this.statusArrayFilter[index]} OR `;
            } else {
              query += `notification.status:${this.statusArrayFilter[index]} )`;
            }
          }
        }
      }
    }
    // END STATUS

    // DATE

    // EQUAL
    if (this.dateEqual.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.dateEqual.length === 1) {
        query += this.isIssue ?
          `notification.lastUpdated:${this.dateEqual[0]}`
          : `notification.dateCreated:${this.dateEqual[0]}`;
      } else {
        for (let index = 0; index < this.dateEqual.length; index++) {
          if (index === 0) {
            query += this.isIssue ?
              `( notification.lastUpdated:${this.dateEqual[index]} OR `
              : `( notification.dateCreated:${this.dateEqual[index]} OR `;
          } else {
            if (this.dateEqual.length - 1 !== index) {
              query += this.isIssue ?
                `notification.lastUpdated:${this.dateEqual[index]} OR `
                : `notification.dateCreated:${this.dateEqual[index]} OR `;
            } else {
              query += this.isIssue ?
                `notification.lastUpdated:${this.dateEqual[index]} )`
                : `notification.dateCreated:${this.dateEqual[index]} )`;
            }
          }
        }
      }
    }

    // BEFORE
    if (this.dateIsBefore.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.dateIsBefore.length === 1) {
        query += this.isIssue ?
          `notification.lastUpdated:[* TO ${this.dateIsBefore[0]}]`
          : `notification.dateCreated:[* TO ${this.dateIsBefore[0]}]`;
      }
    }

    // AFTER
    if (this.dateIsAfter.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.dateIsAfter.length === 1) {
        query += this.isIssue ?
          `notification.lastUpdated:[${this.dateIsAfter[0]} TO *]`
          : `notification.dateCreated:[${this.dateIsAfter[0]} TO *]`;
      }
    }

    // BETWEEN
    if (this.dateBetween.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.dateBetween.length === 1) {
        query += this.isIssue ?
          `notification.lastUpdated:[${this.dateBetween[0]}]`
          : `notification.dateCreated:[${this.dateBetween[0]}]`;
      } else {
        for (let index = 0; index < this.dateBetween.length; index++) {
          if (index === 0) {
            query += this.isIssue ?
              `( notification.lastUpdated:[${this.dateBetween[index]}] OR `
              : `( notification.dateCreated:[${this.dateBetween[index]}] OR `;
          } else {
            if (this.dateBetween.length - 1 !== index) {
              query += this.isIssue ?
                `notification.lastUpdated:[${this.dateBetween[index]}] OR `
                : `notification.dateCreated:[${this.dateBetween[index]}] OR `;
            } else {
              query += this.isIssue ?
                `notification.lastUpdated:[${this.dateBetween[index]}] )`
                : `notification.dateCreated:[${this.dateBetween[index]}] )`;
            }
          }
        }
      }
    }
    // END DATE

    this.applyFilterToTable.emit(query);
  }

  addFilter(event: any) {
    this.filterName = event.target.value;
    this.showApplyFilter = true;
    event.target.selectedIndex = 0;
  }

  resetFilter(typeName: string) {
    switch (typeName) {
      case 'country':
        this.countryArrayFilter = [];
        break;
      case 'id':
        this.idArrayFilter = [];
        break;
      case 'from':
        this.fromArrayFilter = [];
        break;
      case 'status':
        this.statusArrayFilter = [];
        break;
      case 'responsible':
        this.responsibleArrayFilter = [];
        break;
      case 'dateEqual':
        this.dateEqual = [];
        break;
      case 'dateIsBefore':
        this.dateIsBefore = [];
        break;
      case 'dateIsAfter':
        this.dateIsAfter = [];
        break;
      case 'dateBetween':
        this.dateBetween = [];
        break;
      default:
        break;
    }
    this.applyFilter();
  }

  deleteFilter(typeName: string, name: any) {
    switch (typeName) {
      case 'from':
        const indexFrom = this.fromArrayFilter.indexOf(name);
        this.fromArrayFilter.splice(indexFrom, 1);
        break;
      case 'id':
        const indexId = this.idArrayFilter.indexOf(name);
        this.idArrayFilter.splice(indexId, 1);
        break;
      case 'country':
        const indexCountry = this.countryArrayFilter.indexOf(name);
        this.countryArrayFilter.splice(indexCountry, 1);
        break;
      case 'status':
        const indexStatus = this.statusArrayFilter.indexOf(name);
        this.statusArrayFilter.splice(indexStatus, 1);
        break;
      case 'responsible':
        const indexResponsible = this.responsibleArrayFilter.indexOf(name);
        this.responsibleArrayFilter.splice(indexResponsible, 1);
        break;
      case 'dateEqual':
        const indexEqual = this.dateEqual.indexOf(name);
        this.dateEqual.splice(indexEqual, 1);
        break;
      case 'dateIsBefore':
        const indexBefore = this.dateIsBefore.indexOf(name);
        this.dateIsBefore.splice(indexBefore, 1);
        break;
      case 'dateIsAfter':
        const indexAfter = this.dateIsAfter.indexOf(name);
        this.dateIsAfter.splice(indexAfter, 1);
        break;
      case 'dateBetween':
        const indexBetween = this.dateBetween.indexOf(name);
        this.dateBetween.splice(indexBetween, 1);
        break;
      default:
        break;
    }
    this.setFilterQuery();
  }

  cleanFilter() {
    this.idArrayFilter = [];
    this.fromArrayFilter = [];
    this.countryArrayFilter = [];
    this.statusArrayFilter = [];
    this.responsibleArrayFilter = [];
    this.dateEqual = [];
    this.dateIsAfter = [];
    this.dateBetween = [];
    this.dateIsBefore = [];
    this.mssgEmpty = false;
    this.setFilterQuery();
  }

  resetVariable() {
    this.filterName = '';
    this.idInput = '';
    this.fromInput = '';
    this.statusInput = '';
    this.responsibleInput = '';
    this.dateFilter = '';
    this.dateAfter = '';
    this.dateBefore = '';
    this.countrySelected = null;
    this.CountryFilter.reset();
    this.showApplyFilter = false;
    this.showDataBetween = false;
    this.disabledEditAfterDate = true;
  }

  handleBeforeInBetweenDateChange() {
    if (this.dateBefore) {
      this.dateBeforeBS = this.dateBefore;
      this.dateBefore = moment(this.dateBefore).format('YYYY-MM-DD');
      this.disabledEditAfterDate = false;
    } else {
      this.disabledEditAfterDate = true;
    }
  }

  selectDates() {
    this.dateFilter === 'dateBetween' ? this.showDataBetween = true : this.showDataBetween = false;
  }

  handleClickFilterToggle() {
    this.initCountryFilter();
    this.resetVariable();
    this.collapse = !this.collapse;
    this.setCollapse.emit(this.collapse);
    this.mssgEmpty = false;
  }

}
