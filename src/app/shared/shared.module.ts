import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {SideBarComponent, FilterComponent, TableComponent, DrowboxCalendarComponent,
        PieGraphicComponent, LineGraphicComponent, BarGraphicComponent} from './shared.index';
import {AvatarModule} from 'ngx-avatar';
import {DollarSign, FileText, BarChart2, AlertTriangle, Mail, Users, Settings, Monitor} from 'angular-feather/icons';
import {FeatherModule} from 'angular-feather';
import {ChartsModule} from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import {HTTP_INTERCEPTORS, HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

// Select some icons (use an object, not an array)
const icons = {
  DollarSign, FileText, BarChart2, AlertTriangle, Mail, Users, Settings, Monitor,
};


// ANGULAR MATERIAL
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatToolbarModule,
  MatSlideToggleModule,
  MatCheckboxModule,
  MatAutocompleteModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTabsModule,
  MatRadioModule,
  MatTooltipModule
} from '@angular/material';
import {PipeModule} from '../pipes/pipe.module';


@NgModule({
  declarations: [
    SideBarComponent,
    TableComponent,
    PieGraphicComponent,
    LineGraphicComponent,
    DrowboxCalendarComponent,
    BarGraphicComponent,
    FilterComponent
  ],
  imports: [
    FormsModule,
    AvatarModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatStepperModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatPaginatorModule,
    MatRippleModule,
    MatCardModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatTabsModule,
    AvatarModule,
    MatTooltipModule,
    MatTabsModule,
    MatRadioModule,
    FeatherModule.pick(icons),
    BrowserModule,
    FormsModule,
    ChartsModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    PipeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    })
  ],
  exports: [
    SideBarComponent,
    TableComponent,
    FilterComponent,
    PieGraphicComponent,
    LineGraphicComponent,
    BarGraphicComponent,
    DrowboxCalendarComponent,
    FormsModule,
    AvatarModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatStepperModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatPaginatorModule,
    MatRippleModule,
    MatCardModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatTooltipModule,
    MatRadioModule,
    MatTabsModule,
  ]
})

export class SharedModule {
}
