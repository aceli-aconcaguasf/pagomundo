import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClient} from '@angular/common/http';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

// REDUCER
import { reducer } from './app.reducer'

// ROUTES
import {AppRoutingModule} from './app-routing.module';

// MODULES
import {ServiceModule} from './services/service.module';
import {PagesModule} from './pages/pages.module';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { StoreModule } from "@ngrx/store";

// PAGES
import {PagesComponent} from './pages/pages.component';
import {LoginComponent} from './login/login.component';
import {LandingComponent} from './landing/landing.component';

// INTERCEPTOR
import {ErrorHandleInterceptor, LoaderInterceptor} from './services/interceptor/interceptor.index';
import {LoaderService, LoginGuard} from './services/service.index';

// ICONS
import {FeatherModule} from 'angular-feather';
import {DownloadCloud, UploadCloud, Filter, User,
        Key, Mail, Save, Edit, ChevronDown, ChevronUp,
        File, Menu, RefreshCcw, FilePlus, Sidebar,
        UserPlus, Users, UserCheck, List, FileText, CheckCircle,
        HelpCircle, MinusCircle, PlusCircle, Trash2, Bell,
        XSquare, ExternalLink, CheckSquare, PlusSquare,
        Calendar, ChevronLeft, ChevronRight, CreditCard,
        Home, UserX, Download, ChevronsDown, ChevronsUp} from 'angular-feather/icons';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// Select some icons (use an object, not an array)
const icons = {
  DownloadCloud, UploadCloud, Filter, User, Key, Mail, Save, Edit, Menu, ChevronDown,
  ChevronUp, RefreshCcw, FilePlus, File, Sidebar, UserPlus, Users, UserCheck, List, FileText, Bell,
  CheckCircle, HelpCircle, MinusCircle, PlusCircle, Trash2, XSquare, ExternalLink, CheckSquare,
  PlusSquare, Calendar, ChevronLeft, ChevronRight, CreditCard, Home, UserX, Download, ChevronsDown, ChevronsUp
};


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LoginComponent,
    PagesComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    PagesModule,
    ServiceModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FeatherModule.pick(icons),
    MatPasswordStrengthModule.forRoot(),
    BsDropdownModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    }),
    StoreModule.forRoot({
      language : reducer
    }),
  ],
  providers: [
    LoginGuard,
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorHandleInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
