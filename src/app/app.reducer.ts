import { Action } from "@ngrx/store";
import * as getLanguage from './app.action';
export interface language {
    language: string
}

export const initialState = {
    language: 'en'
}

export function reducer(state: language = initialState, action: getLanguage.LanguageKey) {
    // console.log('ACTION: ', action);
    // console.log('STATE: ', state);
    switch (action.type) {
        case 'KEY':
            return {
                ...state,
                key: action.payload

            }
        default:
            return {
                ...state,
                key: 'en'

            }
    }
}