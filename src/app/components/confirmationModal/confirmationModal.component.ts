import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface ConfirmationData {
  title: string;
}

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmationModal.component.html',
  styleUrls: ['./confirmationModal.component.scss']
})
export class ConfirmationModalComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmationModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmationData) {
  }

  rejectClick() {
    this.dialogRef.close(false);
  }
  acceptClick() {
    this.dialogRef.close(true);
  }

}
