import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ExtendedUser} from '../../config/interfaces';
import {ROLES} from '../../config/enums';
import { TranslateService } from '@ngx-translate/core';

export interface DialogData {
  extendedUser: ExtendedUser;
  reasonType: string;
}

export enum roles_name {
  ROLE_MERCHANT = 'merchant',
  ROLE_PAYEE = 'payee',
}

@Component({
  selector: 'app-note-modal',
  templateUrl: './userModal.component.html',
  styleUrls: ['./userModal.component.scss']
})
export class UserModalComponent {
  roles = ROLES;
  roles_name = roles_name;
  cardNumberError = false;
  cardNumberLettersError = false;
  seeSetCardNumber = false;
  titleTrauctor = "";
  constructor(public dialogRef: MatDialogRef<UserModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private translate: TranslateService) {
    this.data.extendedUser.statusReason = '';
    this.getTraductorTitle(this.data.reasonType);
    if (((this.data.reasonType === 'Enable' && !this.data.extendedUser.useDirectPayment)
      || this.data.reasonType === 'Activate') && this.data.extendedUser.role === ROLES.PAYEE && !this.data.extendedUser.cardNumber) {
      this.data.extendedUser.cardNumber = '';
      this.seeSetCardNumber = true;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  save() {
    if (this.seeSetCardNumber) {
      const cardNumber = this.data.extendedUser.cardNumber.replace(/-/g, '');
      if (cardNumber.length !== 16) {
        this.cardNumberError = true;
        return false;
      } else if (!cardNumber.match('^[0-9]+$')) {
        this.cardNumberLettersError = true;
        return false;
      }
      this.data.extendedUser.cardNumber = cardNumber;
      this.dialogRef.close(this.data.extendedUser);
    } else {
      this.dialogRef.close(this.data.extendedUser);
    }
  }


  cardNumberChangeHandler() {
    this.cardNumberError = false;
    this.cardNumberLettersError = false;
  }

  getTraductorTitle(reasonType:any): string {
    let traductor = '';
    let translete:any;
    switch(reasonType){
      case 'Disable':
        translete = this.translate.get( 'STATUS.DISABLED' );
        this.titleTrauctor = translete.value;
        break;
      case 'Delete':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.DELETE' );
        this.titleTrauctor = translete.value;
      break;
      case 'Activate':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.ACTIVATE' );
        this.titleTrauctor = translete.value;
      break;
      case 'Enable':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.ENABLE' );
        this.titleTrauctor = translete.value;
      break;
    }
    return traductor;
  }
}
