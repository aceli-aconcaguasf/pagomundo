import {Component, Inject, Input} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


export interface DialogData {
  notification: any;
  reasonType: string;
}

@Component({
  selector: 'app-issue-reason-modal',
  templateUrl: './issueStatusReasonModal.component.html',
  styleUrls: ['./issueStatusReasonModal.component.scss']
})
export class IssueStatusReasonModalComponent {
  constructor(public dialogRef: MatDialogRef<IssueStatusReasonModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.data.notification.statusReason = '';
    }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  updateNotification() {
    this.dialogRef.close(true);
  }
}
