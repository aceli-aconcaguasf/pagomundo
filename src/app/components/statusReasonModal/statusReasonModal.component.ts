import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  entity: any;
  title: string;
  reasonType: string;
}

@Component({
  selector: 'app-status-reason-modal',
  templateUrl: './statusReasonModal.component.html',
  styleUrls: ['./statusReasonModal.component.scss']
})
export class StatusReasonModalComponent {

  constructor(public dialogRef: MatDialogRef<StatusReasonModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.data.entity.statusReason = '';
    }

  onNoClick(): void {
    const result = {
      value: false,
    };
    this.dialogRef.close(result);
  }

  save() {
    const result = {
      value: true,
      entity: this.data.entity,
    };
    this.dialogRef.close(result);
  }

}
