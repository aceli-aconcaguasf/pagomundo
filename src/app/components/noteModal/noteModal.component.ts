import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

export interface DialogData {
  paymentId: number;
  note: string;
  paymentStatusReason: string;
  type: string;
  reasonType: string;
  notificationIssue: any;
  fundReason: string;
  withdrawalReason: string;
}

@Component({
  selector: 'app-note-modal',
  templateUrl: './noteModal.component.html',
  styleUrls: ['./noteModal.component.scss']
})
export class NoteModalComponent {
  titleTrauctor = "";
  changeClass:number;
  reasonTypeTraductor:string = "";
  length: string = '250';
  lengthReal: number;
  constructor(
    public dialogRef: MatDialogRef<NoteModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private translate: TranslateService) {
      this.data.paymentStatusReason = '';
      this.data.fundReason = '';
      this.data.withdrawalReason = '';
      let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
      this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
      if(!!this.data.reasonType) {
        this.getTraductorTitle(this.data.reasonType);
      }
    }

    actualiza(){
      this.lengthReal = this.data.note.length;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  updateNotificationNote() {
    this.dialogRef.close(this.data.note);
  }

  cancelRequestMoneyMovement() {
    const result = {
      value: false,
    };
    this.dialogRef.close(result);
  }

  saveRequestFund() {
    const result = {
      value: true,
      reason: this.data.fundReason
    };
    this.dialogRef.close(result);
  }

  saveRequestWithdrawal() {
    const result = {
      value: true,
      reason: this.data.withdrawalReason
    };
    this.dialogRef.close(result);
  }

  getTraductorTitle(reasonType:any): string {
    let traductor = '';
    let translete:any;
    console.log(reasonType)
    switch(reasonType){
      case 'Reverse':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.REVERT' );
        break;
      case 'Cancel':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.CANCEL' );
        break;
      case 'Disable':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.DISABLED' );
        break;
      case 'Reject':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.REJECT' );
      break;
      case 'Accept':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.ACCEPT' );
      break;
      case 'Enable':
        translete = this.translate.get( 'BUTTON_OPTION_INFO.ENABLE' );
      break;
    }
    this.titleTrauctor = translete.value;
    this.reasonTypeTraductor = this.titleTrauctor;
    return traductor;
  }
}
