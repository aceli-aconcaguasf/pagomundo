import { AfterViewInit, ChangeDetectorRef, Component, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TooltipPosition } from '@angular/material/tooltip';
import { MatPaginator, MatSort } from '@angular/material';
import { ExtendedUser, User } from '../../config/interfaces';
import { DataFinderService } from '../../services/data-finder.service';
import { UserService } from '../../services/user/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserSessionService } from '../../services/service.index';
import { ROLES, STATUS, STATUSES_COLOR, STATUSES_USER } from '../../config/enums';
import { StatusReasonModalComponent } from '../statusReasonModal/statusReasonModal.component';
import { MessageNotificationComponent } from '../messageNotification/messageNotification.component';
import { ProfileModalComponent } from '../profileModal/profileModal.component';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';

export interface DialogData {
  extendedUserId: number;
  extendedUserRole: string;
}

@Component({
  selector: 'app-related-modal',
  templateUrl: './relatedUserModal.component.html',
  styleUrls: ['./relatedUserModal.component.scss']
})
export class RelatedUserModalComponent implements AfterViewInit {

  keys = Object.keys;
  uri = '/api/_search/extended-user-relations';
  query = '';
  user: User;
  extendedUser: ExtendedUser;
  roles = ROLES;
  extendedUserRole: string;

  positionOptions: TooltipPosition = 'above';

  statusId = '';
  statuses = STATUSES_USER;
  statusesColor = STATUSES_COLOR;

  nameFilter = '';
  nameInput = '';
  idNumberInput = '';
  emailInput = '';
  filterName = '';

  userName = [];
  nameStatus = [];
  idNumberArr = [];
  emailArr = [];
  countryFilter = [];
  countries = [];
  countrySelected: any;
  filteredPayeeCountries: Observable<string[]> | any;
  countryForm: FormGroup;

  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;
  showApplyFilter = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  tableData: any = [];
  pageSize = 10;

  firstInit = true;
  collapse = false;
  changeClass: number;
  errorMessage: any;
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  keyLanguage$: Observable<any> = this.store.select('language');
  constructor(public dialogRef: MatDialogRef<RelatedUserModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public userService: UserService,
    public userSessionService: UserSessionService,
    public dialog: MatDialog,
    private dataFinder: DataFinderService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private translate: TranslateService,
    private store: Store<language>) {
    this.extendedUserRole = this.data.extendedUserRole;
    this.countryForm = new FormGroup({
      country: new FormControl('', Validators.required),
    });
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss");
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
    this.keyLanguage$.subscribe(item => {
      const language: any = item;
      this.translate.use(language.key);
      this.errorMessage = this.translate.get('ERROR_MESSAGES.ERROR_GETTING_PAYMENTS');
      this.defineColumnsTable();
    });
  }

  get CountryFilter() { return this.countryForm.get('country'); }

  ngAfterViewInit() {
    this.defineColumnsTable();
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    if (this.firstInit) {
      const extendedUserId = this.data.extendedUserId;
      this.userService.getExtendedUserById(extendedUserId).subscribe(
        extendedUser => {
          this.extendedUser = extendedUser;
          this.user = extendedUser.user;
          this.firstInit = false;
          this.loadQuery();
          this.loadTableData();
        }
      );
    } else {
      this.loadQuery();
      this.loadTableData();
    }
    this.cdRef.detectChanges();
  }

  loadQuery() {
    switch (this.extendedUserRole) {
      case ROLES.RESELLER:
      case ROLES.PAYEE:
        this.query = `userRelated.id:${this.user.id} AND status:${STATUS.ACCEPTED} AND extendedUser:*`;
        break;
      case ROLES.MERCHANT:
        this.query = `userRelated.id:${this.user.id} AND status:${STATUS.ACCEPTED} AND extendedUser.role:${ROLES.PAYEE}`;
        break;
      default:
        break;
    }
    this.query += this.getFilterQueryString() !== '' ? ` AND ${this.getFilterQueryString()}` : '';
  }

  loadTableData() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.tableData = data;
      });
  }

  initCountryFilter() {
    this.filteredPayeeCountries = this.CountryFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => this._countryFilter(value))
      );
  }

  private _countryFilter(value: string): string[] {
    if (value) {
      if (value.length >= 2) {
        this.loadCountry(value);
      } else {
        this.countries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.countries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadCountry(name: string) {
    if (this.extendedUserRole === ROLES.MERCHANT) {
      this.dataFinder.getCountriesForPayeeAndName(name).subscribe(data => {
        this.countries = data.content;
      });
    } else if (this.extendedUserRole === ROLES.PAYEE) {
      this.dataFinder.getCountriesForName(name).subscribe(data => {
        this.countries = data.content;
      }
      );
    }
  }

  displayFnPayeeCountry(country): string {
    return country ? country.name : country;
  }

  onChangeCountry() {
    if (this.CountryFilter.value.id) {
      this.countrySelected = this.CountryFilter.value;
    } else {
      this.countrySelected = null;
    }
  }

  defineColumnsTable() {
    let name: any = this.translate.get('REPORTS_MESSAGES.NAME');
    let loginName: any = this.translate.get('REPORTS_MESSAGES.EMAIL');
    let country: any = this.translate.get('REPORTS_MESSAGES.COUNTRY');
    let status: any = this.translate.get('REPORTS_MESSAGES.COUNTRY');
    let mobileNumber: any = this.translate.get('TEXT.MOBILE_NUMBER');
    if (this.userSessionService.isSuperAdmin() && this.extendedUserRole === ROLES.RESELLER) {
      this.columnNames = {
        fullName: name.value, id: 'ID', loginName: loginName.value, country: country.value,
        status: status.value, mobileNumber: mobileNumber.value, action: ''
      };
    } else {
      this.columnNames = {
        fullName: name.value, id: 'ID', loginName: loginName.value,
        country: country.value, status: status.value, mobileNumber: mobileNumber.value
      };
    }

    this.columns = Object.keys(this.columnNames);
  }

  addFilter(event: any) {
    this.filterName = event.target.value;
    this.showApplyFilter = true;
    event.target.selectedIndex = 0;
  }

  handleClickFilterToggle() {
    this.initCountryFilter();
    this.collapse = !this.collapse;
    this.resetVariable();
  }

  resetFilter(typeName: string) {
    switch (typeName) {
      case 'country':
        this.countryFilter = [];
        break;
      case 'payee':
        this.userName = [];
        break;
      case 'status':
        this.nameStatus = [];
        break;
      case 'idNumber':
        this.idNumberArr = [];
        break;
      case 'email':
        this.emailArr = [];
        break;
      default:
        break;
    }
    this.ngAfterViewInit();
  }

  deleteFilter(typeName: string, name: any) {
    switch (typeName) {
      case 'userName':
        const indexPayee = this.userName.indexOf(name);
        this.userName.splice(indexPayee, 1);
        break;
      case 'country':
        const indexCountry = this.countryFilter.indexOf(name);
        this.countryFilter.splice(indexCountry, 1);
        break;
      case 'status':
        const indexStatus = this.nameStatus.indexOf(name);
        this.nameStatus.splice(indexStatus, 1);
        break;
      case 'idNumber':
        const indexIdNumber = this.idNumberArr.indexOf(name);
        this.idNumberArr.splice(indexIdNumber, 1);
        break;
      case 'email':
        const indexEmail = this.emailArr.indexOf(name);
        this.emailArr.splice(indexEmail, 1);
        break;
      default:
        break;
    }
    this.ngAfterViewInit();
  }

  getFilterQueryString(): string {
    let query = '';

    // COUNTRY
    if (this.countryFilter.length > 0) {
      if (this.countryFilter.length === 1) {
        query += `extendedUser.postalCountry.id:${this.countryFilter[0].id}`;
      } else {
        for (let index = 0; index < this.countryFilter.length; index++) {
          if (index === 0) {
            query += `( extendedUser.postalCountry.id:${this.countryFilter[index].id} OR `;
          } else {
            if (this.countryFilter.length - 1 !== index) {
              query += `extendedUser.postalCountry.id:${this.countryFilter[index].id} OR `;
            } else {
              query += `extendedUser.postalCountry.id:${this.countryFilter[index].id} )`;
            }
          }
        }
      }
    }
    // END COUNTRY

    // NAME
    if (this.userName.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.userName.length === 1) {
        query += `extendedUser.fullName:*${this.userName[0]}*`;
      } else {
        for (let index = 0; index < this.userName.length; index++) {
          if (index === 0) {
            query += `( extendedUser.fullName:*${this.userName[index]}* OR `;
          } else {
            if (this.userName.length - 1 !== index) {
              query += `extendedUser.fullName:*${this.userName[index]}* OR `;
            } else {
              query += `extendedUser.fullName:*${this.userName[index]}* )`;
            }
          }
        }
      }
    }
    // END NAME

    // STATUS
    if (this.nameStatus.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.nameStatus.length === 1) {
        query += `extendedUser.status:${this.nameStatus[0]}`;
      } else {
        for (let index = 0; index < this.nameStatus.length; index++) {
          if (index === 0) {
            query += `( extendedUser.status:${this.nameStatus[index]} OR `;
          } else {
            if (this.nameStatus.length - 1 !== index) {
              query += `extendedUser.status:${this.nameStatus[index]} OR `;
            } else {
              query += `extendedUser.status:${this.nameStatus[index]} )`;
            }
          }
        }
      }
    }
    // END STATUS

    // ID NUMBER
    if (this.idNumberArr.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.idNumberArr.length === 1) {
        query += `extendedUser.idNumber:*${this.idNumberArr[0]}*`;
      } else {
        for (let index = 0; index < this.idNumberArr.length; index++) {
          if (index === 0) {
            query += `( extendedUser.idNumber:*${this.idNumberArr[index]}* OR `;
          } else {
            if (this.idNumberArr.length - 1 !== index) {
              query += `extendedUser.idNumber:*${this.idNumberArr[index]}* OR `;
            } else {
              query += `extendedUser.idNumber:*${this.idNumberArr[index]}* )`;
            }
          }
        }
      }
    }
    // END ID NUMBER

    // EMAIL
    if (this.emailArr.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.emailArr.length === 1) {
        query += `extendedUser.email:${this.emailArr[0]}`;
      } else {
        for (let index = 0; index < this.emailArr.length; index++) {
          if (index === 0) {
            query += `( extendedUser.email:${this.emailArr[index]} OR `;
          } else {
            if (this.emailArr.length - 1 !== index) {
              query += `extendedUser.email:${this.emailArr[index]} OR `;
            } else {
              query += `extendedUser.email:${this.emailArr[index]} )`;
            }
          }
        }
      }
    }
    // END EMAIL

    return query;
  }

  applyFilter() {
    switch (this.filterName) {

      case 'country':
        if (this.countrySelected) {
          let notDuplicated = true;
          for (const country of this.countryFilter) {
            if (country.id === this.countrySelected.id) {
              notDuplicated = false;
            }
          }
          if (notDuplicated) {
            this.countryFilter.push(this.countrySelected);
          }
        }
        break;

      case 'status':
        if (this.statusId !== '') {
          const indexStatus = this.nameStatus.indexOf(this.statusId);
          if (indexStatus === -1) {
            this.nameStatus.push(this.statusId);
          }
        }
        break;

      case 'name':
        const indexName = this.userName.indexOf(this.nameInput);
        if (indexName === -1) {
          this.userName.push(this.nameInput);
        }
        break;

      case 'idNumber':
        const indexIdNumber = this.idNumberArr.indexOf(this.idNumberInput);
        if (indexIdNumber === -1) {
          this.idNumberArr.push(this.idNumberInput);
        }
        break;

      case 'email':
        const indexEmail = this.emailArr.indexOf(this.emailInput);
        if (indexEmail === -1) {
          this.emailArr.push(this.emailInput);
        }
        break;

      default:
        break;
    }
    this.ngAfterViewInit();
    this.resetVariable();
  }

  resetVariable() {
    this.filterName = '';
    this.nameFilter = '';
    this.statusId = '';
    this.nameInput = '';
    this.idNumberInput = '';
    this.emailInput = '';
    this.countrySelected = null;
    this.CountryFilter.reset();
    this.showApplyFilter = false;
  }

  cleanFilter() {
    this.userName = [];
    this.nameStatus = [];
    this.countryFilter = [];
    this.idNumberArr = [];
    this.emailArr = [];
    this.ngAfterViewInit();
  }

  closeModal() {
    this.dialogRef.close();
  }

  openRemoveAssociationConfirmationModal(extendedUserRelation) {
    const dialogRef = this.dialog.open(StatusReasonModalComponent, {
      data: {
        entity: extendedUserRelation,
        title: 'Unlink Merchant',
        reasonType: 'unlink Merchant',
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      let successfulAssociationRemove: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SUCCESSFUL_ASSOCIATION');
      if (result.value) {
        const extendedUserRelations = [];
        const obj = {
          id: result.entity.id,
          status: STATUS.CANCELLED,
          statusReason: result.entity.statusReason,
        };
        extendedUserRelations.push(obj);
        this.userService.removeMerchantReSellerAssociation(extendedUserRelations).subscribe(() => {
          this.showMessage(successfulAssociationRemove.value, 'success');
          this.loadTableData();
        });
      }
    });
  }

  openRelatedUserProfile(extendedUser: ExtendedUser): void {
    this.dialog.open(ProfileModalComponent, {
      height: extendedUser.role === ROLES.PAYEE ? '885px' : '800px',
      width: '2000px',
      data: {
        extendedUser,
      }
    });
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  canSeeIconForPayee(row): boolean {
    const role: string = row.extendedUser.role;
    return role === ROLES.PAYEE;
  }

}
