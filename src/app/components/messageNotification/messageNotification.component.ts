import { TranslateService } from '@ngx-translate/core';
import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {UserSessionService} from '../../services/user/userSession.service';
import {STATUS} from '../../config/enums';
import {NotificationService} from '../../services/notification/notification.service';
import {ReportService} from '../../services/report/report.service';

@Component({
  selector: 'app-message-notification',
  templateUrl: './messageNotification.component.html',
  styleUrls: ['./messageNotification.component.scss']
})

export class MessageNotificationComponent implements OnInit {

  isNotification = false;
  notificationReceiverObj: any;
  downloadName = '';
  pathLink = '';

  close = false;
  show = false;
  showTitle = false;
  title: string;
  message: string;

  constructor(public userSessionService: UserSessionService,
              private cdRef: ChangeDetectorRef,
              private notificationService: NotificationService,
              private reportService: ReportService,
              private translate: TranslateService
              ) { }

  ngOnInit() {}

  setTypeMessage(message: any, type: string, showTitle?: boolean, title?: string, time?: number) {
    let fund_accepted:any = this.translate.get( 'TEXT.FUND_ACCEPTED' );
    let pass_successful:any = this.translate.get( 'MESSAGE.PASSWORD_CHANGE_SUCCESFUL' );
    let duration = time ? time : 6000;
    this.isNotification = false;
    this.show = true;
    if (showTitle) {
      this.showTitle = showTitle;
      this.title = title;
    }
    if (typeof message === 'object' && message.static) {
      this.message = message.mss;
      this.close = true;
      duration = 10000000000;
    } else {
      this.message = message;
      if( message === 'Fund accepted') {
        this.message = fund_accepted.value;
      } else if( message === 'Password changed successfully') {
        this.message = pass_successful.value;
      }
    }

    setTimeout(() => {
      switch (type) {
        case 'error':
          document.getElementById('dialog').style.backgroundColor = '#E57373';
          break;
        case 'success':
          document.getElementById('dialog').style.backgroundColor = '#7BB74B';
          break;
        case 'warning':
          document.getElementById('dialog').style.backgroundColor = '#FFBB33';
          break;
        default:
          break;
      }
    }, 100);
    setTimeout(() => {
      this.show = false;
    }, duration);
    this.cdRef.detectChanges();
  }


  downloadReportNotification(notificationReceiver) {
    this.notificationReceiverObj = notificationReceiver;
    this.message = `${notificationReceiver.subject}`;
    const arrayPath = notificationReceiver.notification.description.split('/');
    this.downloadName = arrayPath[2].slice(0, arrayPath[2].indexOf('.xlsx'));
    this.pathLink = `/assets/fileBucket/downloadedStatisticsReport/${arrayPath[2]}`;
    this.isNotification = true;
  }

  onDownloadReportOrCloseMsg() {
    this.isNotification = false;
    this.reportService.setNotificationReportValue(false);
    this.readNotification(this.notificationReceiverObj);
  }

  readNotification(notificationReceiver) {

    notificationReceiver.status = STATUS.ACCEPTED;
    const notificationUpdate = {
      id: notificationReceiver.id,
      status: STATUS.ACCEPTED,
    };
    this.notificationService.updateNotificationReceiverWithoutLoader(notificationUpdate).subscribe();
  }
}
