import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ExtendedUser} from '../../config/interfaces';
import {ROLES} from '../../config/enums';

export interface DialogData {
  extendedUser: ExtendedUser;
  readOnly: boolean;
  role: string;
}

@Component({
  selector: 'app-found-modal',
  templateUrl: './commissionModal.component.html',
  styleUrls: ['./commissionModal.component.scss']
})
export class CommissionModalComponent {

  extendedUser: ExtendedUser;
  readonly: boolean;
  useMerchantCommission: boolean;
  resellerFixedCommission: number;
  resellerPercentageCommission: number;
  bankCommission: number;
  fxCommission: number;
  rechargeCost: number;
  seeCommission: boolean;
  invalidInput = false;
  roles = ROLES;
  changeClass:number;
  constructor(public dialogRef: MatDialogRef<CommissionModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    this.extendedUser = this.data.extendedUser;
    this.readonly = this.data.readOnly;
    if (this.extendedUser.role === ROLES.MERCHANT) {
      this.useMerchantCommission = this.extendedUser.useMerchantCommission;
      this.seeCommission = this.useMerchantCommission;
      this.initMerchantValueCommission();
    } else if (this.extendedUser.role === ROLES.RESELLER) {
      this.loadReSellerValuesCommission();
    }
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  static truncNumber(x, position = 0) {
    const s = x.toString();
    const decimalLength = s.indexOf('.') !== -1 ?
      s.indexOf('.') + 1
      : (s.indexOf(',') !== -1 ?
        s.indexOf(',') + 1 : 0);
    const numStr = s.substr(0, decimalLength + position);
    return Number(numStr);
  }

  initMerchantValueCommission() {
    this.loadMerchantValuesCommission();
    this.seeCommission = this.useMerchantCommission;
  }

  loadMerchantValuesCommission() {
    this.bankCommission = this.extendedUser.bankCommission
    ? (this.extendedUser.bankCommission === 1
      ? this.extendedUser.bankCommission * 100
      : CommissionModalComponent.truncNumber((this.extendedUser.bankCommission * 100), 2))
    : 0;

    this.fxCommission = this.extendedUser.fxCommission
      ? (this.extendedUser.fxCommission === 1
        ? this.extendedUser.fxCommission * 100
        : CommissionModalComponent.truncNumber((this.extendedUser.fxCommission * 100), 2))
      : 0;

    this.rechargeCost = this.extendedUser.rechargeCost ? this.extendedUser.rechargeCost : 0;
  }

  handleClickEditMerchantCommission() {
    this.invalidInput = false;
    if (this.useMerchantCommission) {
      if (this.validateMerchantCommissionsInput()) {
        this.extendedUser.useMerchantCommission = this.useMerchantCommission;
        // @ts-ignore
        this.extendedUser.bankCommission = this.bankCommission > 0 ? this.bankCommission.toFixed(2) / 100 : 0;
        // @ts-ignore
        this.extendedUser.fxCommission = this.fxCommission > 0 ? this.fxCommission.toFixed(2) / 100 : 0;
        this.extendedUser.rechargeCost = this.rechargeCost;
        this.updateCommission();
      } else {
        this.invalidInput = true;
      }
    } else {
      this.extendedUser.useMerchantCommission = this.useMerchantCommission;
      this.updateCommission();
    }
  }

  updateCommission() {
    const result = {
      value: 'UPDATE',
      extendedUser: this.extendedUser
    };
    this.dialogRef.close(result);

  }

  changeUseMerchantCommission(event) {
    this.useMerchantCommission = event.checked;
    this.initMerchantValueCommission();
  }

  validateMerchantCommissionsInput(): boolean {
    let value = true;
    if (this.rechargeCost === null || this.rechargeCost < 0
      || this.bankCommission === null || this.bankCommission < 0
      || this.fxCommission === null || this.fxCommission < 0) {
       value = false;
    }
    return value;
  }

  closeModal(): void {
    const result = {
      value: false,
    };
    this.dialogRef.close(result);
  }

  errorInput(type: any): boolean {
    let value = false;
    switch (type) {
      case 'RESELLER_FIXED_COMMISSION':
        value = this.resellerFixedCommission === null;
        break;
      case 'RESELLER_PERCENTAGE_COMMISSION':
        value = this.resellerPercentageCommission > 100 ||  this.resellerPercentageCommission === null;
        break;
      case 'BANK_COMMISSION':
        value = ((!this.bankCommission && this.bankCommission !== 0) || this.bankCommission < 0) && this.invalidInput;
        break;
      case 'FX_COMMISSION':
        value = ((!this.fxCommission && this.fxCommission !== 0) || this.fxCommission < 0) && this.invalidInput;
        break;
      case 'RECHARGE_COST':
        value = ((!this.rechargeCost && this.rechargeCost !== 0) || this.rechargeCost < 0) && this.invalidInput;
        break;
    }
    return value;
  }

  // RESELLER

  handleClickEditReSellerCommission() {
    if (this.validateResellerCommissionsInput()) {
      this.resellerPercentageCommission = this.resellerPercentageCommission / 100;
      this.extendedUser.resellerPercentageCommission = Number(this.resellerPercentageCommission.toFixed(4));
      this.extendedUser.resellerFixedCommission = this.resellerFixedCommission;
      this.updateCommission();
    }
  }

  loadReSellerValuesCommission() {
    this.resellerFixedCommission = this.extendedUser.resellerFixedCommission
      ? this.extendedUser.resellerFixedCommission
      : 0;

    this.resellerPercentageCommission = this.extendedUser.resellerPercentageCommission
        ? CommissionModalComponent.truncNumber((this.extendedUser.resellerPercentageCommission * 100), 2)
        : 0;
  }

  validateResellerCommissionsInput(): boolean {
    let value = true;
    if (this.resellerFixedCommission === null || this.resellerFixedCommission < 0) {
      value = false;
    }
    if (this.resellerPercentageCommission === null || this.resellerPercentageCommission < 0 || this.resellerPercentageCommission > 100) {
      value = false;
    }
    return value;
  }


  validateKeyDownValueHandler(event) {
    const key = event.which;
    if ( key === 43 || key === 45 || key === 101 ) {
      event.preventDefault();
    }
  }
}
