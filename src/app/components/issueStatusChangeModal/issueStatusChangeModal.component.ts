import {Component, Inject, AfterContentInit, ViewChild, ChangeDetectorRef} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {NotificationService} from '../../services/notification/notification.service';
import {STATUSES_COLOR, ISSUES_STATUS_NAME} from '../../config/enums';
import {MatPaginator, MatSort} from '@angular/material';
import {merge, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {DataFinderService} from '../../services/data-finder.service';

export interface DialogData {
  notification: any;
}

@Component({
  selector: 'app-issue-status-modal',
  templateUrl: './issueStatusChangeModal.component.html',
  styleUrls: ['./issueStatusChangeModal.component.scss']
})
export class IssueStatusChangeModalComponent implements AfterContentInit {

  notification: any;
  statuses = ISSUES_STATUS_NAME;
  issueStatusColor = STATUSES_COLOR;

  // Table Variable
  totalElements = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  dataTable: any = [];
  pageSize = 5;

  errorMessage: 'Error getting status';
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(public dialogRef: MatDialogRef<IssueStatusChangeModalComponent>,
              @Inject(MAT_DIALOG_DATA) public notificationData: DialogData,
              private dataFinder: DataFinderService,
              private cdRef: ChangeDetectorRef) {
    this.notification = notificationData.notification;
  }

  closeModal(): void {
    this.dialogRef.close();
  }

  ngAfterContentInit() {
    setTimeout(() => {
      this.loadTable();
    }, 0);
  }

  loadTable() {
    this.setColumnsName();
    const uri = '/api/_search/notification-status-changes';
    const query = `notification.id:${this.notification.id}`;

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(uri, query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.totalElements = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
      this.dataTable = data;
    });
    this.cdRef.detectChanges();
  }

  setColumnsName() {
    this.columnNames = {
      dateCreated: 'Date', status: 'Status', reason: 'Reason', assigned: 'Assigned'
    };
    this.columns = Object.keys(this.columnNames);
  }

}
