import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserService} from '../../services/user/user.service';

export interface DialogData {
  extendedUserRelation: any;
}

@Component({
  selector: 'app-note-modal',
  templateUrl: './nicknameModal.component.html',
  styleUrls: ['./nicknameModal.component.scss']
})
export class NicknameModalComponent {

  nickname = '';
  showError = false;

  constructor(public dialogRef: MatDialogRef<NicknameModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private userService: UserService) {
    this.nickname = data.extendedUserRelation.nickname;
  }

  closeModal(): void {
    this.dialogRef.close(false);
  }

  saveNickname() {
    this.showError = false;
    const extUserRelation = {
      id: this.data.extendedUserRelation.id,
      status: this.data.extendedUserRelation.status,
      nickname: this.nickname
    };

    this.userService.updateExtendedUserRelationNickname(extUserRelation)
      .subscribe(() => {
        this.dialogRef.close(true);
    }, () => {
        this.showError = true;
    });
  }
}
