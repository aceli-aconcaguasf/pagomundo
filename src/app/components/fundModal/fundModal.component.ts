import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ExtendedUser} from '../../config/interfaces';
import {UserSessionService} from '../../services/user/userSession.service';

enum TYPE_DATA {
  FUND_MERCHANT = 'FUND_MERCHANT', // as admin, fund merchant
  REQUEST_FUND = 'REQUEST_FUND', // as merchant, request fund
  REQUEST_WITHDRAWAL = 'REQUEST_WITHDRAWAL', // as reseller, request withdrawal
}

export interface DialogData {
  extendedUser: ExtendedUser;
  type: string;
}

@Component({
  selector: 'app-found-modal',
  templateUrl: './fundModal.component.html',
  styleUrls: ['./fundModal.component.scss']
})
export class FundModalComponent {

  extendedUser: ExtendedUser;
  typeData = TYPE_DATA;
  newBalance: number;
  newBalanceError = false;
  amount = 0;
  changeClass:any;
  constructor(public dialogRef: MatDialogRef<FundModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              public userSessionService: UserSessionService) {
    this.extendedUser = this.data.extendedUser;
    this.newBalance = this.extendedUser.balance;
    this.data.type === this.typeData.REQUEST_WITHDRAWAL ?  this.amount = 500 : this.amount = 0;
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  closeModal(): void {
    if (this.data.type === this.typeData.REQUEST_FUND || this.data.type === this.typeData.REQUEST_WITHDRAWAL) {
      const obj = {
        value: false,
      };
      this.dialogRef.close(obj);
    } else {
      this.dialogRef.close();
    }
  }

  changeFund(value: number) {
    console.log(this.amount + value);
    if (this.data.type === this.typeData.FUND_MERCHANT) {
      this.amount = this.amount + value;
      this.changeBalance();
    } if (this.amount + value >= 10000) {
      return;
    }  
    else {
      if (this.amount + value >= 0) {
        this.amount = this.amount + value;
      } else {
        this.amount = 0;
      }
    }
  }

  changeWithdrawal(value: number) {
    if (this.data.type === this.typeData.FUND_MERCHANT) {
      this.amount = this.amount + value;
      this.changeBalance();
    } if (this.amount + value >= 10000) {
      return;
    } 
    else {
      if (this.amount + value >= 500) {
        this.amount = this.amount + value;
      }
       else {
        this.amount = 500;
      }
    }
  }

  changeBalance() {
    this.newBalance = this.extendedUser.balance + this.amount;
    this.newBalance < 0 ? this.newBalanceError = true : this.newBalanceError = false;
  }

  setMinimum(): number {
    if (this.data.type === this.typeData.FUND_MERCHANT) {
      return -this.extendedUser.balance;
    } else if (this.data.type === this.typeData.REQUEST_WITHDRAWAL) {
      return 500;
    } else {
      return 0;
    }
  }

  requestFund() {
    const obj = {
      value: true,
      amount: this.amount
    };
    this.dialogRef.close(obj);
  }

  requestWithdrawal() {
    const obj = {
      value: true,
      amount: this.amount
    };
    this.dialogRef.close(obj);
  }

   validacionInput(e: any) {
    if(String(this.amount).length > 6 || this.amount > 1000000) {
      return false;
    }
  }
}
