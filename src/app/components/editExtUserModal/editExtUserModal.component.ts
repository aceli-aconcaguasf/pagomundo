import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageNotificationComponent} from '../messageNotification/messageNotification.component';

export interface ConfirmationData {
  extendedUser: any;
}

@Component({
  selector: 'app-edit-ext-user-modal',
  templateUrl: './editExtUserModal.component.html',
  styleUrls: ['./editExtUserModal.component.scss']
})
export class EditExtUserModalComponent implements OnInit {

  extendedUserForm: FormGroup;

  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor(
    public dialogRef: MatDialogRef<EditExtUserModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmationData) {
  }

  get email() {
    return this.extendedUserForm.get('email');
  }

  ngOnInit(): void {
    this.initExtendedUserForm(this.data.extendedUser);
  }

  initExtendedUserForm(extendedUser) {
    this.extendedUserForm = new FormGroup({
      email: new FormControl(
        extendedUser && extendedUser.login ? extendedUser.login : '', [Validators.required, Validators.email])
    });
  }

  rejectClick() {
    const obj = {
      withValue: false,
    };
    this.dialogRef.close(obj);
  }

  updateEmailClick() {
    if (this.extendedUserForm.valid) {
      const obj = {
        withValue: true,
        newEmail: this.email.value,
      };
      this.dialogRef.close(obj);
    } else {
      this.messageComponent.setTypeMessage('Enter a valid email', 'error');
      return false;
    }
  }
}
