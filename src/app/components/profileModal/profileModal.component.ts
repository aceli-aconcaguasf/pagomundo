import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ExtendedUser, User } from '../../config/interfaces';
import { FormControl } from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { UserSessionService } from '../../services/user/userSession.service';
import { MessageNotificationComponent } from '../messageNotification/messageNotification.component';
import { ROLES, STATUS } from '../../config/enums';
import { PayeeFormComponent, UserFormComponent, BankFormComponent, PayeeCompanyFormComponent, AdminFormComponent } from '../components.index';
import { CardNumberModalComponent } from '../cardNumberModal/cardNumberModal.component';
import { TranslateService } from '@ngx-translate/core';

export interface DialogData {
  extendedUser: ExtendedUser;
}

@Component({
  selector: 'app-profile-modal',
  templateUrl: './profileModal.component.html',
  styleUrls: ['./profileModal.component.scss']
})
export class ProfileModalComponent implements OnInit {

  roles = ROLES;
  status = STATUS;
  profileUser: User;
  profileExtendedUser: any;
  userImg: string;
  userEmail: string;
  loading = false;
  errorMessage: string;

  countryPostalId = 0;
  profileUserRole: string;
  loggedUserRole: string;

  adminFormInvalid = false;
  userFormInvalid = false;
  payeeFormInvalid = false;
  payeeCompanyFormInvalid = false;
  bankFormInvalid = false;

  modifyMode = false;
  cardNumberError = false;
  cardNumberLettersError = false;
  cardNumberValue = '';

  checked = false;
  canChangePaymentMethod: boolean;
  bankPaymentMethod = false;
  selectedIndex = new FormControl(0);
  changeClass: any;
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  @ViewChild('AdminForm', { static: false }) adminFormComponent: AdminFormComponent;
  @ViewChild('UserForm', { static: false }) userFormComponent: UserFormComponent;
  @ViewChild('PayeeForm', { static: false }) payeeFormComponent: PayeeFormComponent;
  @ViewChild('PayeeCompanyForm', { static: false }) payeeCompanyFormComponent: PayeeCompanyFormComponent;
  @ViewChild('BankForm', { static: false }) bankFormComponent: BankFormComponent;
  userBrazilShowBank = [
    'matheusoliveirakuhn@gmail.com',
    'acarolkunst@gmail.com',
    'dressy00@gmail.com',
    'marianapedraza86@gmail.com',
    'thayanarcardoso@gmail.com',
    'eujamesmartinez@gmail.com',
    '1011079p2@ilsparent.com'
  ];

  constructor(public dialogRef: MatDialogRef<ProfileModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public userService: UserService,
    public dialog: MatDialog,
    public userSessionService: UserSessionService,
    private translate: TranslateService) {

    this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
    this.loggedUserRole = this.userSessionService.loadRole();
    this.profileExtendedUser = this.data.extendedUser;
    this.profileUser = this.profileExtendedUser.user;
    this.profileUserRole = this.profileExtendedUser.role;
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss");
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  ngOnInit() {
    this.initUser();
  }

  initUser() {

    this.profileUser.imageUrl ? this.userImg = this.profileUser.imageUrl : this.userImg = null;
    this.userEmail = this.profileUser.email.trim().toLocaleLowerCase();
    if (this.profileUserRole === ROLES.PAYEE) {
      this.countryPostalId = this.profileExtendedUser.postalCountry ? this.profileExtendedUser.postalCountry.id : '';
      this.initBankInfoValues(this.profileExtendedUser);
    }
  }

  // ONLY PAYEE
  initBankInfoValues(extendedUser: ExtendedUser) {
    this.canChangePaymentMethod = extendedUser.canChangePaymentMethod;
    this.bankPaymentMethod = extendedUser.useDirectPayment;
  }

  editProfileButtonHandler() {

    if (this.profileUserRole === ROLES.ADMIN) {
      this.adminFormInvalid = false;
      this.adminFormComponent.enableAdminFormEditing();
    } else if (this.profileUserRole === ROLES.PAYEE) {
      this.bankFormInvalid = false;
      this.bankFormComponent.enableBankFormEditing();

      if (this.profileUser.payeeAsCompany) {
        this.payeeCompanyFormInvalid = false;
        this.payeeCompanyFormComponent.enablePayeeCompanyFormEditing();
      } else {
        this.payeeFormInvalid = false;
        this.payeeFormComponent.enablePayeeFormEditing();
      }

    } else if (this.profileUserRole === ROLES.RESELLER || this.profileUserRole === ROLES.MERCHANT) {
      this.userFormInvalid = false;
      this.userFormComponent.enableUserFormEditing();
    }
    this.modifyMode = true;
  }

  cancelEditProfileButtonHandler() {

    if (this.profileUserRole === ROLES.ADMIN) {
      this.adminFormInvalid = false;
      this.adminFormComponent.initExtendedUser(this.data.extendedUser);

    } else if (this.profileUserRole === ROLES.PAYEE) {
      this.countryPostalId = this.data.extendedUser.postalCountry.id;
      this.bankFormInvalid = false;

      /*
      * countryPostalId is an input variable un payeeFormComponent, payeeCompanyFormComponent and bankFormComponent .
      * If countryPostalId is 0, because the input change and didnt select a country for the list
      * when we cancel editing, it takes milli seconds tu update the value countryPostalId
      * That's why we use setTimeout
      */

      setTimeout(() => {
        if (this.profileUser.payeeAsCompany) {
          this.payeeCompanyFormInvalid = false;
          this.payeeCompanyFormComponent.initExtendedUser(this.data.extendedUser);
        } else {
          this.cardNumberError = false;
          this.cardNumberLettersError = false;
          this.payeeFormInvalid = false;
          this.payeeFormComponent.initExtendedUser(this.data.extendedUser);
        }

        this.initBankInfoValues(this.data.extendedUser);
        this.bankFormComponent.initBankInfo(this.data.extendedUser);

      }, 0);
    } else if (this.profileUserRole === ROLES.RESELLER || this.profileUserRole === ROLES.MERCHANT) {
      this.userFormInvalid = false;
      this.userFormComponent.initExtendedUser(this.data.extendedUser);
    }
    this.disableForm();
  }

  updateHandlerButton() {
    switch (this.profileUserRole) {
      case ROLES.ADMIN:
        this.updateAdmin();
        break;
      case ROLES.RESELLER:
      case ROLES.MERCHANT:
        this.updateUser();
        break;
      case ROLES.PAYEE:
        this.updatePayee();
        break;
    }
  }

  updateAdmin() {
    this.adminFormInvalid = false;
    let messageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_FIELDS');

    if (this.adminFormComponent.adminForm.valid) {

      this.profileExtendedUser.firstName1 = this.adminFormComponent.adminFirstName1.value;
      this.profileExtendedUser.lastName1 = this.adminFormComponent.adminLastName1.value;

      this.updateUserCallMethod(this.profileExtendedUser);

    } else {
      this.adminFormInvalid = true;
      this.showMessage(messageTranslate.value, 'error');
      return false;
    }
  }

  updateUser() {
    this.userFormInvalid = false;
    this.evaluateUserCountry('postal');
    this.evaluateUserCountry('residence');
    let messageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_FIELDS');

    if (this.userFormComponent.userForm.valid) {
      this.profileExtendedUser.company = this.userFormComponent.userCompany.value;
      this.profileExtendedUser.postalAddress =
        `${this.userFormComponent.userPostalAddress.value} | ${this.userFormComponent.userCityPostal.value} | ${this.userFormComponent.usercpPostalAddress.value}`;
      this.profileExtendedUser.postalCountry.id = this.userFormComponent.userCountryPostal.value.id;
      this.profileExtendedUser.residenceAddress =
        `${this.userFormComponent.userResidenceAddress.value} | ${this.userFormComponent.userCityResidence.value} | ${this.userFormComponent.usercpResidenceAddress.value}`;
      this.profileExtendedUser.residenceCountry.id = this.userFormComponent.userCountryResidence.value.id;
      this.profileExtendedUser.firstName1 = this.userFormComponent.userFirstName1.value;
      this.profileExtendedUser.lastName1 = this.userFormComponent.userLastName1.value;
      this.profileExtendedUser.birthDate = new Date(this.userFormComponent.userBirthDate.value);
      this.profileExtendedUser.idNumber = this.userFormComponent.userIdNumber.value;
      this.profileExtendedUser.mobileNumber = this.userFormComponent.userMobileNumber.value;
    } else {
      this.userFormInvalid = true;
      this.showMessage(messageTranslate.value, 'error');
      return false;
    }

    this.updateUserCallMethod(this.profileExtendedUser);
  }

  updatePayee() {
    this.bankFormInvalid = false;
    let messageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_REQUIRED_FIELDS');

    // PAYEE COMPANY
    if (this.profileUser.payeeAsCompany) {
      this.payeeCompanyFormInvalid = false;

      if (this.payeeCompanyFormComponent.noPostalCountrySelected
        || this.payeeCompanyFormComponent.noResidenceCountrySelected || this.payeeCompanyFormComponent.noPostalCitySelected) {
        this.payeeCompanyFormInvalid = true;
        this.showMessage(messageTranslate.value, 'error');
        this.changeTabView(0);
        return false;
      }

      if (this.payeeCompanyFormComponent.payeeCompanyForm.valid) {

        this.profileExtendedUser.lastName1 = this.payeeCompanyFormComponent.legalRepresentativeLastName.value;
        this.profileExtendedUser.firstName1 = this.payeeCompanyFormComponent.legalRepresentativeFirstName.value;
        this.profileExtendedUser.email = this.payeeCompanyFormComponent.legalRepresentativeEmail.value;

        const residenceAddress = this.payeeCompanyFormComponent.legalRepresentativeResidenceAddress.value;
        const residenceCity = this.payeeCompanyFormComponent.legalRepresentativeCityResidence.value;
        const residenceCP = this.payeeCompanyFormComponent.legalCPRepresentativeResidenceAddress.value;

        this.profileExtendedUser.residenceAddress = `${residenceAddress} | ${residenceCity} | ${residenceCP}`;
        this.profileExtendedUser.residenceCountry = {
          id: this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value, name: ''
        };

        this.profileExtendedUser.idNumber = this.payeeCompanyFormComponent.legalRepresentativeIdNumber.value;
        if (this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value.id === 1) { // id 1 = Colombia
          this.profileExtendedUser.idType = { id: 1, name: '' };
        } else if (this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value.id === 2) { // id 2 = Mexico
          this.profileExtendedUser.idType = { id: 15, name: '' };
        }
        this.profileExtendedUser.mobileNumber = this.payeeCompanyFormComponent.legalRepresentativeMobileNumber.value;

        this.profileExtendedUser.company = this.payeeCompanyFormComponent.companyName.value;
        this.profileExtendedUser.phoneNumber = this.payeeCompanyFormComponent.companyPhoneNumber.value;
        this.profileExtendedUser.birthDate = new Date(this.payeeCompanyFormComponent.companyConstitutionDate.value);

        this.profileExtendedUser.taxId = this.payeeCompanyFormComponent.companyTaxId.value;
        this.profileExtendedUser.idTypeTaxId = { id: this.payeeCompanyFormComponent.companyTaxIdType.value, name: '' };

        this.profileExtendedUser.postalAddress = `${this.payeeCompanyFormComponent.companyPostalAddress.value} | ${this.payeeCompanyFormComponent.companyCpPostalAddress.value}`
        this.profileExtendedUser.postalCountry.id = +this.payeeCompanyFormComponent.companyCountryPostal.value;
        this.profileExtendedUser.postalCity = {
          id: +this.payeeCompanyFormComponent.companyCityPostal.value, name: '',
          country: { id: 0, name: '' }
        };
      } else {
        this.changeTabView(0);
        this.showMessage(messageTranslate.value, 'error');
        this.payeeCompanyFormInvalid = true;
        return false;
      }
      // PAYEE COMPANY END

      // PAYEE
    } else {// PAYEE user

      this.payeeFormInvalid = false;
      this.cardNumberError = false;
      this.cardNumberLettersError = false;

      if (this.payeeFormComponent.noPostalCountrySelected
        || this.payeeFormComponent.noPostalCitySelected) {
        this.payeeFormInvalid = true;
        this.showMessage(messageTranslate.value, 'error');
        this.changeTabView(0);
        return false;
      }

      if (this.payeeFormComponent.payeeForm.valid) {
        // USER INFO
        this.profileExtendedUser.firstName1 = this.payeeFormComponent.payeeFirstName1.value;
        this.profileExtendedUser.lastName1 = this.payeeFormComponent.payeeLastName1.value;
        this.profileExtendedUser.firstName2 = this.payeeFormComponent.payeeFirstName2.value;
        this.profileExtendedUser.lastName2 = this.payeeFormComponent.payeeLastName2.value;
        this.profileExtendedUser.birthDate = new Date(this.payeeFormComponent.payeeBirthDate.value);
        this.profileExtendedUser.postalAddress = `${this.payeeFormComponent.payeePostalAddress.value} | ${this.payeeFormComponent.payeeCpPostalAddress.value}`
        this.profileExtendedUser.postalCity = { id: this.payeeFormComponent.payeeCityPostal.value };
        this.profileExtendedUser.postalCountry.id = +this.payeeFormComponent.payeeCountryPostal.value;
        this.profileExtendedUser.residenceCity = { id: this.payeeFormComponent.payeeCityPostal.value };
        this.profileExtendedUser.idNumber = this.payeeFormComponent.payeeIdNumber.value;
        this.profileExtendedUser.idType = { id: this.payeeFormComponent.payeeIdType.value };
        this.profileExtendedUser.phoneNumber = this.payeeFormComponent.payeePhoneNumber.value;
        this.profileExtendedUser.mobileNumber = this.payeeFormComponent.payeeMobileNumber.value;
        this.profileExtendedUser.maritalStatus = this.payeeFormComponent.payeeMaritalStatus.value;
        this.profileExtendedUser.gender = this.payeeFormComponent.payeeGender.value;
        this.profileExtendedUser.taxId = this.payeeFormComponent.payeeTaxId.value;

        // BANK INFO
        this.profileExtendedUser.canChangePaymentMethod = this.canChangePaymentMethod;
        this.profileExtendedUser.useDirectPayment = this.bankPaymentMethod;

      } else {
        this.payeeFormInvalid = true;
        this.changeTabView(0);
        this.showMessage(messageTranslate.value, 'error');
        return false;
      }
    }

    if (this.bankPaymentMethod) {
      let messageErrorTranslate: any;
      if (this.bankFormComponent.bankForm.valid) {
        switch (this.countryPostalId) {
          case 1: // ID 1 = COLOMBIA
            this.profileExtendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
            this.profileExtendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
            if (this.profileExtendedUser.directPaymentBank && this.profileExtendedUser.directPaymentCity) {
              this.profileExtendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
              this.profileExtendedUser.directPaymentCity = { id: this.bankFormComponent.bankCity.value };

              /**
               *               this.profileExtendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
              this.profileExtendedUser.directPaymentCity.id !== null ? this.profileExtendedUser.directPaymentCity.id = this.bankFormComponent.bankCity.value.id :
              this.profileExtendedUser.directPaymentCity = { id: this.bankFormComponent.bankCity.value.id };
               */
            } else {
              this.profileExtendedUser.directPaymentBank = { id: this.bankFormComponent.bank.value };
              this.profileExtendedUser.directPaymentCity = { id: this.bankFormComponent.bankCity.value };
            }
            break;
          case 2: // ID 2 = MEXICO
            this.profileExtendedUser.bankAccountNumber = this.bankFormComponent.clabeBankAccountNumber.value;
            break;
          case 66: // ID 66 = ARGENTINA
            this.profileExtendedUser.bankAccountNumber = this.bankFormComponent.cbuBankAccountNumber.value;
            break;
          case 74: // ID 74 = BRAZIL
            this.profileExtendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
            this.profileExtendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
            this.profileExtendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
            this.profileExtendedUser.bankBranch = this.bankFormComponent.bankBranch.value;
            break;
          case 77: // 77 = CHILE
            this.profileExtendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
            this.profileExtendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
            this.profileExtendedUser.directPaymentBank = { id: this.bankFormComponent.bank.value }
            break;
          case 101: // 101 = Perú
            this.profileExtendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value.trim();
            this.profileExtendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
            this.profileExtendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
            break;
            case 115: // 115 = VENEZUELA
            this.profileExtendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value.trim();
            this.profileExtendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
            this.profileExtendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
            break;
        }
      } else {
        this.bankFormInvalid = true;
        switch (this.countryPostalId) {
          case 74:
          case 1: // ID 1 = COLOMBIA, 74 BBRAZIL
            messageErrorTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_REQUIRED_BANKS');
            this.showMessage(messageErrorTranslate.value, 'error');
            break;
          case 2: // ID 2 = MEXICO
            messageErrorTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CLABE_18_NUMBERS');
            this.showMessage(messageErrorTranslate.value, 'error');
            break;
          case 66: // ID 66 = ARGENTINA
            messageErrorTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CBU_MUST_HAVE_NUMBERS');
            this.showMessage(messageErrorTranslate.value, 'error');
            break;
        }
        return false;
      }
    } else {
      if (!this.evaluateCardNumber(this.bankFormComponent.cardNumber.value)) {
        return false;
      }
      this.profileExtendedUser.cardNumber =
        this.bankFormComponent.cardNumber.value ? this.bankFormComponent.cardNumber.value.replace(/-/g, '') : '';
    }

    this.updateUserCallMethod(this.profileExtendedUser);
  }

  updateUserCallMethod(extendedUser) {
    this.userService.updateExtendedUser(extendedUser).subscribe(
      extUser => {
        this.data.extendedUser = extUser;
        this.profileExtendedUser = this.data.extendedUser;
        if (this.profileUserRole === ROLES.ADMIN) {
          this.adminFormComponent.initExtendedUser(this.profileExtendedUser);
        } else if (this.profileUserRole === ROLES.PAYEE) {
          // this.profileUser.payeeAsCompany
          //   ? this.payeeCompanyFormComponent.initExtendedUser(this.profileExtendedUser)
          //   : this.payeeFormComponent.initExtendedUser(this.profileExtendedUser);
          // this.bankFormComponent.initBankInfo(this.profileExtendedUser);
          this.changeTabView(0);
        } else {
          this.userFormComponent.initExtendedUser(this.profileExtendedUser);
        }
        this.disableForm();
        let messageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_PROFILE_UPDATE');
        this.showMessage(messageTranslate.value, 'success');
      }
    );
  }

  disableForm() {

    if (this.profileUserRole === ROLES.ADMIN) {
      this.adminFormComponent.disableAdminFormEditing();

    } else if (this.profileUserRole === ROLES.PAYEE) {
      this.profileUser.payeeAsCompany
        ? this.payeeCompanyFormComponent.disablePayeeCompanyFormEditing()
        : this.payeeFormComponent.disablePayeeFormEditing();
      setTimeout(() => {
        this.bankFormComponent.disableBankFormEditing();
      }, 0);
    } else if (this.profileUserRole === ROLES.RESELLER || this.profileUserRole === ROLES.MERCHANT) {
      this.userFormComponent.disableUserFormEditing();
    }
    this.modifyMode = false;
  }

  closeModal(): void {
    this.dialogRef.close();
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  // ONLY MERCHANT
  evaluateUserCountry(type: string) {
    if (type === 'postal') {
      this.userFormComponent.userCountryPostal.value && this.userFormComponent.userCountryPostal.value.id ?
        this.userFormComponent.userCountryPostal.setErrors(null)
        : this.userFormComponent.userCountryPostal.setErrors({ incorrect: true });
    } else {
      this.userFormComponent.userCountryResidence.value && this.userFormComponent.userCountryResidence.value.id ?
        this.userFormComponent.userCountryResidence.setErrors(null)
        : this.userFormComponent.userCountryResidence.setErrors({ incorrect: true });
    }
  }

  // ONLY PAYEE
  changeTabView(index) {
    this.selectedIndex.setValue(index);
  }

  // ONLY PAYEE
  selectPaymentMethod(event) {
    this.bankFormComponent.cleanBankFormValidator();
    this.bankPaymentMethod = event.checked;
    this.bankFormComponent.initBankInfo(this.profileExtendedUser);
    if (this.bankPaymentMethod) {
      this.bankFormComponent.setBankFormValidator();
    } else {
      this.bankFormInvalid = false;
    }

    if (!this.bankPaymentMethod && this.userSessionService.haveStatus() && !this.profileExtendedUser.cardNumber) {
      /*const dialogRef = this.dialog.open(CardNumberModalComponent, {
        disableClose: true,
        data: {
          extendedUser: this.profileExtendedUser,
          adminView: true,
        },
      });
      dialogRef.afterClosed().subscribe(value => {
        if (value) {
          switch (value.typeResult) {
            case 'CANCEL':
              this.bankPaymentMethod = true;
              this.bankFormComponent.initBankInfo(this.profileExtendedUser);
              this.disableForm();
              break;
            case 'CONTINUE':
              this.profileExtendedUser.status = 'CREATED';
              this.profileExtendedUser.useDirectPayment = false;
              this.userService.updateExtendedUser(this.profileExtendedUser).subscribe(
                () => {
                  this.initUser();
                  let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_PROFILE_UPDATE' );
                  this.showMessage( messageTranslate.value, 'success');
                  this.disableForm();
                  return true;
                }
              );
              break;
            default:
              break;
          }
        }
      });*/
    }
  }

  // ONLY PAYEE
  evaluateCardNumber(cardNumber: any): boolean {
    this.cardNumberError = false;
    this.cardNumberLettersError = false;
    this.cardNumberValue = '';
    this.cardNumberValue = cardNumber.replace(/-/g, '');

    let messageTranslate: any;

    if (this.cardNumberValue.length === 0) {
      this.cardNumberError = true;
      messageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CARD_NUMBER_REQUIRED');
      this.showMessage(messageTranslate.value, 'error');
    } else if (!this.cardNumberValue.match('^[0-9]+$')) {
      this.cardNumberLettersError = true;
      messageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.INVALID_CARD_LETTERS');
      this.showMessage(messageTranslate.value, 'error');
      return false;
    } else if (this.cardNumberValue.length !== 16) {
      this.cardNumberError = true;
      messageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.INVALID_CARD_NUMBERS');
      this.showMessage(messageTranslate.value, 'error');
      return false;
    } else {
      return true;
    }
  }

  changePostalCountry(id) {
    this.countryPostalId = id;
    if (this.countryPostalId !== 0) {
      setTimeout(() => {
        this.bankFormComponent.setBankFormValidator();
      }, 100);
    }
  }

  whenSeeChangePaymentMethod(): boolean {
    let canSee = false;

    if (this.countryPostalId === 2) { // ID 66 = ARGENTINA
      canSee = true;
    }
    return canSee;
  }
}