import {Component, OnInit, ChangeDetectorRef, Input} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ROLES} from '../../../config/enums';
import {ExtendedUser, User} from '../../../config/interfaces';
import {UserService} from '../../../services/user/user.service';
import {UserSessionService} from '../../../services/user/userSession.service';
import {noWhitespaceValidator} from '../../../shared/functions';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admin-form',
  templateUrl: './adminForm.component.html'
})
export class AdminFormComponent implements OnInit {

  @Input() extendedUser = {} as ExtendedUser;
  @Input() sessionUser: User;
  @Input() modifyMode: boolean;
  @Input() adminFormInvalid: boolean;
  @Input() modalView: boolean;

  adminForm: FormGroup;
  roles = ROLES;

  constructor(public userService: UserService,
              public userSessionService: UserSessionService,
              private cdRef: ChangeDetectorRef,
              private translate: TranslateService) {
    this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
  }

  // ADMIN GET FORM ELEMENTS
  get adminFirstName1() { return this.adminForm.get('firstName1'); }
  get adminLastName1() { return this.adminForm.get('lastName1'); }
  // get adminEmail() { return this.adminForm.get('email'); }

  ngOnInit() {
    this.initUser();
  }

  initUser() {
    this.initExtendedUser(this.extendedUser);
  }

  initExtendedUser(extendedUser: ExtendedUser) {
    this.adminForm = new FormGroup({
      lastName1: new FormControl(
        extendedUser && extendedUser.lastName1 ? extendedUser.lastName1 : this.sessionUser.lastName,
        [Validators.required, noWhitespaceValidator]),
      firstName1: new FormControl(
        extendedUser && extendedUser.firstName1 ? extendedUser.firstName1 : this.sessionUser.firstName,
        [Validators.required, noWhitespaceValidator]),
      email: new FormControl(extendedUser && extendedUser.email ? extendedUser.email : this.sessionUser.email,
        [Validators.required, Validators.email]),
    });
    this.adminForm.disable();
    this.cdRef.detectChanges();
  }

  enableAdminFormEditing() {
    this.adminForm.enable({emitEvent: false});
  }

  disableAdminFormEditing() {
    this.adminForm.disable({emitEvent: false});
  }

  canReadOnly(type: string): boolean {
    let value: boolean;
    switch (type) {
      case 'FIRST_NAME_1':
      case 'FIRST_LAST_NAME_1':
        this.modalView ? value = !this.modifyMode : value = true;
        break;
      default:
        break;
    }
    return value;
  }

  validacionInput(e: any, type: string, addCHaracterSpecialc = null) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = '';
    if (type === 'numeros') {
      numeros = '0123456789';
    } else if (type === 'letras') {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz';
    } else {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    };

    if (addCHaracterSpecialc !== null) {
      numeros = numeros + addCHaracterSpecialc;
    }
/*     const numeros = type === 'numeros' ? "0123456789" :
    'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 '; */
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }
}