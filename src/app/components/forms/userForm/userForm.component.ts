import {OnInit, ChangeDetectorRef, Component, Input, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AUTOCOMPLETE_TYPE, ROLES} from '../../../config/enums';
import {ExtendedUser, User} from '../../../config/interfaces';
import {UserService} from '../../../services/user/user.service';
import {UserSessionService} from '../../../services/user/userSession.service';
import {DataFinderService} from '../../../services/data-finder.service';
import {noWhitespaceValidator, determinateMaxDate} from '../../../shared/functions';
import {AutocompleteInputComponent} from '../../autocompleteInput/autocompleteInput.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user-form',
  templateUrl: './userForm.component.html',
  styleUrls: ['./userForm.component.scss']
})
export class UserFormComponent implements OnInit {

  @Input() extendedUser = {} as ExtendedUser;
  @Input() sessionUser: User;
  @Input() modifyMode: boolean;
  @Input() userFormInvalid: boolean;
  @Input() modalView: boolean;
  @Input() createUser: boolean;
  @Input() emailAlreadyExistError: boolean;


  maxDate = new Date();

  userForm: FormGroup;
  roles = ROLES;
  autocompleteType = AUTOCOMPLETE_TYPE;

  noPostalCountrySelected = false;
  noResidenceCountrySelected = false;

  disabledDateBirth = false;
  public day = '';
  public month = '';
  public year = '';
  public yearArray = [];

  banks = [];

  @ViewChild('CompanyCountryAutocomplete', {static: true}) companyCountryAutocomplete: AutocompleteInputComponent;
  @ViewChild('AdministratorCountryAutocomplete', {static: true}) administratorCountryAutocomplete: AutocompleteInputComponent;

  constructor(public userService: UserService,
              public userSessionService: UserSessionService,
              private dataFinder: DataFinderService,
              private cdRef: ChangeDetectorRef,
              private translate: TranslateService) {
    this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
  }

  // MERCHANT GET FORM ELEMENTS
  get userLastName1() { return this.userForm.get('lastName1'); }
  get userFirstName1() { return this.userForm.get('firstName1'); }
  get userEmail() { return this.userForm.get('email'); }
  get userCompany() { return this.userForm.get('company'); }
  get userIdNumber() { return this.userForm.get('idNumber'); }
  get userTaxId() { return this.userForm.get('taxId'); }
  get usercpResidenceAddress() { return this.userForm.get('cpResidenceAddress'); }
  get userResidenceAddress() { return this.userForm.get('residenceAddress'); }
  get userPostalAddress() { return this.userForm.get('postalAddress'); }
  get usercpPostalAddress() { return this.userForm.get('cpPostalAddress'); }
  get userMobileNumber() { return this.userForm.get('mobileNumber'); }
  get userBirthDate() { return this.userForm.get('birthDate'); }
  get userCountryResidence() { return this.userForm.get('countryResidence'); }
  get userCityResidence() { return this.userForm.get('cityResidence'); }
  get userCountryPostal() { return this.userForm.get('countryPostal'); }
  get userCityPostal() { return this.userForm.get('cityPostal'); }

  ngOnInit() {
    this.setYear();
    this.initExtendedUser(this.extendedUser);
  }

  initExtendedUser(extendedUser: ExtendedUser) {
    let postalCityFullAddress = null;
    let residenceCityFullAddress = null;
    const date = new Date(extendedUser.birthDate);
    if ((!this.modalView && this.userSessionService.haveStatus()) || this.modalView) {
      postalCityFullAddress = extendedUser.postalAddress ? extendedUser.postalAddress.split(' | ') : '';
      residenceCityFullAddress = extendedUser.residenceAddress ? extendedUser.residenceAddress.split(' | ') : '';
    }

    if(extendedUser.birthDate) {
      const date = String(extendedUser.birthDate).substring(0,10).split('-');
      this.day = extendedUser.birthDate === null ? '' : String(parseInt(date[2]));
      this.month = extendedUser.birthDate === null ? '' : String(parseInt(date[1]));
      this.year = extendedUser.birthDate === null ? '' : String(parseInt(date[0]));
    }

    this.userForm = new FormGroup({
      lastName1: new FormControl(
        extendedUser && extendedUser.lastName1 ? extendedUser.lastName1
          : this.sessionUser && this.sessionUser.lastName ? this.sessionUser.lastName : '',
        [Validators.required, noWhitespaceValidator]),
      firstName1: new FormControl(
        extendedUser && extendedUser.firstName1 ? extendedUser.firstName1
          : this.sessionUser && this.sessionUser.firstName ? this.sessionUser.firstName : '',
        [Validators.required, noWhitespaceValidator]),
      idNumber: new FormControl(extendedUser && extendedUser.idNumber ? extendedUser.idNumber : '',
        [Validators.required, noWhitespaceValidator]),
      idType: new FormControl( extendedUser && extendedUser.idNumber ? extendedUser.idNumber : null),
      taxId: new FormControl(extendedUser && extendedUser.taxId ? extendedUser.taxId : '',
        [Validators.required, noWhitespaceValidator]),
      taxIdType: new FormControl(
        extendedUser && extendedUser.idTypeTaxId ? extendedUser.idTypeTaxId.id : null),
      residenceAddress: new FormControl(
        residenceCityFullAddress && residenceCityFullAddress[0] ? residenceCityFullAddress[0] : '',
        [Validators.required, noWhitespaceValidator]),
      cpResidenceAddress: new FormControl(
        residenceCityFullAddress && residenceCityFullAddress[2] ? residenceCityFullAddress[2] : '', Validators.required),
      postalAddress: new FormControl(
        postalCityFullAddress && postalCityFullAddress[0] ? postalCityFullAddress[0] : '',
        [Validators.required, noWhitespaceValidator]),
      cpPostalAddress: new FormControl(
        postalCityFullAddress && postalCityFullAddress[2] ? postalCityFullAddress[2] : '',Validators.required),     
      mobileNumber: new FormControl(extendedUser && extendedUser.mobileNumber ? extendedUser.mobileNumber : ''),
      birthDate: new FormControl(
        extendedUser && extendedUser.birthDate ? new Date(extendedUser.birthDate) : '', Validators.required),
      email: new FormControl(
        extendedUser && extendedUser.email ? extendedUser.email
          : this.sessionUser && this.sessionUser.email ? this.sessionUser.email : '', [ Validators.required, Validators.email]),
      company: new FormControl(extendedUser && extendedUser.company ? extendedUser.company : '',
        [Validators.required, noWhitespaceValidator]),
      countryResidence: new FormControl(
        extendedUser && extendedUser.residenceCountry ? extendedUser.residenceCountry : '', Validators.required),
      cityResidence: new FormControl(
        residenceCityFullAddress && residenceCityFullAddress[1] ? residenceCityFullAddress[1] : '',
        [Validators.required, noWhitespaceValidator]),
      countryPostal: new FormControl(
        extendedUser && extendedUser.postalCountry ? extendedUser.postalCountry : '', Validators.required),
      cityPostal: new FormControl(postalCityFullAddress && postalCityFullAddress[1] ? postalCityFullAddress[1] : '',
        [Validators.required, noWhitespaceValidator]),
    });
    setTimeout(() => {
      if (this.modalView) {
        this.userForm.disable();
      } else {
        if ((this.userSessionService.isMerchant() || this.userSessionService.isReSeller())
          && this.userSessionService.haveStatus() && !this.createUser) {
          this.userForm.disable();
        } else {
          this.companyCountryAutocomplete.initElementFilter();
          this.administratorCountryAutocomplete.initElementFilter();
        }
      }
    }, 200);
    if (this.createUser) {
      this.initEmailValueChange();
    }
    this.cdRef.detectChanges();
  }

  initEmailValueChange() {
    this.userEmail.valueChanges.subscribe(() => {
      this.emailAlreadyExistError = false;
    });
  }

  resetDependencies(eventValue: boolean, type: string) {
    if (type === 'POSTAL') {
      this.noPostalCountrySelected = eventValue;
      this.userCityPostal.reset('', [Validators.required, noWhitespaceValidator]);
    } else {
      this.noResidenceCountrySelected = eventValue;
      this.userCityResidence.reset('', [Validators.required, noWhitespaceValidator]);
    }
  }

  enableUserFormEditing() {
    this.userForm.enable();
    this.disabledDateBirth = false;
    if (this.modalView) {
      this.companyCountryAutocomplete.initElementFilter();
      this.administratorCountryAutocomplete.initElementFilter();
    }
  }

  disableUserFormEditing() {
    this.userForm.disable();
    this.disabledDateBirth = true;
    this.noPostalCountrySelected = false;
    this.noResidenceCountrySelected = false;
  }

  canReadOnly(type: string): boolean {

    let value: boolean;
    switch (type) {
      case 'EMAIL':
        this.createUser ? value = false : value = true;
        break;
      case 'NAME':
        this.modalView ? value = !this.modifyMode
          : this.createUser ? value = false : value = true;
        break;
      case 'ID_NUMBER':
      case 'ADDRESS':
      case 'COUNTRY':
      case 'COMPANY':
      case 'CITY':
        this.modalView ? value = !this.modifyMode
          : this.createUser ? value = false : value = this.userSessionService.haveStatus();
        break;
      case 'PHONE_NUMBER':
      case 'MOBILE_PHONE':
        case 'BIRTH':
        this.modalView ? value = !this.modifyMode
          : this.createUser ? value = false : value = this.userSessionService.haveStatus() && !this.modifyMode;
        break;
    }
    return value;
  }
  //-----------------Inicia calendario-----------
  setBirth() {
     const dateString = this.year + "-" + this.month + "-" + this.day;
    this.userForm.controls['birthDate'].setValue(dateString);
  }

  setYear(){
    let d = new Date();
    let n = ( d.getFullYear() - 18 );
    for(let i = n; i >= 1930; i--) {
      this.yearArray.push(i);
    }
  }

  validacionInput(e: any, type: string, addCHaracterSpecialc = null) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = '';
    if (type === 'numeros') {
      numeros = '0123456789';
    } else if (type === 'letras') {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz ';
    } else {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    };

    if (addCHaracterSpecialc !== null) {
      numeros = numeros + addCHaracterSpecialc;
    }
/*     const numeros = type === 'numeros' ? "0123456789" :
    'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 '; */
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }

/*   validacionInput(e: any, type: string, addCHaracterSpecialc = null) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = type === 'numeros' ? "0123456789" :
    'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 '; 
    if (addCHaracterSpecialc !== null) {
      numeros = numeros + addCHaracterSpecialc;
    }
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  } */
}
