import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ROLES, AUTOCOMPLETE_TYPE, DROWBOX_TYPE } from '../../../config/enums';
import { ExtendedUser, User } from '../../../config/interfaces';
import { UserService } from '../../../services/user/user.service';
import { UserSessionService } from '../../../services/user/userSession.service';
import { DataFinderService } from '../../../services/data-finder.service';
import { AutocompleteInputComponent } from '../../components.index';
import { noWhitespaceValidator, determinateMaxDate } from '../../../shared/functions';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

import { Subject } from 'rxjs';

@Component({
  selector: 'app-payee-form',
  templateUrl: './payeeForm.component.html',
  styleUrls: ['./payeeForm.component.scss']
})
export class PayeeFormComponent implements OnInit {

  changeCountry: Subject<any> = new Subject();

  @Input() extendedUser = {} as ExtendedUser;
  @Input() user: User;
  @Input() countryPostalId: number;
  @Input() modifyMode: boolean;
  @Input() payeeFormInvalid: boolean;
  @Input() modalView: boolean;

  @Output() setCountryPostalId = new EventEmitter<number>();
  @Output() elementChange = new EventEmitter<any>();
  @Output() setPostalCountrySelectedId = new EventEmitter<number>();

  maximoDocumento: number = 25;

  datePickerConfig: Partial<BsDatepickerConfig>;
  maxDate = new Date();
  minDni: number = 6;
  maxDni: number = 10;
  isPeru: boolean = false;

  payeeForm: FormGroup;
  roles = ROLES;
  autocompleteType = AUTOCOMPLETE_TYPE;
  countrys: any[];
  citiesDisabled: boolean = true;

  maritalStatus: string;
  gender: string;
  selectedMaritalStatus = 'SINGLE';
  noPostalCountrySelected = false;
  noPostalCitySelected = false;
  legalIdTypesArr = [];
  naturalIdTypesArr = [];
  changeClass: any;
  public day = '';
  public month = '';
  public year = '';
  public yearArray = [];
  public calendarDisable = false;
  drownBoxType = DROWBOX_TYPE;
  @ViewChild('CityAutocomplete', { static: true }) cityAutocomplete: AutocompleteInputComponent;
  isInterbank: boolean = false;

  constructor(public userService: UserService,
    public userSessionService: UserSessionService,
    private cdRef: ChangeDetectorRef,
    private dataFinder: DataFinderService,
    private translate: TranslateService) {
    this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss");
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  // PAYEE GET FORM ELEMENTS
  get payeeLastName1() { return this.payeeForm.get('lastName1'); }
  get payeeFirstName1() { return this.payeeForm.get('firstName1'); }
  get payeeLastName2() { return this.payeeForm.get('lastName2'); }
  get payeeFirstName2() { return this.payeeForm.get('firstName2'); }
  get payeeEmail() { return this.payeeForm.get('email'); }
  get payeeGender() { return this.payeeForm.get('gender'); }
  get payeeMaritalStatus() { return this.payeeForm.get('maritalStatus'); }
  get payeeIdNumber() { return this.payeeForm.get('idNumber'); }
  get payeeTaxId() { return this.payeeForm.get('taxId'); }
  get payeePostalAddress() { return this.payeeForm.get('postalAddress'); }
  get payeeCpPostalAddress() { return this.payeeForm.get('cpPostalAddress'); }
  get payeeMobileNumber() { return this.payeeForm.get('mobileNumber'); }
  get payeePhoneNumber() { return this.payeeForm.get('phoneNumber'); }
  get payeeBirthDate() { return this.payeeForm.get('birthDate'); }
  get payeeCountryPostal() { return this.payeeForm.get('countryPostal'); }
  get payeeCityPostal() { return this.payeeForm.get('cityPostal'); }
  get payeeIdType() { return this.payeeForm.get('idType'); }
  get payeeTaxIdType() { return this.payeeForm.get('taxIdType'); }

  ngOnInit() {
    this.dataFinder.idBank$.subscribe(banco => {
      this.changeBankPeru(banco);
    })
    this._loadCountry();
    this.initUser();
    this.setYear();
  }

  _loadCountry() {

    this.dataFinder.getCountriesForSelect().subscribe(
      data => {
        this.countrys = data.content;
      });
  }

  initUser() {
    this.initExtendedUser(this.extendedUser);
    if (this.extendedUser !== undefined && this.extendedUser !== null) {
      this.validationsPeru(this.extendedUser);
    }
  }

  validationsPeru (extendedUser: ExtendedUser) {
    if (extendedUser.postalCountry && extendedUser.postalCountry.id) {
      if (extendedUser.postalCountry.id === 101){
        this.isPeru = true;
        if (extendedUser && extendedUser.directPaymentBank) {
          this.changeBankPeru(extendedUser.directPaymentBank.id)
        }
      } else {
        this.isPeru = false;
      }

    }
  }

  changeBankPeru(bankId : number) {
    if (bankId === 202) {
      this.isInterbank = true;
      this.minDni = 6;
      this.maxDni = 10
    } else {
      this.isInterbank = false;
      this.minDni = 8;
      this.maxDni = 8;
    }

    this.payeeForm.controls["idNumber"].setValidators([Validators.required, noWhitespaceValidator,Validators.minLength(this.minDni), Validators.maxLength(this.maxDni)]);
    if (this.payeeForm.controls["idNumber"].value.length < this.minDni || this.payeeForm.controls["idNumber"].value.length > this.maxDni ) {
    this.payeeForm.controls["idNumber"].setErrors(Validators);
    } 


  }

  initExtendedUser(extendedUser: ExtendedUser) {
    let postalCityFullAddress = null;
    if ((!this.modalView && this.userSessionService.haveStatus()) || this.modalView) {
      postalCityFullAddress = extendedUser.postalAddress ? extendedUser.postalAddress.split(' | ') : '';
    }
    if (extendedUser) {
      const date = String(extendedUser.birthDate).substring(0, 10).split('-');
      this.day = extendedUser.birthDate === null ? '' : String(parseInt(date[2]));
      this.month = extendedUser.birthDate === null ? '' : String(parseInt(date[1]));
      this.year = extendedUser.birthDate === null ? '' : String(parseInt(date[0]));
    }
    this.payeeForm = new FormGroup({
      lastName1: new FormControl(
        extendedUser && extendedUser.lastName1 ? extendedUser.lastName1 : this.user.lastName,
        [Validators.required, noWhitespaceValidator]),
      lastName2: new FormControl(extendedUser && extendedUser.lastName2 ? extendedUser.lastName2 : ''),
      firstName1: new FormControl(extendedUser && extendedUser.firstName1 ? extendedUser.firstName1 : this.user.firstName,
        [Validators.required, noWhitespaceValidator]),
      firstName2: new FormControl(extendedUser && extendedUser.firstName2 ? extendedUser.firstName2 : ''),
      idNumber: new FormControl(extendedUser && extendedUser.idNumber ? extendedUser.idNumber : '',
        [Validators.required, noWhitespaceValidator]),
      gender: new FormControl(extendedUser && extendedUser.gender ? extendedUser.gender : '', Validators.required),
      taxId: new FormControl(extendedUser && extendedUser.taxId ? extendedUser.taxId : ''),
      maritalStatus: new FormControl(
        extendedUser && extendedUser.maritalStatus ? extendedUser.maritalStatus : this.selectedMaritalStatus, Validators.required),
      postalAddress: new FormControl(postalCityFullAddress && postalCityFullAddress[0] ? postalCityFullAddress[0] : '',
        [Validators.required]),
      cpPostalAddress: new FormControl(postalCityFullAddress && postalCityFullAddress[1] ? postalCityFullAddress[1] : ''),
      mobileNumber: new FormControl(extendedUser && extendedUser.mobileNumber ? extendedUser.mobileNumber : ''),
      phoneNumber: new FormControl(extendedUser && extendedUser.phoneNumber ? extendedUser.phoneNumber : '', noWhitespaceValidator),
      birthDate: new FormControl(
        extendedUser && extendedUser.birthDate ? new Date(extendedUser.birthDate) : '', Validators.required),
      email: new FormControl(extendedUser && extendedUser.email ? extendedUser.email : this.user.email, Validators.required),
      countryPostal: new FormControl(extendedUser && extendedUser.postalCountry ? extendedUser.postalCountry.id : '', Validators.required),
      cityPostal: new FormControl(extendedUser && extendedUser.postalCity ? extendedUser.postalCity : '', Validators.required),
      idType: new FormControl(extendedUser && extendedUser.idType ? extendedUser.idType.id : null, Validators.required),
      taxIdType: new FormControl(extendedUser && extendedUser.idTypeTaxId ? extendedUser.idTypeTaxId.id : null),
    });
    this.setValuePayeeUserSessionStorage();

    if (extendedUser && extendedUser.postalCountry) {
      this.changeCountry.next(extendedUser.postalCountry.id);
    }

    this.setValidationForCountry(this.countryPostalId);
    setTimeout(() => {
      if (this.modalView) {
        this.getIdTypes('NATURAL', false);
        this.payeeForm.disable();
      } else {
        if (this.userSessionService.haveStatus()) {
          if (extendedUser.confirmedProfile) {
            this.payeeForm.disable();
            this.payeeForm.controls["countryPostal"].disable();
          } else {
            this.initPostalCityFilter();
          }
          this.getIdTypes('NATURAL', false);
        }
      }

      this.cdRef.detectChanges();
    }, 200);
  }

  resetCity() {
    this.payeeCityPostal.reset();
    this.payeeCityPostal.disable();
  }

  resetIdType() {
    this.payeeIdType.reset();
    this.payeeIdType.disable();
  }

  initPostalCityFilter() {
    this.payeeCityPostal.enable();
    // this.cityAutocomplete.initElementFilter();
  }

  onChangeCountry(country) {
    if (country.target.value) {
      this.payeeCpPostalAddress.reset();
      this.payeePostalAddress.reset();
      this.payeeIdNumber.reset();
      this.payeeTaxIdType.reset();
      this.countryPostalId = parseInt(country.target.value);
      this.payeeCityPostal.enable();
      this.setPostalCountrySelectedId.emit(this.countryPostalId);
      this.setCountryPostalId.emit(this.countryPostalId);
      this.noPostalCountrySelected = false;
      this.setValidationForCountry(this.countryPostalId);
      this.changeCountry.next(this.countryPostalId);
      this.initPostalCityFilter();
      this.getIdTypes('NATURAL', true);
    }
  }

  resetIdTypeAndCity() {
    this.resetCity();
    this.resetIdType();
  }

  getIdTypes(type: string, isChangeCountryCall: boolean) {
    this.dataFinder.getIdTypesByCountryIdAndType(this.countryPostalId, type).subscribe(
      data => {

        if (isChangeCountryCall) {

          if (type === 'NATURAL') {
            this.naturalIdTypesArr = data.content;
            if(this.countryPostalId == 66) {
              this.naturalIdTypesArr = this.naturalIdTypesArr.filter( item => item.id === 18);
            }
            this.payeeIdType.enable();
            if (this.naturalIdTypesArr.length === 1) {
              this.payeeIdType.setValue(this.naturalIdTypesArr[0].id);
              this.payeeIdType.updateValueAndValidity();
            }
          } else {
            this.legalIdTypesArr = data.content;
            this.payeeTaxIdType.enable();
            if (this.legalIdTypesArr.length === 1) {
              this.payeeTaxIdType.setValue(this.legalIdTypesArr[0].id);
              this.payeeTaxIdType.updateValueAndValidity();
            }
          }
        }
        else {
          type === 'NATURAL' ? this.naturalIdTypesArr = data.content : this.legalIdTypesArr = data.content;
          if(type === 'NATURAL' && this.countryPostalId === 66) {
            this.naturalIdTypesArr = this.naturalIdTypesArr.filter( item => item.id === 18);
          }
        }
      }
    );
  }

  enablePayeeFormEditing() {
    this.calendarDisable = false;
    this.payeeForm.enable({ emitEvent: false });
    this.initPostalCityFilter();
    if (this.modalView) {
    } else {
      this.payeeIdType.disable();
      this.payeeGender.disable();
      if (this.canReadOnly('POSTAL_CITY')) {
              this.payeeCityPostal.disable();
            }
      this.payeeForm.controls.countryPostal.disable();
    }
  }

  disablePayeeFormEditing() {
    this.calendarDisable = true;
    this.payeeForm.disable({ emitEvent: false });
    this.payeeIdType.disable();
    this.payeeGender.disable();
    this.payeeMaritalStatus.disable();
    this.noPostalCountrySelected = false;
    this.noPostalCitySelected = false;
    this.payeeForm.controls.countryPostal.disable();
  }

  canReadOnly(type: string): boolean {
    let value: boolean;
    switch (type) {
      case 'ID_NUMBER':
        value = (this.userSessionService.loadRole() === ROLES.ADMIN || this.userSessionService.loadRole() === ROLES.SUPER_ADMIN || this.userSessionService.loadRole() === ROLES.PAYEE) ? false : true;
        break;
      case 'FIRST_NAME_1':
      case 'FIRST_LAST_NAME_1':
      case 'FIRST_NAME_2':
      case 'FIRST_LAST_NAME_2':
      case 'POSTAL_COUNTRY':
      case 'POSTAL_CITY':
        this.calendarDisable = this.modalView ? value = !this.modifyMode : value = this.userSessionService.haveStatus() && this.extendedUser.confirmedProfile;
        break;
      case 'PHONE_NUMBER':
      case 'POSTAL_ADDRESS':
      case 'TAX_ID':
      case 'MOBILE_PHONE':
        this.modalView ?
          value = !this.modifyMode : value = this.userSessionService.haveStatus() && this.extendedUser.confirmedProfile && !this.modifyMode;
        break;
    }
    return value;
  }

  seeFirstNameAndLastName(type): boolean {
    switch (type) {
      case 'FIRST_NAME_1':
      case 'LAST_NAME_1_2':
      case 'COUNTRY_CITY':
        return !this.userSessionService.haveStatus()
          || (this.userSessionService.haveStatus() && this.extendedUser && !this.extendedUser.confirmedProfile)
          || this.modalView;

      case 'FIRST_LAST_NAME_2':
        return (this.userSessionService.haveStatus() && this.extendedUser && this.extendedUser.confirmedProfile) && !this.modalView;

      default:
        return false;
    }
  }

  setBirth() {
    const dateString = this.year + "-" + this.month + "-" + this.day;
    this.payeeForm.controls['birthDate'].setValue(dateString);
  }

  setYear() {
    let d = new Date();
    let n = ( d.getFullYear() - 18 );
    for (let i = n; i >= 1930; i--) {
      this.yearArray.push(i);
    }
  }

  validacionInputCountry(e: any, country: any) {
    let type: string = '';

    switch (country) {
      case 101: // perú
      type = 'numeros';
        break;
      case 74: // brazil
      type = 'numeros';
        break;
      case 66: // argentina
      type = 'numeros';
        break;
      case 1: // ID 1 = COLOMBIA
      type = 'numeros';
        break;
      case 2: // ID 2 = MEXICO
      type = 'numerosYletras';
        break;
      case 115: // VENEZUELA
      type = 'numerosYletras';
        break;
      case 77: //CHILE
      type = 'numerosYletras';
        break;

    }

    const key = e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = '';
    if (type === 'numeros') {
      numeros = '0123456789';
    } else if (type === 'letras') {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz ';
    } else {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789';
    }
    /*     const numeros = type === 'numeros' ? "0123456789" :
        'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 '; */
    const especiales = ['8', '164', '165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key == especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }

  setValuePayeeUserSessionStorage(){
    const objSession = {
      firstName2: this.payeeForm.controls['firstName2'].value,
      lastName1: this.payeeForm.controls['lastName1'].value,
      lastName2: this.payeeForm.controls['lastName2'].value,
      birthDate: this.payeeForm.controls['birthDate'].value,
      gender: this.payeeForm.controls['gender'].value,
      noCeluar: this.payeeForm.controls['mobileNumber'].value
    }
    sessionStorage.setItem('dataProfileForBank', JSON.stringify(objSession));
  }

  validacionInput(e: any, type: string) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = '';
    if (type === 'numeros') {
      numeros = '0123456789';
    } else if (type === 'letras') {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz ';
    } else {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    }
/*     const numeros = type === 'numeros' ? "0123456789" :
    'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 '; */
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }

  setValidationForCountry(country) {
    if (country == 74 || country == 77) {
      this.payeeForm.controls['cpPostalAddress'].clearValidators();
      this.payeeForm.controls['cpPostalAddress'].setValidators([Validators.required]);
      this.payeeForm.controls['cpPostalAddress'].updateValueAndValidity();
    } else {
      this.payeeForm.controls['cpPostalAddress'].clearValidators();
      this.payeeForm.controls['cpPostalAddress'].updateValueAndValidity();
    }

    if (country == 74 || country == 66) { //Brazil
      this.payeeForm.controls['idNumber'].clearValidators();
      this.payeeForm.controls['idNumber'].setValidators([Validators.required, Validators.minLength(11),
      Validators.maxLength(11)]); // 11 de longitud por qu ees payee empresa
      this.payeeForm.controls['idNumber'].updateValueAndValidity();
    } else if (country == 2) {
      this.payeeForm.controls['idNumber'].clearValidators();
      this.payeeForm.controls['idNumber'].setValidators([Validators.required, Validators.minLength(12),
      Validators.maxLength(13)]); // 11 de longitud por qu ees payee empresa
      this.payeeForm.controls['idNumber'].updateValueAndValidity();
    } else if (country == 77 ) {
      this.payeeForm.controls['idNumber'].clearValidators();
      this.payeeForm.controls['idNumber'].setValidators([Validators.required, Validators.minLength(8),
      Validators.maxLength(9)]); // 11 de longitud por qu ees payee empresa
      this.payeeForm.controls['idNumber'].updateValueAndValidity();
    } else if (country == 115) {
      this.payeeForm.controls['idNumber'].clearValidators();
      this.payeeForm.controls['idNumber'].setValidators([Validators.required, Validators.minLength(8), Validators.pattern('^[EeVv]{1}[0-9]{7,8}'),
      Validators.maxLength(9)]); // 11 de longitud por qu ees payee empresa
      this.payeeForm.controls['idNumber'].updateValueAndValidity();
    } else {
      this.payeeForm.controls['idNumber'].clearValidators();
      this.payeeForm.controls['idNumber'].setValidators([Validators.required]);
      this.payeeForm.controls['idNumber'].updateValueAndValidity();
    }
  }

  changeType(evento: any) {
if (evento.target.value === '1: 25' || evento.target.value === '2: 26') { // pasaporte o cedula de extrangería
  this.payeeForm.controls['idNumber'].setValidators([Validators.required, Validators.maxLength(12)]);

  this.maximoDocumento = 12;

} else {
  this.payeeForm.controls['idNumber'].setValidators([Validators.required, Validators.maxLength(25)]);

this.maximoDocumento = 25;

  }

  //idNumber taxIdType
  // this.payeeForm.value.idNumber = '';
  this.payeeForm.controls['idNumber'].setValue(''), Validators.required;

}

}
