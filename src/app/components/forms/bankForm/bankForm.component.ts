import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {ExtendedUser} from '../../../config/interfaces';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../services/user/user.service';
import {UserSessionService} from '../../../services/user/userSession.service';
import {DataFinderService} from '../../../services/data-finder.service';
import {AUTOCOMPLETE_TYPE, BANK_ACCOUNT_TYPES, DROWBOX_TYPE, ROLES, STATUS} from '../../../config/enums';
import {noWhitespaceValidator} from '../../../shared/functions';
import { TranslateService } from '@ngx-translate/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { PaymentCardFormComponent } from '../payment-card-form/payment-card-form.component';
import { Subject } from 'rxjs';
import { element } from 'protractor';
@Component({
  selector: 'app-bank-form',
  templateUrl: './bankForm.component.html',
  styleUrls: ['./bankForm.component.scss']
})
export class BankFormComponent implements  OnInit {

  @Input() extendedUser = {} as ExtendedUser;
  @Input() countryPostalId: number;
  @Input() modifyMode: boolean;
  @Input() bankFormInvalid: boolean = false;
  @Input() bankPaymentMethod: boolean;
  @Input() cardNumberError: boolean;
  @Input() cardNumberLettersError: boolean;
  @Input() modalView: boolean;
  @Input() payeeFormComponent: any;
  autocompleteType = AUTOCOMPLETE_TYPE;
  drownBoxType = DROWBOX_TYPE;
  changeCountry: Subject<any> = new Subject();
  keys = Object.keys;
  bankForm: FormGroup;
  accountType = BANK_ACCOUNT_TYPES;
  noPostalCitySelected = false;
  selectedBankAccountType = 'AHO';
  banks = [];
  cities = [];
  pageGetBank = 0;
  pageGetCity = 0;
  maxcuenta : number = 20; // MaxLength de cuenta en Perú
  // minCuenta : number = 10; // MinLength de cuenta en Perú

  countryPostalIdAux = null;
  constructor(public userService: UserService,
    public userSessionService: UserSessionService,
    private dataFinder: DataFinderService,
    public dialog: MatDialog,
    private cdRef: ChangeDetectorRef,
    private translate: TranslateService) {
      this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    }

  static setCardNumberFormatAsPayeeView(cardNumber): string {
    let value;
    if (cardNumber.length > 0) {
      const arrNumber = cardNumber.match(/.{1,4}/g);
      value = `xxxx-xxxx-xxxx-${arrNumber[3]}`;
    } else {
      value = '';
    }
    return value;
  }

  static setCardNumberFormatAsSuperOrAdminView(cardNumber): string {
    const arrNumber = cardNumber.match(/.{1,4}/g);
    return  `${arrNumber[0]}-${arrNumber[1]}-${arrNumber[2]}-${arrNumber[3]}`;
  }

  setCardNumberFormat(cardNumber): string {
    if (this.userSessionService.isSuperOrAdmin()) {
      return BankFormComponent.setCardNumberFormatAsSuperOrAdminView(cardNumber);
    } else {
      return BankFormComponent.setCardNumberFormatAsPayeeView(cardNumber);
    }
  }

  // BANK GET FORM ELEMENTS
  get bank() { return this.bankForm.get('bank'); }
  get bankCity() { return this.bankForm.get('bankCity'); }
  get bankAccountNumber() { return this.bankForm.get('bankAccountNumber'); }
  get cardNumber() { return this.bankForm.get('cardNumber'); }
  get bankAccountType() { return this.bankForm.get('bankAccountType'); }
  get clabeBankAccountNumber() { return this.bankForm.get('clabeBankAccountNumber'); }
  get cbuBankAccountNumber() { return this.bankForm.get('cbuBankAccountNumber'); }
  get bankBranch() { return this.bankForm.get('bankBranch'); }

  ngOnInit() {
    this.loadBanksAndCitiesForBank();
    this.initBankInfo(this.extendedUser);
  }

  initBankInfo(extendedUser: ExtendedUser) {
    this.bankForm = new FormGroup({
      bank: new FormControl(extendedUser && extendedUser.directPaymentBank ? extendedUser.directPaymentBank.id : null),
      bankBranch: new FormControl(extendedUser && extendedUser.bankBranch ? extendedUser.bankBranch : null),
      bankCity: new FormControl(extendedUser && extendedUser.directPaymentCity ? extendedUser.directPaymentCity : null),
      bankAccountType: new FormControl(
        extendedUser && extendedUser.bankAccountType ? extendedUser.bankAccountType : this.selectedBankAccountType),
      bankAccountNumber: new FormControl(extendedUser && extendedUser.bankAccountNumber ? extendedUser.bankAccountNumber : ''),
      clabeBankAccountNumber: new FormControl(extendedUser && extendedUser.bankAccountNumber ? extendedUser.bankAccountNumber : ''),
      cbuBankAccountNumber: new FormControl(extendedUser && extendedUser.bankAccountNumber ? extendedUser.bankAccountNumber : ''),
      cardNumber: new FormControl(
        extendedUser && extendedUser.cardNumber ? this.setCardNumberFormat(extendedUser.cardNumber) : ''),
    });

    this.changeCountry.next(this.countryPostalId);
    if (this.modalView) {
      if (!this.modifyMode) {
        this.bankForm.disable();
      }
    } else {
      if (this.userSessionService.haveStatus()) {
        if (this.extendedUser.confirmedProfile) {
          !this.modifyMode ? this.bankForm.disable() : this.bankForm.enable();
        } else {
          this.setBankFormValidator();
        }
      }
    }
  }

  enableBankFormEditing() {
    this.bankForm.enable();
    this.setBankFormValidator();
  }

  
  disableBankFormEditing() {
    this.bankForm.disable();
    this.cleanBankFormValidator();
  }

  setBankFormValidator() {
    this.loadBanksAndCitiesForBank();
    this.cleanBankFormValidator();
    if (!this.userSessionService.isPayee() && this.countryPostalIdAux !== null && this.countryPostalIdAux !== this.countryPostalId) {
      this.resetBankFormValidator();
    }
    this.changeCountry.next(this.countryPostalId);
    this.countryPostalIdAux = this.countryPostalId;
    switch (this.countryPostalId) {
      case 1: // ID 1 = COLOMBIA
        this.bank.setValidators(Validators.required);
        this.bankCity.setValidators(Validators.required);
        this.bankAccountType.setValidators(Validators.required);
        this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator]);
        break;
      case 2: // ID 2 = MEXICO
        this.clabeBankAccountNumber.setValidators(
          [Validators.required, Validators.minLength(18), Validators.maxLength(18), Validators.pattern('^[0-9]+$')]);
        break;
      case 66: // ID 66 = ARGENTINA
       this.cbuBankAccountNumber.setValidators(
          [Validators.required, Validators.minLength(22), Validators.maxLength(22), Validators.pattern('^[0-9]+$')]);
        break;
      case 74: // ID 74 = BRAZIL
        this.bank.setValidators(Validators.required);
        this.bankAccountType.setValidators(Validators.required);
        this.bankAccountNumber.setValidators([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(13),
          Validators.pattern('^[0-9]+$'),
          noWhitespaceValidator
        ]);

        this.bankBranch.setValidators([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(4),
          Validators.pattern('^[0-9]+$'),
          noWhitespaceValidator
        ]);
        break;
      case 77: // ID 77 = CHILE
        this.bankAccountType.setValidators([Validators.required]);
        this.bank.setValidators([Validators.required]);
        this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator]);
        break;

        case 101: // ID 101 = PERÚ
        this.bank.setValidators(Validators.required);
        this.verificaBanco(this.bank.value);
        this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta), Validators.maxLength(this.maxcuenta)]);
        break;
        case 115: // ID 115 = VENEZUELA
        this.bankAccountType.setValidators([Validators.required]);
        this.bank.setValidators(Validators.required);
        // this.verificaBanco(this.bank.value);
        this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.maxLength(20), Validators.minLength(20)]);
        break;
    }

    this.updateBankFormValidator();
  }

  cleanBankFormValidator() {
    this.bank.clearValidators();
    this.bankCity.clearValidators();
    this.bankAccountType.clearValidators();
    this.bankAccountNumber.clearValidators();
    this.clabeBankAccountNumber.clearValidators();
    this.cbuBankAccountNumber.clearValidators();
    this.bankBranch.clearValidators();
    this.updateBankFormValidator();
  }
resetBankFormValidator() {
  this.bank.reset();
  this.bankCity.reset();
  this.bankAccountNumber.reset();
  this.clabeBankAccountNumber.reset();
  this.cbuBankAccountNumber.reset();
  this.bankBranch.reset();
}

  updateBankFormValidator() {
    this.bank.updateValueAndValidity();
    this.bankCity.updateValueAndValidity();
    this.bankAccountType.updateValueAndValidity();
    this.bankAccountNumber.updateValueAndValidity();
    this.clabeBankAccountNumber.updateValueAndValidity();
    this.cbuBankAccountNumber.updateValueAndValidity();
    this.bankBranch.updateValueAndValidity();
  }

	_getPageBank() {
    const uri = '/api/_search/direct-payment-banks?query=originBank.country.id:' + this.countryPostalId;
    const _this = this;

    this.dataFinder._getBank(uri, _this.pageGetBank).subscribe(
        (data: any) => {
          for (const element of data.content) {

            if (element.destinyBank) {
              if (this.banks.length !== 0) {
                let isRepeat : boolean = false;
                 this.banks.find(bank => {
                  if (bank.id === element.destinyBank.id) {
                    isRepeat = true;
                  }
                })
                if (!isRepeat){
                  this.banks.push(element.destinyBank);
                }
              } else {
                this.banks.push(element.destinyBank);
              }

            }
        }

            this.banks.sort((a, b) => {
                if (a.name.toLowerCase() > b.name.toLowerCase()) {
                    return 1;
                }
                if (a.name.toLowerCase() < b.name.toLowerCase()) {
                    return -1;
                }
                return 0;
            });

            let pageCurrents = Math.ceil(data.totalElements / 100);
            if ((_this.pageGetBank + 1) <= pageCurrents) {

                _this.pageGetBank = _this.pageGetBank + 1;
                _this._getPageBank();
            }
        }
    );
}

  loadBanksAndCitiesForBank() {

    const uri = '/api/_search/direct-payment-banks?query=originBank.country.id:' + this.countryPostalId;
    const _this = this;
    this.banks = [];
    _this.pageGetBank = 0;

    this.dataFinder._getBank(uri, _this.pageGetBank).subscribe(
        (data: any) => {

          for (const element of data.content) {

            if (element.destinyBank) {
              if (this.banks.length !== 0) {
                let isRepeat : boolean = false;
                 this.banks.find(bank => {
                  if (bank.id === element.destinyBank.id) {
                    isRepeat = true;
                  }
                })
                if (!isRepeat){
                  this.banks.push(element.destinyBank);
                }
              } else {
                this.banks.push(element.destinyBank);
              }

            }
        }

            this.banks.sort((a, b) => {
                if (a.name.toLowerCase() > b.name.toLowerCase()) {
                    return 1;
                }
                if (a.name.toLowerCase() < b.name.toLowerCase()) {
                    return -1;
                }
                return 0;
            });

            let pageCurrents = Math.ceil(data.totalElements / 100);
            if ((_this.pageGetBank + 1) <= pageCurrents) {

                _this.pageGetBank = _this.pageGetBank + 1;
                _this._getPageBank();
            }
        }
    );
  }

  _getPageCity() {

      const uri = '/api/_search/cities?query=country.id:' + this.countryPostalId;
      const _this = this;

      this.dataFinder._getBank(uri, _this.pageGetCity).subscribe(
          (data: any) => {

              for (const element of data.content) {

                  this.cities.push( element );
              }

              this.cities.sort((a, b) => {
                  if (a.name.toLowerCase() > b.name.toLowerCase()) {
                      return 1;
                  }
                  if (a.name.toLowerCase() < b.name.toLowerCase()) {
                      return -1;
                  }
                  return 0;
              });

              let pageCurrents = Math.ceil(data.totalElements / 100);
              if ((_this.pageGetCity + 1) <= pageCurrents) {

                  _this.pageGetCity = _this.pageGetCity + 1;
                  _this._getPageCity();
              }
          }
      );
  }



  seeCardInProcessLegend(): boolean {

    if (this.userSessionService.haveStatus() && !this.modalView && this.extendedUser.status !== STATUS.FAILED) {
      if (!this.extendedUser.cardNumber) {
        return true;
      }
    } else {
      return this.modalView && !this.bankPaymentMethod && !this.extendedUser.cardNumber;
    }
  }

  seeCardNumber(type): boolean {
    if (this.userSessionService.isPayee() && type === 'PAYEE') {
      return !!(this.userSessionService.haveStatus() && this.extendedUser.cardNumber);
    } else if (this.userSessionService.isSuperOrAdmin()  && type === 'ADMIN') {
      return !!(!this.bankPaymentMethod && this.extendedUser.cardNumber);
    }
  }

  openDialogFormPaymentCard(): void {
    const user = this.extendedUser;
    const dialogRef = this.dialog.open(PaymentCardFormComponent, {
      width: '1100px',
      data: { user }
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
    });
  }

  validacionInput(e: any, type: string) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    const numeros = type === 'numeros' ? "0123456789" :
    'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }


    // Acciones al cambiar tipo de cuenta (debito / credito)
    onchangeTipoCuenta(element : any) {
      this.bankAccountNumber.setValue('');
      let banco = +this.bank.value;
      if (element === 'AHO') { // saving acount
          if (banco === 200) { // BCP
            this.maxcuenta = 13;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if ( banco === 201) { // BBVA
            this.maxcuenta = 20;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if ( banco === 202) { // INTERBANK
            this.maxcuenta = 13;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if ( banco === 203) { // SCOTIABANK
            this.maxcuenta = 10;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          }
      } else { // cuenta corriente
        if (banco === 200) { // BCP
          this.maxcuenta = 14;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if ( banco === 201) { // BBVA
          this.maxcuenta = 20;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if ( banco === 202) { // INTERBANK
          this.maxcuenta = 13;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if ( banco === 203) { // SCOTIABANK
          this.maxcuenta = 10;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        }
      }
    }
  
    onchangeBanco(element : any) {
      this.bankAccountNumber.setValue('');
      let bancoID = element.slice(3).trim()
      this.dataFinder.idBank$.emit(+bancoID);
      this.maxcuenta = 20;
      let tipoCuenta = this.bankAccountType.value;
       if (tipoCuenta === 'AHO') { // saving acount
          if (bancoID === '200') { // BCP
            this.maxcuenta = 13;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if (bancoID === '201') { // BBVA
            this.maxcuenta = 20;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if ( bancoID === '202') { // INTERBANK
            this.maxcuenta = 13;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if ( bancoID === '203') { // SCOTIABANK
            this.maxcuenta = 10;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          }
      } else { // cuenta corriente
        if (bancoID === '200') { // BCP
          this.maxcuenta = 14;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if (bancoID === '201') { // BBVA
          this.maxcuenta = 20;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if ( bancoID === '202') { // INTERBANK
          this.maxcuenta = 13;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if ( bancoID === '203') { // SCOTIABANK
          this.maxcuenta = 10;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        }
      } 
    }

    verificaBanco(element: any) {
      if (element !== null && element !== undefined){
      let tipoCuenta = this.bankAccountType.value;
       if (tipoCuenta === 'AHO') { // saving acount
          if (element === 200) { // BCP
            this.maxcuenta = 13;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if (element === 201) { // BBVA
            this.maxcuenta = 20;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if ( element === 202) { // INTERBANK
            this.maxcuenta = 13;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          } else if ( element === 203) { // SCOTIABANK
            this.maxcuenta = 10;
            this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
          }
      } else { // cuenta corriente
        if (element === 200) { // BCP
          this.maxcuenta = 14;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if (element === 201) { // BBVA
          this.maxcuenta = 20;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if ( element === 202) { // INTERBANK
          this.maxcuenta = 13;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        } else if ( element === 203) { // SCOTIABANK
          this.maxcuenta = 10;
          this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)]);
        }
      } 
    } else {
      this.bankAccountNumber.setValidators([Validators.required, noWhitespaceValidator]);
    }
    }
}

