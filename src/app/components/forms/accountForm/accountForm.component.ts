import { TranslateService } from '@ngx-translate/core';
import {Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {ROLES, STATUS} from '../../../config/enums';
import {ExtendedUser, User} from '../../../config/interfaces';
import {UserService} from '../../../services/user/user.service';
import {UserSessionService} from '../../../services/user/userSession.service';
import {MatDialog} from '@angular/material';

export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  if (formGroup.get('newPassword').value === formGroup.get('newPassword2').value) {
    return null;
  } else {
    return {passwordMismatch: true};
  }
};

export const emailMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  if (formGroup.get('newEmail').value === formGroup.get('newEmailRepeat').value) {
    return null;
  } else {
    return {emailMismatch: true};
  }
};


@Component({
  selector: 'app-account-form',
  templateUrl: './accountForm.component.html',
  styleUrls: ['./accountForm.component.scss']
})
export class AccountFormComponent implements OnInit {

  @Input() extendedUser = {} as ExtendedUser;
  @Input() user = {} as User;

  @Output() showFileMsg = new EventEmitter<string>();
  @Output() showMsgChangePW = new EventEmitter<string>();
  @Output() showMsgChangeEmail = new EventEmitter<any>();

  accountForm: FormGroup;
  roles = ROLES;
  status = STATUS;
  file: File;

  imageUserUpload: File;
  imageUserTemp: string | ArrayBuffer;
  formatUserError = false;

  imageLogoUpload: File;
  imageLogoTemp: string | ArrayBuffer;
  formatLogoError = false;

  textSizeRatio = 3;
  seeChangeEmail = false;
  seeChangePassword = false;
  seeUpdateLogo = false;
  emailForm: FormGroup;
  passwordGroup: FormGroup;
  showDetails = false;
  showErrorMsg = false;
  samePassword = false;
  currentEmailError = false;
  loading = false;
  errorMessage: string;
  // pattern = new RegExp(/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,40}$/);

  constructor(public userService: UserService,
              public userSessionService: UserSessionService,
              public dialog: MatDialog,
              private translate: TranslateService,
              private formBuilder: FormBuilder,
              private cdRef: ChangeDetectorRef) {
                this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
  }

  // ACCOUNT GET FORM ELEMENTS
  get lastName() { return this.accountForm.get('lastName'); }
  get firstName() { return this.accountForm.get('firstName'); }
  get email() { return this.accountForm.get('email'); }


  // EMAIL GET
  get oldPassword() { return this.passwordGroup.get('oldPassword'); }
  get newPassword() { return this.passwordGroup.get('newPassword'); }
  get newPassword2() { return this.passwordGroup.get('newPassword2'); }
  get newEmail() { return this.emailForm.get('newEmail'); }
  get newEmailRepeat() { return this.emailForm.get('newEmailRepeat'); }

  ngOnInit() {
    this.initUser();
    this.setTextSizeRatio();
  }

  setTextSizeRatio() {
   this.textSizeRatio =  this.extendedUser.fullName.split(' ').length <= 3
      ? 3 : this.extendedUser.fullName.split(' ').length - 1;
  }

  initUser() {
    this.initExtendedUser(this.extendedUser);
  }

  initExtendedUser(extendedUser: ExtendedUser) {
    this.accountForm = new FormGroup({
      lastName: new FormControl(
        extendedUser && extendedUser.user
          ? extendedUser.user.lastName
          : (extendedUser && extendedUser.lastName1 ? extendedUser.lastName1 : ''), Validators.required),
      firstName: new FormControl(
        extendedUser && extendedUser.user
          ? extendedUser.user.firstName
          : (extendedUser && extendedUser.firstName1 ? extendedUser.firstName1 : ''), Validators.required),
      email: new FormControl(
        extendedUser && extendedUser.user
          ? extendedUser.user.login
          : (extendedUser && extendedUser.email ? extendedUser.email : ''), Validators.required),
    });
  }

  openChangePasswordHandler() {
    this.currentEmailError = false;
    this.seeUpdateLogo = false;
    this.seeChangeEmail = false;
    this.seeChangePassword = !this.seeChangePassword;
    this.initPasswordValues();
  }

  openChangeEmailHandler() {
    this.currentEmailError = false;
    this.seeUpdateLogo = false;
    this.seeChangePassword = false;
    this.seeChangeEmail = !this.seeChangeEmail;
    this.initEmailValues();
  }

  showHideUpdateLogoHandler() {
    if (this.seeUpdateLogo) {
      this.cleanLogoVariables();
    } else {
      setTimeout(() => {
        this.cleanLogoVariables();
      }, 200);
    }
    this.seeUpdateLogo = !this.seeUpdateLogo;
    this.hideChangeEmailOrPasswordHandler();
  }

  hideChangeEmailOrPasswordHandler() {
    this.currentEmailError = false;
    this.showErrorMsg = false;
    this.seeChangePassword = false;
    this.seeChangeEmail = false;
  }

  chooseImage(file: File, type: string) {
    this.formatUserError = false;
    this.formatLogoError = false;

    if (type === 'user') {
      if (!file) {
        this.imageUserUpload = null;
        return;
      }

      if (file.type.indexOf('image') < 0) {
        this.imageUserUpload = null;
        return;
      }

      this.imageUserUpload = file;

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => this.imageUserTemp = reader.result;

    } else {

      if (!file) {
        this.imageLogoUpload = null;
        this.imageLogoTemp = null;
        return;
      }

      if (file.size > 5000000) {
        file = null;
        this.cleanLogoVariables();
        this.showFileMsg.emit('size_error');
        return;
      }

      if (file.type.indexOf('image/jpeg') === 0 || file.type.indexOf('image/png') === 0 || file.type.indexOf('image/svg') === 0) {
        this.file = file;
        this.imageLogoUpload = file;

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => this.imageLogoTemp = reader.result;

      } else {
        file = null;
        this.cleanLogoVariables();
        this.formatLogoError = true;
        this.showFileMsg.emit('format_error');
        return;
      }

    }
  }

  cleanLogoVariables() {
    this.imageLogoTemp = null;
    this.imageLogoUpload = null;
    (document.getElementById('logo-input') as HTMLInputElement).value = '';
  }

  updateImage(type: string) {
    if (type === 'logo') {
      this.userService.updateLogoReSeller(this.extendedUser.id, this.file).subscribe(
        extendedUser => {
          this.extendedUser = extendedUser;
          this.cleanLogoVariables();
          this.showFileMsg.emit('upload_correct');
          this.userService.refreshReSellerLogoObservable(extendedUser);
        }
      );
    }
  }

  fileNameSelected(filePath): string {
    const errorMessageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.NO_FILE_WAS_CHOSEN');
    if (filePath) {
      const file = filePath.split('\\');
      return file[file.length - 1];
    } else {
      return errorMessageTranslate.value;
    }
  }

  initEmailValues() {
    this.emailForm = this.formBuilder.group({
      newEmail: ['', [Validators.required, Validators.email]],
      newEmailRepeat: ['', [Validators.required, Validators.email]],
    }, {validator: emailMatchValidator});
  }

  initPasswordValues() {
    this.passwordGroup = this.formBuilder.group({
      oldPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      newPassword2: ['', [Validators.required]]
    }, {validator: passwordMatchValidator});

    this.oldPassword.valueChanges.subscribe(() => this.showErrorMsg = false);
  }

  ValidateIsNotCurrentEmail() {
    this.currentEmailError = this.newEmail.value === this.user.email;
    this.onEmailInput();
  }

  onEmailInput() {
    if (this.emailForm.hasError('emailMismatch')) {
      this.newEmailRepeat.setErrors([{emailMismatch: true}]);
    } else {
      this.newEmailRepeat.setErrors(null);
    }
  }

  onPasswordInput() {
    if (this.passwordGroup.hasError('passwordMismatch')) {
      this.newPassword2.setErrors([{passwordMismatch: true}]);
    } else {
      this.newPassword2.setErrors(null);
    }
  }

  changePasswordAccount() {
    this.showErrorMsg = false;
    if (this.oldPassword.value === this.newPassword2.value) {
      this.samePassword = true;
    } else if (this.oldPassword.valid && this.newPassword2.valid) {
      this.loading = true;
      this.userService.accountChangePassword(this.oldPassword.value, this.newPassword2.value).subscribe(
        () => {
          this.loading = false;
          this.showMsgChangePW.emit('Password changed successfully');
          this.hideChangeEmailOrPasswordHandler();
        },
        error => {
          this.oldPassword.setErrors({incorrect: true});
          if (error.error.errorCode === 'User.004') {
            this.errorMessage = 'Invalid actual password';
          } else {
            this.errorMessage = error.error.title;
          }
          this.showErrorMsg = true;
          this.loading = false;
        }
      );
    } else {
      this.showErrorMsg = true;
    }
  }

  changeEmailAccount() {
    this.userService.changeExtendedUserEmail(this.user.email, this.newEmailRepeat.value).subscribe(() => {
      const obj = {
        title: 'Email change request',
        msg: 'You will received an email to your old email register'
      };
      this.showMsgChangeEmail.emit(obj);
      this.hideChangeEmailOrPasswordHandler();
    });
  }

  changeNewPassword(event) {
    this.samePassword = false;
    this.newPassword.setValue(event.target.value);
    this.onPasswordInput();
  }

  onStrengthChanged() {
    this.cdRef.detectChanges();
  }

  restoreLogoPMI() {
    const extUser = {
      id: this.extendedUser.id,
      imageAddressUrl: '',
    };

    this.userService.updateExtendedUser(extUser).subscribe(extendedUser => {
      this.extendedUser = extendedUser;
      this.userService.refreshReSellerLogoObservable(extendedUser);
    });
  }

  canSeeRestoreLogoButtonHandler(): boolean {
    let value = true;
    if (this.extendedUser.imageAddressUrl === null || this.extendedUser.imageAddressUrl === '') {
      value = false;
    }
    return value;
  }
}
