import { BankService } from './../../../services/bank/bank.service';
import { TranslateService } from '@ngx-translate/core';
import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { DialogRole, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { noWhitespaceValidator } from 'src/app/shared/functions';
import { MessageNotificationComponent } from '../../messageNotification/messageNotification.component';
@Component({
  selector: 'app-payment-card-form',
  templateUrl: './payment-card-form.component.html',
  styleUrls: ['./payment-card-form.component.scss']
})
export class PaymentCardFormComponent implements OnInit {
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  formData: FormGroup;
  formImages: FormGroup;
  day = [];
  yearArray = [];
  estadosReplucia = [
    { valor: "01", nombre: "AGUASCALIENTES" },
    { valor: "02", nombre: "BAJA CALIFORNIA" },
    { valor: "03", nombre: "BAJA CALIFORNIA SUR" },
    { valor: "04", nombre: "CAMPECHE" },
    { valor: "05", nombre: "COAHUILA DE ZARAGOZA" },
    { valor: "06", nombre: "COLIMA" },
    { valor: "07", nombre: "CHIAPAS" },
    { valor: "08", nombre: "CHIHUAHUA" },
    { valor: "09", nombre: "CIUDAD DE MEXICO" },
    { valor: "10", nombre: "DURANGO" },
    { valor: "11", nombre: "GUANAJUATO" },
    { valor: "12", nombre: "GUERRERO" },
    { valor: "13", nombre: "HIDALGO" },
    { valor: "14", nombre: "JALISCO" },
    { valor: "15", nombre: "MEXICO" },
    { valor: "16", nombre: "MICHOACAN DE OCAMPO" },
    { valor: "17", nombre: "MORELOS" },
    { valor: "18", nombre: "NAYARIT" },
    { valor: "19", nombre: "NUEVO LEON" },
    { valor: "20", nombre: "OAXACA" },
    { valor: "21", nombre: "PUEBLA" },
    { valor: "22", nombre: "QUERETARO" },
    { valor: "23", nombre: "QUINTANA ROO" },
    { valor: "24", nombre: "SAN LUIS POTOSI" },
    { valor: "25", nombre: "SINALOA" },
    { valor: "26", nombre: "SONORA" },
    { valor: "27", nombre: "TABASCO" },
    { valor: "28", nombre: "TAMAULIPAS" },
    { valor: "29", nombre: "TLAXCALA" },
    { valor: "30", nombre: "TLAXCALA" },
    { valor: "31", nombre: "YUCATAN" },
    { valor: "32", nombre: "ZACATECAS" }
  ];
  dataUser: any;
  nameFile = {
    identifacionName: "",
    proofAddressName: "",
    lifeTestName: ""
  }
  idNotify = "";
  keygen = "";
  isValid = "";
  deviceId = "";
  constructor(private cdRef: ChangeDetectorRef,
    private translate: TranslateService,
    private bankService: BankService,
    public dialogRef: MatDialogRef<PaymentCardFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogRole) { }

  ngOnInit() {
    this.dataUser = this.data;
    this.initFormData();
  }
  initFormData() {
    let day = null;
    let month = null;
    let year = null;
    let nombre2 = null;
    let apellido1 = null;
    let apellido2 = null;
    let genero = null;
    let numeroCelular = null;
    let dataProfileForBank = JSON.parse(sessionStorage.getItem('dataProfileForBank'))
    console.log(dataProfileForBank);
    if (dataProfileForBank !== null) {
      nombre2 = dataProfileForBank.firstName2;
      apellido1 = dataProfileForBank.lastName1;
      apellido2 = dataProfileForBank.lastName2;
      genero = dataProfileForBank.gender === "FEMALE" ? "F" : "M";
      numeroCelular = dataProfileForBank.noCeluar;
      day = dataProfileForBank.birthDate.split('-')[2];
      month = dataProfileForBank.birthDate.split('-')[1];
      year = dataProfileForBank.birthDate.split('-')[0];
    }
    if (this.dataUser.user.birthDate !== null) {
      const date = String(this.dataUser.user.birthDate).substring(0, 10).split('-');
      day = this.dataUser.user.birthDate === null ? '' : String(parseInt(date[2]));
      month = this.dataUser.user.birthDate === null ? '' : String(parseInt(date[1]));
      year = this.dataUser.user.birthDate === null ? '' : String(parseInt(date[0]));
    }
    console.log(this.dataUser.user);
    this.formData = new FormGroup({
      apellidoPaterno: new FormControl(this.dataUser.user.lastName1, [Validators.required, noWhitespaceValidator]),
      apellidoMaterno: new FormControl(apellido2, [Validators.required, noWhitespaceValidator]),
      nombres: new FormControl(this.dataUser.user.firstName1, [Validators.required, noWhitespaceValidator]),
      dia: new FormControl(day, Validators.required),
      mes: new FormControl(month, Validators.required),
      año: new FormControl(year, Validators.required),
      fechaNacimiento: new FormControl(null),
      lugarNacimiento: new FormControl(null, [Validators.required, noWhitespaceValidator]),
      sexo: new FormControl(genero, Validators.required),
      CURP: new FormControl(null, [Validators.required, Validators.maxLength(18), Validators.minLength(18)]),
      noCelular: new FormControl(numeroCelular, [Validators.required, noWhitespaceValidator, Validators.maxLength(10), Validators.minLength(10)]),
      correoElectronico: new FormControl(this.dataUser.user.email, [Validators.required, noWhitespaceValidator, Validators.email]),
      calle: new FormControl(null, [Validators.required, noWhitespaceValidator]),
      noInteriorxterior: new FormControl(null),
      noExterior: new FormControl(null, [Validators.required, noWhitespaceValidator]),
      codigoPostal: new FormControl(null, [Validators.required, noWhitespaceValidator]),
      colonia: new FormControl(null, [Validators.required, noWhitespaceValidator]),
      noIdentificacion: new FormControl(null, [Validators.required, noWhitespaceValidator]),
      fechaExpedicionID: new FormControl(null),
      diaFechaExpedicionID: new FormControl(null, Validators.required),
      mesFechaExpedicionID: new FormControl(null, Validators.required),
      anioFechaExpedicionID: new FormControl(null, Validators.required),
      fechaVencimientoID: new FormControl(null),
      diaVencimientoID: new FormControl(null, Validators.required),
      mesVencimientoID: new FormControl(null, Validators.required),
      anioVencimientoID: new FormControl(null, Validators.required),
      canal: new FormControl(11),
      RFC: new FormControl(null, [Validators.maxLength(13), Validators.minLength(12)]),
      referenciaRENAPO: new FormControl(null),
      referenciaLISTAS: new FormControl(null),
      IdNotify: new FormControl(null),
    });

    for (let i = 1; i <= 31; i++) {
      this.day.push(i);
    }

    for (let i = 1; i <= 31; i++) {
      this.day.push(i);
    }

    let d = new Date();
    let n = d.getFullYear();
    for (let i = n; i >= 1930; i--) {
      this.yearArray.push(i);
    }
    this.cdRef.detectChanges();
  }

  fileEvent(fileInput: Event, type: string) {
    let file = (<HTMLInputElement>fileInput.target).files[0];
    console.log(file.type);
    if (file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/png") {
      switch (type) {
        case 'identificacion':
          this.nameFile.identifacionName = file.name;
          break;
        case 'address':
          this.nameFile.proofAddressName = file.name;
          break;
        case 'testLife':
          this.nameFile.proofAddressName = file.name;
          break;
      }

    } else {
      let paynoFoundsees: any = this.translate.get('BUTTON_OPTION_INFO.ONLY_IMAGES');
      this.showMessage(paynoFoundsees.value, 'error');
    }
  }

  showMessage(message: string, type: string) {
    console.log(message, type);
    this.messageComponent.setTypeMessage(message, type);
  }

  setFecha(type: string) {
    let dateString;
    switch (type) {
      case 'fechaNacimiento':
        dateString = String(this.formData.controls['año'].value + this.formData.controls['mes'].value + this.formData.controls['dia'].value);
        if (this.formData.controls['año'].value !== null && this.formData.controls['mes'].value !== null && this.formData.controls['dia'].value !== null) {
          this.formData.controls['fechaNacimiento'].setValue(dateString);
        }
        break;
      case 'fechaVenciientoIdentificacion':
        dateString = String(this.formData.controls['anioVencimientoID'].value + this.formData.controls['mesVencimientoID'].value + this.formData.controls['diaVencimientoID'].value);
        if (this.formData.controls['anioVencimientoID'].value !== null && this.formData.controls['mesVencimientoID'].value !== null && this.formData.controls['diaVencimientoID'].value !== null) {
          this.formData.controls['fechaVencimientoID'].setValue(dateString);
        }
        break;
      case 'fechaExpedicionIdentificacion':
        dateString = String(this.formData.controls['anioFechaExpedicionID'].value + this.formData.controls['mesFechaExpedicionID'].value + this.formData.controls['diaFechaExpedicionID'].value);
        if (this.formData.controls['anioFechaExpedicionID'].value !== null && this.formData.controls['mesFechaExpedicionID'].value !== null && this.formData.controls['diaFechaExpedicionID'].value !== null) {
          this.formData.controls['fechaExpedicionID'].setValue(dateString);
        }
        break;
    }
  }

  generateCode() {
    /**
     * {
      "idNotify": "20210317105429",
      "keygen": "14DV8C1X"
      }
     */
    this.deviceId = this.generateRandomCode(8) + "-" + this.generateRandomCode(4) + "-" +
      this.generateRandomCode(4) + "-" + this.generateRandomCode(4) + "-" + this.generateRandomCode(12);
    const body = {
      notifyTo: this.formData.controls.noCelular.value,
      deviceId: this.deviceId
    }
    this.bankService.generateCode(body).subscribe(req => {
      this.idNotify = req.idNotify;
      this.keygen = req.keygen;
      this.authenticateCode();
    },
      err => {
        console.log('SEND USER BANK REGISTER', err);
      });
  }

  authenticateCode() {
    /**
     * {
        {"isValid": true}
      }
     */
    const body = {
      notifyId: this.idNotify, // Code given in Generate Code endpoint
      deviceId: this.deviceId,
      keygen: this.keygen, // Keygen given in Generate Code endpoint
      notifyTo: this.formData.controls.noCelular.value
    }
    this.bankService.generateCode(body).subscribe(req => {
      if (this.isValid) {
        this.sendUser();
      } else {
        this.messageComponent.setTypeMessage('Error al autentificar', 'error');
      }
    },
      err => {
        console.log('SEND USER BANK REGISTER', err);
      });
  }

  sendUser() {
    this.bankService.bankIntegrationFinitech(this.formData.value).subscribe(req => {
      let bodyFinish = {
        valid: true,
        userBanc: this.formData.value
      }
      if (req.codigoRespuesta === '00') {
        this.messageComponent.setTypeMessage(req.descripcionRespuesta, 'success');
        this.dialogRef.close(bodyFinish);
      } else {
        this.messageComponent.setTypeMessage(req.descripcionRespuesta, 'error');
        bodyFinish.valid = false;
      }
    },
      err => {
        console.log('SEND USER BANK REGISTER', err);
      });
  }

  generateRandomCode = (num) => {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let result1 = '';
    const charactersLength = characters.length;
    for (let i = 0; i < num; i++) {
      result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result1;
  }

  validacionInput(e: any, type: string) {
    const key = e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    const numeros = type === 'numeros' ? "0123456789" :
      'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key == especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }
}
