import { Component, OnInit, ChangeDetectorRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AUTOCOMPLETE_TYPE, DROWBOX_TYPE, ROLES } from '../../../config/enums';
import { ExtendedUser, User } from '../../../config/interfaces';
import { Observable } from 'rxjs';
import { UserService } from '../../../services/user/user.service';
import { UserSessionService } from '../../../services/user/userSession.service';
import { DataFinderService } from '../../../services/data-finder.service';
import { noWhitespaceValidator, determinateMaxDate } from '../../../shared/functions';
import { AutocompleteInputComponent } from '../../autocompleteInput/autocompleteInput.component';
import { TranslateService } from '@ngx-translate/core';

import { Subject } from 'rxjs';


@Component({
  selector: 'app-payee-company-form',
  templateUrl: './payeeCompanyForm.component.html',
  styleUrls: ['./payeeCompanyForm.component.scss']
})
export class PayeeCompanyFormComponent implements OnInit {

  changeCountry: Subject<any> = new Subject();

  @Input() extendedUser = {} as ExtendedUser;
  @Input() user: User;
  @Input() countryPostalId: number;
  @Input() modifyMode: boolean;
  @Input() payeeCompanyFormInvalid: boolean;
  @Input() modalView: boolean;

  @Output() setPostalCountrySelectedId = new EventEmitter<number>();

  datePickerConfig: Partial<BsDatepickerConfig>;
  maxDate = new Date();

  payeeCompanyForm: FormGroup;
  roles = ROLES;
  autocompleteType = AUTOCOMPLETE_TYPE;

  noPostalCountrySelected = false;
  noPostalCitySelected = false;
  noResidenceCountrySelected = false;

  countrys: any[];
  citiesDisabled: boolean = true;

  legalIdTypesArr = [];
  naturalIdTypesArr = [];
  postalCities = [];
  postalCountries = [];
  filteredPostalCountries: Observable<string[]> | any;
  filteredPostalCities: Observable<string[]> | any;
  residenceCountries = [];
  filteredResidenceCountries: Observable<string[]> | any;
  drownBoxType = DROWBOX_TYPE;
  public day = '';
  public month = '';
  public year = '';
  public yearArray = [];
  @ViewChild('CityAutocomplete', { static: true }) cityAutocomplete: AutocompleteInputComponent;

  constructor(public userService: UserService,
    public userSessionService: UserSessionService,
    private cdRef: ChangeDetectorRef,
    private dataFinder: DataFinderService,
    private translate: TranslateService) {
    this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
    this.loadDatePickerConfig();
  }

  // PAYEE COMPANY GET FORM ELEMENTS
  get legalRepresentativeLastName() { return this.payeeCompanyForm.get('legalRepresentativeLastName'); }
  get legalRepresentativeFirstName() { return this.payeeCompanyForm.get('legalRepresentativeFirstName'); }
  get legalRepresentativeEmail() { return this.payeeCompanyForm.get('legalRepresentativeEmail'); }
  get legalRepresentativeIdNumber() { return this.payeeCompanyForm.get('legalRepresentativeIdNumber'); }
  get legalRepresentativeIdType() { return this.payeeCompanyForm.get('legalRepresentativeIdType'); }
  get legalRepresentativeMobileNumber() { return this.payeeCompanyForm.get('legalRepresentativeMobileNumber'); }
  get legalRepresentativeResidenceAddress() { return this.payeeCompanyForm.get('legalRepresentativeResidenceAddress'); }
  get legalCPRepresentativeResidenceAddress() { return this.payeeCompanyForm.get('legalCPRepresentativeResidenceAddress'); }
  get legalRepresentativeCountryResidence() { return this.payeeCompanyForm.get('legalRepresentativeCountryResidence'); }
  get legalRepresentativeCityResidence() { return this.payeeCompanyForm.get('legalRepresentativeCityResidence'); }
  get companyTaxId() { return this.payeeCompanyForm.get('companyTaxId'); }
  get companyTaxIdType() { return this.payeeCompanyForm.get('companyTaxIdType'); }
  get companyPostalAddress() { return this.payeeCompanyForm.get('companyPostalAddress'); }
  get companyCpPostalAddress() { return this.payeeCompanyForm.get('companyCpPostalAddress'); }
  get companyConstitutionDate() { return this.payeeCompanyForm.get('companyConstitutionDate'); }
  get companyName() { return this.payeeCompanyForm.get('companyName'); }
  get companyCountryPostal() { return this.payeeCompanyForm.get('companyCountryPostal'); }
  get companyCityPostal() { return this.payeeCompanyForm.get('companyCityPostal'); }
  get companyPhoneNumber() { return this.payeeCompanyForm.get('companyPhoneNumber'); }

  ngOnInit() {
    this.initUser();
    this._loadCountry();
    this.setYear();
  }

  _loadCountry() {

    this.dataFinder.getCountriesForSelect().subscribe(
      data => {
        this.countrys = data.content;
      });
  }

  initUser() {
    this.initExtendedUser(this.extendedUser);
  }

  initExtendedUser(extendedUser: ExtendedUser) {
    let residenceCityFullAddress = null;
    let postalCityFullAddress = null;

    if (this.userSessionService.haveStatus()) {
      postalCityFullAddress = extendedUser.postalAddress ? extendedUser.postalAddress.split(' | ') : '';
      residenceCityFullAddress = extendedUser.residenceAddress ? extendedUser.residenceAddress.split(' | ') : '';
    }
    if (extendedUser && extendedUser !== null) {

      const date = String(extendedUser.birthDate).substring(0, 10).split('-');
      this.day = extendedUser.birthDate === null ? '' : String(parseInt(date[2]));
      this.month = extendedUser.birthDate === null ? '' : String(parseInt(date[1]));
      this.year = extendedUser.birthDate === null ? '' : String(parseInt(date[0]));
    }
    this.payeeCompanyForm = new FormGroup({
      // LEGAL REPRESENTATIVE INFO
      legalRepresentativeLastName: new FormControl(
        extendedUser && extendedUser.lastName1 ? extendedUser.lastName1 : this.user.lastName,
        [Validators.required, noWhitespaceValidator]),
      legalRepresentativeFirstName: new FormControl(
        extendedUser && extendedUser.firstName1 ? extendedUser.firstName1 : this.user.firstName,
        [Validators.required, noWhitespaceValidator]),
      legalRepresentativeEmail: new FormControl(
        extendedUser && extendedUser.email ? extendedUser.email : this.user.email, Validators.required),
      legalRepresentativeIdNumber: new FormControl(
        extendedUser && extendedUser.idNumber ? extendedUser.idNumber : '',
        [Validators.required, noWhitespaceValidator]),
      legalRepresentativeIdType: new FormControl(
        extendedUser && extendedUser.idType ? extendedUser.idType.id : null),
      legalRepresentativeMobileNumber: new FormControl(
        extendedUser && extendedUser.mobileNumber ? extendedUser.mobileNumber : '',
        [Validators.required, noWhitespaceValidator]),
      legalRepresentativeResidenceAddress: new FormControl(
        residenceCityFullAddress && residenceCityFullAddress[0] ? residenceCityFullAddress[0] : '',
        [Validators.required, noWhitespaceValidator]),
      legalCPRepresentativeResidenceAddress: new FormControl(
        residenceCityFullAddress && residenceCityFullAddress[2] ? residenceCityFullAddress[2] : ''),
      legalRepresentativeCountryResidence: new FormControl(
        extendedUser && extendedUser.residenceCountry ? extendedUser.residenceCountry.id : '', Validators.required),
      legalRepresentativeCityResidence: new FormControl(
        residenceCityFullAddress && residenceCityFullAddress[1] ? residenceCityFullAddress[1] : '',
        [Validators.required, noWhitespaceValidator]),

      // COMPANY INFO
      companyTaxId: new FormControl(
        extendedUser && extendedUser.taxId ? extendedUser.taxId : '',
        [Validators.required, noWhitespaceValidator]),
      companyTaxIdType: new FormControl(
        extendedUser && extendedUser.idTypeTaxId ? extendedUser.idTypeTaxId.id : null, Validators.required),
      companyPostalAddress: new FormControl(
        postalCityFullAddress && postalCityFullAddress[0] ? postalCityFullAddress[0] : '',
        [Validators.required, noWhitespaceValidator]),
      companyCpPostalAddress: new FormControl(
        postalCityFullAddress && postalCityFullAddress[1] ? postalCityFullAddress[1] : ''),
      companyConstitutionDate: new FormControl(
        extendedUser && extendedUser.birthDate ? new Date(extendedUser.birthDate) : '', Validators.required),
      companyName: new FormControl(
        extendedUser && extendedUser.company ? extendedUser.company : '',
        [Validators.required, noWhitespaceValidator]),
      companyCountryPostal: new FormControl(
        extendedUser && extendedUser.postalCountry ? extendedUser.postalCountry.id : '', Validators.required),
      companyCityPostal: new FormControl(
        extendedUser && extendedUser.postalCity ? extendedUser.postalCity : '', Validators.required),
      companyPhoneNumber: new FormControl(
        extendedUser && extendedUser.phoneNumber ? extendedUser.phoneNumber : '',
        [Validators.required, noWhitespaceValidator]),
    });
   
    if (extendedUser && extendedUser.postalCountry) {

      this.changeCountry.next(extendedUser.postalCountry.id);
    }
    this.setValidationForCountry(this.countryPostalId);
    setTimeout(() => {
      this.setCompanyTaxIdValidationForArgentina();
      if (this.modalView) {
        this.getIdTypes('LEGAL', false);
        this.payeeCompanyForm.disable();
      } else {
        if (this.userSessionService.haveStatus()) {
          if (extendedUser.confirmedProfile) {
            this.payeeCompanyForm.disable();
          }
          else {
            this.initPostalCityFilter();
          }
          this.getIdTypes('LEGAL', false);
        }
      }

      this.cdRef.detectChanges();
    }, 200);
  }

  initPostalCityFilter() {
    this.companyCityPostal.enable();
    // this.cityAutocomplete.initElementFilter();
  }

  resetCity() {
    this.companyCityPostal.reset();
    this.companyCityPostal.disable();
  }

  resetTaxIdType() {
    this.companyTaxIdType.reset();
    this.companyTaxIdType.disable();
  }

  resetTaxIdTypeAndCity() {
    this.noPostalCitySelected = false;
    this.resetCity();
    this.resetTaxIdType();
  }


  onChangeCompanyCountry(country) {
    if (country.target.value) {
      this.companyTaxIdType.reset();
      this.companyTaxId.reset();
      this.companyPostalAddress.reset();
      this.companyCpPostalAddress.reset();
      this.countryPostalId = country.target.value;
      this.companyCityPostal.enable();
      this.changeCountry.next(this.countryPostalId);
      this.setPostalCountrySelectedId.emit(this.countryPostalId);
      this.noPostalCountrySelected = false;
      this.initPostalCityFilter();
      this.setValidationForCountry(this.countryPostalId);
      this.getIdTypes('LEGAL', true);
    }
  }


  setCompanyTaxIdValidationForArgentina() {
    if (this.payeeCompanyForm.get('companyCountryPostal').value.id === 66) { // id = 66 ARGENTINA
      this.payeeCompanyForm.get('companyTaxId').setValidators([Validators.required,
      Validators.minLength(11), Validators.maxLength(11), Validators.pattern('^[0-9]+$')]);
      this.payeeCompanyForm.get('companyTaxId').updateValueAndValidity();
    }
  }

  onChangeLegalRepresentativeCountry() {
    this.noResidenceCountrySelected = false;
    this.legalCPRepresentativeResidenceAddress.reset();
    this.legalRepresentativeResidenceAddress.reset();
    this.legalRepresentativeCityResidence.reset();
    this.legalRepresentativeIdNumber.reset();
    // this.getIdTypes('NATURAL', true);
  }

  enablePayeeCompanyFormEditing() {
    this.payeeCompanyForm.enable({ emitEvent: false });
    if (this.modalView) {
    } else {
        this.companyTaxIdType.disable();
        this.companyCountryPostal.disable();
        this.legalRepresentativeCountryResidence.disable();
        if (this.canReadOnly('COMPANY_POSTAL')) {
          this.companyCityPostal.disable();
        }
    }
  }


  disablePayeeCompanyFormEditing() {
    this.payeeCompanyForm.disable({ emitEvent: false });
    this.noPostalCountrySelected = false;
    this.noPostalCitySelected = false;
    this.noResidenceCountrySelected = false;
  }

  getIdTypes(type: string, isChangeCountryCall: boolean) {
    this.dataFinder.getIdTypesByCountryIdAndType(this.countryPostalId, type).subscribe(
      data => {
        if (isChangeCountryCall) {

          if (type === 'NATURAL') {

            this.naturalIdTypesArr = data.content;
            if(this.countryPostalId == 66) {
              this.naturalIdTypesArr = this.naturalIdTypesArr.filter( item => item.id === 18);
            }
            this.legalRepresentativeIdType.enable();
            if (this.naturalIdTypesArr.length === 1) {
              this.legalRepresentativeIdType.setValue(this.naturalIdTypesArr[0].id);
              this.legalRepresentativeIdType.updateValueAndValidity();
            }
          } else {
            this.legalIdTypesArr = data.content;
            if(this.countryPostalId == 66) {
              this.legalIdTypesArr = this.legalIdTypesArr.filter( item => item.id === 18);
            }
            if (this.countryPostalId === 66) {//Argentina
              this.payeeCompanyForm.controls['companyTaxIdType'].clearValidators();
              this.payeeCompanyForm.controls['companyTaxIdType'].updateValueAndValidity();
            } else {
              this.payeeCompanyForm.controls['companyTaxIdType'].clearValidators();
              this.payeeCompanyForm.controls['companyTaxIdType'].setValidators([Validators.required]);
              this.payeeCompanyForm.controls['companyTaxIdType'].updateValueAndValidity();
            }
            this.companyTaxIdType.enable();
            if (this.legalIdTypesArr.length === 1) {
              this.companyTaxIdType.setValue(this.legalIdTypesArr[0].id);
              this.companyTaxIdType.updateValueAndValidity();
            }
          }
        } else {
          type === 'NATURAL' ? this.naturalIdTypesArr = data.content : this.legalIdTypesArr = data.content;
          if(type === 'NATURAL' && this.countryPostalId == 66) {
            this.naturalIdTypesArr = this.naturalIdTypesArr.filter( item => item.id === 18);
          }
          if(type === 'LEGAL' && this.countryPostalId == 66) {
            this.legalIdTypesArr = this.legalIdTypesArr.filter( item => item.id === 18);
          }
        }
      }
    );
  }

  loadDatePickerConfig() {
    this.datePickerConfig = Object.assign(
      {},
      {
        dateInputFormat: 'YYYY-MM-DD',
        dateInputFormatSend: 'YYYY-MM-DD'
      }
    );
    this.maxDate = determinateMaxDate();
  }

  canReadOnly(type: string): boolean {
    let value: boolean;
    switch (type) {
      case 'COMPANY_TAX':
        value = this.countryPostalId != 66 && this.userSessionService.isPayee() ?
        this.userSessionService.haveStatus() && this.extendedUser && this.extendedUser.confirmedProfile : false;
        break;
      case 'LEGAL_USER_DATA':
        this.modalView ? value = !this.modifyMode : value = true;
        break;
      case 'COMPANY_POSTAL':
      case 'COMPANY_NAME':
      case 'LEGAL_RESIDENCE':
      case 'LEGAL_ID_NUMBER':
      case 'COUNTRY_PAYEE':
        this.modalView
          ? value = !this.modifyMode
          : value = (this.userSessionService.haveStatus() && this.extendedUser && this.extendedUser.confirmedProfile);
        break;
      case 'COMPANY_DATE':
      case 'COMPANY_PHONE':
      case 'LEGAL_MOBILE_PHONE':
      case 'COMPANY_CP':
      case 'LEGAL_CP_RESIDENCE':
        this.modalView ?
          value = !this.modifyMode
          : value = (this.userSessionService.haveStatus() && this.extendedUser && this.extendedUser.confirmedProfile && !this.modifyMode);
        break;
    }
    return value;
  }
  //-----------------Inicia calendario-----------
  setBirth() {
    const dateString = this.year + "-" + this.month + "-" + this.day;
    this.payeeCompanyForm.controls['companyConstitutionDate'].setValue(new Date(dateString));
  }

  setYear() {
    var d = new Date();
    var n = d.getFullYear();
    for (var i = n; i >= 1930; i--) {
      this.yearArray.push(i);
    }
  }
//------------ Termina calendario ------------------------------
/**
 * Formulario para la validaciones del input
 * @param e la letra ingresada
 * @param type tipo de validacion que se requiere
 * @param addCHaracterSpecialc se agrega un caracter en especial para que lo valide
 * @returns true o flse
 */
  validacionInput(e: any, type: string, addCHaracterSpecialc = null) {
    const key = e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = type === 'numeros' ? "0123456789" :
      'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    if (addCHaracterSpecialc !== null) {
      numeros = numeros + addCHaracterSpecialc;
    }
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key == especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }

  setValidationForCountry(country)
  {
    //validacion de CP para paises rippei
    if(country == 66 || country == 74 || country == 77) {
      this.payeeCompanyForm.controls['legalCPRepresentativeResidenceAddress'].clearValidators();
      this.payeeCompanyForm.controls['legalCPRepresentativeResidenceAddress'].setValidators([Validators.required]);
      this.payeeCompanyForm.controls['legalCPRepresentativeResidenceAddress'].updateValueAndValidity();

      this.payeeCompanyForm.controls['companyCpPostalAddress'].clearValidators();
      this.payeeCompanyForm.controls['companyCpPostalAddress'].setValidators([Validators.required]);
      this.payeeCompanyForm.controls['companyCpPostalAddress'].updateValueAndValidity();
    } else {
      this.payeeCompanyForm.controls['legalCPRepresentativeResidenceAddress'].clearValidators();
      this.payeeCompanyForm.controls['legalCPRepresentativeResidenceAddress'].updateValueAndValidity();

      this.payeeCompanyForm.controls['companyCpPostalAddress'].clearValidators();
      this.payeeCompanyForm.controls['companyCpPostalAddress'].updateValueAndValidity();
    }
  }
}