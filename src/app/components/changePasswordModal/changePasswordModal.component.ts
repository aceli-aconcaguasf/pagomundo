import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {FormGroup, FormBuilder, Validators, ValidatorFn, ValidationErrors} from '@angular/forms';
import { UserService } from 'src/app/services/service.index';
import { MessageNotificationComponent } from '../components.index';

export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  if (formGroup.get('newPassword').value === formGroup.get('newPassword2').value) {
    return null;
  } else {
    return {passwordMismatch: true};
  }
};

@Component({
  selector: 'app-change-password-modal',
  templateUrl: './changePasswordModal.component.html',
  styleUrls: ['./changePasswordModal.component.scss']
})
export class ChangePasswordModalComponent implements OnInit {
  passwordGroup: FormGroup;
  showDetails = false;
  showErrorMsg = false;
  samePassword = false;
  loading = false;
  errorMessage: string;
  pattern = new RegExp(/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,40}$/);

  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor( public dialogRef: MatDialogRef<ChangePasswordModalComponent>,
               private formBuilder: FormBuilder,
               private cdRef: ChangeDetectorRef,
               public userService: UserService,
  ) { }

  get oldPassword() { return this.passwordGroup.get('oldPassword'); }
  get newPassword() { return this.passwordGroup.get('newPassword'); }
  get newPassword2() { return this.passwordGroup.get('newPassword2'); }

  ngOnInit() {
    this.passwordGroup = this.formBuilder.group({
      oldPassword: ['', [Validators.required, Validators.pattern(this.pattern)]],
      newPassword: ['', [Validators.required]],
      newPassword2: ['', [Validators.required]]
    }, {validator: passwordMatchValidator});
  }

  onPasswordInput() {
    if (this.passwordGroup.hasError('passwordMismatch')) {
      this.newPassword2.setErrors([{passwordMismatch: true}]);
    } else {
      this.newPassword2.setErrors(null);
    }
  }

  changePasswordAccount() {
    this.showErrorMsg = false;
    if (this.oldPassword.value === this.newPassword2.value) {
      this.samePassword = true;
    } else if (this.oldPassword.valid && this.newPassword2.valid) {
      this.loading = true;
      this.userService.accountChangePassword(this.oldPassword.value, this.newPassword2.value).subscribe(
        data => {
          this.loading = false;
          this.dialogRef.close();
        },
        error => {
          this.errorMessage = error.error.title;
          this.showErrorMsg = true;
          this.loading = false;
        }
      );
    } else {
      this.showErrorMsg = true;
    }
  }

  changeNewPassword(event) {
    this.samePassword = false;
    this.newPassword.setValue(event.target.value);
    this.onPasswordInput();
  }

  cancelChangePasswordButtonHandler() {
    this.dialogRef.close();
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  onStrengthChanged() {
    this.cdRef.detectChanges();
  }

  closeModal(): void {
    this.dialogRef.close();
  }

}
