import {Component} from '@angular/core';
import {Subject} from 'rxjs';
import {LoaderService} from '../../services/service.index';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
  show = false;
  isLoading: Subject<boolean> = this.loaderService.isLoading;
  constructor(private loaderService: LoaderService) {}

  changeStateShow(state: boolean) {
    this.show = state;
  }
}

