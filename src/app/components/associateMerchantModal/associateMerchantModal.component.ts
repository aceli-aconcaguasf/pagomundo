import {Component, Inject, ViewChild} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MerchantTableComponent} from '../components.index';

export interface AssociateData {
  extendedUser: any;
}

@Component({
  selector: 'app-associate-merchant-modal',
  templateUrl: './associateMerchantModal.component.html',
  styleUrls: ['./associateMerchantModal.component.scss']
})
export class AssociateMerchantModalComponent {

  filterQuery: string;
  merchantToAssociateList = [];
  changeClass:number;
  @ViewChild('MerchantTable', {static: false}) merchantTableComponent: MerchantTableComponent;

  constructor(
    public dialogRef: MatDialogRef<AssociateMerchantModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AssociateData) {
      let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
      this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  closeModal() {
    const result = {
      value : false,
    };
    this.dialogRef.close(result);
  }

  addFilterToTable(query) {
    this.filterQuery = query ? query : null;
  }

  addMerchantToList(merchantList) {
    this.merchantToAssociateList = merchantList;
  }

  clearData() {
    this.merchantToAssociateList = [];
    this.merchantTableComponent.clearMerchantList();
  }

  removeMerchantFromList(merchant) {
    this.merchantTableComponent.unCheckElementFromRemoveClick(merchant);
  }

  associateMerchant() {
    const result = {
      value: true,
      merchantToAssociate: this.merchantToAssociateList,
    };
    this.dialogRef.close(result);
  }

}
