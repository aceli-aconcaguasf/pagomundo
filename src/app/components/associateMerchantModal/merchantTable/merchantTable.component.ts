import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {merge, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatPaginator, MatSort} from '@angular/material';
import {DataFinderService, UserSessionService} from 'src/app/services/service.index';
import {TooltipPosition} from '@angular/material/tooltip';
import {ROLES, STATUSES_COLOR} from '../../../config/enums';

@Component({
  selector: 'app-merchant-table',
  templateUrl: './merchantTable.component.html',
  styleUrls: ['./merchantTable.component.scss']
})

export class MerchantTableComponent implements OnChanges {

  @Input() filterQuery: string;

  @Output() public addMerchantToList = new EventEmitter<any>();

  roles = ROLES;
  statusesColor = STATUSES_COLOR;

  uri = '/api/_search/merchants-not-related-to-resellers';
  query = '';

  // Table Variable
  totalElements = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  dataTable: any = [];
  pageSize = 10;
  isComponentInit = false;
  merchantToAssociateList = [];

  positionOptions: TooltipPosition =  'above';

  errorMessage = 'Error getting merchants';
  // @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private dataFinder: DataFinderService,
              public userSessionService: UserSessionService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.menuCollapse && this.isComponentInit) { return; } // Do not refresh table when expanding filter
    this.isComponentInit = true;
    this.initTable();
  }

  initTable() {
    this.setColumnNames();
    setTimeout(() => {
      this.loadDataTable();
    }, 0);
  }

  loadDataTable() {
    this.filterQuery ? this.query += ` AND ${this.filterQuery}` : this.query += '';

    // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // this.paginator.pageIndex = 0;

    // merge(this.sort.sortChange, this.paginator.page)
    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          // return this.dataFinder._search(this.uri, this.query, this.sort.active,
          //   this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.totalElements = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataTable = data;
        this.checkElementSelected();
      });
  }

  setColumnNames() {
    this.columnNames = {
      check: '', company: 'Company', fullName: 'Name',  loginName: 'Login Name | Email', country: 'Country'
    };
    this.columns = Object.keys(this.columnNames);
  }

  addCheckElement(merchant: any) {
    if ((document.getElementById(merchant.id) as HTMLInputElement).checked) {
      this.merchantToAssociateList.push(merchant);
    } else {
      for (const recipient of this.merchantToAssociateList) {
        if (recipient.id === merchant.id) {
          const index = this.merchantToAssociateList.indexOf(recipient);
          this.merchantToAssociateList.splice(index, 1);
        }
      }
    }
    // TODO
    this.addMerchantToList.emit(this.merchantToAssociateList);
  }

  clearMerchantList() {
    this.merchantToAssociateList = [];
    this.unCheckAllElementTable();
  }

  unCheckAllElementTable() {
    this.dataTable.forEach(elem => {
      (document.getElementById(elem.id) as HTMLInputElement).checked = false;
    });
  }

  checkElementSelected() {
    let cont = 0;
    for (const tableElement of this.dataTable) {
      for (const recipientElement of this.merchantToAssociateList) {
        if (tableElement.id === recipientElement.id) {
          cont++;
          setTimeout(() => {
            (document.getElementById(tableElement.id) as HTMLInputElement).checked = true;
          },  0);
        }
      }
    }
  }

  unCheckElementFromRemoveClick(merchant) {
    if (document.getElementById(merchant.id)) {
      (document.getElementById(merchant.id) as HTMLInputElement).checked = false;
    }
    const index = this.merchantToAssociateList.indexOf(merchant);
    if (index !== -1) {
      this.merchantToAssociateList.splice(index, 1);
    }
    this.addMerchantToList.emit(this.merchantToAssociateList);
  }
}
