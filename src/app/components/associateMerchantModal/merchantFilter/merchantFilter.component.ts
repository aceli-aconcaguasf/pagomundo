import {Component, EventEmitter, OnChanges, Output, SimpleChanges} from '@angular/core';
import {map, startWith} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {DataFinderService} from '../../../services/data-finder.service';
import {ROLES} from '../../../config/enums';

@Component({
  selector: 'app-merchant-filter-component',
  templateUrl: './merchantFilter.component.html',
  styleUrls: ['./merchantFilter.component.scss']
})
export class MerchantFilterComponent implements OnChanges {

  @Output() public applyFilterToTable = new EventEmitter<string>();

  roles = ROLES;

  countryForm: FormGroup;
  countries = [];
  countrySelected: any;
  filteredPayeeCountries: Observable<string[]> | any;

  userName = [];
  emailArr = [];
  countryFilter = [];

  showApplyFilter = false;
  collapse = false;

  filterName = '';
  nameFilter = '';
  nameInput = '';
  emailInput = '';
  changeClass:number;
  constructor(private dataFinder: DataFinderService) {
    this.countryForm = new FormGroup({
      country: new FormControl('', Validators.required),
    });
    this.initCountryFilter();
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  get CountryFilter() { return this.countryForm.get('country'); }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.roleFilterSelected) {
      this.resetVariable();
      this.cleanFilter();
      this.setFilterQuery();
    }
  }

  initCountryFilter() {
    this.filteredPayeeCountries = this.CountryFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => this._countryFilter(value))
      );
  }

  handleClickFilterToggle() {
    this.initCountryFilter();
    this.resetVariable();
    this.collapse = !this.collapse;
  }

  private _countryFilter(value: string): string[] {
    if (value) {
      if (value.length >= 2) {
        this.loadCountry(value);
      } else {
        this.countries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.countries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadCountry(name: string) {
    this.dataFinder.getCountriesForName(name).subscribe(data => {
        this.countries = data.content;
      }
    );
  }

  displayFnPayeeCountry(country): string {
    return country ? country.name : country;
  }

  onChangeCountry() {
    if (this.CountryFilter.value.id) {
      this.countrySelected = this.CountryFilter.value;
    } else {
      this.countrySelected = null;
    }
  }

  cleanFilter() {
    this.userName = [];
    this.countryFilter = [];
    this.emailArr = [];
    this.setFilterQuery();
  }

  resetVariable() {
    this.filterName = '';
    this.nameFilter = '';
    this.nameInput = '';
    this.emailInput = '';
    this.countrySelected = null;
    this.CountryFilter.reset();
    this.showApplyFilter = false;
  }

  applyFilter() {
    switch (this.filterName) {

      case 'country':
        if (this.countrySelected) {
          let notDuplicated = true;
          for (const country of this.countryFilter) {
            if (country.id === this.countrySelected.id) {
              notDuplicated = false;
            }
          }
          if (notDuplicated) {
            this.countryFilter.push(this.countrySelected);
          }
        }
        break;

      case 'name':
        const indexName = this.userName.indexOf(this.nameInput);
        if (indexName === -1) {
          this.userName.push(this.nameInput);
        }
        break;

      case 'email':
        const indexEmail = this.emailArr.indexOf(this.emailInput);
        if (indexEmail === -1) {
          this.emailArr.push(this.emailInput);
        }
        break;

      default:
        break;
    }
    this.setFilterQuery();
    this.resetVariable();
  }

  setFilterQuery() {
    let query = '';

    // COUNTRY
    if (this.countryFilter.length > 0) {
      if (this.countryFilter.length === 1) {
        query += `postalCountry.id:${this.countryFilter[0].id}`;
      } else {
        for (let index = 0; index < this.countryFilter.length; index++) {
          if (index === 0) {
            query += `( postalCountry.id:${this.countryFilter[index].id} OR `;
          } else {
            if (this.countryFilter.length - 1 !== index) {
              query += `postalCountry.id:${this.countryFilter[index].id} OR `;
            } else {
              query += `postalCountry.id:${this.countryFilter[index].id} )`;
            }
          }
        }
      }
    }
    // END COUNTRY

    // NAME
    if (this.userName.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.userName.length === 1) {
        query += `fullName:${this.userName[0]}`;
      } else {
        for (let index = 0; index < this.userName.length; index++) {
          if (index === 0) {
            query += `( fullName:${this.userName[index]} OR `;
          } else {
            if (this.userName.length - 1 !== index) {
              query += `fullName:${this.userName[index]} OR `;
            } else {
              query += `fullName:${this.userName[index]} )`;
            }
          }
        }
      }
    }
    // END NAME

    // EMAIL
    if (this.emailArr.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.emailArr.length === 1) {
        query += `email:${this.emailArr[0]}`;
      } else {
        for (let index = 0; index < this.emailArr.length; index++) {
          if (index === 0) {
            query += `( email:${this.emailArr[index]} OR `;
          } else {
            if (this.emailArr.length - 1 !== index) {
              query += `email:${this.emailArr[index]} OR `;
            } else {
              query += `email:${this.emailArr[index]} )`;
            }
          }
        }
      }
    }
    // END EMAIL
    this.applyFilterToTable.emit(query);
  }

  addFilter(event: any) {
    this.filterName = event.target.value;
    this.showApplyFilter = true;
    event.target.selectedIndex = 0;
  }

  resetFilter(typeName: string) {
    switch (typeName) {
      case 'country':
        this.countryFilter = [];
        break;
      case 'payee':
        this.userName = [];
        break;
      case 'email':
        this.emailArr = [];
        break;
      default:
        break;
    }
    this.applyFilter();
  }

  deleteFilter(typeName: string, name: any) {
    switch (typeName) {
      case 'userName':
        const indexPayee = this.userName.indexOf(name);
        this.userName.splice(indexPayee, 1);
        break;
      case 'country':
        const indexCountry = this.countryFilter.indexOf(name);
        this.countryFilter.splice(indexCountry, 1);
        break;
      case 'email':
        const indexEmail = this.emailArr.indexOf(name);
        this.emailArr.splice(indexEmail, 1);
        break;
      default:
        break;
    }
    this.setFilterQuery();
  }

}
