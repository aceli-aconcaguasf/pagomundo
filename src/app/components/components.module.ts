import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {MatDialogModule, MatCardModule, MatTableModule,
        MatPaginatorModule, MatSortModule, MatProgressSpinnerModule,
        MatFormFieldModule, MatInputModule} from '@angular/material';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {
  MessageNotificationComponent,
  MessageConfirmationComponent,
  NoteModalComponent,
  LoaderComponent,
  RelatedUserModalComponent,
  FundModalComponent,
  UserModalComponent,
  ChangePasswordModalComponent,
  CommissionModalComponent,
  ProfileModalComponent,
  RecipientNotificationModalComponent,
  RecipientFilterComponent,
  IssueStatusChangeModalComponent,
  IssueStatusReasonModalComponent,
  ConfirmationModalComponent,
  CardNumberModalComponent,
  EditExtUserModalComponent,
  AssociateMerchantModalComponent,
  MerchantFilterComponent,
  MerchantTableComponent,
  PayeeFormComponent,
  PayeeCompanyFormComponent,
  UserFormComponent,
  BankFormComponent,
  StatusReasonModalComponent,
  AccountFormComponent,
  AdminFormComponent,
  AutocompleteInputComponent,
  NicknameModalComponent,
  PaymentCardFormComponent,
  DrowBoxComponent
} from './components.index';

import {Filter, XCircle, PlusCircle, MinusCircle} from 'angular-feather/icons';
import {FeatherModule} from 'angular-feather';
import {MatPasswordStrengthModule} from '@angular-material-extensions/password-strength';
import {FormsModule} from '@angular/forms';
import {PipeModule} from '../pipes/pipe.module';

// DatePicker

import { BsDatepickerModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Select some icons (use an object, not an array)
const icons = {
  Filter,
  XCircle,
  PlusCircle,
  MinusCircle
};


@NgModule({
  declarations: [
    MessageNotificationComponent,
    MessageConfirmationComponent,
    NoteModalComponent,
    LoaderComponent,
    RelatedUserModalComponent,
    UserModalComponent,
    FundModalComponent,
    ChangePasswordModalComponent,
    CommissionModalComponent,
    ProfileModalComponent,
    RecipientNotificationModalComponent,
    RecipientFilterComponent,
    IssueStatusChangeModalComponent,
    IssueStatusReasonModalComponent,
    ConfirmationModalComponent,
    CardNumberModalComponent,
    EditExtUserModalComponent,
    AssociateMerchantModalComponent,
    MerchantFilterComponent,
    MerchantTableComponent,
    PayeeFormComponent,
    PayeeCompanyFormComponent,
    UserFormComponent,
    BankFormComponent,
    StatusReasonModalComponent,
    AccountFormComponent,
    AdminFormComponent,
    AutocompleteInputComponent,
    DrowBoxComponent,
    NicknameModalComponent,
    PaymentCardFormComponent,
    PaymentCardFormComponent,
    DrowBoxComponent
  ],
  exports: [
    MessageNotificationComponent,
    MessageConfirmationComponent,
    LoaderComponent,
    PayeeFormComponent,
    PayeeCompanyFormComponent,
    UserFormComponent,
    BankFormComponent,
    PaymentCardFormComponent,
    StatusReasonModalComponent,
    AccountFormComponent,
    AdminFormComponent,
    AutocompleteInputComponent,
    NicknameModalComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    PipeModule,
    MatDialogModule,
    MatCardModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    BsDatepickerModule.forRoot(),
    FeatherModule.pick(icons),
    MatPasswordStrengthModule.forRoot(),
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    })
  ]
})

export class ComponentsModule {
}

