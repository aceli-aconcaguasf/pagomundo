import {Component, OnInit} from '@angular/core';
import { Location } from '@angular/common';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {UserSessionService} from '../../services/user/userSession.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-message-confirmation',
  templateUrl: './messageConfirmation.component.html',
  styleUrls: ['./messageConfirmation.component.scss']
})
export class MessageConfirmationComponent implements OnInit {
  
  environment = environment;
  amount: number;
  balance: number;
  messageType: string;
  backgroundColor: string;
  variable: any;
  totalPayments: number;
  totalErrorPayments: number;
  extendedUser: any;
  changeClass:number;
  errorMessageText:string;
  typeMessageError:number;

  constructor(private router: Router,
              private location: Location,
              public userSessionService: UserSessionService,
              private translate: TranslateService) {
                this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    this.extendedUser = userSessionService.loadExtendedUser();
    this.balance = userSessionService.loadExtendedUser().balance;
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  ngOnInit() {
    this.variable = this.location.getState();
    this.messageType = this.variable.messageType;
    this.amount = this.variable.amount;
    this.balance = this.balance - this.amount;
    this.setTypeMessage();
  }

  setTypeMessage() {
    
    if (this.variable.messageType === 'success') {
      this.backgroundColor = '#a2e2c1';
    }
    else {

      let messageTranslate:any;

      if( this.variable.typeMessageError == 0 ){

        messageTranslate = this.translate.get( 'MESSAGE.PAYMENT_COULD_NOT_PROCESSED' );
      }
      else{

        messageTranslate = this.translate.get( 'MESSAGE.PAYMENT_EXCEDED' );
      }
      
      this.errorMessageText = messageTranslate.value;

      this.backgroundColor = '#FFBB33';
      this.totalPayments = this.variable.allPayment;
      this.totalErrorPayments = this.variable.error;
    }
  }

  handleClickPayment() {
    this.router.navigate(['/payment']);
  }
}