import { Component, ChangeDetectorRef, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { DataFinderService, RequestService, UserSessionService } from '../../services/service.index';
import { map, startWith } from 'rxjs/operators';
import { AUTOCOMPLETE_TYPE, ROLES } from '../../config/enums';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-autocomplete-input',
  templateUrl: './autocompleteInput.component.html'
})
export class AutocompleteInputComponent implements OnInit {

  @Input() changeCountry: Subject<any>;
  @Input() inputForm: FormGroup;
  @Input() inputFormControlName: string;
  @Input() elementPlaceholder: string;
  @Input() value: string;
  @Input() type: string;
  @Input() maxLenght: number = 50;
  @Input() id: string;
  @Input() autoInit: boolean;
  @Input() extendedUserConfirmedProfile: boolean;
  @Input() isModifyMode: boolean;
  @Input() isModalView: boolean;
  @Input() readonly: boolean;
  @Input() invalidInput: boolean;
  @Input() noElementSelect: boolean;
  @Input() processPayment: boolean;
  @Input() bankPaymentMethod: boolean;

  @Output() elementChange = new EventEmitter<any>();
  @Output() setNoElementSelected = new EventEmitter<boolean>();

  // PAYEE COUNTRY FORM
  @Output() resetDependencies = new EventEmitter<any>();
  @Output() setCountryPostalId = new EventEmitter<number>();

  // PAYEE COUNTRY SEARCH
  @Output() resetRelatedVariables = new EventEmitter<any>();

  // CITY
  @Input() countryId: number;

  elements = [];
  typeAutocomplete = AUTOCOMPLETE_TYPE;
  filteredElements: Observable<string[]> | any;
  pageGetCity = 0;
  cities = [];
  citiesId = [];
  initCitys = 0;
  initCountry: number;
  bandInitDefatulValue = false;
  dataName = '';
  countryPostalIdAux = null;
  public dataComplete = null;
  noFindCity = false;
  constructor(private cdRef: ChangeDetectorRef,
    private userSessionService: UserSessionService,
    public requestService: RequestService,
    private dataFinder: DataFinderService) { }

  get autocomplete() { return this.inputForm.get(this.inputFormControlName); }

  ngOnInit(): void {
    if (this.type === AUTOCOMPLETE_TYPE.CITY) {
      this.dataName = this.autocomplete.value ? this.autocomplete.value.name : '';
    } 
    if (this.autoInit) {
      this.initElementFilter();
    }

    if (this.changeCountry) {
      this.changeCountry.subscribe(event => {
        this.countryId = event;
        if (this.type === AUTOCOMPLETE_TYPE.CITY && !this.userSessionService.isPayee()
        && Number(this.countryPostalIdAux) !== Number(this.countryId)) {
          this.dataName = '';
          this.countryPostalIdAux = this.countryId;
          this.autocomplete.reset();
        } 
      });
    }
  }

  initElementFilter() {
    this.filteredElements = this.autocomplete.valueChanges
      .pipe(
        startWith(''),
        map(value => this._elementFilter(value))
      );
  }

  private _elementFilter(value: string): string[] {
    if (typeof value === 'object') {
      return;
    }
    // console.log( "_elementFilter()" );
    switch (this.type) {

      case AUTOCOMPLETE_TYPE.COUNTRY:
        // console.log( "_elementFilter() => AUTOCOMPLETE_TYPE.COUNTRY" );
        if (this.isModalView && this.isModifyMode && this.autocomplete.dirty) {
          this.setNoElementSelected.emit(true);
          // this.resetDependencies.emit(''); resetIdType();
        } else if (!this.isModalView) {

          if ((!this.userSessionService.haveStatus() && this.autocomplete.dirty)
            || (this.userSessionService.haveStatus() && this.extendedUserConfirmedProfile)) {
            this.setNoElementSelected.emit(true);

          } else if (this.userSessionService.haveStatus() &&
            !this.extendedUserConfirmedProfile && this.autocomplete.dirty) {
            this.setNoElementSelected.emit(true);
          }
        }
        break;
      case AUTOCOMPLETE_TYPE.PAYEE_COUNTRY:
        // console.log( "_elementFilter() => AUTOCOMPLETE_TYPE.PAYEE_COUNTRY" );
        if (value && this.autoInit) {
          this.resetRelatedVariables.emit('');
        } else {

          if (this.isModalView && this.isModifyMode && this.autocomplete.dirty) {
            this.resetValuesForElement();

          } else if (!this.isModalView) {

            if (!this.userSessionService.haveStatus() || (this.userSessionService.haveStatus() && this.extendedUserConfirmedProfile)) {
              this.setCountryPostalId.emit(0);
              this.resetDependencies.emit('');

              // first admission direct approval
            } else if (this.userSessionService.haveStatus() && !this.extendedUserConfirmedProfile && this.autocomplete.dirty) {
              this.resetValuesForElement();
            }
          }
        }
        break;

      case AUTOCOMPLETE_TYPE.MERCHANT:
        if (value) {
          this.elementChange.emit(null);
        }
        break;

      default:
        break;
    }

    if (value) {
      this.loadElements(value);


      const filterValue: any = value.toString().toLowerCase();
      let valueLookingFor;
      if (this.type === AUTOCOMPLETE_TYPE.MERCHANT) {
        valueLookingFor = this.elements.find(elem => elem.company.toString().toLowerCase() === value.toString().toLowerCase());
      }
      else {

        valueLookingFor = this.elements.find(elem => elem.name.toString().toLowerCase() === value.toString().toLowerCase());
      }

      if (valueLookingFor) {
        this.autocomplete.setValue(valueLookingFor);
        this.onChangeElement();
      }

      this.cdRef.detectChanges();

      return this.type === AUTOCOMPLETE_TYPE.MERCHANT
        ? this.elements.filter(elem => elem.company.toString().toLowerCase().includes(filterValue))
        : this.elements.filter(elem => elem.name.toString().toLowerCase().includes(filterValue));
    }
  }

  resetValuesForElement() {
    this.setNoElementSelected.emit(true);
    this.setCountryPostalId.emit(0);
    this.resetDependencies.emit('');
  }

  loadElements(name: string) {
    // console.info( "AC::loadElements()" );
    switch (this.type) {
      case AUTOCOMPLETE_TYPE.COUNTRY:

        // console.log("loadElements() => AUTOCOMPLETE_TYPE.COUNTRY");
        this.dataFinder.getCountriesForName(name).subscribe(
          data => {
            this.elements = data.content;
          });
        break;

      case AUTOCOMPLETE_TYPE.PAYEE_COUNTRY:
        this.dataFinder.getCountriesForPayeeAndName(name).subscribe(
          data => {
            this.elements = data.content;
            if (this.processPayment && !this.bankPaymentMethod) {
              this.elements = this.elements.filter(elem => elem.id !== "");
            }
          });
        break;

      case AUTOCOMPLETE_TYPE.CITY:
        this.elements = [];
        for (const element of this.cities) {

          this.elements.push(element);
        }
        break;

      case AUTOCOMPLETE_TYPE.MERCHANT:
        if (this.userSessionService.isSuperOrAdmin()) {
          this.dataFinder.getMerchantForCompany(name).subscribe(
            data => {
              this.elements = data.content;
            });
        } else if (this.userSessionService.isReSeller()) {
          this.dataFinder.getMerchantOfReSellerForCompoany(this.userSessionService.loadExtendedUser().user.id, name).subscribe(
            data => {
              this.elements = [];
              for (const userRelated of data.content) {
                this.elements.push(userRelated.extendedUser);
              }
            });
        }
        break;
      default:
        break;
    }
  }

  displayFnAutocomplete(elem): string {
    return elem ? (elem.company || elem.name) : elem;
  }

  onChangeElement() {
    if (this.autocomplete.value.id) {
      this.elementChange.emit(this.autocomplete.value);
    }
  }

  getStyle(): string {
    return this.autoInit
      ? this.processPayment ? 'short-filter-input' : 'filter-input'
      : 'form-control';
  }

  validacionInput(e: any) {
    const key = e.keyCode || e.which;
    const teclado = String.fromCharCode(key).toLowerCase();
    const typeSelect = 'abcdefghijklmnñopqrstuvwxyz ';
    const especiales = ['8', '164', '165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key == especiales[i]) {
        teclado_especial = true;
      }
    }

    if (typeSelect.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    } else {
       if ( this.dataName.length > 2) {
        this.getDataAutocompleteService();
      }
    }
  }

  getDataAutocompleteService() {
    if (this.type === AUTOCOMPLETE_TYPE.CITY) {
      if (this.autocomplete.value === '') {
        this.noFindCity = true;
        this.noElementSelect = true;
      } else {
        this.noFindCity = false;
        this.noElementSelect = false;
      }
      const city = this.dataName;
      this.dataComplete = null;
      if (city.length > 2) {
        const url = '/api/cities/citiesbycountry';
        const body = {
          subStr: city,
          countryId: this.countryId
        };
        this.requestService.postRequest(url, body).subscribe(req => {
          const request: any = req;
          this.dataComplete = request;
          if (request.length === 0 || this.autocomplete.value === null) {
            this.noFindCity = true;
            this.noElementSelect = true;
          } 
        });
      } else {
        this.dataComplete = null;
      }
    }
  }

  setDataInput(data) {
    this.autocomplete.setValue(data);
    this.dataName = data.name;
    this.onChangeElement();
    this.dataComplete = null;
    this.noFindCity = false;
    this.noElementSelect = false;
  }

}