import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ExtendedUser} from '../../config/interfaces';
import {ROLES} from '../../config/enums';

export interface DialogData {
  extendedUser: ExtendedUser;
  adminView: boolean;
}

interface ResultData {
  extendedUser: ExtendedUser;
  typeResult: string;
}

@Component({
  selector: 'app-card-number-modal',
  templateUrl: './cardNumberModal.component.html',
  styleUrls: ['./cardNumberModal.component.scss']
})
export class CardNumberModalComponent {

  extendedUser: ExtendedUser;
  roles = ROLES;
  changeClass:number;
  constructor(
    public dialogRef: MatDialogRef<CardNumberModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.extendedUser = this.data.extendedUser;
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  closeModal(): void {
    const result = {} as ResultData;
    result.extendedUser = this.data.extendedUser;
    result.typeResult = 'CANCEL';
    this.dialogRef.close(result);
  }

  handlerContinueButton() {
    const result = {} as ResultData;
    result.extendedUser = this.data.extendedUser;
    result.typeResult = 'CONTINUE';
    this.dialogRef.close(result);
  }

}
