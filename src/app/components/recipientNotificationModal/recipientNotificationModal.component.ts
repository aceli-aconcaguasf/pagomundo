import {Component, Inject, ViewChild, OnInit} from '@angular/core';
import {ExtendedUser, RecipientData} from '../../config/interfaces';
import {UserSessionService} from 'src/app/services/service.index';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TableComponent} from '../../shared/table/table.component';
import {ROLES, GROUP_RECEIVER} from '../../config/enums';
import {MessageNotificationComponent} from '../messageNotification/messageNotification.component';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

export interface RecipientModalData {
  list: any[];
  roleRecipientList: string;
  receiverGroupType: string;
  extendedUserData: any;
}

@Component({
  selector: 'app-issue-modal',
  templateUrl: './recipientNotificationModal.component.html',
  styleUrls: ['./recipientNotificationModal.component.scss']
})

export class RecipientNotificationModalComponent implements OnInit {

  extendedUser: ExtendedUser;
  roles = ROLES;
  role: string;

  roleAllUserSelected: string;
  roleAllUserSelectedTraductor: string;
  filterQuery: string;

  recipientList = [];
  recipientData = {} as RecipientData;
  quantityRecipients = 0;
  relatedUser: ExtendedUser;

  seeRoleAllUserSelected = false;
  seeRelatedUserRecipient = false;
  relatedUserFrom: string;

  // Toggle
  checked = false;
  disabled = false;

  // Radio Button
  roleOption: string;
  options = [];
  keyLanguage$: Observable<any> = this.store.select('language');
  changeClass:any;
  @ViewChild('RecipientTable', {static: false}) tableComponent: TableComponent;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor(public dialogRef: MatDialogRef<RecipientNotificationModalComponent>,
              @Inject(MAT_DIALOG_DATA) public dataModal: RecipientModalData,
              public userSessionService: UserSessionService,
              private translate: TranslateService,
              private store: Store<language>) {
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
    this.extendedUser = this.userSessionService.loadExtendedUser();
    this.role = this.userSessionService.loadRole();
    setTimeout(() => {
      this.initRoleOption();
      this.initRadioButtonOptions();
    }, 500);
    this.keyLanguage$.subscribe(item => {
      this.initRoleOption();
      this.initRadioButtonOptions();
      const language: any = item
      this.translate.use(language.key);
    });
  }

  ngOnInit(): void {
    this.initRadioButtonOptions();
    this.initializeData();
  }

  initRoleOption() {
    this.userSessionService.isSuperAdmin() ? this.roleOption = ROLES.ADMIN
      : this.userSessionService.isAdmin() ? this.roleOption = ROLES.RESELLER
      // tslint:disable-next-line:no-unused-expression
      : this.userSessionService.isReSeller() ? this.roleOption = ROLES.MERCHANT : null;
  }

  initRadioButtonOptions() {
    let admin:any = this.translate.get('REPORTS_MESSAGES.ADMIN');
    let reseller:any = this.translate.get('TEXT.RESELLER');
    let merchant:any = this.translate.get('DATA_TABLE.MERCHANT_COMPANY');
    let payye:any = this.translate.get('REPORTS_MESSAGES.PAYEE_BUSINESS_ACCOUNT');
    if (this.userSessionService.isSuperAdmin()) {
      this.  options = [
        { name: admin.value, role: this.roles.ADMIN, checked: true },
        { name: reseller.value, role: this.roles.RESELLER, checked: false},
        { name: merchant.value, role: this.roles.MERCHANT, checked: false},
        { name: payye.value, role: this.roles.PAYEE, checked: false},
      ];
    } else if (this.userSessionService.isAdmin()) {
      this.options = [
        { name: reseller.value, role: this.roles.RESELLER, checked: false},
        { name: merchant.value, role: this.roles.MERCHANT, checked: false},
        { name: payye.value, role: this.roles.PAYEE, checked: false},
      ];
    }
  }

  initializeData() {
    switch (this.dataModal.receiverGroupType) {
      case GROUP_RECEIVER.ALL_ADMINS:
        this.roleOption = ROLES.ADMIN;
        setTimeout(() => {
          this.allUsers(true);
        }, 500);
        break;
      case GROUP_RECEIVER.ALL_MERCHANTS:
        this.roleOption = ROLES.MERCHANT;
        setTimeout(() => {
          this.allUsers(true);
        }, 500);
        break;
      case GROUP_RECEIVER.ALL_PAYEES:
        this.roleOption = ROLES.PAYEE;
        setTimeout(() => {
          this.allUsers(true);
        }, 500);
        break;
      case GROUP_RECEIVER.ALL_MERCHANTS_FROM_RESELLER:
        this.roleOption = ROLES.RESELLER;
        setTimeout(() => {
          this.tableComponent.addAllRelatedUser(this.dataModal.extendedUserData);
        }, 0);
        break;
      case GROUP_RECEIVER.ALL_PAYEES_FROM_MERCHANT:
        this.roleOption = ROLES.MERCHANT;
        setTimeout(() => {
          this.tableComponent.addAllRelatedUser(this.dataModal.extendedUserData);
        }, 0);
        break;
      case GROUP_RECEIVER.ALL_MERCHANTS_FROM_PAYEE:
        this.roleOption = ROLES.PAYEE;
        setTimeout(() => {
          this.tableComponent.addAllRelatedUser(this.dataModal.extendedUserData);
        }, 0);
        break;
      case GROUP_RECEIVER.RECEIVERS:
        this.roleOption = this.dataModal.roleRecipientList;
        this.roleOption === ROLES.PAYEE && this.userSessionService.isAdmin() ? this.checked = true : this.checked = false;
        setTimeout(() => {
          this.recipientList = this.dataModal.list;
          this.quantityRecipients = this.recipientList.length;
          this.tableComponent.recipientList = this.dataModal.list;
        }, 0);
        break;
    }
  }

  addRecipientToNotification() {
    this.recipientData.withValue = true;

    if (this.seeRoleAllUserSelected) {

      switch (this.role) { // For role logged
        case ROLES.SUPER_ADMIN:
        case ROLES.ADMIN:

          switch (this.roleOption) { // For filter role
            case ROLES.ADMIN:
              this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_ADMINS;
              break;
            case ROLES.RESELLER:
              this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_RESELLERS;
              break;
            case ROLES.MERCHANT:
              this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_MERCHANTS;
              break;
            case ROLES.PAYEE:
              this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_PAYEES;
              break;
            default:
              break;
          }
          break;

        case ROLES.RESELLER:
          this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_MERCHANTS_FROM_RESELLER;
          this.recipientData.user = this.extendedUser;
          break;

        case ROLES.MERCHANT:
          this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_PAYEES_FROM_MERCHANT;
          this.recipientData.user = this.extendedUser;
          break;

        case ROLES.PAYEE:
          this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_MERCHANTS_FROM_PAYEE;
          this.recipientData.user = this.extendedUser;
          break;
      }

    } else if (this.seeRelatedUserRecipient && this.relatedUserFrom === ROLES.RESELLER) {
      this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_MERCHANTS_FROM_RESELLER;
      this.recipientData.user = this.relatedUser;

    } else if (this.seeRelatedUserRecipient && this.relatedUserFrom === ROLES.MERCHANT) {
      this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_PAYEES_FROM_MERCHANT;
      this.recipientData.user = this.relatedUser;

    } else if (this.seeRelatedUserRecipient && this.relatedUserFrom === ROLES.PAYEE) {
      this.recipientData.groupReceivers = GROUP_RECEIVER.ALL_MERCHANTS_FROM_PAYEE;
      this.recipientData.user = this.relatedUser;

    } else {
      this.recipientData.groupReceivers = GROUP_RECEIVER.RECEIVERS;
      this.recipientData.recipients = this.recipientList;
      this.recipientData.roleRecipientList = this.roleOption;
    }

    this.dialogRef.close(this.recipientData);
  }

  closeModal(): void {
    this.recipientData.withValue = false;
    this.dialogRef.close(this.recipientData);
  }

  // FOR RESELLER
  // selectRoleFilter(event) {
  //   event.checked ? this.roleOption = ROLES.PAYEE : this.roleOption = ROLES.MERCHANT;
  //   this.resetVariable();
  // }

  addRecipientToList(recipientList) {
    this.recipientList = recipientList;
    this.quantityRecipients = this.recipientList.length;
    this.seeRelatedUserRecipient = false;
    this.seeRoleAllUserSelected = false;
  }

  addAllRelatedUserRecipient(relatedUserRecipient) {
    this.relatedUser = relatedUserRecipient.relatedUser;
    this.quantityRecipients = relatedUserRecipient.totalRelatedUser;
    this.relatedUserFrom = relatedUserRecipient.userFrom;
    this.seeRoleAllUserSelected = false;
    this.seeRelatedUserRecipient = true;
  }

  addFilterToTable(query) {
    this.filterQuery = query ? query : '';
  }

  removeRecipientFromList(recipient) {
    this.tableComponent.unCheckElement(recipient);
  }

  allUsers(value) {
    let admin:any = this.translate.get('TEXT.ADMINISTRATORS');
    let reseller:any = this.translate.get('TEXT.RESELLER_LOWERCASE');
    let merchant:any = this.translate.get('TEXT.MERCHANTS');
    let payees:any = this.translate.get('TEXT.PAYEES');
    if (value) {
      this.quantityRecipients = this.tableComponent ? this.tableComponent.totalElements : 0;
      switch (this.role) {
        case ROLES.SUPER_ADMIN:
        case ROLES.ADMIN:
          switch (this.roleOption) {
            case ROLES.ADMIN:
              this.roleAllUserSelected = admin;
              this.roleAllUserSelectedTraductor = admin.value;
              break;
            case ROLES.RESELLER:
              this.roleAllUserSelected = reseller.value;
              break;
            case ROLES.MERCHANT:
              this.roleAllUserSelected = merchant.value;
              break;
            case ROLES.PAYEE:
              this.roleAllUserSelected = payees.value;
              break;
          }
          break;
        case ROLES.RESELLER:
          this.roleAllUserSelected = merchant.value;
          break;
        case ROLES.MERCHANT:
          this.roleAllUserSelected = payees.value;
          break;
        case ROLES.PAYEE:
          this.roleAllUserSelected = merchant.value;
          break;
      }
    }
    this.seeRoleAllUserSelected = value;
    this.seeRelatedUserRecipient = false;
  }

  resetVariable() {
    this.seeRelatedUserRecipient = false;
    this.seeRoleAllUserSelected = false;
    this.quantityRecipients = 0;
    this.recipientList = [];
  }

  clearData() {
    this.resetVariable();
    this.tableComponent.clearRecipientList();
  }

  setAllMessageButton(): string {
    let admin:any = this.translate.get('BUTTON_OPTION_INFO.ALL');
    let reseller:any = this.translate.get('BUTTON_OPTION_INFO.ALL_PAYEES');
    let merchanALL:any = this.translate.get('BUTTON_OPTION_INFO.ALL_MERCHANTS');
    switch (this.role) {
      case ROLES.SUPER_ADMIN:
      case ROLES.ADMIN:
        return admin.value;
      case ROLES.MERCHANT:
        return reseller.value;
      case ROLES.RESELLER:
      case ROLES.PAYEE:
        return merchanALL.value;
    }
  }

  showMsg(msg) {
    this.showMessage(msg.text, msg.type);
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

}
