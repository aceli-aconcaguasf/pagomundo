import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../services/service.index';
import {MessageNotificationComponent} from '../components/messageNotification/messageNotification.component';
import {FirstLoginData} from '../config/interfaces';
import {TranslateService} from '@ngx-translate/core';

export enum TYPE_LANDING {
 CREATE_ACCOUNT = 'CREATE_ACCOUNT',
 CREATE_ACCOUNT_SIGN_IN = 'CREATE_ACCOUNT_SIGN_IN',
 CHANGE_EMAIL_SUCCESS = 'CHANGE_EMAIL_SUCCESS',
}

export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  if (formGroup.get('password').value === formGroup.get('password2').value) {
    return null;
  } else {
    return {passwordMismatch: true};
  }
};

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  minPw = 8;
  formGroup: FormGroup;
  firstLoginData = {} as FirstLoginData ;
  typeLanding = TYPE_LANDING;
  landingType: string;

  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  signIn = false;
  showDetails = false;
  loading = false;
  key: string;
  logoImg:string;
  changeClass:number = 0;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private cdRef: ChangeDetectorRef,
              private formBuilder: FormBuilder,
              public userService: UserService,
              private translate: TranslateService) {

                let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
                if( Pm14m3r1c4sCh4ng3Cl4ss == "1" ){
                  this.logoImg = "assets/img/logo-pago-mundo.png";
                  this.changeClass = 1;
                }
                else{
                  this.logoImg = "assets/img/logo.svg";
                  this.changeClass = 0;
                }

    const urlSegmentArray = this.route.snapshot.url;
    this.key = this.route.snapshot.paramMap.get('key');
    this.initPage(urlSegmentArray[0].path);
  }

  initPage(path: string) {

    if (path === 'createAccount') {
      this.landingType = TYPE_LANDING.CREATE_ACCOUNT;
      this.userService.verifyKey(this.key).subscribe(
        data => {
          this.translate.use(data.langKey);
          this.firstLoginData = data;
        },
        () => {
          this.router.navigate(['/login']).then();
        }
      );
    }
    else if (path === 'changeEmail') {
      this.userService.changeExtendedUserEmailValidation(this.key).subscribe(
        (data) => {
          this.translate.use(data.langKey);
          this.landingType = TYPE_LANDING.CHANGE_EMAIL_SUCCESS;
        },
        () => {
          this.router.navigate(['/login']).then();
        }
      );
    }
  }

  /* Shorthands for form controls (used from within template) */
  get password() { return this.formGroup.get('password'); }
  get password2() { return this.formGroup.get('password2'); }

  ngOnInit(): void {
    // this.key = this.route.snapshot.paramMap.get('key');
    this.formGroup = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(this.minPw)]],
      password2: ['', [Validators.required]]
    }, {validator: passwordMatchValidator});
    this.cdRef.detectChanges();
  }

  changePasswordOne(event) {
    this.password.setValue(event.target.value);
    this.onPasswordInput();
  }

  /* Called on each input in either password field */
  onPasswordInput() {
    if (this.formGroup.hasError('passwordMismatch')) {
      this.password2.setErrors([{passwordMismatch: true}]);
    } else {
      this.password2.setErrors(null);
    }
  }

  onStrengthChanged() {
    this.cdRef.detectChanges();
  }

  handleClickCreateAccountButton() {
    this.loading = true;
    this.userService.accountResetPassword(this.password2.value, this.key).subscribe(
      () => {
        this.loading = false;
        this.landingType = TYPE_LANDING.CREATE_ACCOUNT_SIGN_IN;
        // this.signIn = true;
      },
      () => {
        this.loading = false;
      }
    );
  }

  handleClickSignInButton() {
    this.router.navigate(['/login']).then();
  }
}