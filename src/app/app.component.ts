import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UserSessionService } from './services/user/userSession.service';
import { MessageNotificationComponent } from './components/messageNotification/messageNotification.component';
import { empty, Observable, Subject } from 'rxjs';
import { ErrorHandlerService } from './services/errorHandler/errorHandler.service';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { language } from './app.reducer';
import * as getLanguage from './app.action';
import { TokenService, UserService } from './services/service.index';
import { User } from './config/interfaces';
import { filter } from 'rxjs/operators';
declare var gtag: (arg0: string, arg1: string, arg2: { page_path: string; }) => void;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  errorMessage: Subject<string> = this.errorHandlerService.errorMessage;
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;

  constructor(private router: Router,
    private errorHandlerService: ErrorHandlerService,
    private route: ActivatedRoute,
    private userSessionService: UserSessionService,
    private translate: TranslateService,
    public userService: UserService,
    private store: Store<language>,
    private tokenService: TokenService,
  ) {

    const navEndEvents$ = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    );

    navEndEvents$.subscribe((event: NavigationEnd) => {
      gtag('config', 'G-77MNT2Z0C2', {
        'page_path': event.urlAfterRedirects
      });
    });

    let windowLocationSearch = window.location.search;
    let windowLocationHref = window.location.href;

    if (String(windowLocationHref).split('/')[2] === 'payments.pagomundo.com' ||
      String(windowLocationHref).split('/')[2] === 'qa-payments.pagomundo.com' ||
      String(windowLocationHref).split('/')[2] === 'sandbox-payments.pagomundo.com') {
      window.localStorage.setItem("Pm1-4m3r1c4s-ch4ng3Cl4ss", "1");
    }
    else if (windowLocationSearch.indexOf("pagomundo=1") != -1) {
      window.localStorage.setItem("Pm1-4m3r1c4s-ch4ng3Cl4ss", "1");
    }
    else {
      if (window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss") != "1") {
        window.localStorage.setItem("Pm1-4m3r1c4s-ch4ng3Cl4ss", "0");
      }
    }

    this.getMobileOperatingSystem();
    this.store.dispatch(new getLanguage.LanguageKey(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3")));
    let userSession = JSON.parse(window.localStorage.getItem("Pm1-4m3r1c4s-us3r"));
    let token = window.localStorage.getItem("Pm1-4m3r1c4s-t0k3n");

    if (userSession && window.localStorage.getItem("Pm1-4m3r1c4s-us3r-3xt3nd") != "undefined") {
      translate.setDefaultLang('en');
      translate.addLangs(['en', 'es', 'pt']);
      translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
      this.tokenService.saveToken(token);
      let user = JSON.parse(window.localStorage.getItem("Pm1-4m3r1c4s-us3r"));

      let userExtend = JSON.parse(window.localStorage.getItem("Pm1-4m3r1c4s-us3r-3xt3nd"));
      this.userSessionService.saveUserSession(user, userExtend);
    }

    this.errorMessage.subscribe((errorMessage) => {
      this.showMessage(errorMessage, 'error');
    });
  }


  ngOnInit(): void {

    this.route.queryParams.subscribe(params => {
      if (params.key) {
        const arrayPath = this.router.routerState.snapshot.url.split('/');
        const arrayPath2 = arrayPath[2].split('?');
        if (arrayPath[2] && arrayPath[2] === 'reset') {
          this.router.navigate(['/createAccount', params.key]).then();
        } else if (arrayPath2[0] && arrayPath2[0] === 'change-email-confirmation') {
          this.router.navigate(['/changeEmail', params.key]).then();
        }
      }
    });
  }

  showMessage(message: string, type: string, showTitle?: boolean, title?: string) {
    if (this.messageComponent) {
      this.messageComponent.setTypeMessage(message, type, showTitle, title);
    }
  }

  getMobileOperatingSystem() {
    const userAgent = navigator.userAgent || navigator.vendor;
    // Windows Phone debe ir primero porque su UA tambien contiene "Android"
    if (/windows phone/i.test(userAgent) || /android/i.test(userAgent) || /iPad|iPhone|iPod/.test(userAgent)) {
      return this.userSessionService.saveIsMobile(true);
    } else {
      return this.userSessionService.saveIsMobile(false);
    }
  }
}
