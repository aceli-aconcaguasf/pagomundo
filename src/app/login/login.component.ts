import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {TokenService, UserService, UserSessionService} from '../services/service.index';
import {MessageNotificationComponent} from '../components/messageNotification/messageNotification.component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../config/interfaces';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userForm: FormGroup;
  emailForm: FormGroup;
  forgotPassword = false;
  loading = false;
  errorValidation = false;
  validateEnvironment = environment.isProd;
  logoImg:string;
  changeClass:number = 0;
  version = 'V.1612202101';
  // pattern = new RegExp(/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,40}$/);

  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor(private router: Router,
              public userService: UserService,
              public userSessionService: UserSessionService,
              private formBuilder: FormBuilder,
              private tokenService: TokenService) {
                this.validatePagomundo();
                let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
                if( Pm14m3r1c4sCh4ng3Cl4ss == "1" ){
                  this.logoImg = "assets/img/logo-pago-mundo.png";
                  this.changeClass = 1;
                }
                else{
                  this.logoImg = "assets/img/logo.svg";
                  this.changeClass = 0;
                }
  }

  /* Shorthands for form controls (used from within template) */
  get username() {
    return this.userForm.get('username');
  }

  get password() {
    return this.userForm.get('password');
  }

  get email() {
    return this.emailForm.get('email');
  }

  ngOnInit() {

    window.localStorage.removeItem( "Pm1-4m3r1c4s-us3r" );
    window.localStorage.removeItem( "Pm1-4m3r1c4s-l4ngu4g3" );
    window.localStorage.removeItem( "Pm1-4m3r1c4s-us3r-3xt3nd" );
    window.localStorage.removeItem( "Pm1-4m3r1c4s-t0k3n" );
    // window.localStorage.removeItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );

    this.userForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      // password: ['', [Validators.required, Validators.pattern(this.pattern)]]
      password: ['', [Validators.required]]
    });

    this.emailForm = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ]))
    });
  }

  validatePagomundo(){
    let windowLocationSearch = window.location.search;
    let windowLocationHref = window.location.href;
    // console.log('URL',windowLocationHref);
    // console.log('Search',windowLocationSearch);
    // console.log('Band',(windowLocationSearch.indexOf("pagomundo=1")));

    if(String(windowLocationHref).split('/')[2] === 'payments.pagomundo.com' ||
    String(windowLocationHref).split('/')[2] === 'qa-payments.pagomundo.com' ||
    String(windowLocationHref).split('/')[2] === 'sandbox-payments.pagomundo.com') {
      window.localStorage.setItem("Pm1-4m3r1c4s-ch4ng3Cl4ss", "1");
    }
    else if (windowLocationSearch.indexOf("pagomundo=1") != -1) {
      window.localStorage.setItem("Pm1-4m3r1c4s-ch4ng3Cl4ss", "1");
    }
    if (windowLocationSearch.indexOf("pagomundo=1") != -1) {
      window.localStorage.setItem("Pm1-4m3r1c4s-ch4ng3Cl4ss", "1");
    }
    else {
      if (  window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss") != "1") {
        window.localStorage.setItem("Pm1-4m3r1c4s-ch4ng3Cl4ss", "0");
      }
    }
  }
  setForgotPassword(value: boolean) {
    this.errorValidation = false;
    this.forgotPassword = value;
  }

  resetPassword() {
    if (this.email.valid) {
      this.loading = true;
      this.userService.accountResetPasswordInit(this.email.value).subscribe(
        () => {
          this.showMessage('A link to reset the password has been sent to your email', 'success');
          this.loading = false;
          this.forgotPassword = false;
        },
        () => {
          this.loading = false;
        }
      );
    } else {
      this.errorValidation = true;
    }
  }

  handleClickLoginButton() {
    if (this.username.valid && this.password.valid) {
      this.userService.authLogin(this.username.value.trim(), this.password.value.trim()).subscribe(
        token => {
          this.tokenService.saveToken(token.id_token);

          window.localStorage.setItem( "Pm1-4m3r1c4s-t0k3n", token.id_token );

          this.userService.getAccount().subscribe(
            (user: User) => {
              window.localStorage.setItem( "Pm1-4m3r1c4s-us3r", JSON.stringify( user ) );
              window.localStorage.setItem( "Pm1-4m3r1c4s-l4ngu4g3", user.langKey );
              this.userService.getExtendedUserByUserId(user.id).subscribe(
                extendedUser  => {

                  if( extendedUser.content.length > 0 ){
                    if(
                    extendedUser.content[0].reseller && ( extendedUser.content[0].reseller.name == "Nobeteck"
                      || extendedUser.content[0].reseller.company == "NOBETEK LLC"
                      || extendedUser.content[0].reseller.id == 3208 ) ){
                      window.localStorage.setItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss", "1" );
                    } else if (extendedUser.content[0].email === 'israpiston+0321001@gmail.com') {
                      window.localStorage.setItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss", "1" );
                    }
                  }
                  window.localStorage.setItem( "Pm1-4m3r1c4s-us3r-3xt3nd", JSON.stringify( extendedUser.content[0] ) );
                  this.userSessionService.saveUserSession(user, extendedUser.content[0]);
                  if (this.userSessionService.loadUserStatus() === 'USERSTATELESS' ||
                      this.userSessionService.loadUserStatus() === 'CREATED') {
                      this.router.navigate(['/profile']).then();
                  } else {
                    if (!extendedUser.content[0].confirmedProfile && this.userSessionService.isPayee()) {
                      this.router.navigate(['/profile']).then();
                    } else {
                      this.router.navigate(['/dashboard']).then();
                    }
                  }
                }
              );
            }
          );
        }
      );
    } else {
      this.errorValidation = true;
    }
  }

  showMessage(message: string, type: string) {

    this.messageComponent.setTypeMessage(message, type);
  }

  resetError() {
    this.errorValidation = false;
  }
}
