import { Action } from "@ngrx/store";

export const KEY = 'KEY';

export class LanguageKey implements Action {
    readonly type = KEY;

    constructor(public payload: string){}
}

export type KeyActions = LanguageKey ;