import {Component, OnInit} from '@angular/core';
import {UserSessionService} from 'src/app/services/service.index';

export enum Roles {
    ADMIN = 'ROLE_ADMIN',
    MERCHANT = 'ROLE_MERCHANT',
    PAYEE = 'ROLE_PAYEE',
  }

@Component({
  selector: 'app-mobile-message',
  templateUrl: './mobileMessage.component.html',
  styleUrls: ['./mobileMessage.component.scss']
})
export class MobileMessageComponent implements OnInit {
   roles = Roles;
   role: string;

  constructor(public userSessionService: UserSessionService) {
    this.role = this.userSessionService.loadRole();
  }
  ngOnInit() {
  }

}
