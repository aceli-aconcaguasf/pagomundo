import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {merge, of as observableOf} from 'rxjs';
import {MatDialog, MatPaginator, MatSort, TooltipPosition} from '@angular/material';
import {DataFinderService, UserService, UserSessionService} from '../../../services/service.index';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {ExtendedUser} from '../../../config/interfaces';
import {ROLES, STATUS, STATUSES_FUND, STATUSES_COLOR, MONEY_MOVEMENT} from '../../../config/enums';
import {environment} from '../../../../environments/environment';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-table-fund',
  templateUrl: './tableFund.component.html',
  styleUrls: ['./tableFund.component.scss']
})
export class TableFundComponent implements OnChanges {

  @Input() extendedUser: ExtendedUser;
  @Input() role: string;
  @Input() filter: string;
  @Input() historical: boolean;

  @Output() public openActionFundModal = new EventEmitter<any>();

  keys = Object.keys;
  environment = environment;
  uri = '/api/_search/extended-user-funds';
  query = '';
  positionOptions: TooltipPosition = 'above';

  data: any[] = [];
  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  roles = ROLES;
  status = STATUS;
  statuses = STATUSES_FUND;
  statusesColor = STATUSES_COLOR;

  resultsLength = 0;
  pageSize = 10;
  isLoadingResults = true;
  isErrorOccurred = false;

  errorMessage = 'Error getting funds history';

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private userService: UserService,
              public userSessionService: UserSessionService,
              public dialog: MatDialog,
              private cdRef: ChangeDetectorRef,
              private dataFinder: DataFinderService,
              private translate: TranslateService) {
      this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
      setTimeout(() => {
        this.setColumnsName();
      }, 500);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initTable();
  }

  initTable() {
    this.setColumnsName();
    setTimeout(() => {
      this.isSuperOrAdmin() && this.historical ? this.pageSize = 8 : this.pageSize = 10;
      this.loadTable();
    }, 0);
  }

  isSuperOrAdmin(): boolean {
    return this.role === ROLES.SUPER_ADMIN || this.role === ROLES.ADMIN;
  }

  setColumnsName() {
    let dateTranslate:any = this.translate.get( 'DATA_TABLE.DATE' );
    let statusTranslate:any = this.translate.get( 'DATA_TABLE.STATUS' );
    let amountTranslate:any = this.translate.get( 'DATA_TABLE.AMOUNT' );
    let merchantTranslate:any = this.translate.get( 'TEXT.MERCHANT' );
    let statusReasonTranslate:any = this.translate.get( 'DATA_TABLE.STATUS_REASON' );

    if (this.isSuperOrAdmin()) {
      if (this.historical) {
        this.columnNames = {
          dateCreated: dateTranslate.value, status: statusTranslate.value, amount: amountTranslate.value, merchant: merchantTranslate.value, statusReason: statusReasonTranslate.value,
        };
      } else {
        this.columnNames = {
          dateCreated: dateTranslate.value, status: statusTranslate.value, amount: amountTranslate.value, merchant: merchantTranslate.value, statusReason: statusReasonTranslate.value, action: ''
        };
      }
    } else {
      this.columnNames = {
        lastUpdated: dateTranslate.value, status: statusTranslate.value, amount: amountTranslate.value, statusReason: statusReasonTranslate.value, action: ''
      };
    }

    this.columns = Object.keys(this.columnNames);
  }

  loadTable() {
    this.query = this.setQuery();

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
    this.cdRef.detectChanges();
  }

  setQuery(): string {
    let query = '';
    this.role === ROLES.MERCHANT ?
      query = `extendedUser.id:${this.extendedUser.id} AND category:REQUESTED AND type:${MONEY_MOVEMENT.FUNDING} `
      : this.historical ?
        query = `(status:${STATUS.ACCEPTED} OR status:${STATUS.REJECTED}) AND category:REQUESTED AND type:${MONEY_MOVEMENT.FUNDING} `
        : query = `status:${STATUS.CREATED} AND category:REQUESTED AND type:${MONEY_MOVEMENT.FUNDING} `;

    if (this.filter && this.historical) {
      query += ` AND ${this.filter}`;
    }

    return query;
  }

  handleActionFund(fund: any, reasonType: string): void {
    /*
    * reasonType:
    *  Cancel
    *  Accept
    *  Reject
    */
    const result = {
      fund,
      reasonType,
    };
    this.openActionFundModal.emit(result);
  }

  sortTable(): string {
    let sort;
    if (this.isSuperOrAdmin()) {
      sort = this.historical ? 'lastUpdated' : 'dateCreated';
    } else {
      sort = 'lastUpdated';
    }
    return sort;
  }
}