import {Component, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {MessageNotificationComponent} from '../../components/messageNotification/messageNotification.component';
import {UserService, UserSessionService} from '../../services/service.index';
import {ExtendedUser} from '../../config/interfaces';
import {FundModalComponent} from '../../components/fundModal/fundModal.component';
import {ROLES, STATUS} from '../../config/enums';
import {NoteModalComponent} from '../../components/noteModal/noteModal.component';
import {FormControl} from '@angular/forms';
import {TableFundComponent} from './tableFund/tableFund.component';
import {TranslateService} from '@ngx-translate/core';

export enum STATUSES_FUND_FILTER {
  ACCEPTED = 'Accepted',
  REJECTED = 'Rejected',
}

@Component({
  selector: 'app-fund',
  templateUrl: './fund.component.html',
  styleUrls: ['./fund.component.scss']
})
export class FundComponent {

  extendedUser: ExtendedUser;
  role: string;
  roles = ROLES;
  queryFilter: string;
  status = STATUS;

  selectedIndex = new FormControl(0);
  collapse = false;

  // FILTERS

  keys = Object.keys;
  statuses = STATUSES_FUND_FILTER;
  showApplyFilter = false;
  nameInput = '';
  filterName = '';
  statusId = '';
  nameMerchant = [];
  nameStatus = [];
  mssgEmpty = false;
  errorMessage = 'Error getting funds history';
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  changeClass:number;
  @ViewChild('TableFund', {static: false}) tableFund: TableFundComponent;
  @ViewChild('TableFundHistorical', {static: false}) tableFundHistorical: TableFundComponent;

  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private userService: UserService,
              private userSessionService: UserSessionService,
              public dialog: MatDialog,
              private translate: TranslateService) {
    this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    this.extendedUser = this.userSessionService.loadExtendedUser();
    this.role = this.userSessionService.loadRole();
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  isSuperOrAdmin(): boolean {
    return this.role === ROLES.SUPER_ADMIN || this.role === ROLES.ADMIN;
  }

  openFundModal(): void {
    let messageTranslate:any;

    if (this.extendedUser.status !== STATUS.ACCEPTED) {

      messageTranslate = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.IN_DISABLED_STATE' );
      this.showMessage( messageTranslate.value, 'warning');
      return;
    } else {
      const dialogRef = this.dialog.open(FundModalComponent, {
        data: {
          extendedUser: this.extendedUser,
          type: 'REQUEST_FUND'
        },
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(response => {
        if (response.value) {
          this.userService.requestFunds(response.amount).subscribe(
            () => {
              messageTranslate = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.FUNDS_REQUST' );
              this.showMessage( messageTranslate.value, 'success');
              this.tableFund.loadTable();
            }
          );
        }
      });
    }
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  openActionFundModal(fundObject): void {
    /*
    * fundObject = {fund, reasonType}
    * reasonType:
    *  Cancel
    *  Accept
    *  Reject
    */
    const dialogRef = this.dialog.open(NoteModalComponent, {
      data: {
        type: 'fund_reason',
        reasonType: fundObject.reasonType
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      let actionMsg;
      if (result.value) {
        const updateFund = {
          id: fundObject.fund.id,
          reason: result.reason
        };

        switch (fundObject.reasonType) {
          case 'Accept':
            Object.defineProperty(updateFund, 'status', {value: this.status.ACCEPTED, enumerable: true});
            actionMsg = 'accepted';
            break;
          case 'Cancel':
            Object.defineProperty(updateFund, 'status', {value: this.status.CANCELLED, enumerable: true});
            actionMsg = 'cancelled';
            break;
          case 'Reject':
            Object.defineProperty(updateFund, 'status', {value: this.status.REJECTED, enumerable: true});
            actionMsg = 'rejected';
            break;
        }

        this.userService.updateFunds(updateFund).subscribe(
          () => {
            this.showMessage(`Fund ${actionMsg}`, 'success');
            this.tableFund.loadTable();
            if (this.isSuperOrAdmin()) {
              this.tableFundHistorical.loadTable();
            }
        });
      }
    });
  }

  changeTab(index) {
    this.selectedIndex.setValue(index);
    this.collapse = false;
    this.cleanFilter();
  }

  addFilter(event: any) {
    this.mssgEmpty = false;
    this.filterName = event.target.value;
    this.showApplyFilter = true;
    event.target.selectedIndex = 0;
  }

  handleClickFilterToggle() {
    this.resetVariable();
    this.collapse = !this.collapse;
    this.mssgEmpty = false;
  }

  resetVariable() {
    this.filterName = '';
    this.statusId = '';
    this.nameInput = '';
    this.showApplyFilter = false;
  }

  cleanFilter() {
    this.nameStatus = [];
    this.nameMerchant = [];
    this.queryFilter = this.getQueryFilterString();
  }

  resetFilter(typeName: string) {
    switch (typeName) {
      case 'merchant':
        this.nameMerchant = [];
        break;
      case 'status':
        this.nameStatus = [];
        break;
      default:
        break;
    }
    this.queryFilter = this.getQueryFilterString();
  }

  deleteFilter(typeName: string, name: any) {
    switch (typeName) {
      case 'merchant':
        const indexMerchant = this.nameMerchant.indexOf(name);
        this.nameMerchant.splice(indexMerchant, 1);
        break;
      case 'status':
        const indexStatus = this.nameStatus.indexOf(name);
        this.nameStatus.splice(indexStatus, 1);
        break;
      default:
        break;
    }
    this.queryFilter = this.getQueryFilterString();
  }

  applyFilter() {
    this.mssgEmpty = false;
    switch (this.filterName) {
      case 'status':
        if (this.statusId !== '') {
          const indexStatus = this.nameStatus.indexOf(this.statusId);
          if (indexStatus === -1) {
            this.nameStatus.push(this.statusId);
          }
        } else {
          this.nameStatus = [];
        }
        break;
      case 'name':
        if (this.nameInput !== '') {
          const indexMerchant = this.nameMerchant.indexOf(this.nameInput);
          if (indexMerchant === -1) {
            this.nameMerchant.push(this.nameInput);
          }
        }
        else {
          this.mssgEmpty = true;
          this.nameMerchant = [];
        }
        break;
      default:
        break;
    }
    this.resetVariable();
    this.queryFilter = this.getQueryFilterString();
  }

  getQueryFilterString(): string {
    let queryFilter = '';

    // MERCHANT NAME
    if (this.nameMerchant.length > 0) {
      if (this.nameMerchant.length === 1) {
        queryFilter += `extendedUser.fullName:${this.nameMerchant[0]}`;
      } else {
        for (let index = 0; index < this.nameMerchant.length; index++) {
          if (index === 0) {
            queryFilter += `( extendedUser.fullName:${this.nameMerchant[index]} OR `;
          } else {
            if (this.nameMerchant.length - 1 !== index) {
              queryFilter += `extendedUser.fullName:${this.nameMerchant[index]} OR `;
            } else {
              queryFilter += `extendedUser.fullName:${this.nameMerchant[index]} )`;
            }
          }
        }
      }
    }
    // END NAME

    // STATUS
    if (this.nameStatus.length > 0) {
      if (queryFilter !== '') {
        queryFilter += ' AND ';
      }
      if (this.nameStatus.length === 1) {
        queryFilter += `status:${this.nameStatus[0]}`;
      } else {
        for (let index = 0; index < this.nameStatus.length; index++) {
          if (index === 0) {
            queryFilter += `( status:${this.nameStatus[index]} OR `;
          } else {
            if (this.nameStatus.length - 1 !== index) {
              queryFilter += `status:${this.nameStatus[index]} OR `;
            } else {
              queryFilter += `status:${this.nameStatus[index]} )`;
            }
          }
        }
      }
    }
    // END STATUS

    return queryFilter;
  }
}