import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { ExtendedUser, User } from '../../config/interfaces';
import { MessageNotificationComponent } from '../../components/messageNotification/messageNotification.component';
import { FormControl, Validators } from '@angular/forms';
import { UserSessionService } from 'src/app/services/service.index';
import {
  BankFormComponent, CardNumberModalComponent, PayeeCompanyFormComponent,
  PayeeFormComponent, UserFormComponent
} from 'src/app/components/components.index';
import { MatDialog } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { ROLES, STATUS } from '../../config/enums';
import { TranslateService } from '@ngx-translate/core';

import swal from 'sweetalert2';//swalert2

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  roles = ROLES;
  status = STATUS;
  sessionUser: User;
  extendedUser = {} as ExtendedUser;
  loading = false;

  countryPostalId: any = 0;
  modifyMode = false;
  userFormInvalid = false;
  payeeFormInvalid = false;
  payeeCompanyFormInvalid = false;
  bankFormInvalid = false;

  checked = true;
  bankPaymentMethod = true;
  canChangePaymentMethod = true;
  selectedIndex = new FormControl(0);

  countShowMessagePaymentMethodMexico: number = 1;

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  changeClass: number;
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  @ViewChild('PayeeForm', { static: false }) payeeFormComponent: PayeeFormComponent;
  @ViewChild('PayeeCompanyForm', { static: false }) payeeCompanyFormComponent: PayeeCompanyFormComponent;
  @ViewChild('UserForm', { static: false }) userFormComponent: UserFormComponent;
  @ViewChild('BankForm', { static: false }) bankFormComponent: BankFormComponent;

  userBrazilShowBank = [
    'matheusoliveirakuhn@gmail.com',
    'acarolkunst@gmail.com',
    'dressy00@gmail.com',
    'marianapedraza86@gmail.com',
    'thayanarcardoso@gmail.com',
    'eujamesmartinez@gmail.com',
    '1011079p2@ilsparent.com'
  ];

  constructor(public userService: UserService,
    public userSessionService: UserSessionService,
    public dialog: MatDialog,
    private translate: TranslateService) {
    this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss");
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  ngOnInit() {
    this.getInfoUser();
    this.initUser();
  }

  initUser() {
    this.initExtendedUser();
    if (this.userSessionService.isPayee()) {
      if (this.userSessionService.haveStatus() && this.extendedUser.postalCountry) {
        this.countryPostalId = this.extendedUser.postalCountry.id;
      }
      this.initBankInfoValues(this.extendedUser);
    }
  }

  initExtendedUser() {
    if (this.userSessionService.haveStatus()) {
      this.extendedUser = this.userSessionService.loadExtendedUser();
      if (!this.extendedUser.confirmedProfile && this.extendedUser.role === ROLES.PAYEE) {
        this.changeTabView(1);
      } else {
        this.changeTabView(0);
      }
    } else {
      this.changeTabView(1);
      this.extendedUser = null;
    }
  }

  initBankInfoValues(extendedUser: ExtendedUser) {
    if (this.userSessionService.haveStatus()) {
      this.canChangePaymentMethod = extendedUser.canChangePaymentMethod;
      this.bankPaymentMethod = extendedUser.useDirectPayment;
    } else {
      this.sessionUser.payeeAsCompany ? this.canChangePaymentMethod = false : this.canChangePaymentMethod = true;
    }
  }

  selectPaymentMethod(event) {

    this.bankFormComponent.cleanBankFormValidator();
    this.bankPaymentMethod = event.checked;
    this.bankFormComponent.initBankInfo(this.extendedUser);
    if (this.bankPaymentMethod) {
      this.bankFormComponent.setBankFormValidator();
    } else {
      this.bankFormComponent.bankFormInvalid = false;
    }

    //if( this.countryPostalId == 2 && !event.checked && this.countShowMessagePaymentMethodMexico == 1 ){
    if (this.countryPostalId == 2 && !event.checked) {

      this.countShowMessagePaymentMethodMexico = 2;
      this._showMessageBankMexico();
    }

    /* if (!this.bankPaymentMethod && this.userSessionService.haveStatus() && !this.extendedUser.cardNumber) {
   const dialogRef = this.dialog.open(CardNumberModalComponent, {
       disableClose: true,
       data: {
         extendedUser: this.extendedUser,
         adminView: false,
       },
     });
     dialogRef.afterClosed().subscribe(value => {
       if (value) {
         switch (value.typeResult) {
           case 'CANCEL':
             this.bankPaymentMethod = true;
             this.bankFormComponent.initBankInfo(this.extendedUser);
             this.disableForm();
             break;
           case 'CONTINUE':
             this.extendedUser.status = 'CREATED';
             this.extendedUser.useDirectPayment = false;
             this.userService.updateExtendedUser(this.extendedUser).subscribe(
               extUser => {
                 this.userSessionService.saveExtendedUser(extUser);
                 this.initUser();
                 messageTranslate = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_PROFILE_UPDATE' );
                 this.showMessage( messageTranslate.value, 'success');
                 this.disableForm();
                 return true;
               }
             );
             break;
           default:
             break;
         }
       }
     });
   }*/
  }

  _showMessageBankMexico() {

    let errorMessageTranslate: any = this.translate.get('MESSAGE.CHANGE_PAYMENT_MEX');

    swal.fire({
      title: '',
      text: errorMessageTranslate.value,
      icon: 'warning',
      showCancelButton: false,
      confirmButtonText: 'OK',
      confirmButtonColor: '#3085d6',
      reverseButtons: true
    });
  }

  editProfileButtonHandler() {
    if (this.userSessionService.isPayee()) {
      this.bankFormInvalid = false;
      this.bankFormComponent.enableBankFormEditing();
      if (this.sessionUser.payeeAsCompany) {
        this.payeeCompanyFormInvalid = false;
        this.payeeCompanyFormComponent.enablePayeeCompanyFormEditing();
      } else {
        this.payeeFormInvalid = false;
        this.payeeFormComponent.enablePayeeFormEditing();
      }
    } else {
      this.userFormInvalid = false;
      this.userFormComponent.enableUserFormEditing();
    }

    this.modifyMode = true;
  }

  cancelEditProfileButtonHandler() {
    this.disableForm();
    if (this.userSessionService.isPayee()) {
      this.initBankInfoValues(this.extendedUser);
      this.bankFormComponent.initBankInfo(this.extendedUser);
      if (this.sessionUser.payeeAsCompany) {
        this.payeeCompanyFormComponent.initExtendedUser(this.extendedUser);
      } else {
        this.payeeFormComponent.initExtendedUser(this.extendedUser);
      }
    } else {
      this.userFormComponent.initExtendedUser(this.extendedUser);
    }
  }

  createExtendedUser() {

    let errorMessageTranslate: any;
    this.payeeFormInvalid = false;
    this.payeeCompanyFormInvalid = false;
    let extendedUserObj;

    if (this.bankPaymentMethod && this.bankFormComponent.clabeBankAccountNumber.invalid
      && this.countryPostalId == 2) {
      this.bankFormInvalid = true;
      this.changeTabView(2);
      return false;
    }

    if (this.bankPaymentMethod &&
      (this.bankFormComponent.cbuBankAccountNumber.invalid || this.bankFormComponent.cbuBankAccountNumber.value === '') &&
      this.countryPostalId == 66) {
      this.bankFormInvalid = true;
      errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CBU_MUST_HAVE_NUMBERS');
      this.showMessage(errorMessageTranslate.value, 'error');
      this.changeTabView(2);
      return false;
    }

    //Validaciones para Paises
    if (this.countryPostalId == 74) {
console.log({
  payee: this.sessionUser.payeeAsCompany,
  bankMethos: this.bankPaymentMethod,
  bankForm: this.bankFormComponent.bankForm.value
});
      if (this.sessionUser.payeeAsCompany) {

        if (this.bankPaymentMethod &&
          (this.bankFormComponent.bankForm.value.bankAccountNumber.length < 6
            || this.bankFormComponent.bankForm.value.bankAccountNumber.length > 13)) {
          this.bankFormInvalid = true;
          this.changeTabView(2);
          errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.NUMBER_BANK_COMPANY_BRAZIL');
          this.showMessage(errorMessageTranslate.value, 'error');
          return false;
        }
      }
      else {
        if (this.bankPaymentMethod && (this.bankFormComponent.bankForm.value.bankAccountNumber.length < 6
          || this.bankFormComponent.bankForm.value.bankAccountNumber.length > 13)) {
          this.bankFormInvalid = true;
          this.changeTabView(2);
          errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.NUMBER_BANK_PERSON_BRAZIL');
          this.showMessage(errorMessageTranslate.value, 'error');
          return false;
        }
      }
    } else if (this.countryPostalId == 2) {

      if (this.bankFormComponent.bankForm.value.clabeBankAccountNumber == '') {
        this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CLABE_18_NUMBERS');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }

    } else if (this.countryPostalId == 101 || this.countryPostalId == 115) {

      if (this.bankFormComponent.bankForm.status === "INVALID" || this.bankFormComponent.bankForm.value.bank === null) {
        this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false; 
      }

/*       if (this.payeeFormComponent.payeeForm.controls
        || this.payeeFormComponent.noPostalCitySelected) {
        this.payeeFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        this.changeTabView(0);
        return false;
      } */

      if (this.bankFormComponent.bankForm.value.bankAccountNumber == '' && this.bankFormComponent.bankForm.value.bank == null) {
        this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.BANK_CLABE_EMPY');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false; 
      } else if (this.bankFormComponent.bankForm.value.bankAccountNumber == '') {
        this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CLABE_EMPY');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false; 
      } else if (this.bankFormComponent.bankForm.value.bank == null) {
        this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.BANK_EMPY');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false; 
      }
    }
    else if (this.countryPostalId == 77) {

      if (this.bankFormComponent.bankForm.value.bankAccountNumber.length > 13 || this.bankFormComponent.bankForm.value.bankAccountNumber.length < 2) {
        this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_RUT_10');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }
    }
    else if (this.countryPostalId == 1) {
      if (this.bankFormComponent.bankForm.value.bankAccountNumber == "") {
        this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_BANK_FORM_COLOMBIA');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }

    }
    else {

      if (this.bankPaymentMethod && !this.bankFormComponent.bankForm.valid) {
        this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }
    }

    if (this.sessionUser.payeeAsCompany) {
      // PAYEE COMPANY
      if (this.payeeCompanyFormComponent.noPostalCountrySelected
        || this.payeeCompanyFormComponent.noResidenceCountrySelected || this.payeeCompanyFormComponent.noPostalCitySelected) {
        this.payeeCompanyFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        this.changeTabView(0);
        return false;
      }
      if (this.payeeCompanyFormComponent.countryPostalId === 66) {
        this.payeeCompanyFormComponent.payeeCompanyForm.controls['companyTaxIdType'].clearValidators();
        this.payeeCompanyFormComponent.payeeCompanyForm.controls['companyTaxIdType'].updateValueAndValidity();
      } else {
        this.payeeCompanyFormComponent.payeeCompanyForm.controls['companyTaxIdType'].clearValidators();
        this.payeeCompanyFormComponent.payeeCompanyForm.controls['companyTaxIdType'].setValidators([Validators.required]);
        this.payeeCompanyFormComponent.payeeCompanyForm.controls['companyTaxIdType'].updateValueAndValidity();
      }

      if (this.payeeCompanyFormComponent.payeeCompanyForm.valid) {

        const residenceAddress = this.payeeCompanyFormComponent.legalRepresentativeResidenceAddress.value;
        const residenceCity = this.payeeCompanyFormComponent.legalRepresentativeCityResidence.value;
        const cpAdress = this.payeeCompanyFormComponent.legalCPRepresentativeResidenceAddress.value;

        let idType;
        if (
          this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value.id === 1 ||
          this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value === 1
        ) {
          idType = 1;
        }
        else if (
          this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value.id === 2 ||
          this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value === 2
        ) {
          idType = 15;
        }

        extendedUserObj = {
          user: {
            id: this.sessionUser.id,
          },
          lastName1: this.payeeCompanyFormComponent.legalRepresentativeLastName.value,
          firstName1: this.payeeCompanyFormComponent.legalRepresentativeFirstName.value,
          email: this.payeeCompanyFormComponent.legalRepresentativeEmail.value,
          idNumber: this.payeeCompanyFormComponent.legalRepresentativeIdNumber.value,
          idType: { id: +idType },
          mobileNumber: this.payeeCompanyFormComponent.legalRepresentativeMobileNumber.value,
          residenceAddress: `${residenceAddress} | ${residenceCity} | ${cpAdress}`,

          residenceCountry: {
            id: ((this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value.id != null) ? this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value.id : this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value),
          },

          taxId: this.payeeCompanyFormComponent.companyTaxId.value,
          idTypeTaxId: { id: +this.payeeCompanyFormComponent.companyTaxIdType.value },
          company: this.payeeCompanyFormComponent.companyName.value,
          phoneNumber: this.payeeCompanyFormComponent.companyPhoneNumber.value,
          birthDate: new Date(this.payeeCompanyFormComponent.companyConstitutionDate.value),

          postalAddress: `${this.payeeCompanyFormComponent.companyPostalAddress.value} | ${this.payeeCompanyFormComponent.companyCpPostalAddress.value}`,
          postalCity: {
            id: +this.payeeCompanyFormComponent.companyCityPostal.value,
          },
          /**
           *           postalCity: {
            id: +((this.payeeCompanyFormComponent.companyCityPostal.value.id != null) ? this.payeeCompanyFormComponent.companyCityPostal.value.id : this.payeeCompanyFormComponent.companyCityPostal.value),
          },
           */
          postalCountry: {
            id: +((this.payeeCompanyFormComponent.companyCountryPostal.value.id != null) ? this.payeeCompanyFormComponent.companyCountryPostal.value.id : this.payeeCompanyFormComponent.companyCountryPostal.value),
          },

          canChangePaymentMethod: false,
          useDirectPayment: true,
        };
      } else {
        this.changeTabView(0);
        this.payeeCompanyFormInvalid = true;

        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }
      // END PAYEE COMPANY
    }
    else {
      // PAYEE USER

      if (this.payeeFormComponent.noPostalCountrySelected
        || this.payeeFormComponent.noPostalCitySelected) {
        this.payeeFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        this.changeTabView(0);
        return false;
      }

      if (this.payeeFormComponent.payeeForm.valid) {
        extendedUserObj = {
          user: {
            id: this.sessionUser.id,
          },
          lastName1: this.payeeFormComponent.payeeLastName1.value,
          lastName2: this.payeeFormComponent.payeeLastName2.value,
          firstName1: this.payeeFormComponent.payeeFirstName1.value,
          firstName2: this.payeeFormComponent.payeeFirstName2.value,
          idNumber: this.payeeFormComponent.payeeIdNumber.value,
          gender: this.payeeFormComponent.payeeGender.value,
          taxId: this.payeeFormComponent.payeeTaxId.value,
          maritalStatus: this.payeeFormComponent.payeeMaritalStatus.value,
          mobileNumber: this.payeeFormComponent.payeeMobileNumber.value,
          phoneNumber: this.payeeFormComponent.payeePhoneNumber.value,
          birthDate: new Date(this.payeeFormComponent.payeeBirthDate.value),
          email: this.payeeFormComponent.payeeEmail.value,
          idType: {
            id: +this.payeeFormComponent.payeeIdType.value,
          },

          postalCountry: {
            id: +((this.payeeFormComponent.payeeCountryPostal.value.id != null) ? this.payeeFormComponent.payeeCountryPostal.value.id : parseInt(this.payeeFormComponent.payeeCountryPostal.value)),
          },
          residenceCountry: {
            id: +((this.payeeFormComponent.payeeCountryPostal.value.id != null) ? this.payeeFormComponent.payeeCountryPostal.value.id : parseInt(this.payeeFormComponent.payeeCountryPostal.value)),
          },

          // postalCity: {
          //   id: +((this.payeeFormComponent.payeeCityPostal.value.id != null) ? this.payeeFormComponent.payeeCityPostal.value.id : parseInt(this.payeeFormComponent.payeeCityPostal.value)),
          // },
          postalCity: {
            id: +this.payeeFormComponent.payeeCityPostal.value,
          },
          residenceCity: {
            id: +this.payeeFormComponent.payeeCityPostal.value,
          },
          // residenceCity: {
          //   id: +((this.payeeFormComponent.payeeCityPostal.value.id != null) ? this.payeeFormComponent.payeeCityPostal.value.id : parseInt(this.payeeFormComponent.payeeCityPostal.value)),
          // },
          residenceAddress: this.payeeFormComponent.payeePostalAddress.value,
          postalAddress: `${this.payeeFormComponent.payeePostalAddress.value} | ${this.payeeFormComponent.payeeCpPostalAddress.value}`,
          canChangePaymentMethod: this.canChangePaymentMethod,
          useDirectPayment: this.bankPaymentMethod,
        };
      }
      else {
        this.changeTabView(0);
        this.payeeFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }
      // END PAYEE
    }

    if (this.bankPaymentMethod) {
      // SET  BANK INFO
      // ID 1 = COLOMBIA
      // ID 2 = MEXICO
      // ID 1 = BRAZI
      // ID 77 = CHILE
      // ID 101 = PERÚ
      // ID 115 = VENEZUELA

      if (this.countryPostalId == 1) {
        Object.defineProperty(extendedUserObj, 'bankAccountNumber',
          { value: this.bankFormComponent.bankAccountNumber.value, enumerable: true });
        Object.defineProperty(extendedUserObj, 'bankAccountType',
          { value: this.bankFormComponent.bankAccountType.value, enumerable: true });
        Object.defineProperty(extendedUserObj, 'directPaymentBank',
          { value: {}, enumerable: true });
        Object.defineProperty(extendedUserObj.directPaymentBank, 'id',
          { value: this.bankFormComponent.bank.value, enumerable: true });

        Object.defineProperty(extendedUserObj, 'directPaymentCity',
          { value: {}, enumerable: true });
        Object.defineProperty(extendedUserObj.directPaymentCity, 'id',
          { value: this.bankFormComponent.bankCity.value, enumerable: true });
      } else if (this.countryPostalId == 2) {
        Object.defineProperty(extendedUserObj, 'bankAccountNumber',
          { value: this.bankFormComponent.clabeBankAccountNumber.value, enumerable: true });
      } else if (this.countryPostalId == 66) {
        Object.defineProperty(extendedUserObj, 'bankAccountNumber',
          { value: this.bankFormComponent.cbuBankAccountNumber.value, enumerable: true });
      } else if (this.countryPostalId == 74) {
        Object.defineProperty(extendedUserObj, 'bankBranch', {
          value: this.bankFormComponent.bankBranch.value,
          enumerable: true
        });
        Object.defineProperty(extendedUserObj, 'bankAccountNumber',
          { value: this.bankFormComponent.bankAccountNumber.value, enumerable: true });
        Object.defineProperty(extendedUserObj, 'bankAccountType',
          { value: this.bankFormComponent.bankAccountType.value, enumerable: true });
        Object.defineProperty(extendedUserObj, 'directPaymentBank',
          { value: {}, enumerable: true });
        Object.defineProperty(extendedUserObj.directPaymentBank, 'id',
          { value: this.bankFormComponent.bank.value, enumerable: true });
        Object.defineProperty(extendedUserObj, 'directPaymentCity',
          { value: {}, enumerable: true });
        Object.defineProperty(extendedUserObj.directPaymentCity, 'id',
          { value: this.bankFormComponent.bankCity.value, enumerable: true });
      } else if (this.countryPostalId == 77) {
        Object.defineProperty(extendedUserObj, 'bankAccountNumber',
          { value: this.bankFormComponent.bankAccountNumber.value, enumerable: true });
        Object.defineProperty(extendedUserObj, 'bankAccountType',
          { value: this.bankFormComponent.bankAccountType.value, enumerable: true });
        Object.defineProperty(extendedUserObj, 'directPaymentBank',
          { value: {}, enumerable: true });
        Object.defineProperty(extendedUserObj.directPaymentBank, 'id',
          { value: this.bankFormComponent.bank.value, enumerable: true });
      } else if (this.countryPostalId == 101) {
        Object.defineProperty(extendedUserObj, 'bankAccountNumber',
          { value: this.bankFormComponent.bankAccountNumber.value.trim(), enumerable: true });
        Object.defineProperty(extendedUserObj, 'bankAccountType',
          { value: this.bankFormComponent.bankAccountType.value, enumerable: true });
        Object.defineProperty(extendedUserObj, 'directPaymentBank',
          { value: {}, enumerable: true });
        Object.defineProperty(extendedUserObj.directPaymentBank, 'id',
          { value: this.bankFormComponent.bank.value, enumerable: true });
      } else if (this.countryPostalId == 115) {
      Object.defineProperty(extendedUserObj, 'bankAccountNumber',
        { value: this.bankFormComponent.bankAccountNumber.value.trim(), enumerable: true });
      Object.defineProperty(extendedUserObj, 'bankAccountType',
        { value: this.bankFormComponent.bankAccountType.value, enumerable: true });
      Object.defineProperty(extendedUserObj, 'directPaymentBank',
        { value: {}, enumerable: true });
      Object.defineProperty(extendedUserObj.directPaymentBank, 'id',
        { value: this.bankFormComponent.bank.value, enumerable: true });
    }
    }
    this.userService.createExtendedUser(extendedUserObj).subscribe(
      extendedUser => {
        this.userService.refreshReSellerLogoObservable(extendedUser);
        this.changeTabView(0);

        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_PRFILE');

        this.showMessage(errorMessageTranslate.value, 'success');
        this.disableForm();
        this.initUser();
        setTimeout(() => {
          this.sessionUser.payeeAsCompany
            ? this.payeeCompanyFormComponent.initExtendedUser(extendedUser)
            : this.payeeFormComponent.initExtendedUser(extendedUser);
        }, 0);
      }
    );
  }

  confirmedPayeeProfile() {

    let errorMessageTranslate: any;
    this.payeeFormInvalid = false;
    this.payeeCompanyFormInvalid = false;

    if (this.countryPostalId == 66 && this.bankPaymentMethod && this.bankFormComponent.clabeBankAccountNumber.invalid) {
      this.bankFormInvalid = true;
      errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CBU_MUST_HAVE_NUMBERS');
      this.showMessage(errorMessageTranslate.value, 'error');
      this.changeTabView(2);
      return false;
    }

    if (this.bankPaymentMethod && this.bankFormComponent.cbuBankAccountNumber.invalid) {
      this.bankFormInvalid = true;
      errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CBU_MUST_HAVE_NUMBERS');
      this.showMessage(errorMessageTranslate.value, 'error');
      this.changeTabView(2);
      return false;
    }

    if (this.bankPaymentMethod && !this.bankFormComponent.bankForm.valid) {
      this.bankFormInvalid = true;
      errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_REQUIRED_BANKS');
      this.showMessage(errorMessageTranslate.value, 'error');
      this.changeTabView(2);
      return false;
    }

    // PAYEE COMPANY
    if (this.sessionUser.payeeAsCompany) {

      if (this.payeeCompanyFormComponent.noPostalCountrySelected
        || this.payeeCompanyFormComponent.noResidenceCountrySelected || this.payeeCompanyFormComponent.noPostalCitySelected) {
        this.payeeCompanyFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        this.changeTabView(1);
        return false;
      }

      const residenceAddress = this.payeeCompanyFormComponent.legalRepresentativeResidenceAddress.value;
      const residenceCity = this.payeeCompanyFormComponent.legalRepresentativeCityResidence.value;
      const cpAdress = this.payeeCompanyFormComponent.legalCPRepresentativeResidenceAddress.value;
      if (this.payeeCompanyFormComponent.payeeCompanyForm.valid) {
        this.extendedUser.residenceAddress = `${residenceAddress} | ${residenceCity} | ${cpAdress}`;
        this.extendedUser.residenceCountry = {
          id: this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value, name: ''
        };

        this.extendedUser.idNumber = this.payeeCompanyFormComponent.legalRepresentativeIdNumber.value;
        switch (this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value) {
          case 1: // id 1 = Colombia
            this.extendedUser.idType = { id: 1, name: '' };
            break;
          case 2: // id 2 = Mexico
            this.extendedUser.idType = { id: 15, name: '' };
            break;
          case 66: // id 66 = Argentina
            this.extendedUser.idType = { id: 17, name: '' };
            break;
          case 74: // id 74 = Braziñ
            this.extendedUser.idType = { id: 19, name: '' };
            break;
          case 77: // id 77 =   Chile
            this.extendedUser.idType = { id: 77, name: '' };
            break;
          case 101: // id 101 =   Perú
            this.extendedUser.idType = { id: 101, name: '' };
            break;
          case 115: // id 115 =   VENEZUELA
            this.extendedUser.idType = { id: 115, name: '' };
            break;
        }
        this.extendedUser.mobileNumber = this.payeeCompanyFormComponent.legalRepresentativeMobileNumber.value;

        this.extendedUser.company = this.payeeCompanyFormComponent.companyName.value;
        this.extendedUser.birthDate = new Date(this.payeeCompanyFormComponent.companyConstitutionDate.value);
        this.extendedUser.taxId = this.payeeCompanyFormComponent.companyTaxId.value;
        this.extendedUser.idTypeTaxId = { id: this.payeeCompanyFormComponent.companyTaxIdType.value, name: '' };
        this.extendedUser.phoneNumber = this.payeeCompanyFormComponent.companyPhoneNumber.value;
        this.extendedUser.postalAddress = `${this.payeeCompanyFormComponent.companyPostalAddress.value} | ${this.payeeCompanyFormComponent.companyCpPostalAddress.value}`,
          this.extendedUser.postalCountry.id = +this.payeeCompanyFormComponent.companyCountryPostal.value.id;
        this.extendedUser.postalCity = {
          id: +this.payeeCompanyFormComponent.companyCityPostal.value, name: '',
          country: { id: 0, name: '' }
        };

        this.extendedUser.canChangePaymentMethod = false;
        this.extendedUser.useDirectPayment = true;
        this.extendedUser.confirmedProfile = true;
      } else {
        this.payeeCompanyFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        this.changeTabView(1);
        return false;
      }
      // END PAYEE COMPANY
    }
    else {
      // PAYEE

      if (this.payeeFormComponent.noPostalCountrySelected
        || this.payeeFormComponent.noPostalCitySelected) {
        this.payeeFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        this.changeTabView(1);
        return false;
      }

      if (this.payeeFormComponent.payeeForm.valid) {
        if (this.extendedUser.idType === null) {
          this.extendedUser.idType = {
            id: this.payeeFormComponent.payeeIdType.value,
            name: ''
          }
        }

        this.extendedUser.lastName1 = this.payeeFormComponent.payeeLastName1.value;
        this.extendedUser.lastName2 = this.payeeFormComponent.payeeLastName2.value;
        this.extendedUser.firstName1 = this.payeeFormComponent.payeeFirstName1.value;
        this.extendedUser.firstName2 = this.payeeFormComponent.payeeFirstName2.value;
        this.extendedUser.idNumber = this.payeeFormComponent.payeeIdNumber.value;
        this.extendedUser.gender = this.payeeFormComponent.payeeGender.value;
        this.extendedUser.taxId = this.payeeFormComponent.payeeTaxId.value;
        this.extendedUser.maritalStatus = this.payeeFormComponent.payeeMaritalStatus.value;
        this.extendedUser.mobileNumber = this.payeeFormComponent.payeeMobileNumber.value;
        this.extendedUser.phoneNumber = this.payeeFormComponent.payeePhoneNumber.value;
        this.extendedUser.birthDate = new Date(this.payeeFormComponent.payeeBirthDate.value);
        this.extendedUser.email = this.payeeFormComponent.payeeEmail.value;
        this.extendedUser.idType.id = this.payeeFormComponent.payeeIdType.value;
        this.extendedUser.residenceAddress = this.payeeFormComponent.payeePostalAddress.value;
        this.extendedUser.postalAddress = `${this.payeeFormComponent.payeePostalAddress.value} | ${this.payeeFormComponent.payeeCpPostalAddress.value}`,
          this.extendedUser.postalCountry.id = +((this.payeeFormComponent.payeeCountryPostal.value.id != null) ? this.payeeFormComponent.payeeCountryPostal.value.id : parseInt(this.payeeFormComponent.payeeCountryPostal.value));
        this.extendedUser.residenceCountry = {
          id: +((this.payeeFormComponent.payeeCountryPostal.value.id != null) ? this.payeeFormComponent.payeeCountryPostal.value.id : parseInt(this.payeeFormComponent.payeeCountryPostal.value)),
          name: ``
        };
        this.extendedUser.postalCity = {
          id: +this.payeeFormComponent.payeeCityPostal.value, 
          name: '',
          country: { id: 0, name: '' }
        };
        this.extendedUser.residenceCity = {
          id: +this.payeeFormComponent.payeeCityPostal.value, 
          name: '',
          country: { id: 0, name: '' }
        };
        // this.extendedUser.postalCity = {
        //   id: + ((this.payeeFormComponent.payeeCityPostal.value.id != null) ? this.payeeFormComponent.payeeCityPostal.value.id : parseInt(this.payeeFormComponent.payeeCityPostal.value)), name: '',
        //   country: { id: 0, name: '' }
        // };
        // this.extendedUser.residenceCity = {
        //   id: +((this.payeeFormComponent.payeeCityPostal.value.id != null) ? this.payeeFormComponent.payeeCityPostal.value.id : parseInt(this.payeeFormComponent.payeeCityPostal.value)), name: '',
        //   country: { id: 0, name: '' }
        // };

        this.extendedUser.canChangePaymentMethod = this.canChangePaymentMethod;
        this.extendedUser.useDirectPayment = this.bankPaymentMethod;
        this.extendedUser.confirmedProfile = true;
      } else {
        this.changeTabView(1);
        this.payeeFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }
      // END PAYEE
    }
    if (this.bankPaymentMethod) {
      // SET BANK INFO
      switch (this.countryPostalId) {
        case 1: // ID 1 = COLOMBIA
          this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
          this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
          this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
          this.extendedUser.directPaymentCity.id = this.bankFormComponent.bankCity.value;
          break;
        case 2: // ID 2 = MEXICO
          this.extendedUser.bankAccountNumber = this.bankFormComponent.clabeBankAccountNumber.value;
          break;
        case 66: // ID 66 = ARGENTINA
          this.extendedUser.bankAccountNumber = this.bankFormComponent.cbuBankAccountNumber.value;
          break;
        case 74: // ID 1 = BRAZIL
          this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
          this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
          this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
          this.extendedUser.bankBranch = this.bankFormComponent.bankBranch.value;
          break;
        case 77: // ID 77 = CHILE
          this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
          this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
          this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
          break;
        case 101: // ID 101 = Perú
          this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
          this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
          this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value.trim();
          break;
        case 115: // ID 115 = VENEZUELA
          this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
          this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
          this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value.trim();
          break;
      }
    }
    this.userService.updateExtendedUser(this.extendedUser).subscribe(
      extendedUser => {
        this.userSessionService.saveExtendedUser(extendedUser);
        this.changeTabView(1);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_PRFILE');
        this.showMessage(errorMessageTranslate.value, 'success');
        this.disableForm();
        this.initUser();
        this.sessionUser.payeeAsCompany
          ? this.payeeCompanyFormComponent.initExtendedUser(extendedUser)
          : this.payeeFormComponent.initExtendedUser(extendedUser);
      }
    );
  }

  updateUser() {
    let errorMessageTranslate: any;
    this.payeeFormInvalid = false;
    this.userFormInvalid = false;
console.log(this.payeeCompanyFormComponent);
    if (this.userSessionService.isPayee()) {

      if (this.sessionUser.payeeAsCompany) {
        // PAYEE COMPANY
        if (this.payeeCompanyFormComponent.noPostalCountrySelected
          || this.payeeCompanyFormComponent.noResidenceCountrySelected || this.payeeCompanyFormComponent.noPostalCitySelected) {
          errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
          this.showMessage(errorMessageTranslate.value, 'error');
          return false;
        }

        if (this.payeeCompanyFormComponent.payeeCompanyForm.valid) {
          if (this.payeeCompanyFormComponent.companyConstitutionDate.value) {
            this.extendedUser.birthDate = new Date(this.payeeCompanyFormComponent.companyConstitutionDate.value);
          } else {
            this.extendedUser.birthDate = null;
          }
          this.extendedUser.phoneNumber = this.payeeCompanyFormComponent.companyPhoneNumber.value;
          this.extendedUser.mobileNumber = this.payeeCompanyFormComponent.legalRepresentativeMobileNumber.value;
          this.extendedUser.residenceCountry = { id: +(this.payeeCompanyFormComponent.legalRepresentativeCountryResidence.value), name: ""};
        } else {
          this.payeeCompanyFormInvalid = true;
          errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
          this.showMessage(errorMessageTranslate.value, 'error');
          return false;
        }
        // END PAYEE COMPANY
      } else {
        // PAYEE PERSON
        if (this.payeeFormComponent.noPostalCountrySelected
          || this.payeeFormComponent.noPostalCitySelected) {
          errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
          this.showMessage(errorMessageTranslate.value, 'error');
          return false;
        }

        if (this.payeeFormComponent.payeeForm.valid) {
          this.extendedUser.taxId = this.payeeFormComponent.payeeTaxId.value;
          this.extendedUser.birthDate = new Date(this.payeeFormComponent.payeeBirthDate.value);

          // this.extendedUser.postalAddress = this.payeeFormComponent.payeePostalAddress.value;
          this.extendedUser.postalAddress = `${this.payeeFormComponent.payeePostalAddress.value} | ${this.payeeFormComponent.payeeCpPostalAddress.value}`,
            // this.extendedUser.cpPostalAddress = this.payeeFormComponent.payeeCpPostalAddress.value;
            this.extendedUser.mobileNumber = this.payeeFormComponent.payeeMobileNumber.value;
          this.extendedUser.phoneNumber = this.payeeFormComponent.payeePhoneNumber.value;
          this.extendedUser.maritalStatus = this.payeeFormComponent.payeeMaritalStatus.value;
          this.extendedUser.postalCity.id = +((this.payeeFormComponent.payeeCityPostal.value.id != null) ? this.payeeFormComponent.payeeCityPostal.value.id : this.payeeFormComponent.payeeCityPostal.value);
        } else {
          this.payeeFormInvalid = true;
          errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
          this.showMessage(errorMessageTranslate.value, 'error');
          return false;
        }
      }
      // END PAYEE PERSON
    } else {
      if (this.userFormComponent.userForm.valid) {
        this.extendedUser.mobileNumber = this.userFormComponent.userMobileNumber.value;
        this.extendedUser.birthDate = new Date(this.userFormComponent.userBirthDate.value);
      } else {
        this.userFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }
    }
    console.log(this.extendedUser);
    if (this.countryPostalId == 101 || this.countryPostalId == 115) {

      if (this.bankFormComponent.bankForm.status === "INVALID" || this.bankFormComponent.bankForm.value.bank === null) {
        // this.bankFormInvalid = true;
        this.changeTabView(2);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        this.updateUserBankInfo();
        return false; 
      }

      if (!this.payeeFormComponent.payeeForm.valid) {
        this.changeTabView(1);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false; 
      }

    }
    this.userService.updateExtendedUser(this.extendedUser).subscribe(
      extUser => {
        this.userSessionService.saveExtendedUser(extUser);
        this.initUser();
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_PROFILE_UPDATE');
        this.showMessage(errorMessageTranslate.value, 'success');
        this.disableForm();
        return true;
      }
    );
  }

  updateUserBankInfo() {
    let errorMessageTranslate: any;
    let extendedUserObj = {};
    this.bankFormInvalid = false;

    if (this.bankPaymentMethod) {
      if (this.bankFormComponent.bankForm.valid) {
        if (this.extendedUser.directPaymentBank) {
          this.extendedUser.useDirectPayment = this.bankPaymentMethod;

          switch (this.countryPostalId) {
            case 1: // 1 = Colombia
              this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
              this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
              this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
              this.extendedUser.directPaymentCity.id = this.bankFormComponent.bankCity.value;
              break;
            case 2: // 2 = Mexico
              this.extendedUser.bankAccountNumber = this.bankFormComponent.clabeBankAccountNumber.value;
              break;
            case 66: // 66 = Argentina
              this.extendedUser.bankAccountNumber = this.bankFormComponent.cbuBankAccountNumber.value;
              break;
            case 74: // 74 = BRAZIL
              this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
              this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
              this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
              this.extendedUser.bankBranch = this.bankFormComponent.bankBranch.value;
              break;
            case 77: // 77 = CHILE
              this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value;
              this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
              this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
              break;
            case 101: // 101 = Perú
              this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value.trim();
              this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
              this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
              break;
            case 115: // 115 = VENEZUELA
              this.extendedUser.bankAccountNumber = this.bankFormComponent.bankAccountNumber.value.trim();
              this.extendedUser.bankAccountType = this.bankFormComponent.bankAccountType.value;
              this.extendedUser.directPaymentBank.id = this.bankFormComponent.bank.value;
              break;
          }
          extendedUserObj = this.extendedUser;
        }
        else {
          switch (this.countryPostalId) {
            case 1: // 1 = Colombia
              extendedUserObj = {
                id: this.extendedUser.id,
                useDirectPayment: this.bankPaymentMethod,
                bankAccountNumber: this.bankFormComponent.bankAccountNumber.value,
                bankAccountType: this.bankFormComponent.bankAccountType.value,
                directPaymentBank: {
                  id: this.bankFormComponent.bank.value,
                },
                directPaymentCity: {
                  id: this.bankFormComponent.bankCity.value,
                },
              };
              break;
            case 2: // 2 = Mexico
              extendedUserObj = {
                id: this.extendedUser.id,
                useDirectPayment: this.bankPaymentMethod,
                bankAccountNumber: this.bankFormComponent.clabeBankAccountNumber.value,
              };
              break;
            case 66: // 66 = Argentina
              extendedUserObj = {
                id: this.extendedUser.id,
                useDirectPayment: this.bankPaymentMethod,
                bankAccountNumber: this.bankFormComponent.cbuBankAccountNumber.value,
              };
              break;
            case 74: // 1 = Brazil
              extendedUserObj = {
                id: this.extendedUser.id,
                useDirectPayment: this.bankPaymentMethod,
                bankAccountNumber: this.bankFormComponent.bankAccountNumber.value,
                bankBranch: this.bankFormComponent.bankBranch.value,
                bankAccountType: this.bankFormComponent.bankAccountType.value,
                directPaymentBank: {
                  id: this.bankFormComponent.bank.value,
                },
              };
              break;
            case 77: // 77 = CHILE

              extendedUserObj = {
                id: this.extendedUser.id,
                bankAccountNumber: this.bankFormComponent.bankAccountNumber.value,
                bankAccountType: this.bankFormComponent.bankAccountType.value,
                directPaymentBank: {
                  id: this.bankFormComponent.bank.value,
                },
              };
              break;
            case 101: // 101 = Perú

              extendedUserObj = {
                id: this.extendedUser.id,
                bankAccountNumber: this.bankFormComponent.bankAccountNumber.value.trim(),
                bankAccountType: this.bankFormComponent.bankAccountType.value,
                directPaymentBank: {
                  id: this.bankFormComponent.bank.value,
                },
              };
              break;
            case 115: // 115 = VENEZUELA

              extendedUserObj = {
                id: this.extendedUser.id,
                bankAccountNumber: this.bankFormComponent.bankAccountNumber.value.trim(),
                bankAccountType: this.bankFormComponent.bankAccountType.value,
                directPaymentBank: {
                  id: this.bankFormComponent.bank.value,
                },
              };
              break;
          }
        }
      } else {
        this.bankFormInvalid = true;
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_REQUIRED_BANKS');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }
    } else {
      this.extendedUser.useDirectPayment = this.bankPaymentMethod;
      extendedUserObj = this.extendedUser;
    }

    if (this.countryPostalId == 101 || this.countryPostalId == 115) {
      if (!this.payeeFormComponent.payeeForm.valid) {
        this.changeTabView(1);
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
        this.showMessage(errorMessageTranslate.value, 'error');
        this.updateUser()
        return false; 
      }
    }

    this.userService.updateExtendedUser(extendedUserObj).subscribe(
      extUser => {
        this.userSessionService.saveExtendedUser(extUser);
        this.initUser();
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_BANK_UPDATED');
        this.showMessage(errorMessageTranslate.value, 'success');
        this.disableForm();
        return true;
      }
    );
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  showMessageWithOpt(title: string, message: string, type: string, time: number) {
    this.messageComponent.setTypeMessage(message, type, true, title, time);
  }

  disableForm() {
    if (this.userSessionService.isPayee()) {

      this.bankFormComponent.disableBankFormEditing();
      if (this.sessionUser.payeeAsCompany) {
        this.payeeCompanyFormComponent.disablePayeeCompanyFormEditing();
      } else {
        this.payeeFormComponent.disablePayeeFormEditing();
      }
    } else {
      this.userFormComponent.disableUserFormEditing();
    }
    this.modifyMode = false;
  }

  changeTabView(index) {
    this.selectedIndex.setValue(index);
  }

  changePostalCountry(id) {
    this.countryPostalId = id;
    if (id === 66) { // ID 66 = ARGENTINA
      this.bankPaymentMethod = true;
    }
    if (id !== 0) {
      setTimeout(() => {
        this.bankFormComponent.cleanBankFormValidator();
        this.bankFormComponent.setBankFormValidator();
      }, 0);
    }
  }

  seeEditProfileButtonHandler(): boolean {
    if (this.userSessionService.isPayee() && this.userSessionService.haveStatus() && !this.extendedUser.confirmedProfile) {
      // this.modifyMode = true;
      return false;
    }
    else if (this.userSessionService.role === 'ROLE_MERCHANT' && !this.extendedUser.confirmedProfile) {// Entra cuando se crea un payee desde merchart y confirmas el payee
      return true;
    }
    else {
      return this.userSessionService.loadUserStatus() === 'ACCEPTED' && !this.modifyMode &&
        (this.userSessionService.isReSeller() ||
          this.userSessionService.isPayee());
    }
  }

  fileUploadMessageHandler(type: string) {
    let errorMessageTranslate: any;

    let typeMsg = 'error';
    let msg;
    switch (type) {
      case 'format_error':
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.INCORRECT_FILE_FORMAT');
        msg = errorMessageTranslate.value;
        break;
      case 'size_error':
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.FILE_SIZE_MUST_BE_5MB');
        msg = errorMessageTranslate.value;
        break;
      case 'upload_correct':
        typeMsg = 'success';
        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.FILE_UPLOAD_CORRECTLY');
        msg = errorMessageTranslate.value;
        break;
    }
    this.showMessage(msg, typeMsg);
  }

  whenSeeChangePaymentMethod(): boolean {
    let canSee = false;

    if (this.countryPostalId === 2) { // ID 66 = ARGENTINA
      canSee = true;
    }
    return canSee;
  }

  validacionInput(e: any, type: string) {
    const key = e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    const numeros = type === 'numeros' ? "0123456789" :
      'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    const especiales = ['8', '164', '165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key == especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }

  getInfoUser() {
    this.sessionUser = this.userSessionService.loadUser();
    this.userService.getExtendedUserByUserId(this.sessionUser.id).subscribe( extendedUser => {
      this.userSessionService.saveUserSession(this.sessionUser, extendedUser.content[0]);
      // this.initUser();
    });
  }
}