import {ChangeDetectorRef, Component, EventEmitter,
        Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {merge, of as observableOf} from 'rxjs';
import {MatPaginator, MatSort} from '@angular/material';
import {DataFinderService} from '../../../../services/data-finder.service';
import {ExtendedUser} from '../../../../config/interfaces';
import {ROLES} from '../../../../config/enums';

@Component({
  selector: 'app-table-user-setting',
  templateUrl: './tableUserSetting.component.html',
  styleUrls: ['./tableUserSetting.component.scss']
})
export class TableUserSettingComponent implements OnChanges {

  @Input() countrySelected: any;
  @Input() roleFilterSelected: string;
  @Input() menuCollapse: boolean;

  @Output() public openCommissionMerchantEditModal = new EventEmitter<any>();
  @Output() public openCommissionReSellerEditModal = new EventEmitter<any>();

  roles = ROLES;
  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;
  isComponentInit = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  dataTable: ExtendedUser[] = [];
  pageSize = 10;

  uri = '/api/_search/extended-users';
  query = '';

  errorMessage = 'Error getting users';
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private dataFinder: DataFinderService,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.menuCollapse && this.isComponentInit) { return; } // Do not refresh table when expanding filter
    this.isComponentInit = true;
    this.initTable();
  }

  initTable() {
    this.setColumnDefinition();
    setTimeout(() => {
      this.loadTable();
    }, 0);
  }

  setColumnDefinition() {
    if (this.roleFilterSelected === ROLES.RESELLER) {
      this.columnNames = {
        id: 'ID Number', fullName: 'Reseller', commissionFixed: 'Fixed Commission',
        commissionPercentage: 'Percentage Commission', action: ''
      };
    } else {
      this.columnNames = {
        id: 'ID Number', company: 'Company', fullName: 'Merchant', bankCommission: 'Bank Commission',
        fxCommission: 'Fx Commission', rechargeCost: 'Recharge Cost',
        useMerchantCommission: 'Use Merchant Commission', action: ''
      };
    }
    this.columns = Object.keys(this.columnNames);
  }

  loadTable() {
    this.setQuery();
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataTable = data;
    });

    this.cdRef.detectChanges();
  }

  setQuery() {
    this.query = `role:${this.roleFilterSelected} AND postalCountry.id:${this.countrySelected.id}`;
  }

  callOpenCommissionEditModal(extendedUser: ExtendedUser): void {
    if (this.roleFilterSelected === ROLES.RESELLER) {
      this.openCommissionReSellerEditModal.emit(extendedUser);
    } else {
      this.openCommissionMerchantEditModal.emit(extendedUser);
    }
  }

}
