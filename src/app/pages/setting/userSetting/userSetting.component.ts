import {Component, OnInit, ViewChild} from '@angular/core';
import {DataFinderService} from '../../../services/data-finder.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ExtendedUser} from '../../../config/interfaces';
import {CommissionModalComponent} from '../../../components/components.index';
import {MessageNotificationComponent} from '../../../components/messageNotification/messageNotification.component';
import {ROLES} from '../../../config/enums';
import {UserService} from '../../../services/user/user.service';
import {TableUserSettingComponent} from './tableUserSetting/tableUserSetting.component';

@Component({
  selector: 'app-user-setting',
  templateUrl: './userSetting.component.html',
  styleUrls: ['./userSetting.component.scss']
})
export class UserSettingComponent implements OnInit {

  roles = ROLES;
  countries = [];
  countryForm: FormGroup;
  filteredCountries: Observable<string[]> | any;
  countrySelected: any;

  selectedIndex = new FormControl(0);
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;

  errorMessage = 'Error getting users';
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  @ViewChild('MerchantTable', {static: false}) merchantTableUserSetting: TableUserSettingComponent;
  @ViewChild('ReSellerTable', {static: false}) reSellerTableUserSetting: TableUserSettingComponent;

  constructor(private dataFinder: DataFinderService,
              private userService: UserService,
              public dialog: MatDialog) {
    this.countryForm = new FormGroup({
      country: new FormControl('', Validators.required),
    });
  }

  get country() { return this.countryForm.get('country'); }

  ngOnInit() {
    this.initCountryFilter();
  }

  // COUNTRY SEARCH
  initCountryFilter() {
    this.filteredCountries = this.country.valueChanges
      .pipe(
        startWith(''),
        map(value => this._countryFilter(value))
      );
  }

  private _countryFilter(value: string): string[] {
    if (value) {
      if (value.length >= 2) {
        this.loadCountry(value);
      } else {
        this.countries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.countries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadCountry(name: string) {
    this.dataFinder.getCountriesForName(name).subscribe(
      data => {
        this.countries = data.content;
      }
    );
  }

  displayCountry(country): string {
    return country ? country.name : country;
  }
  // END COUNTRY SEARCH

  onChangeCountry() {
    if (this.country.value.id) {
      this.countrySelected = this.country.value;
    } else {
      this.countrySelected = null;
    }
  }

  changeTab() {
    this.countrySelected = null;
    this.countryForm.reset();
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  openCommissionMerchantEditModal(extendedUser: ExtendedUser): void {
    const dialogRef = this.dialog.open(CommissionModalComponent, {
      data: {extendedUser, readOnly: false, role: ROLES.MERCHANT}
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.value === 'UPDATE') {
        this.userService.updateExtendedUser(result.extendedUser).subscribe(
          () => {
            this.merchantTableUserSetting.initTable();
            this.showMessage('Merchant commission update', 'success');
          });

      }
    });
  }

  openCommissionReSellerEditModal(extendedUser: ExtendedUser): void {
    const dialogRef = this.dialog.open(CommissionModalComponent, {
      data: {extendedUser, readOnly: false, role: ROLES.RESELLER},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.value === 'UPDATE') {
        const body = {
          id: result.extendedUser.id,
          resellerFixedCommission: result.extendedUser.resellerFixedCommission,
          resellerPercentageCommission: result.extendedUser.resellerPercentageCommission,
        };
        this.userService.updateExtendedUser(body).subscribe(
          () => {
            this.reSellerTableUserSetting.initTable();
            this.showMessage('Reseller commission update', 'success');
          });
      }
    });
  }
}
