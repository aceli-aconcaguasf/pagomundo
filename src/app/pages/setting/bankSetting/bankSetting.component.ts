import { Component, OnInit, ViewChild} from '@angular/core';
import {DataFinderService} from '../../../services/data-finder.service';
import {Observable} from 'rxjs';
import { map, startWith} from 'rxjs/operators';
import {MatDialog} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BankService} from '../../../services/service.index';
import {MessageNotificationComponent} from '../../../components/messageNotification/messageNotification.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-bank-setting',
  templateUrl: './bankSetting.component.html',
  styleUrls: ['./bankSetting.component.scss']
})
export class BankSettingComponent implements OnInit {

  bankId = '';
  bankAccountId = '';
  bankCommission = 0;
  fxCommission = 0;
  rechargeCost = 0;
  banks = [];
  bankAccounts = [];
  countries = [];
  countryForm: FormGroup;
  filteredCountries: Observable<string[]> | any;
  countrySelected: any;
  bankAccount: any;
  invalidInput = false;
  seeBank = false;

  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  viewEditBankCommission = false;
  selectedIndex = new FormControl(0);
  changeClass:number;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor(private dataFinder: DataFinderService,
              private bankService: BankService,
              public dialog: MatDialog,
              private translate: TranslateService) {
    
                this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    this.countryForm = new FormGroup({
      country: new FormControl('', Validators.required),
    });
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  static truncNumber(x, position = 0) {
    const s = x.toString();
    const decimalLength = s.indexOf('.') !== -1 ?
      s.indexOf('.') + 1
      : (s.indexOf(',') !== -1 ?
        s.indexOf(',') + 1 : 0);
    const numStr = s.substr(0, decimalLength + position);
    return Number(numStr);
  }

  get country() { return this.countryForm.get('country'); }

  ngOnInit() {
    this.initCountryFilter();
  }

  // COUNTRY SEARCH
  initCountryFilter() {
    this.filteredCountries = this.country.valueChanges
      .pipe(
        startWith(''),
        map(value => this._countryFilter(value))
      );
  }

  private _countryFilter(value: string): string[] {
    this.seeBank = false;
    this.resetValues();
    if (value) {
      if (value.length >= 2) {
        this.loadCountry(value);
      } else {
        this.countries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.countries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadCountry(name: string) {
    this.dataFinder.getCountriesForName(name).subscribe(
      data => {
        this.countries = data.content;
      }
    );
  }

  displayCountry(country): string {
    return country ? country.name : country;
  }
  // END COUNTRY SEARCH

  onChangeCountry() {
    if (this.country.value.id) {
      this.resetValues();
      this.countrySelected = this.country.value;
      this.dataFinder.getAllBankAccounts(this.countrySelected.id).subscribe(
        accounts => {
          this.seeBank = true;
          const arrayBankId = [];
          for (const account of accounts.content) {
            const index = account.bank.id;
            if (arrayBankId.indexOf(index) === -1) {
              arrayBankId.push(index);
              this.dataFinder.getBanksById(index).subscribe(
                bank => {
                  if (bank.content[0]) {
                    this.banks.push(bank.content[0]);
                  }
                });
            }
          }
        });
    } else {
      this.countrySelected = null;
    }
  }

  resetValues() {
    this.countrySelected = null;
    this.bankId = '';
    this.bankAccountId = '';
    this.bankAccount = null;
    this.banks = [];
    this.seeBank = false;
    if (document.getElementById('banks')) {
      (document.getElementById('banks') as HTMLSelectElement).selectedIndex = 0;
    }
    if (document.getElementById('bankAccount')) {
      (document.getElementById('bankAccount') as HTMLSelectElement).selectedIndex = 0;
    }
  }

  selectBank(event) {
    this.bankAccountId = '';
    if (document.getElementById('bankAccount')) {
      (document.getElementById('bankAccount') as HTMLSelectElement).selectedIndex = 0;
    }
    this.bankId = event.target.value;
    this.dataFinder.getBankAccountsByBankId(this.bankId).subscribe(data => {
      this.bankAccounts = data.content;
    });
  }

  selectBankAccount(event) {
    this.bankAccountId = event.target.value;
    this.bankAccount = this.bankAccounts.find(bank => {
      return bank.id.toString() === this.bankAccountId;
    });
    this.loadCommission();
  }

  loadCommission() {
    this.bankCommission = BankSettingComponent.truncNumber((this.bankAccount.bankCommission * 100), 2);
    this.fxCommission = BankSettingComponent.truncNumber((this.bankAccount.fxCommission * 100), 2);
    this.rechargeCost = this.bankAccount.rechargeCost;
  }

  handleClickEditBankCommission(type: string) {
    this.invalidInput = false;
    switch (type) {
      case 'edit':
        this.viewEditBankCommission = true;
        break;
      case 'cancel':
        this.loadCommission();
        this.viewEditBankCommission = false;
        break;
      case 'save':
        if (this.validateCommissionsInput()) {
          const bankAccountUpdate = {
            id: this.bankAccount.id,
            // @ts-ignore
            bankCommission: (this.bankCommission.toFixed(2) / 100),
            // @ts-ignore
            fxCommission: (this.fxCommission.toFixed(2) / 100),
            rechargeCost: this.rechargeCost,
          };
          this.bankService.updateBankAccount(bankAccountUpdate).subscribe(
            (data) => {
              this.bankAccount = data;
              this.loadCommission();
              let messageTranslate:any = this.translate.get( 'MESSAGE.BANK_ACCOUNT_UPDATE' );
              this.showMessage( messageTranslate.value, 'success');
              this.viewEditBankCommission = false;
            });
        } else {
          this.invalidInput = true;
        }
        break;
      default:
        break;
    }
  }

  validateCommissionsInput(): boolean {
    let value = true;
    if (this.rechargeCost === null || this.rechargeCost < 0
      || this.bankCommission === null || this.bankCommission < 0
      || this.fxCommission === null || this.fxCommission < 0) {
      value = false;
    }
    return value;
  }

  errorInput(type: any): boolean {
    let value = false;
    switch (type) {
      case 'BANK_COMMISSION':
        value = (this.bankCommission === null || this.bankCommission < 0) && this.invalidInput;
        break;
      case 'FX_COMMISSION':
        value = (this.fxCommission === null || this.fxCommission < 0) && this.invalidInput;
        break;
      case 'RECHARGE_COST':
        value = (this.rechargeCost === null || this.rechargeCost < 0) && this.invalidInput;
        break;
    }
    return value;
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

}
