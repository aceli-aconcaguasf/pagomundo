import {Routes} from '@angular/router';
import {UserOnlyProfileViewGuard} from '../../services/guards/userOnlyProfileView.guard.service';
import {UserSettingComponent} from './userSetting/userSetting.component';
import {BankSettingComponent} from './bankSetting/bankSetting.component';


export const settingRoutes: Routes = [
  {path: 'bankSettings', component: BankSettingComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'userSettings', component: UserSettingComponent, canActivate: [UserOnlyProfileViewGuard]}
];

