import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import {BehaviorSubject, Observable} from 'rxjs';
import { language } from 'src/app/app.reducer';
import {UserService} from '../../services/user/user.service';

@Component({
  selector: 'app-report',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  keyLanguage$: Observable<any> = this.store.select('language');
  navLinks = [
    {label: 'Bank', path: 'bankSettings'},
    {label: 'Users', path: 'userSettings'},
  ];

  constructor(private userService: UserService,
    private translate: TranslateService,
    private store: Store<language>) {
      this.keyLanguage$.subscribe(item => {
        const language: any = item
        this.translate.use(language.key);
        let bank:any = this.translate.get('DATA_TABLE.BANK');
        let users:any = this.translate.get('TITLES.USERS');
        this.navLinks = [
          {label: bank.value, path: 'bankSettings'},
          {label: users.value, path: 'userSettings'},
        ];
      });
    }

  ngOnInit() {}

}
