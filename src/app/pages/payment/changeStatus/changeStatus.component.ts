import {AfterViewInit, Component, Input, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {merge, of as observableOf, BehaviorSubject, Observable} from 'rxjs';
import {DataFinderService} from '../../../services/data-finder.service';
import {environment} from '../../../../environments/environment';
import {MessageNotificationComponent} from '../../../components/components.index';
import {PaymentService} from '../../../services/payment/payment.service';
import {UserService} from 'src/app/services/service.index';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TooltipPosition} from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-change-status',
  templateUrl: './changeStatus.component.html',
  styleUrls: ['./changeStatus.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class ChangeStatusComponent implements AfterViewInit {

  environment = environment;
  bankAccounts = [];
  bankAccountId = '';
  totalToProcess = [];

  banks = [];
  allBankAccounts = [];
  bankId = '';
  @Input() role: any;

  uri = '/api/_search/transactions';
  query = '';
  statusReason = '';

  payeeCountries = [];
  countrySelected: any;
  filteredPayeeCountries: Observable<string[]> | any;
  countryForm: FormGroup;
  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;
  seeBank = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  data: any[] = [];

  keyLanguage$: Observable<any> = this.store.select('language');
  errorMessage = 'Error getting payments.';
  positionOptions: TooltipPosition =  'above';
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  statingTypingPlaceholder:any;
  statusReasonPlaceholder:any;
  changeClass:any;
  constructor(private router: Router,
              private paymentService: PaymentService,
              private dataFinder: DataFinderService,
              private userService: UserService,
              private translate: TranslateService,
              private store: Store<language>) {
    this.countryForm = new FormGroup({
      countryPayee: new FormControl('', Validators.required),
    });
    setTimeout(() => {
      this.setTable();
    }, 500);
    this.keyLanguage$.subscribe(item => {
      const language: any = item
      this.translate.use(language.key);
      let errorMessageTranslate:any = this.translate.get( 'ERROR_MESSAGES.ERROR_GETTING_USERS' );
      this.errorMessage = errorMessageTranslate.value;
      this.ngAfterViewInit();
    });
    this.initPayeeCountryFilter();
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  get CountryPayee() { return this.countryForm.get('countryPayee'); }

  ngAfterViewInit() {
    this.setTable();
  }
  setTable() {
    let admin:any = this.translate.get( 'REPORTS_MESSAGES.ADMIN' );
    let merchant:any = this.translate.get( 'DATA_TABLE.MERCHANT_COMPANY' );
    let payye:any = this.translate.get( 'REPORTS_MESSAGES.PAYEE_BUSINESS_ACCOUNT' );
    let lastUpdated:any = this.translate.get( 'DATA_TABLE.DATE' );
    let amountBeforeCommission:any = this.translate.get( 'DATA_TABLE.AMOUNT' );
    let amountAfterCommission:any = this.translate.get( 'DATA_TABLE.AFTER_COMMISSION' );
    let amountLocalCurrency:any = this.translate.get( 'DATA_TABLE.LOCAL_CURRENCY' );

    this.columnNames = {
      check: '',
      id: 'Id', 
      admin: admin.value,
      merchant: merchant.value,
      payee: payye.value,
      lastUpdated: lastUpdated.value,
      amountBeforeCommission: amountBeforeCommission.value,
      amountAfterCommission: amountAfterCommission.value,
      amountLocalCurrency: amountLocalCurrency.value
    };
    this.columns = Object.keys(this.columnNames);
  }
  initPayeeCountryFilter() {
    this.filteredPayeeCountries = this.CountryPayee.valueChanges
      .pipe(
        startWith(''),
        map(value => this._payeeCountryFilter(value))
      );
  }

  private _payeeCountryFilter(value: string): string[] {
    this.bankId = '';
    this.bankAccountId = '';
    this.seeBank = false;
    if (value) {
      if (value.length >= 2) {
        this.loadPayeeCountry(value);
      } else {
        this.payeeCountries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.payeeCountries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadPayeeCountry(name: string) {
    this.dataFinder.getCountriesForPayeeAndName(name).subscribe(
      data => {
        this.payeeCountries = data.content;
      });
  }

  displayFnPayeeCountry(country): string {
    return country ? country.name : country;
  }

  allCheckElement() {
    // allCheck
    if ((document.getElementById('allCheck') as HTMLInputElement).checked) {
      this.totalToProcess = [...this.data];
      for (const process of this.totalToProcess) {
        (document.getElementById(process.id) as HTMLInputElement).checked = true;
      }
    } else {
      for (const process of this.totalToProcess) {
        (document.getElementById(process.id) as HTMLInputElement).checked = false;
      }
      this.totalToProcess = [];
    }
  }

  getQueryString(): string {
    let query = 'status:IN_PROCESS';
    query += this.bankAccountId !== ''
      ? ' AND bankAccount.id:' + this.bankAccountId
      : '';
    return query;
  }


  loadTable() {
    this.query = this.getQueryString();
    // If the user changes the sort order, reset back to the first page.
    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query,
            this.sort.active, this.sort.direction, 25);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          // this.totalToProcess = [...data.content];
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
  }

  onChangeCountry() {
    this.banks = [];
    this.allBankAccounts = [];
    if (document.getElementById('banks')) {
      (document.getElementById('banks') as HTMLSelectElement).selectedIndex = 0;
    }
    if (this.CountryPayee.value.id) {
      this.countrySelected = this.CountryPayee.value;
      this.dataFinder.getAllBankAccounts(this.countrySelected.id).subscribe(
        accounts => {
          this.seeBank = true;
          this.allBankAccounts = accounts.content;
          const arrayBankId = [];
          for (const account of this.allBankAccounts) {
            const index = account.bank.id;
            if (arrayBankId.indexOf(index) === -1) {
              arrayBankId.push(index);
              this.dataFinder.getBanksById(index).subscribe(
                bank => {
                  if (bank.content[0]) {
                    this.banks.push(bank.content[0]);
                  }
                });
            }
          }
        });
    } else {
      this.countrySelected = null;
      this.seeBank = false;
    }
  }

  selectBank(event) {
    this.bankAccountId = '';
    if (document.getElementById('bankAccount')) {
      (document.getElementById('bankAccount') as HTMLSelectElement).selectedIndex = 0;
    }
    this.bankId = event.target.value;
    this.bankAccounts = this.allBankAccounts.filter( account => account.bank.id === +this.bankId);
  }

  selectBankAccount(event) {
    this.bankAccountId = event.target.value;
    setTimeout(() => {
      this.loadTable();
    }, 0);
  }

  addCheckElement(payment: any) {
    if ((document.getElementById(payment.id) as HTMLInputElement).checked) {
      this.totalToProcess.push(payment);
    } else {
      const index = this.totalToProcess.indexOf(payment);
      if (index !== -1) {
        this.totalToProcess.splice(index, 1);
      }
    }
    (document.getElementById('allCheck') as HTMLInputElement).checked = this.data.length === this.totalToProcess.length;

  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  changePaymentStatus(changeType: string) {
    if (this.totalToProcess.length > 0) {
      let status = '';
      let message = '';
      
      let payAccept:any = this.translate.get( 'MESSAGE.PAYMENTS_ACCEPTED' );
      let payReject:any = this.translate.get( 'MESSAGE.PAYMENTS_REJECTED' );
      let mustHaveStatu:any = this.translate.get( 'MESSAGE.MUST_HAVE_STATUS_REASON' );

      switch (changeType) {
        case 'ACCEPT':
          status = 'ACCEPTED';
          message = payAccept.value;
          break;
        case 'REJECT':
          status = 'REJECTED';
          message = payReject.value;
          if (this.statusReason.length <= 0) {
            this.showMessage( mustHaveStatu.value, 'error');
            return null;
          }
          break;
        default:
          break;
      }

      this.paymentService.changeMassivePaymentStatus(this.totalToProcess, this.statusReason, status).subscribe(
        data => {
          if (data.transactionsWithErrors.length === 0) {
            this.showMessage(message, 'success');
          } else {
            let selectNameTraducer:any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SOME_PAYMENTS_COULD_NOT_CHANGE');
            this.showMessage(selectNameTraducer.value, 'warning');
          }
          this.reloadPage();
        });
    } else {
      let selectNameTraducer:any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SELECT_ONE_PAYMENT');
      this.showMessage(selectNameTraducer.value, 'error');
    }
  }

  reloadPage() {
    this.loadTable();
    this.statusReason = '';
  }
}