import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DataFinderService, PaymentService, UserService, UserSessionService} from '../../../services/service.index';
import {environment} from '../../../../environments/environment';
import {MessageNotificationComponent} from '../../../components/components.index';
import {ExtendedUser, Payee, User} from '../../../config/interfaces';
import {BehaviorSubject, Observable} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatTableDataSource, TooltipPosition} from '@angular/material';
import {AUTOCOMPLETE_TYPE, ROLES} from '../../../config/enums';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { language } from 'src/app/app.reducer';


@Component({
  selector: 'app-new-payment',
  templateUrl: './newPayment.component.html',
  styleUrls: ['./newPayment.component.scss'],
})


export class NewPaymentComponent implements OnInit {
  steps = [];
  countrySelected: any;
  countryForm: FormGroup;
  autocompleteType = AUTOCOMPLETE_TYPE;

  index = 1;
  totalToPay = 0;
  currentAccount = 0;
  check = `<i class="icofont-check-alt"></i>`;
  positionOptions: TooltipPosition = 'above';
  genericDescription = '';

  invalidateStep = false;

  payeesData: Payee[] = [];
  payees: Payee[] = [];
  nameSearch = '';
  idNumberSearch = '';
  nicknameSearch = '';

  payeesToPayData: Payee[] = [];
  payeesToPayFilter: Payee[] = [];
  payeesToPay: Payee[] = [];
  rightFilter: string;

  preSelectedPayees: Payee[] = [];
  postSelectedPayees: Payee[] = [];

  data: any[];
  environment = environment;
  extendedUser: ExtendedUser;
  user: User;

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  keyLanguage$: Observable<any> = this.store.select('language');
  searchPayeesNickname:any;
  selectPayees:any;
  addAmount:any;
  confirmPayment:any;
  changeClass:number;
  constructor(private router: Router,
              private dataFinder: DataFinderService,
              private userService: UserService,
              public userSessionService: UserSessionService,
              private paymentService: PaymentService,
              private translate: TranslateService,
              private store: Store<language>) {
                this.keyLanguage$.subscribe(item => {
                  const language: any = item
                  this.translate.use(language.key);
                  this.selectPayees = this.translate.get('BUTTON_OPTION_INFO.SELECTED_PAYEES_STEP');
                  this.addAmount = this.translate.get('BUTTON_OPTION_INFO.ADD_AMOUNT');
                  this.confirmPayment = this.translate.get('BUTTON_OPTION_INFO.CONFIRM_PAYMENTS');
                  this.steps = [
                    {step: 1, stepIcon: '1', title: this.selectPayees.value, isActive: true},
                    {step: 2, stepIcon: '2', title:  this.addAmount.value, isActive: false},
                    {step: 3, stepIcon: '3', title: this.confirmPayment.value, isActive: false}
                  ];
                })
    this.user = this.userSessionService.loadUser();
    this.extendedUser = this.userSessionService.loadExtendedUser();
    this.currentAccount = this.extendedUser.balance;
    this.countryForm = new FormGroup({
      countryPayee: new FormControl('', Validators.required),
    });
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  ngOnInit() {
    this.userService.accountBalance.subscribe(value => {
      this.extendedUser.balance = value;
      this.currentAccount = value;
      this.sumAmount();
    });
  }

  displayedColumns = ['delete', 'name', 'country', 'id', 'amount', 'euro', 'description']; // step 2
  displayedColumns3 = ['name', 'country', 'id', 'amount', 'euro', 'description']; // step 3
  // dataSource: this.payeesToPay;

  get countryPayee() {
    return this.countryForm.get('countryPayee');
  }

  resetRelatedVariables() {
    this.countrySelected = null;
  }

  onChangeCountry(country) {
    this.countrySelected = country;
    this.nameSearch = '';
    this.idNumberSearch = '';
    this.loadTablePayee();
  }

  loadTablePayee() {
    this.payeesData = [];
    const uri = '/api/_search/extended-user-relations';
    let query = `userRelated.id:${this.user.id} AND extendedUser.role:${ROLES.PAYEE}` +
      ` AND extendedUser.postalCountry.id:${this.countrySelected.id}`;

    query += this.nameSearch && this.nameSearch.length > 2
      ? ` AND ( ( extendedUser.fullName:*${this.nameSearch}* AND extendedUser.user.payeeAsCompany:false )
          OR ( extendedUser.company:*${this.nameSearch}* AND extendedUser.user.payeeAsCompany:true ) )`
      : '';

    query += this.idNumberSearch && this.idNumberSearch.length > 2
      ? ` AND ( ( extendedUser.idNumber:*${this.idNumberSearch}* AND extendedUser.user.payeeAsCompany:false )
          OR ( extendedUser.taxId:*${this.idNumberSearch}* AND extendedUser.user.payeeAsCompany:true ) )`
      : '';

    query += this.nicknameSearch && this.nicknameSearch.length > 2
      ? ` AND ( nickname:*${this.nicknameSearch}* )`
      : '';

    this.dataFinder._search(uri, query, 'extendedUser.lastUpdated', 'desc', 50).subscribe(
      data => {
        const content = data.content;
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < content.length; i++) {
          if (content[i].extendedUser && content[i].extendedUser.status === 'ACCEPTED') {
            const payee = {} as Payee;
            payee.extendedUserId = content[i].extendedUser.id;
            payee.id = content[i].id;
            payee.nickname = content[i].nickname;

            if (content[i].extendedUser.user.payeeAsCompany) {
              payee.fullName = content[i].extendedUser.company ? content[i].extendedUser.company : 'Profile not confirmed';
              payee.idNumber = content[i].extendedUser.taxId;
              payee.payeeAsCompany = true;
            } else {
              payee.fullName = content[i].extendedUser.fullName;
              payee.idNumber = content[i].extendedUser.idNumber;
              payee.payeeAsCompany = false;
            }

            payee.country = content[i].extendedUser.postalCountry;
            payee.amount = null;
            payee.description = '';
            this.payeesData.push(payee);
          }
        }
        for (const payee of this.payeesToPayData) {
          this.payeesData = this.payeesData.filter(pay => pay.id !== payee.id);
        }
        this.payees = [...this.payeesData];
      }
    );
  }

  // Change to the next step
  nextStep() {
    this.invalidateStep = false;
    switch (this.index) {
      case 1:
        if (this.payeesToPay.length === 0) {
          let payees:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.NO_PAYEES_SELECTED');
          this.invalidateStep = true;
          this.showMessage(payees.value, 'error');
        }
        break;
      case 2:
        for (const payee of this.payeesToPay) {
          if (this.currentAccount < 0) {
          let paynoFoundsees:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.INSUFFICIENT_FUNDS');
            this.showMessage(paynoFoundsees.value, 'error');
            this.invalidateStep = true;
          }
          if (payee.amount < 10 || payee.amount === null) {
             let paynoFoundsees:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AMOUNTS_MUST_BE_GREATER_10');
            this.showMessage(paynoFoundsees.value , 'error');
            this.invalidateStep = true;
            break;
          }
        }
        break;
      default:
        break;
    }
    if (!this.invalidateStep) {
      this.index++;
      switch (this.index) {
        case 2:
          this.sumAmount();
          break;
        case 3:
          this.setGenericDescription();
          break;
        default:
          break;
      }
      this.setActiveClass(this.index);
    }

  }

  // Change to the back step
  backStep() {
    this.index--;
    this.setActiveClass(this.index);
  }

  // On Double click add payees
  // Move selected payees from 'Your Payyes' to 'Selected Payees'
  addElement(payee: Payee) {
    this.payeesToPayData.push(payee);
    this.payeesToPay = [...this.payeesToPayData];
    this.payeesData = this.payeesData.filter(pay => pay.id !== payee.id);
    this.payees = [...this.payeesData];
    this.nameSearch = '';
  }

  // On Double click remove payees(step one)
  // Move selected payees from 'Selected Payees' to 'Your Payees'
  uncheckElement(payee: Payee) {
    this.payeesData.push(payee);
    this.payees = [...this.payeesData];
    this.payeesToPayData = this.payeesToPayData.filter(pay => pay.id !== payee.id);
    this.payeesToPay = [...this.payeesToPayData];
    this.rightFilter = '';
  }

  // Add payees
  // Move selected payees from 'Your Payees' to 'Selected Payees'
  addAmountElement() {
    if (this.preSelectedPayees.length !== 0) {
      this.payeesData.forEach(payee => {
        payee.isSelected = false;
      });
      for (const pre of this.preSelectedPayees) {
        this.payeesData = this.payeesData.filter(pay => pay.id !== pre.id);
        this.payeesToPayData.push(pre);
      }
      this.payeesToPay = [...this.payeesToPayData];
      this.payees = [...this.payeesData];
      this.preSelectedPayees = [];
      this.nameSearch = '';
    }
  }

  // Add all payees
  // Move all payees from 'Your Payyes' to 'Selected Payees'
  addAllElements() {
    this.preSelectedPayees = [];
    this.payeesData.forEach(payee => {
      payee.isSelected = false;
    });
    for (const pay of this.payees) {
      this.payeesToPayData.push(pay);
    }
    for (const elem of this.payeesToPayData) {
      this.payeesData = this.payeesData.filter(recip => recip.id !== elem.id);
    }
    this.payeesToPay = [...this.payeesToPayData];
    this.payees = [...this.payeesData];
    this.preSelectedPayees = [];
    this.nameSearch = '';
  }

  // Remove payees
  // Move selected payees from 'Selected Payees' to 'Your Payees'
  removeAmountElement() {
    if (this.postSelectedPayees.length !== 0) {
      this.payeesToPayData.forEach(payee => {
        payee.isSelected = false;
      });
      for (const post of this.postSelectedPayees) {
        this.payeesToPayData = this.payeesToPayData.filter(pay => pay.id !== post.id);
        this.payeesData.push(post);
      }
      this.payeesToPay = [...this.payeesToPayData];
      this.payees = [...this.payeesData];
      this.postSelectedPayees = [];
      this.rightFilter = '';
    }
  }

  // Remove all payees
  // Move all payees from 'Selected Payees' to 'Your Payees'
  removeAllElements() {
    this.payeesToPayData.forEach(payee => {
      payee.isSelected = false;
    });
    for (const pay of this.payeesToPay) {
      this.payeesData.push(pay);
    }
    for (const elem of this.payeesToPay) {
      this.payeesToPayData = this.payeesToPayData.filter(recip => recip.id !== elem.id);
    }
    this.payees = [...this.payeesData];
    this.payeesToPay = [...this.payeesToPayData];
    this.postSelectedPayees = [];
    this.rightFilter = '';
  }

  // Right table filter method
  payeesRightFilter(event) {
    if (this.rightFilter && this.rightFilter.length > 0) {
      this.payeesToPayFilter = this.payeesToPayData.filter(elem => {
        if (elem.fullName.toLowerCase().includes(event.toLowerCase()) || elem.id.toString().includes(event)) {
          return true;
        }
      });
      this.payeesToPay = [...this.payeesToPayFilter];
    } else {
      this.payeesToPay = [...this.payeesToPayData];
    }
  }

  // Method that select payees from 'Your Payyes' table
  preSelectElement(payee: Payee) {
    const index = this.preSelectedPayees.indexOf(payee);
    if (index === -1) {
      this.preSelectedPayees.push(payee);
      payee.isSelected = true;
    } else {
      this.preSelectedPayees.splice(index, 1);
      payee.isSelected = false;
    }
  }

  // Method that select payees from 'Selected Payyes' table
  postSelectElement(payee: Payee) {
    const index = this.postSelectedPayees.indexOf(payee);
    if (index === -1) {
      this.postSelectedPayees.push(payee);
      payee.isSelected = true;
    } else {
      this.postSelectedPayees.splice(index, 1);
      payee.isSelected = false;
    }
  }

  setActiveClass(index: number) {
    for (const step of this.steps) {
      step.isActive = index === step.step || index > step.step;
    }
  }

  // Remove payees(Step two) cross icon
  // Move selected payees from 'Selected Payees' to 'Your Payyes'
  removeElement(payee: Payee) {
    payee.amount = null;
    payee.description = '';
    this.payeesData.push(payee);
    this.payees = [...this.payeesData];
    this.payeesToPayData = this.payeesToPayData.filter(pay => pay.id !== payee.id);
    this.payeesToPay = [...this.payeesToPayData];
    if (this.payeesToPay.length === 0) {
      this.backStep();
    }
    this.sumAmount();
  }

  sumAmount() {
    this.totalToPay = 0;
    for (const payee of this.payeesToPay) {
      this.totalToPay = this.totalToPay + (+payee.amount);
    }
    this.currentAccount = this.extendedUser.balance - this.totalToPay;
    if (this.currentAccount < 0) {
      document.getElementById('error').style.transition = 'all 500ms ease-in-out';
    }
  }

  confirmPayments() {
    for (const payee of this.payeesToPay) {
        if (payee.amount < 10 || payee.amount === null) {
         let paynoFoundsees:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AMOUNTS_MUST_BE_GREATER_10');
        this.showMessage(paynoFoundsees.value , 'error');
        this.invalidateStep = true;
        break;
        }
    }
    if (this.invalidateStep) {
      this.invalidateStep = false;
      return;
    }
    this.paymentService.createPayment(this.payeesToPay).subscribe((data) => {
      if (data.transactionsWithErrors.length !== 0) {
        const processedPayment = data.transactions;
        let newTotalToPay = 0;
        for (const payment of processedPayment) {
          newTotalToPay = newTotalToPay + payment.amountBeforeCommission;
        }
        let typeError = 0;
        if( data.transactionsWithErrors[0].statusReason == "Limite alcanzado" ){

          typeError = 1;
        }

        this.router.navigateByUrl('/messageConfirmation',
          {
            state: {
              messageType: 'warning',
              amount: newTotalToPay,
              allPayment: this.payeesToPay.length,
              error: data.transactionsWithErrors.length,
              typeMessageError:typeError
            }
          }).then();
      } else {
        this.router.navigateByUrl('/messageConfirmation', {state: {messageType: 'success', amount: this.totalToPay}}).then();
      }
      this.userService.getExtendedUserByUserId(this.user.id).subscribe(
        extendedUser => {
          this.userSessionService.saveUserSession(this.user, extendedUser.content[0]);
          this.userService.updateAccountBalanceObservable(extendedUser.content[0].balance);
        });
    });
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  setGenericDescription() {
    if (this.genericDescription) {
      for (const pay of this.payeesToPay) {
        if (pay.description === '') {
          pay.description = this.genericDescription;
        }
      }
    }
  }

  eraseDescriptionHandler() {
    if (this.genericDescription === '') {
      for (const pay of this.payeesToPay) {
        pay.description = '';
      }
    } else {
      for (const pay of this.payeesToPay) {
        if (pay.description === this.genericDescription) {
          pay.description = '';
        }
      }
    }
    this.genericDescription = '';
  }

  getAmountToPayee(payee: Payee) {
    if (payee.amount >= 10) {
      payee.localCurrency = payee.amount * payee.country.currencyValue;
      return payee.localCurrency;
    } else {
      return 0;
    }
  }

  validacionInput(e: any, type: string, cantidad: string, addCHaracterSpecialc = null) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = '';
    if (type === 'cantidad') {
      if (+cantidad < 9999.99){
       if (cantidad.includes('.') ) {
        let ubicaPunto: number = cantidad.indexOf('.');
        let cantidadCaracteres: number = cantidad.length;
        if ((ubicaPunto + 1) === (cantidadCaracteres - 2)) {
          return false;
        } else {
          numeros = '0123456789';
        }      
      } else {
        numeros = '0123456789.';
      }
    } else {
      if (+cantidad < 99999.99) {
        if (!cantidad.includes('.') ) {
          numeros = '.';
        } else {
          let ubicaPunto: number = cantidad.indexOf('.');
          let cantidadCaracteres: number = cantidad.length;
          if ((ubicaPunto + 1) === (cantidadCaracteres - 2)) {
            return false;
          } else {
            numeros = '0123456789';
          }
        }
      } else {
        return false;
      }

    }
    } else if (type === 'numeros') {
      numeros = '0123456789';
    } else if (type === 'letras') {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz ';
    } else {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    }

    if (addCHaracterSpecialc !== null) {
      numeros = numeros + addCHaracterSpecialc;
    }
/*     const numeros = type === 'numeros' ? "0123456789" :
    'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 '; */
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }

}
