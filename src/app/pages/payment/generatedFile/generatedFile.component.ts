import { Router } from '@angular/router';
import { Component, AfterViewInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BankService, DataFinderService, UserService } from '../../../services/service.index';
import { merge, Observable, of as observableOf } from 'rxjs';
import { MatPaginator, MatSort } from '@angular/material';
import { startWith, switchMap, catchError, map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { STATUSES, STATUSES_COLOR } from '../../../config/enums';
import { Location } from '@angular/common';
import { MessageNotificationComponent } from '../../../components/messageNotification/messageNotification.component';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';

import swal from 'sweetalert2';//swalert2
import { ContentObserver } from '@angular/cdk/observers';

@Component({
  selector: 'app-generated-file',
  templateUrl: './generatedFile.component.html',
  styleUrls: ['./generatedFile.component.scss']
})
export class GeneratedFileComponent implements AfterViewInit, OnDestroy {

  uri = '/api/_search/transaction-groups';
  query = '';

  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;
  alreadyRefreshing = true;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  data: any[] = [];
  pageSize = 12;
  statuses = STATUSES;
  statusesColor = STATUSES_COLOR;

  refreshTable: any;

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  errorMessage = 'Error getting files.';
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  keyLanguage$: Observable<any> = this.store.select('language');
  constructor(private dataFinder: DataFinderService,
    private location: Location,
    private cdRef: ChangeDetectorRef,
    private userService: UserService,
    private translate: TranslateService,
    private bankService: BankService,
    private router: Router,
    private store: Store<language>) {
    this.keyLanguage$.subscribe(item => {
      const language: any = item
      this.translate.use(language.key);
      let errorMessage: any = this.translate.get('ERROR_MESSAGES.ERROR_GETTING_FILES');
      this.errorMessage = errorMessage.value;
      this.initColumnDef();
    });
  }

  ngAfterViewInit() {
    this.setAutomaticRefresh();
    const variable: any = this.location.getState();
    if (variable.withErrors) {
      this.messageComponent.setTypeMessage('Some payments could not be processed', 'warning');
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.refreshTable);
    this.alreadyRefreshing = true;
  }

  selectPayment(data, idCountry: number) {

    let messageTitle: any = this.translate.get('MESSAGE.GENERATE_FILE_TITLE');
    let messageText: any = this.translate.get('MESSAGE.ALERT_RIPPLE');
    let messageTextRippe: any = this.translate.get('BUTTON_OPTION_INFO.PROCESS_DIRECTLY_RIPPLE');
    let messageBankAccount: any = this.translate.get('BUTTON_OPTION_INFO.PROCESS_BANK_ACCOUNT');
    let denyButton = "Payments";
    let bankAccount
    let cancelButton = "Bexs";
    let confirmButton = messageTextRippe.value;
    const botonDisabled = {
      rippleButton: false, // denyButton
      fileName: true, // confirmed
      fileNameBex: true // cancel
    };

    /**
     * Validacion boton cuando el banco es ripple
     * 197 Riple Chile
     * 198 Riple Brazil
     * 199 Riple Argentina
     * 204 Riple Peru
     * 215 Riple Venezuela

     */

    if (data.bankAccount.bank.id == 197 // Chile
      || data.bankAccount.bank.id == 198
      || data.bankAccount.bank.id == 199
      || data.bankAccount.bank.id == 204
      || data.bankAccount.bank.id == 215) {
      botonDisabled.rippleButton = true;
    }
    if (idCountry == 2) {
      
      /**
       * FileNameBex = ABC
        FileName = STP
       */
      denyButton = 'STP';
      cancelButton = 'ABC';
      botonDisabled.fileName = data.fileName === null ? false : true;
      botonDisabled.fileNameBex = data.fileNameBex === null ? false : true;
     
      if(botonDisabled.fileNameBex) {
        confirmButton = messageBankAccount.value
        botonDisabled.rippleButton = data.fileBankAccount === null ? false : true;
      }
    } else if (idCountry == 66) { // argentina
      botonDisabled.fileName = data.fileName === null ? false : true;
      botonDisabled.fileNameBex = data.fileNameBex === null ? false : true;
    } else if (idCountry == 74) { //brazil
      botonDisabled.fileName = data.fileName === null ? false : true;
      botonDisabled.fileNameBex = data.fileNameBex === null ? false : true;
    } else if (idCountry == 77) {// chile
      denyButton = "Security Bank";
      botonDisabled.fileName = data.fileName === null ? false : true;
      botonDisabled.fileNameBex = null;
    } else if (idCountry == 101) { //peru
      botonDisabled.fileName = data.fileName === null ? false : true;
      botonDisabled.fileNameBex = data.fileNameBex === null ? false : true;
    }else if (idCountry == 115) { //Venezuela
      botonDisabled.fileName = data.fileName === null ? false : true;
      botonDisabled.fileNameBex = data.fileNameBex === null ? false : true;
    }


    swal.fire({
      title: messageTitle.value,
      footer: botonDisabled.rippleButton && idCountry != 2 ? messageText.value : null,
      icon: 'warning',
      showCancelButton: botonDisabled.fileNameBex,
      showDenyButton: botonDisabled.fileName,
      showConfirmButton: botonDisabled.rippleButton,
      confirmButtonText: confirmButton,
      cancelButtonText: cancelButton,
      denyButtonText: denyButton,
      confirmButtonColor: 'red',
      denyButtonColor: '#3085d6',
      cancelButtonColor: 'green',
      reverseButtons: true
    }).then((result) => {
      if (result.isDenied) {

         window.open(this.getFilePath(data.fileName, 'pathLink'));
      }
      else if (result.dismiss === swal.DismissReason.cancel) {

         window.open(this.getFilePath(data.fileNameBex, 'pathLink'));
      } else if (result.isConfirmed) {
        if(idCountry == 2) {

          window.open(this.getFilePath(data.fileBankAccount, 'pathLink'));
        } else {
          console.log(data);
          this.sendServiceTransactionGroups(data);
        }
  
      }
    });
  }

  sendServiceTransactionGroups(data) {
    const body = {
      bankAccount: {
        id: data.bankAccount.id,
        accountNumber: data.bankAccount.accountNumber
      },
      directPayment: true,
      exchangeRate: data.exchangeRate,
      transactions: data.transactions
    }
    this.bankService.transactionGroups(body).subscribe(req => {
      const body = req;
      localStorage.setItem('ResultTransactionGroups', JSON.stringify(body));
      this.router.navigate(['/payment']);
    }, err => {
      console.log(err);
    });
  }

  loadTable() {
    this.initColumnDef();

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query,
            this.sort.active, this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => (this.data = data));

    this.cdRef.detectChanges();
  }

  changePaginationHandler() {
    if (this.paginator.pageIndex === 0 && !this.alreadyRefreshing) {
      this.alreadyRefreshing = true;
      this.setAutomaticRefresh();
    } else if (this.paginator.pageIndex !== 0) {
      this.alreadyRefreshing = false;
      clearTimeout(this.refreshTable);
    }
  }

  initColumnDef() {
    let fileName: any = this.translate.get('DATA_TABLE.FILE');
    let status: any = this.translate.get('DATA_TABLE.STATUS');
    let dateCreated: any = this.translate.get('DATA_TABLE.DATE');
    let country: any = this.translate.get('PROFILE.COUNTRY');
    let bank: any = this.translate.get('DATA_TABLE.BANK');
    let bankAccount: any = this.translate.get('PROFILE.BANK_ACCOUNT');
    this.columnNames = {
      action: ' ',
      fileName: fileName.value,
      status: status.value,
      dateCreated: dateCreated.value,
      country: country.value,
      bank: bank.value,
      bankAccount: bankAccount.value
    };
    this.columns = Object.keys(this.columnNames);
  }

  getFilePath(path: string, type: string): string {
    if (path) {
      const arrayPath = path.split('/');
      switch (type) {
        case 'fileName':
        case 'downloadName':
          return arrayPath[arrayPath.length - 1];
        case 'pathLink':
          let pathReturn = '/assets/fileBucket';
          for (let i = 1; i < arrayPath.length; i++) {
            pathReturn += `/${arrayPath[i]}`;
          }
          return pathReturn;
        default:
          break;
      }
    }
  }

  setAutomaticRefresh() {
    this.loadTable();

    this.refreshTable = setTimeout(() => {
      this.setAutomaticRefresh();
    }, 10000);
    // OnDestroy or pageIndex different zero we clear the setTimeout
  }

  refreshButtonHandler() {
    this.loadTable();
  }
}
