import { AfterViewInit, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf, BehaviorSubject, Observable } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { DataFinderService } from '../../services/data-finder.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { PaymentService, UserService, UserSessionService } from '../../services/service.index';
import { NoteModalComponent } from '../../components/noteModal/noteModal.component';
import { MatDialog, TooltipPosition } from '@angular/material';
import { MessageNotificationComponent } from '../../components/components.index';
import { ExtendedUser, User } from '../../config/interfaces';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PROCESS_TYPE, ROLES, STATUS, STATUSES, STATUSES_COLOR } from '../../config/enums';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { determinateMaxDate } from '../../shared/functions';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import * as getLanguage from '../../app.action';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class PaymentComponent implements AfterViewInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  positionOptions: TooltipPosition = 'above';

  keys = Object.keys;
  environment = environment;
  user: User;

  defaultCurrency = environment.defaultCurrency;
  collapse = false;
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;

  role: any;
  accountBalance: any;

  uri = '/api/_search/transactions';
  query = '';

  payeeCountries = [];
  countrySelected: any;
  filteredPayeeCountries: Observable<string[]> | any;
  countryForm: FormGroup;

  roles = ROLES;
  status = STATUS;
  statuses = STATUSES;
  statusesColor = STATUSES_COLOR;

  statusId = '';
  dateFilter = '';
  dateBefore = '';
  dateBeforeBS = '';
  dateAfter = '';
  showDataBetween = false;

  nameFilter = null;
  nameInput = '';
  nicknameInput = '';
  emailInput = '';
  idTransactionInput = '';

  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;
  showApplyFilter = false;
  onlyPayeeCompanyArr = [];
  onlyPayeeCompany = false;
  checked = false;
  disabled = false;

  columns: string[] = [];
  columnsHistory: string[] = []; // Historial
  details: string[] = [];
  columnNames: { [key: string]: string } = {};
  columnNamesHist: { [key: string]: string } = {};
  detailNames: { [key: string]: string } = {};
  data: any[] = [];
  pageSize = 12;

  nameAdmin = [];
  namePayee = [];
  namePayeeCompany = [];
  nameMerchant = [];
  nameStatus = [];
  nicknameArr = [];
  emailArr = [];
  idTransaction = [];
  countryFilter = [];
  filterName = '';

  dateEqual = [];
  dateIsBefore = [];
  dateIsAfter = [];
  dateBetween = [];

  expandedElement: null;

  maxDateBS: Date;
  maxDate = new Date();
  minDate = '';
  disabledEditAfterDate = true;

  whoArr = [];

  // isMobile: boolean;
  //tablaHistorial
  fecha: any;
  usuario: any;
  estatus: any;
  razonestatus: any;
  //finTablaHistorial

  extendedUser: ExtendedUser;
  errorMessage:any;
  keyLanguage$: Observable<any> = this.store.select('language');
  merchant:any;
  payye:any;
  lastUpdated:any;
  amountBeforeCommission:any;
  amountAfterCommission:any;
  amountLocalCurrency:any;
  payeeNusinessCurrency:any;
  countryCurrency:any;
  dateCurrency:any;
  statusCurrency:any;
  statusReasonCurrency:any;
  creationDateCurrency:any;
  banComissionCurrency:any;
  fxComissionCurrency :any;
  rechargeCostCurrency:any;
  exchangeRateCurrency:any;
  bankRefenceCurrency:any;
  fileNameCurrency:any;
  descriptionCurrency:any;
  commisisionPaidCurrency:any;
  nicknamedCurrency:any;
  businessAccounts:any;
  amountMustBe100:any;
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  changeClass:any;

  arrayStatusTraductor = [];
  constructor(private dataFinder: DataFinderService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private userService: UserService,
    public userSessionService: UserSessionService,
    private paymentService: PaymentService,
    public dialog: MatDialog,
    private translate: TranslateService,
    private store: Store<language>) {
      setTimeout(() => {
        this.initDatePicker();
        this.trancutorsItem();
        this.setNames(this.role);
      }, 500);
    this.keyLanguage$.subscribe(item => {
      const language: any = item;
      this.translate.use(language.key);
      setTimeout(() => {
        this.setNames(this.role);
      }, 400);
      this.trancutorsItem();
      this.initDatePicker();
      this.initWhoArray();
    });
    this.user = this.userSessionService.loadUser();
    this.role = this.userSessionService.loadRole();
    this.accountBalance = this.userService.accountBalance;
    this.extendedUser = this.userSessionService.loadExtendedUser();
    // this.isMobile = this.userSessionService.loadIsMobile();
    this.initDatePicker();
    this.countryForm = new FormGroup({
      countryPayee: new FormControl('', Validators.required),
    });
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  trancutorsItem(){
      this.errorMessage = this.translate.get('ERROR_MESSAGES.ERROR_GETTING_PAYMENTS');
      this.merchant = this.translate.get('DATA_TABLE.MERCHANT_COMPANY');
      this.payye = this.translate.get('REPORTS_MESSAGES.PAYEE_BUSINESS_ACCOUNT');
      this.lastUpdated = this.translate.get('DATA_TABLE.DATE');
      this.amountBeforeCommission = this.translate.get('DATA_TABLE.AMOUNT');
      this.amountAfterCommission = this.translate.get('DATA_TABLE.AFTER_COMMISSION');
      this.amountLocalCurrency = this.translate.get('DATA_TABLE.LOCAL_CURRENCY');
      this.payeeNusinessCurrency = this.translate.get('REPORTS_MESSAGES.PAYEE_BUSINESS_ACCOUNT');
      this.countryCurrency = this.translate.get('REPORTS_MESSAGES.COUNTRY');
      this.dateCurrency = this.translate.get('REPORTS_MESSAGES.DATE');
      this.statusCurrency = this.translate.get('REPORTS_MESSAGES.STATUS');
      this.statusReasonCurrency = this.translate.get('REPORTS_MESSAGES.STATUS_REASON');
      this.fecha = this.translate.get('REPORTS_MESSAGES.DATE'); //Historial
      this.usuario = this.translate.get('TITLES.USERS'); //Historial
      this.estatus = this.translate.get('REPORTS_MESSAGES.STATUS'); //Historial
      this.razonestatus = this.translate.get('BUTTON_OPTION_INFO.STATUS_REASON'); //Historial
      this.creationDateCurrency = this.translate.get('REPORTS_MESSAGES.CREATION_dATE');
      this.banComissionCurrency = this.translate.get('REPORTS_MESSAGES.BACK_COMMISSION');
      this.fxComissionCurrency = this.translate.get('REPORTS_MESSAGES.FIX_COMMISSION');
      this.rechargeCostCurrency = this.translate.get('REPORTS_MESSAGES.RECHARGE_COST');
      this.exchangeRateCurrency = this.translate.get('REPORTS_MESSAGES.EXCHANGE_RATE');
      this.bankRefenceCurrency = this.translate.get('BUTTON_OPTION_INFO.BANK_REFERENCE');
      this.fileNameCurrency = this.translate.get('REPORTS_MESSAGES.FILE_NAME');
      this.descriptionCurrency = this.translate.get('REPORTS_MESSAGES.DESCRIPTION');
      this.commisisionPaidCurrency = this.translate.get('REPORTS_MESSAGES.COMMISSION_PAID');
      this.nicknamedCurrency = this.translate.get('REPORTS_MESSAGES.NICKNAME');
      this.businessAccounts = this.translate.get('DATA_TABLE.BUSINESS_ACCOUNTS');
      this.amountMustBe100  = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.AMOUNTS_MUST_BE_GREATER');
      this.arrayStatusTraductor = [
        { name: this.translate.get('STATUS.PENDING'), key: 'CREATED'},
        { name: this.translate.get('STATUS.IN_PROCESS'), key: 'IN_PROCESS'},
        { name: this.translate.get('STATUS.COMPLETED'), key: 'ACCEPTED'},
        { name: this.translate.get('STATUS.REJECTED'), key: 'REJECTED'},
        { name: this.translate.get('STATUS.CANCELLED'), key: 'CANCELLED'},
        { name: this.translate.get('STATUS.FAILED'), key: 'FAILED'}];
  }
  initDatePicker() {
    this.maxDateBS = new Date();
    this.maxDateBS.setDate(this.maxDateBS.getDate());
    this.datePickerConfig = Object.assign({},
      {
        dateInputFormat: 'YYYY-MM-DD',
        dateInputFormatSend: 'YYYY-MM-DD',
      });
    this.maxDate = determinateMaxDate();
  }

  get CountryPayee() { return this.countryForm.get('countryPayee'); }

  ngAfterViewInit() {
    this.setNames(this.role);
    this.query = this.setQueryRole(this.extendedUser.id);
    this.query += this.getFilterQueryString() !== '' ? ` AND ${this.getFilterQueryString()}` : '';

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.data = data;
        if(this.data.length === 0) {
          let message: any = this.translate.get('TEXT.DONT_FOUND_RESULTS');
          this.messageComponent.setTypeMessage(message.value, 'warning', false,'', 2000 );
        }
      });

    this.cdRef.detectChanges();
  }

  initPayeeCountryFilter() {
    this.filteredPayeeCountries = this.CountryPayee.valueChanges
      .pipe(
        startWith(''),
        map(value => this._payeeCountryFilter(value))
      );
  }

  private _payeeCountryFilter(value: string): string[] {
    if (value) {
      if (value.length >= 2) {
        this.loadPayeeCountry(value);
      } else {
        this.payeeCountries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.payeeCountries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadPayeeCountry(name: string) {
    this.dataFinder.getCountriesForPayeeAndName(name).subscribe(
      data => {
        this.payeeCountries = data.content;
      });
  }

  displayFnPayeeCountry(country): string {
    return country ? country.name : country;
  }

  setQueryRole(id: number): string {
    let query;
    switch (this.role) {
      case ROLES.RESELLER:
        query = `reseller.id:${id}`;
        break;
      case ROLES.MERCHANT:
        query = `merchant.id:${id}`;
        break;
      case ROLES.PAYEE:
        query = `payee.id:${id}`;
        break;
      default:
        query = '*';
        break;
    }
    return query;
  }

  setNames(role: string) {
    switch (role) {
      case ROLES.SUPER_ADMIN:
      case ROLES.ADMIN:
          this.columnNames = {
            id: 'Id',
            admin: 'Admin',
            merchant:this.merchant.value,
            payee:this.payeeNusinessCurrency.value,
            country:this.countryCurrency.value,
            lastUpdated:this.dateCurrency.value,
            status:this.statusCurrency.value,
            amountBeforeCommission:this.amountBeforeCommission.value,
            amountLocalCurrency:this.amountLocalCurrency.value,
            action: '',
            expandAction: ''
          };
          this.detailNames = {
            statusReason: this.statusReasonCurrency.value,
            dateCreated: this.creationDateCurrency.value,
            bankCommission: this.banComissionCurrency.value,
            fxCommission: this.fxComissionCurrency.value,
            rechargeCost: this.rechargeCostCurrency.value,
            exchangeRate: this.exchangeRateCurrency.value,
            amountAfterCommission: this.amountAfterCommission.value,
            bankReference: this.bankRefenceCurrency.value,
            fileName: this.fileNameCurrency.value,
            description: this.descriptionCurrency.value
          };
    
        break;
      case ROLES.RESELLER:
          this.columnNames = {
            id: 'Id',
            admin: 'Admin',
            merchant: this.merchant.value,
            payee: this.payeeNusinessCurrency.value,
            country: this.countryCurrency.value,
            lastUpdated: this.dateCurrency.value,
            status: this.statusCurrency.value,
            amountBeforeCommission: this.amountBeforeCommission.value,
            commissionPaid: this.commisisionPaidCurrency.value,
            action: '', expandAction: ''
          };
          this.detailNames = {
            statusReason: this.statusReasonCurrency.value,
            dateCreated: this.creationDateCurrency.value,
            bankCommission: this.banComissionCurrency.value,
            fxCommission: this.fxComissionCurrency.value,
            rechargeCost: this.rechargeCostCurrency.value,
            exchangeRate: this.exchangeRateCurrency.value,
            amountAfterCommission: this.amountAfterCommission.value,
            bankReference: this.bankRefenceCurrency.value,
            fileName: this.fileNameCurrency.value,
            description: this.descriptionCurrency.value
          };
    
        break;
      case ROLES.MERCHANT:
          this.columnNames = {
            id: 'Id',
            payee: this.payye.value,
            nickname: this.nicknamedCurrency.value,
            country: this.countryCurrency.value,
            lastUpdated: this.dateCurrency.value,
            status: this.statusCurrency.value,
            amountBeforeCommission: this.amountBeforeCommission.value,
            action: '',
            expandAction: ''
          };
          this.detailNames = {
            statusReason: this.statusReasonCurrency.value,
            dateCreated: this.creationDateCurrency.value,
            bankCommission: this.banComissionCurrency.value,
            description: this.descriptionCurrency.value
          };
        
        break;
      case ROLES.PAYEE:
          this.columnNames = {
            id: 'Id',
            merchant: this.merchant.value,
            lastUpdated: this.lastUpdated.value,
            status: this.statusCurrency.value,
            amountLocalCurrency: this.amountLocalCurrency.value,
            expandAction: ''
          };
          this.detailNames = {
            statusReason: this.statusReasonCurrency.value,
            dateCreated: this.creationDateCurrency.value,
            bankReference: this.bankRefenceCurrency.value,
            description: this.descriptionCurrency.value
          };

        
        break;
    }
    this.columns = Object.keys(this.columnNames);
    this.columnNamesHist = {
      fecha: this.fecha,
      usuario: this.usuario,
      estatus: this.estatus,
      razonestatus: this.razonestatus
    };
    this.columnsHistory = Object.keys(this.columnNamesHist); // historial
    this.details = Object.keys(this.detailNames);
  }

  addFilter(event: any) {
    this.filterName = event.target.value;
    this.showApplyFilter = true;
    event.target.selectedIndex = 0;
    if (this.filterName === 'name') {
      this.initWhoArray();
    }
  }

  initWhoArray() {
    this.nameFilter = null;
    switch (this.role) {
      case ROLES.SUPER_ADMIN:
      case ROLES.ADMIN:
      case ROLES.RESELLER:
        this.whoArr = [
          { value: 'admin', title: 'Admin' },
          { value: 'payee', title: this.payye.value },
          { value: 'payeeCompany', title:  this.businessAccounts.value },
          { value: 'merchant', title: this.merchant.value },
        ];
        break;
      case ROLES.MERCHANT:
        this.whoArr = [
          { value: 'payee', title: this.payye.value },
          { value: 'payeeCompany', title:  this.businessAccounts.value },
        ];
        break;
      case ROLES.PAYEE:
        this.whoArr = [
          { value: 'merchant', title: this.merchant.value },
        ];
        this.nameFilter = this.whoArr[0].value;
        break;
    }
  }

  resetFilter(typeName: string) {
    switch (typeName) {
      case 'country':
        this.countryFilter = [];
        break;
      case 'merchant':
        this.nameMerchant = [];
        break;
      case 'seeOnly':
        this.onlyPayeeCompanyArr = [];
        this.onlyPayeeCompany = false;
        break;
      case 'admin':
        this.nameAdmin = [];
        break;
      case 'payeeCompany':
        this.namePayeeCompany = [];
        break;
      case 'payee':
        this.namePayee = [];
        break;
      case 'nickname':
        this.nicknameArr = [];
        break;
      case 'status':
        this.nameStatus = [];
        break;
      case 'dateEqual':
        this.dateEqual = [];
        break;
      case 'dateIsBefore':
        this.dateIsBefore = [];
        break;
      case 'dateIsAfter':
        this.dateIsAfter = [];
        break;
      case 'dateBetween':
        this.dateBetween = [];
        break;
      case 'email':
        this.emailArr = [];
        break;
      case 'transactionID':
        this.idTransaction = [];
        break;
      default:
        break;
    }
    this.ngAfterViewInit();
  }

  deleteFilter(typeName: string, name: any) {
    switch (typeName) {
      case 'admin':
        const indexAdmin = this.nameAdmin.indexOf(name);
        this.nameAdmin.splice(indexAdmin, 1);
        break;
      case 'merchant':
        const indexMerchant = this.nameMerchant.indexOf(name);
        this.nameMerchant.splice(indexMerchant, 1);
        break;
      case 'payeeCompany':
        const indexPayeeCompany = this.namePayeeCompany.indexOf(name);
        this.namePayeeCompany.splice(indexPayeeCompany, 1);
        break;
      case 'payee':
        const indexPayee = this.namePayee.indexOf(name);
        this.namePayee.splice(indexPayee, 1);
        break;
      case 'nickname':
        const indexNickname = this.nicknameArr.indexOf(name);
        this.nicknameArr.splice(indexNickname, 1);
        break;
      case 'country':
        const indexCountry = this.countryFilter.indexOf(name);
        this.countryFilter.splice(indexCountry, 1);
        break;
      case 'status':
        const indexStatus = this.nameStatus.indexOf(name);
        this.nameStatus.splice(indexStatus, 1);
        break;
      case 'dateEqual':
        const indexEqual = this.dateEqual.indexOf(name);
        this.dateEqual.splice(indexEqual, 1);
        break;
      case 'dateIsBefore':
        const indexBefore = this.dateIsBefore.indexOf(name);
        this.dateIsBefore.splice(indexBefore, 1);
        break;
      case 'dateIsAfter':
        const indexAfter = this.dateIsAfter.indexOf(name);
        this.dateIsAfter.splice(indexAfter, 1);
        break;
      case 'dateBetween':
        const indexBetween = this.dateBetween.indexOf(name);
        this.dateBetween.splice(indexBetween, 1);
        break;
      case 'email':
        const indexEmail = this.emailArr.indexOf(name);
        this.emailArr.splice(indexEmail, 1);
        break;
      case 'transactionID':
        const indexTRansactionID = this.idTransaction.indexOf(name);
        this.idTransaction.splice(indexTRansactionID, 1);
        break;
      default:
        break;
    }
    this.ngAfterViewInit();
  }

  getFilterQueryString(): string {
    let query = '';

    // COUNTRY
    if (this.countryFilter.length > 0) {
      if (this.countryFilter.length === 1) {
        query += `currency.country.id:${this.countryFilter[0].id}`;
      } else {
        for (let index = 0; index < this.countryFilter.length; index++) {
          if (index === 0) {
            query += `( currency.country.id:${this.countryFilter[index].id} OR `;
          } else {
            if (this.countryFilter.length - 1 !== index) {
              query += `currency.country.id:${this.countryFilter[index].id} OR `;
            } else {
              query += `currency.country.id:${this.countryFilter[index].id} )`;
            }
          }
        }
      }
    }
    // END COUNTRY

    // WHICH PAYEE SEE
    if (this.onlyPayeeCompanyArr.length === 1) {
      if (query !== '') {
        query += ' AND ';
      }
      query += `payee.user.payeeAsCompany:${this.onlyPayeeCompanyArr[0].value}`;
    }
    // END WHICH PAYEE SEE

    // NAME
    if (this.nameAdmin.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.nameAdmin.length === 1) {
        query += `admin.fullName:*${this.nameAdmin[0]}*`;
      } else {
        for (let index = 0; index < this.nameAdmin.length; index++) {
          if (index === 0) {
            query += `( admin.fullName:*${this.nameAdmin[index]}* OR `;
          } else {
            if (this.nameAdmin.length - 1 !== index) {
              query += `admin.fullName:*${this.nameAdmin[index]}* OR `;
            } else {
              query += `admin.fullName:*${this.nameAdmin[index]}* )`;
            }
          }
        }
      }
    }

    if (this.namePayeeCompany.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.namePayeeCompany.length === 1) {
        query += `payee.company:*${this.namePayeeCompany[0]}*`;
      } else {
        for (let index = 0; index < this.namePayeeCompany.length; index++) {
          if (index === 0) {
            query += `( payee.company:*${this.namePayeeCompany[index]}* OR `;
          } else {
            if (this.namePayeeCompany.length - 1 !== index) {
              query += `payee.company:*${this.namePayeeCompany[index]}* OR `;
            } else {
              query += `payee.company:*${this.namePayeeCompany[index]}* )`;
            }
          }
        }
      }
    }

    if (this.namePayee.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.namePayee.length === 1) {
        query += `payee.fullName:*${this.namePayee[0]}*`;
      } else {
        for (let index = 0; index < this.namePayee.length; index++) {
          if (index === 0) {
            query += `( payee.fullName:*${this.namePayee[index]}* OR `;
          } else {
            if (this.namePayee.length - 1 !== index) {
              query += `payee.fullName:*${this.namePayee[index]}* OR `;
            } else {
              query += `payee.fullName:*${this.namePayee[index]}* )`;
            }
          }
        }
      }
    }

    if (this.nameMerchant.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.nameMerchant.length === 1) {
        query += `merchant.company:*${this.nameMerchant[0]}*`;
      } else {
        for (let index = 0; index < this.nameMerchant.length; index++) {
          if (index === 0) {
            query += `( merchant.company:*${this.nameMerchant[index]}* OR `;
          } else {
            if (this.nameMerchant.length - 1 !== index) {
              query += `merchant.company:*${this.nameMerchant[index]}* OR `;
            } else {
              query += `merchant.company:*${this.nameMerchant[index]}* )`;
            }
          }
        }
      }
    }
    // END NAME

    // NICKNAME
    // TODO
    if (this.nicknameArr.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.nicknameArr.length === 1) {
        query += `extendedUserRelation.nickname:${this.nicknameArr[0]}`;
      } else {
        for (let index = 0; index < this.nicknameArr.length; index++) {
          if (index === 0) {
            query += `( extendedUserRelation.nickname:${this.nicknameArr[index]} OR `;
          } else {
            if (this.nicknameArr.length - 1 !== index) {
              query += `extendedUserRelation.nickname:${this.nicknameArr[index]} OR `;
            } else {
              query += `extendedUserRelation.nickname:${this.nicknameArr[index]} )`;
            }
          }
        }
      }
    }
    // END NICKNAME

    // STATUS
    if (this.nameStatus.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.nameStatus.length === 1) {
        query += `status:${this.nameStatus[0]}`;
      } else {
        for (let index = 0; index < this.nameStatus.length; index++) {
          if (index === 0) {
            query += `( status:${this.nameStatus[index]} OR `;
          } else {
            if (this.nameStatus.length - 1 !== index) {
              query += `status:${this.nameStatus[index]} OR `;
            } else {
              query += `status:${this.nameStatus[index]} )`;
            }
          }
        }
      }
    }
    // END STATUS
    // DATE

    if (this.dateEqual.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.dateEqual.length === 1) {
        query += `lastUpdated:${this.dateEqual[0]}`;
      } else {
        for (let index = 0; index < this.dateEqual.length; index++) {
          if (index === 0) {
            query += `( lastUpdated:${this.dateEqual[index]} OR `;
          } else {
            if (this.dateEqual.length - 1 !== index) {
              query += `lastUpdated:${this.dateEqual[index]} OR `;
            } else {
              query += `lastUpdated:${this.dateEqual[index]} )`;
            }
          }
        }
      }
    }

    if (this.dateIsBefore.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.dateIsBefore.length === 1) {
        query += `lastUpdated:[* TO ${this.dateIsBefore[0]}]`;
      }
    }

    if (this.dateIsAfter.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.dateIsAfter.length === 1) {
        query += `lastUpdated:[${this.dateIsAfter[0]} TO *]`;
      }
    }

    if (this.dateBetween.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.dateBetween.length === 1) {
        query += `lastUpdated:[${this.dateBetween[0]}]`;
      } else {
        for (let index = 0; index < this.dateBetween.length; index++) {
          if (index === 0) {
            query += `( lastUpdated:[${this.dateBetween[index]}] OR `;
          } else {
            if (this.dateBetween.length - 1 !== index) {
              query += `lastUpdated:[${this.dateBetween[index]}] OR `;
            } else {
              query += `lastUpdated:[${this.dateBetween[index]}] )`;
            }
          }
        }
      }
    }
    // END DATE

    // EMAIL
    // TODO /api/_search/transactions?query=payee.email:daniarteaga2021@gmail.com
    if (this.emailArr.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.emailArr.length === 1) {
        query += `extendedUserRelation.email:${this.emailArr[0]}`;
      } else {
        for (let index = 0; index < this.emailArr.length; index++) {
          if (index === 0) {
            query += `( extendedUserRelation.email:${this.emailArr[index]} OR `;
          } else {
            if (this.emailArr.length - 1 !== index) {
              query += `extendedUserRelation.email:${this.emailArr[index]} OR `;
            } else {
              query += `extendedUserRelation.email:${this.emailArr[index]} )`;
            }
          }
        }
      }
    }
    // END EMAIL

    // ID TRANSATION
    // TODO
    if (this.idTransaction.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.idTransaction.length === 1) {
        query += `id:${this.idTransaction[0]}`;
      } else {
        for (let index = 0; index < this.idTransaction.length; index++) {
          if (index === 0) {
            query += `( id:${this.idTransaction[index]} OR `;
          } else {
            if (this.idTransaction.length - 1 !== index) {
              query += `id:${this.idTransaction[index]} OR `;
            } else {
              query += `id:${this.idTransaction[index]} )`;
            }
          }
        }
      }
    }
    // END ID TRANSATION
    return query;
  }

  applyFilter() {
    switch (this.filterName) {

      case 'country':
        if (this.countrySelected) {
          let notDuplicated = true;
          for (const country of this.countryFilter) {
            if (country.id === this.countrySelected.id) {
              notDuplicated = false;
            }
          }
          if (notDuplicated) {
            this.countryFilter.push(this.countrySelected);
          }
        }
        break;

      case 'payeeCompany':
        if (this.onlyPayeeCompanyArr.length !== 1) {
          const obj = {
            value: this.onlyPayeeCompany,
            text: this.onlyPayeeCompany ?  this.businessAccounts : this.payye
          };
          this.onlyPayeeCompanyArr.push(obj);
        }
        break;

      case 'nickname':
        if (this.nicknameInput !== '') {
          const indexNickname = this.nicknameArr.indexOf(this.nicknameInput);
          if (indexNickname === -1) {
            this.nicknameArr.push(this.nicknameInput);
          }
        }
        break;

      case 'status':
        if (this.statusId !== '') {
          const indexStatus = this.nameStatus.indexOf(this.statusId);
          if (indexStatus === -1) {
            this.nameStatus.push(this.statusId);
          }
        } else {
          this.nameStatus = [];
        }
        break;

      case 'date':
        switch (this.dateFilter) {
          case 'dateEqual':
            if (this.dateBefore !== '') {
              const indexEqual = this.dateEqual.indexOf(this.dateBefore);
              if (indexEqual === -1) {
                this.dateEqual.push(this.dateBefore);
              }
              this.dateEqual[0] = moment(this.dateEqual[0]).format('YYYY-MM-DD');
            }
            break;

          case 'dateIsBefore':
            if (this.dateBefore !== '' && this.dateIsBefore.length === 0) {
              this.dateIsBefore.push(this.dateBefore);

              this.dateIsBefore[0] = moment(this.dateIsBefore[0]).format('YYYY-MM-DD');
              this.dateBefore = moment(this.dateBefore).format('YYYY-MM-DD');
            }
            break;

          case 'dateIsAfter':
            if (this.dateBefore !== '' && this.dateIsAfter.length === 0) {
              this.dateIsAfter.push(this.dateBefore);

              this.dateIsAfter[0] = moment(this.dateIsAfter[0]).format('YYYY-MM-DD');
            }
            break;

          case 'dateBetween':
            if (this.dateBefore !== '' && this.dateAfter !== '') {

              this.dateBefore = moment(this.dateBefore).format('YYYY-MM-DD');
              this.dateAfter = moment(this.dateAfter).format('YYYY-MM-DD');

              const date = `${this.dateBefore} TO ${this.dateAfter}`;
              const indexBeforeBet = this.dateBetween.indexOf(date);
              if (indexBeforeBet === -1) {
                this.dateBetween.push(date);
              }
            }
            break;
          default:
            break;
        }
        break;

      case 'name':
        switch (this.nameFilter) {

          case 'admin':
            const indexAdmin = this.nameAdmin.indexOf(this.nameInput);
            if (indexAdmin === -1) {
              this.nameAdmin.push(this.nameInput);
            }
            break;

          case 'merchant':
            const indexMerchant = this.nameMerchant.indexOf(this.nameInput);
            if (indexMerchant === -1) {
              this.nameMerchant.push(this.nameInput);
            }
            break;

          case 'payeeCompany':
            const indexPayeeCompany = this.namePayeeCompany.indexOf(this.nameInput);
            if (indexPayeeCompany === -1) {
              this.namePayeeCompany.push(this.nameInput);
            }
            break;

          case 'payee':
            const indexPayee = this.namePayee.indexOf(this.nameInput);
            if (indexPayee === -1) {
              this.namePayee.push(this.nameInput);
            }
            break;

          default:
            break;
        }
        break;

      case 'email':
        if (this.emailInput !== '') {
          const indexEmail = this.emailArr.indexOf(this.emailInput);
          if (indexEmail === -1) {
            this.emailArr.push(this.emailInput);
          }
        }
        break;
      case 'idTransaction':
        if (this.idTransactionInput !== '') {
          const indexIdTransaction = this.idTransaction.indexOf(this.idTransactionInput);
          if (indexIdTransaction === -1) {
            this.idTransaction.push(this.idTransactionInput);
          }
        }
        break;
        break;
      default:
        break;
    }
    this.ngAfterViewInit();
    this.resetVariable();
  }

  resetVariable() {
    this.filterName = '';
    this.nicknameInput = '';
    this.dateBefore = '';
    this.dateAfter = '';
    this.dateFilter = '';
    this.statusId = '';
    this.nameInput = '';
    this.emailInput = '';
    this.idTransactionInput = '';
    this.nameFilter = null;
    this.countrySelected = null;
    this.CountryPayee.reset();
    this.disabledEditAfterDate = true;
    this.showDataBetween = false;
    this.showApplyFilter = false;
  }

  cleanFilter() {
    this.nameAdmin = [];
    this.namePayee = [];
    this.dateEqual = [];
    this.nameStatus = [];
    this.nicknameArr = [];
    this.dateIsAfter = [];
    this.dateBetween = [];
    this.dateIsBefore = [];
    this.nameMerchant = [];
    this.countryFilter = [];
    this.emailArr = [];
    this.idTransaction = [];
    this.namePayeeCompany = [];
    this.onlyPayeeCompanyArr = [];
    this.onlyPayeeCompany = false;
    this.ngAfterViewInit();
  }

  selectDates() {
    this.dateFilter === 'dateBetween' ? this.showDataBetween = true : this.showDataBetween = false;
  }

  handleClickNewPaymentButton() {
    if (this.accountBalance.value < 100) {
      this.showMessage(this.amountMustBe100.value, 'warning');
    } else {
      this.router.navigate(['/payment/newPayment']).then();
    }
  }

  handleClickProcessPaymentButton() {
    this.router.navigate(['/payment/processPayment']).then();
  }

  handleClickProcessedFileButton() {
    this.router.navigate(['/payment/files']).then();
  }

  handleClickStatusChangeButton() {
    this.router.navigate(['payment/changePaymentStatus']).then();
  }

  handleClickFilterToggle() {
    this.initPayeeCountryFilter();
    this.resetVariable();
    this.collapse = !this.collapse;
  }

  updateNote(payment: any): void {
    if (this.userSessionService.isSuperOrAdmin() || this.userSessionService.isMerchant() ) {
      const dialogRef = this.dialog.open(NoteModalComponent, {
        data: { paymentId: payment.id, note: payment.notes, type: 'notes' }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          payment.notes = result;
          this.paymentService.updatePaymentNote(payment.id, payment.notes).subscribe(() => {
            this.ngAfterViewInit();
          });
        }
      });
    }
  }
  

  openReasonModal(payment: any, reasonType: string): void {
    const dialogRef = this.dialog.open(NoteModalComponent, {
      data: { paymentId: payment.id, type: 'reason', reasonType }
    });
    let message:any;
    dialogRef.afterClosed().subscribe(result => {
      // result es undefine si se clickea fuera del modal de otra manera se ejecuta la condición
      if (result !== undefined) {
        payment.statusReason = result;
        if (this.userSessionService.isSuperOrAdmin()) {
          if (reasonType === 'Accept') {
            this.paymentService.changePaymentStatus(payment, this.extendedUser.id, 'ADMIN', 'ACCEPTED').subscribe(() => {
              message = this.translate.get('MESSAGE.PAYMENTS_ACCEPTED');
              this.showMessage(message.value,'success');
              this.ngAfterViewInit();
            });
          }  else if (reasonType === 'Reverse') {
            this.paymentService.changePaymentStatus(payment, this.extendedUser.id, 'ADMIN', 'REVERTED').subscribe(() => {
              message = this.translate.get('MESSAGE.PAYMENTS_REVERTED');
              this.showMessage(message.value,'success');
              this.ngAfterViewInit();
            });
          }  else {
            this.paymentService.changePaymentStatus(payment, this.extendedUser.id, 'ADMIN', 'REJECTED').subscribe(() => {
              message = this.translate.get('MESSAGE.PAYMENTS_REJECTED');
              this.showMessage(message.value,'success');
              this.ngAfterViewInit();
            });
          }
        } else {
          this.paymentService.changePaymentStatus(payment, this.extendedUser.id, 'MERCHANT').subscribe(() => {
            this.userService.getExtendedUserByUserId(this.user.id).subscribe(
              extendedUser => {
                this.userSessionService.saveUserSession(this.user, extendedUser.content[0]);
                this.userService.updateAccountBalanceObservable(extendedUser.content[0].balance);
              });
            this.ngAfterViewInit();
          });
        }
      }
    });
  }

  handleBeforeInBetweenDateChange() {
    if (this.dateBefore) {
      this.dateBeforeBS = this.dateBefore;
      this.dateBefore = moment(this.dateBefore).format('YYYY-MM-DD');
      const arrDate = this.dateBefore.split('-');
      this.minDate = `${arrDate[0]}-${arrDate[1]}-${+arrDate[2] + 1}`;
      this.disabledEditAfterDate = false;
    } else {
      this.disabledEditAfterDate = true;
    }
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  onChangeCountry() {
    if (this.CountryPayee.value.id) {
      this.countrySelected = this.CountryPayee.value;
    } else {
      this.countrySelected = null;
    }
  }

  canSeeButtonHandle(type, row): boolean {
    const body: any = JSON.parse(localStorage.getItem('ResultTransactionGroups'));
    let value = false;
    if(body && body !== undefined && body.rippleResults !== undefined && 
      (body.rippleResults.find( item => item.transaction_id == row.id ) || body.rippleResultsWithErrors.find( item => item.transaction_id == row.id ))) {
      return value;
    }
    const existRowStatus = row && row.status;
    switch (type) {
      case 'cancel':
        if (!this.userSessionService.isDisabled() &&
          this.userSessionService.isMerchant() && existRowStatus && row.status === STATUS.CREATED && row.payee.status !== STATUS.REJECTED) {
          value = true;
        }
        break;
      case 'reject':
        if (this.userSessionService.isSuperOrAdmin() && !this.userSessionService.isDisabled() && existRowStatus &&
          (row.status === STATUS.CREATED || row.status === STATUS.FAILED || row.status === STATUS.IN_PROCESS && row.payee.status !== STATUS.REJECTED)
          // || (row.status === STATUS.IN_PROCESS && row.processType === PROCESS_TYPE.FILE_EXPORT))
        ) {
          value = true;
        }
        break;
        case 'reverse':
          if (this.userSessionService.isSuperOrAdmin() && !this.userSessionService.isDisabled() && existRowStatus &&
            ( row.status === STATUS.IN_PROCESS && row.payee.status !== STATUS.REJECTED)
          ) {
            value = true;
          }
          break;
      case 'accept':
        if (this.userSessionService.isSuperOrAdmin() && !this.userSessionService.isDisabled()
          && existRowStatus && row.status === STATUS.IN_PROCESS && row.payee.status !== STATUS.REJECTED) {
          // && existRowStatus && row.status === STATUS.IN_PROCESS && row.processType === PROCESS_TYPE.FILE_EXPORT) {
          value = true;
        }
        break;
    }
    return value;
  }

  seeAutomaticPaymentLegendHandler(row) {
    const existRowStatus = row && row.status && row.processType;
    return existRowStatus && row.status === STATUS.IN_PROCESS && row.processType === PROCESS_TYPE.API_CALL;
  }

  getReSellerCommissionPaid(row): any {
    if (row.status === STATUS.ACCEPTED) {
      return row.resellerCommission;
    } else {
      return (this.extendedUser.resellerFixedCommission + (row.amountBeforeCommission * this.extendedUser.resellerPercentageCommission));
    }
  }

  validacionInput(e: any, type: string, addCHaracterSpecialc = null) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = '';
    if (type === 'numeros') {
      numeros = '0123456789';
    } else if (type === 'letras') {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz ';
    } else {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    }

    if (addCHaracterSpecialc !== null) {
      numeros = numeros + addCHaracterSpecialc;
    }
/*     const numeros = type === 'numeros' ? "0123456789" :
    'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 '; */
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }
}
