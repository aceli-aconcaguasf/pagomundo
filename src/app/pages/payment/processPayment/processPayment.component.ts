import { Router } from '@angular/router';
import { AfterViewInit, Component, Input, ViewChild } from '@angular/core';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { MatDialog, TooltipPosition } from '@angular/material';
import { AUTOCOMPLETE_TYPE } from '../../../config/enums';
import { merge, of as observableOf, BehaviorSubject, Observable } from 'rxjs';
import { DataFinderService, PaymentService, UserService, UserSessionService } from '../../../services/service.index';
import { environment } from '../../../../environments/environment';
import { CommissionModalComponent, MessageNotificationComponent } from '../../../components/components.index';
import { ExtendedUser } from '../../../config/interfaces';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-process-payment',
  templateUrl: './processPayment.component.html',
  styleUrls: ['./processPayment.component.scss']
})

export class ProcessPaymentComponent implements AfterViewInit {
  environment = environment;

  total = 0;
  calculadosExchangeRate = [];
  stateOk: boolean = true;
  totalLocalCurrency = 0;
  responseProcess: any;
  exchangeRate: number;
  currencyCode: string;

  banks = [];
  allBankAccounts = [];
  currencies = [];
  bankAccounts = [];
  totalToProcess = [];
  bankId = null;
  bankAccountId = null;
  bankAccountIdBool: boolean = false;
  buttonCalculateExchangeRate = false;
  disabledButtonCalculateExchangeRate = true;

  @Input() role: any;

  uri = '/api/_search/transactions';
  query = '';

  countrySelected: any;
  countryForm: FormGroup;
  seeBank = false;
  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  data: any[] = [];
  bankCommission = 0;
  fxCommission = 0;
  rechargeCost = 0;

  extendedUser: ExtendedUser;
  positionOptions: TooltipPosition = 'above';
  autocompleteType = AUTOCOMPLETE_TYPE;

  checked = false;
  bankPaymentMethod = false;

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  keyLanguage$: Observable<any> = this.store.select('language');
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  merchant: any;
  payye: any;
  lastUpdated: any;
  amountBeforeCommission: any;
  amountAfterCommission: any;
  amountLocalCurrency: any;
  selectLastPayment: any;
  reportsMessages: any;
  exchangeRateMust: any;
  errorNoQuote: any;
  errorMessage: any;
  changeClass: number;
  bankABCMexico = false;
  remitee: boolean = false;
  dataAux: any;
  isError: boolean = false;
  constructor(private router: Router,
    private dataFinder: DataFinderService,
    public dialog: MatDialog,
    private userService: UserService,
    private userSessionService: UserSessionService,
    private paymentService: PaymentService,
    private translate: TranslateService,
    private store: Store<language>) {
    this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    this.keyLanguage$.subscribe(item => {
      const language: any = item
      this.translate.use(language.key);
      this.errorMessage = this.translate.get('ERROR_MESSAGES.ERROR_GETTING_PAYMENTS');
      this.merchant = this.translate.get('DATA_TABLE.MERCHANT_COMPANY');
      this.payye = this.translate.get('REPORTS_MESSAGES.PAYEE_BUSINESS_ACCOUNT');
      this.lastUpdated = this.translate.get('DATA_TABLE.DATE');
      this.amountBeforeCommission = this.translate.get('DATA_TABLE.AMOUNT');
      this.amountAfterCommission = this.translate.get('DATA_TABLE.AFTER_COMMISSION');
      this.amountLocalCurrency = this.translate.get('DATA_TABLE.LOCAL_CURRENCY');
      this.selectLastPayment = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SELECT_ONE_PAYMENT');
      this.reportsMessages = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.ENTER_EXCHANGE_RATE');
      this.exchangeRateMust = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.EXCHANGE_RATE_MUST');
      this.errorNoQuote = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.ERROR_NO_QUOTE');

    });
    this.countryForm = new FormGroup({
      countryPayee: new FormControl('', Validators.required),
    });
    this.extendedUser = this.userSessionService.loadExtendedUser();
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss");
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  get countryPayee() { return this.countryForm.get('countryPayee'); }

  ngAfterViewInit() {
    this.columnNames = {
      check: '', id: 'Id',
      merchant: this.merchant.value,
      payee: this.payye.value,
      lastUpdated: this.lastUpdated.value,
      amountBeforeCommission: this.amountBeforeCommission.value,
      amountAfterCommission: this.amountAfterCommission.value,
      amountLocalCurrency: this.amountLocalCurrency.value, action: ''
    };
    this.columns = Object.keys(this.columnNames);
  }

  resetRelatedVariables() {
    this.bankId = null;
    this.bankAccountId = null;
    this.bankAccountIdBool = false;
    // this.seeBank = false;
    this.banks = [];
    this.cleanBankAccountSelection();
    this.allBankAccounts = [];
  }

  allCheckElement() {
    // allCheck
    if ((document.getElementById('allCheck') as HTMLInputElement).checked) {
      this.totalToProcess = [...this.data];
      for (const process of this.totalToProcess) {
        if ((document.getElementById(process.id) as HTMLInputElement).disabled === false) {
          (document.getElementById(process.id) as HTMLInputElement).checked = true;
        }      }
        this.disabledButtonCalculateExchangeRate = false;
    } else {
      for (const process of this.totalToProcess) {
        if ((document.getElementById(process.id) as HTMLInputElement).disabled === false) {
          (document.getElementById(process.id) as HTMLInputElement).checked = false;
          }      }
      this.totalToProcess = [];
      this.disabledButtonCalculateExchangeRate = true;
    }
    this.sumTotal(this.totalToProcess);
    this.sumTotalLocalCurrency(this.totalToProcess);
  }

  getQueryString(): string {
    //Si es diferente de colombia o mexico setea automaticamente a banco
    if (this.countrySelected.id === 66 || this.countrySelected.id === 74  || this.countrySelected.id === 77) {
      this.bankPaymentMethod = true;
    }

    let query = `(status:CREATED OR status:FAILED) AND payee.useDirectPayment:${this.bankPaymentMethod}`;
    query += this.bankPaymentMethod ? ` AND currency.country.id:${this.countrySelected.id}`
      : (this.countrySelected.id !== null ? ` AND currency.country.id:${this.countrySelected.id}` : '');
    return query;
  }

  loadTable() {
    this.query = this.getQueryString();
    // If the user changes the sort order, reset back to the first page.
    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query,
            this.sort.active, this.sort.direction, 25);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          this.sumTotal(this.totalToProcess);
          this.sumTotalLocalCurrency(this.totalToProcess);
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataAux = data;
        this.data = data});
  }

  onChangeCountry(country) {
    this.banks = [];
    this.countrySelected = country;
    this.dataFinder.getAllBankAccounts(this.countrySelected.id).subscribe(
      accounts => {
        this.banks = [];
        this.seeBank = true;
        this.allBankAccounts = accounts.content;
        const arrayBankId = [];
        for (const account of this.allBankAccounts) {
          const index = account.bank.id;
          if (arrayBankId.indexOf(index) === -1) {
            arrayBankId.push(index);
            this.banks.push(account.bank);
          }
        }
        if (this.bankPaymentMethod) {
          switch (this.countrySelected.id) {
            case 1: // Country Id 1 = COLOMBIA
              this.bankId = '17'; // BANK ITAU Davivir
              this.banks = this.banks.filter(bank => bank.id === +this.bankId);
              break;
            case 2: // Country Id 2 = MEXICO
              this.bankId = null; // BANK STP y ABC
              this.banks = this.banks.filter(bank => bank.id == 30 || bank.id == 196);
              break;
            case 66: // Country Id 66 = ARGENTINA
              this.bankId = null; // BANK BANCO DO BRASIL and ripple ARGENTINA
              this.banks = this.banks.filter(bank => bank.id == 31 || bank.id == 199);
              this.bankPaymentMethod = true;
              break;
            case 74: // Country Id 74 = BRAZIL
              this.bankId = null; // BANK BANCO DO BRASIL and ripple BRZUL
              this.banks = this.banks.filter(bank => bank.id == 32 || bank.id == 198);

              this.bankPaymentMethod = true;
              break;
            case 77: // 186 - banco Security y ripple chile
              this.bankId = null; //
              this.banks = this.banks.filter(bank => bank.id == 186 || bank.id == 197);
              this.bankPaymentMethod = true;
              break;
          }
          this.loadBankAccountAndCurrencyForDirectPayment(this.countrySelected.id, this.bankId);
          return false;
        } else {

          this.dataFinder.getCurrenciesByCountryId(this.countrySelected.id).subscribe(data => {
            this.currencies = data.content;
            if (this.currencies[0]) {
              this.currencyCode = this.currencies[0].code;
            }
          });

          if (this.countrySelected.id === 2) { // Country Id 2 = Mexico
            // Recharge card doest work for STP (id = 30)
                this.banks = this.banks.filter(bank => bank.id !== 30);
          }

          if (this.banks.length === 1) {
            this.bankId = this.banks[0].id;
            this.bankAccounts = this.allBankAccounts.filter(account => account.bank.id === this.bankId);
          }
          if (this.bankAccounts.length === 1) { this.selectBankAccount(false, this.bankAccounts[0].id); }
        }

        setTimeout(() => {
          this.loadTable();
        }, 0);
      });
  }

  selectBank(event) {
    this.cleanBankAccountSelection();
    this.bankId = event.target.value;
    if(this.bankPaymentMethod && this.bankId == 196) {
      this.bankABCMexico = true;
      this.exchangeRate = 1;
      this.data = this.dataAux.filter(item => this.userSessionService.usePesosMexicanCoin(item.merchant.id));
    } else {
      if (this.bankPaymentMethod && this.bankId == 30) {
        this.data = this.dataAux.filter(item => !this.userSessionService.usePesosMexicanCoin(item.merchant.id));
        }
      this.bankABCMexico = false;
      this.exchangeRate = 0;
    }
    this.bankAccounts = this.allBankAccounts.filter(account => account.bank.id === +this.bankId);
    if (this.bankAccounts.length === 1) { this.selectBankAccount(false, this.bankAccounts[0].id); }
  }

  cleanBankAccountSelection() {
    this.bankAccountId = null;
    this.bankAccountIdBool = false;
    if (document.getElementById('bankAccount')) {
      (document.getElementById('bankAccount') as HTMLSelectElement).selectedIndex = 0;
    }
  }

  selectBankAccount(withEvent: boolean, event) {
    for (const process of [...this.data]) {
      (document.getElementById(process.id) as HTMLInputElement).disabled = false;
    }
    this.isError = false;

    this.exchangeRate = 0;
    this.bankAccountIdBool = false;
    this.bankAccountId = withEvent ? event.target.value : event;
    const bankAccount = this.bankAccounts.find(bank => {
      this.bankAccountIdBool = true;
      return bank.id.toString() === this.bankAccountId.toString();
    });
    // console.log(bankAccount.isRemitee);
    if (bankAccount) {
      this.bankCommission = bankAccount.bankCommission;
      this.fxCommission = bankAccount.fxCommission;
      this.rechargeCost = bankAccount.rechargeCost;
    } else {
      return;
    }
    if (bankAccount.isRemitee) {
      this.remitee = true;
      this.bankABCMexico = true;
      this.buttonCalculateExchangeRate = true;
    } else {
      this.remitee = false;
      this.bankABCMexico = false;
      this.buttonCalculateExchangeRate = false;
    }

    this.sumTotal(this.totalToProcess);
    this.sumTotalLocalCurrency(this.totalToProcess);
  }

  addCheckElement(payment: any) {

    if ((document.getElementById(payment.id) as HTMLInputElement).checked) {
      this.totalToProcess.push(payment);
    } else {
      const index = this.totalToProcess.indexOf(payment);
      if (index !== -1) {
        this.totalToProcess.splice(index, 1);
      }
    }
    (document.getElementById('allCheck') as HTMLInputElement).checked = this.data.length === this.totalToProcess.length;
    this.sumTotal(this.totalToProcess);
    this.sumTotalLocalCurrency(this.totalToProcess);
    if (this.total > 0) {
      this.disabledButtonCalculateExchangeRate = false;
    }

  }

  sumTotal(arrayAmounts: any[]) {
    this.total = 0;
    for (const amount of arrayAmounts) {
      if (amount.merchant.useMerchantCommission) {
        const bankCommission = amount.amountBeforeCommission * amount.merchant.bankCommission;
        const fxCommission = amount.amountBeforeCommission * amount.merchant.fxCommission;
        this.total = this.total + (amount.amountBeforeCommission - amount.merchant.rechargeCost - bankCommission - fxCommission);
      } else {
        const bankCommission = amount.amountBeforeCommission * this.bankCommission;
        const fxCommission = amount.amountBeforeCommission * this.fxCommission;
        this.total = this.total + (amount.amountBeforeCommission - this.rechargeCost - bankCommission - fxCommission);
      }
    }
  }

  sumTotalLocalCurrency(arrayAmounts: any[]) {
    this.totalLocalCurrency = 0;
    for (const amount of arrayAmounts) {
      if (amount.merchant.useMerchantCommission) {
        const bankCommission = amount.amountBeforeCommission * amount.merchant.bankCommission;
        const fxCommission = amount.amountBeforeCommission * amount.merchant.fxCommission;
        this.totalLocalCurrency = this.totalLocalCurrency +
          ((amount.amountBeforeCommission - amount.merchant.rechargeCost - bankCommission - fxCommission) * this.exchangeRate);
      } else {
        const bankCommission = amount.amountBeforeCommission * this.bankCommission;
        const fxCommission = amount.amountBeforeCommission * this.fxCommission;
        this.totalLocalCurrency = this.totalLocalCurrency +
          ((amount.amountBeforeCommission - this.rechargeCost - bankCommission - fxCommission) * this.exchangeRate);
      }
    }
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  processPayments() {
    this.stateOk = true;
    this.totalToProcess.forEach(dato => {
      // this.stateOk = this.calculadosExchangeRate.includes(dato.id);
      if (!this.calculadosExchangeRate.includes(dato.id)) {
        this.stateOk = false;
      }
    })
    if (!this.stateOk && this.remitee) {
      this.showMessage(this.errorNoQuote.value, 'error');
    } else if (this.totalToProcess.length <= 0) {
      this.showMessage(this.selectLastPayment.value, 'error');
    } else if (this.exchangeRate === undefined || this.exchangeRate === null) {
      this.showMessage(this.reportsMessages.value, 'error');
    } else if (this.exchangeRate <= 0) {
      this.showMessage(this.exchangeRateMust.value, 'error');
    } else {
      this.paymentService.processPayment(this.totalToProcess, +this.bankAccountId, this.exchangeRate, this.bankPaymentMethod).subscribe(
        data => {
          if (data.transactionsWithErrors.length === 0) {
            this.responseProcess = data;
            this.router.navigate(['/payment/files']).then();
          } else {
            this.router.navigateByUrl('/payment/files', { state: { withErrors: true } }).then();
          }
        });
    }
  }

  calculateAfterCommission(merchant: any, beforeCommission: number) {
    if (merchant.useMerchantCommission) {
      const bankCommission = beforeCommission * merchant.bankCommission;
      const fxCommission = beforeCommission * merchant.fxCommission;
      return beforeCommission - merchant.rechargeCost - bankCommission - fxCommission;
    } else {
      const bankCommission = beforeCommission * this.bankCommission;
      const fxCommission = beforeCommission * this.fxCommission;
      return beforeCommission - this.rechargeCost - bankCommission - fxCommission;
    }
  }

  calculateLocalCurrency(merchant: any, beforeCommission: number) {
    if (merchant.useMerchantCommission) {
      const bankCommission = beforeCommission * merchant.bankCommission;
      const fxCommission = beforeCommission * merchant.fxCommission;
      return (beforeCommission - merchant.rechargeCost - bankCommission - fxCommission) * this.exchangeRate;
    } else {
      const bankCommission = beforeCommission * this.bankCommission;
      const fxCommission = beforeCommission * this.fxCommission;
      return (beforeCommission - this.rechargeCost - bankCommission - fxCommission) * this.exchangeRate;
    }
  }

  seeMerchantCommissionModal(extendedUser: ExtendedUser): void {
    this.dialog.open(CommissionModalComponent, {
      // width: '300px',
      data: { extendedUser, readOnly: true }
    });
  }

  loadBankAccountAndCurrencyForDirectPayment(countryId, bankId) {
    if (bankId !==  null) {
      this.bankAccounts = this.allBankAccounts.filter(account => account.bank.id === +bankId);
      if (this.bankAccounts.length === 1) {
        this.selectBankAccount(false, this.bankAccounts[0].id);
      }
    }
    this.dataFinder.getCurrenciesByCountryId(countryId).subscribe(currency => {
      this.currencies = currency.content;
      if (this.currencies[0]) {
        this.currencyCode = this.currencies[0].code;
      }
      setTimeout(() => {
        this.loadTable();
      }, 0);
    });


  }

  selectPaymentMethod(event) {
    this.bankPaymentMethod = event.checked;
    // this.countryForm.reset();
    // this.countryForm.updateValueAndValidity();
    this.cleanBankAccountSelection();
    this.resetRelatedVariables();
    this.onChangeCountry(this.countrySelected)
  } 
  
  sendCalcualteExchange() {
    this.isError = false;
    const body = [];
    this.calculadosExchangeRate = [];
    this.totalToProcess.forEach( req => {body.push({id: req.id});
    this.calculadosExchangeRate.push(req.id); // obtenemos los id de los pagos calculados en CalcualteExchange
  });
    this.paymentService.processTransactionsQuote(body, this.bankAccountId).subscribe(
      data => {
        if (data.rippleResults.length !== null && data.rippleResults.length > 0) {

          this.exchangeRate = data.rippleResults[0].rate;
        }
        if (data.rippleResultsWithErrors.length !== null && data.rippleResultsWithErrors.length > 0) {
          this.isError = true;
          for (let i = 0; i < data.rippleResultsWithErrors.length; i++) {
            if (data.rippleResultsWithErrors[i].transaction_id !== null && data.rippleResultsWithErrors[i].transaction_id !== undefined) {
              this.totalToProcess.forEach(req => {
                if (req.id === +data.rippleResultsWithErrors[i].transaction_id) {
                  (document.getElementById(data.rippleResultsWithErrors[i].transaction_id) as HTMLInputElement).checked = false;
                  this.addCheckElement(req);
                }
              });
              (document.getElementById(data.rippleResultsWithErrors[i].transaction_id) as HTMLInputElement).disabled = true;
            }
          }
        } else {
          this.isError = false;
        }
      });
  }

  validacionInput(e: any) {
    console.log(Number(this.exchangeRate))
    if((String(this.exchangeRate).length > 4 || Number(this.exchangeRate) > 99999)
    && Number(this.exchangeRate) !== 0) {
      return false;
    }
  }
}
