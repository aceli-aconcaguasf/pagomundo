import {Component, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserService} from 'src/app/services/service.index';
import {UserSessionService} from '../../services/user/userSession.service';
import {ExtendedUser} from 'src/app/config/interfaces';
import {CHART_TYPE, CHART_SUB_TYPE, ROLES} from '../../config/enums';
import {MessageNotificationComponent} from '../../components/messageNotification/messageNotification.component';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-report',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  extendedUser: ExtendedUser;
  chartSubType = CHART_SUB_TYPE;
  chartType = CHART_TYPE;

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  roles = ROLES;
  role: string;

  keyLanguage$:Observable<any> = this.store.select('language');
  completedTraductor:any;
  numberPaymentsTraductor:any;
  completedPaymentsTraductor:any;
  paymentsTraductor:any;
  merchantsTraductor:any;
  commissinsEarnedTraductor:any;
  payeesAddedTraductor:any;
  payesAddedCountryTraductor:any;
  payeesAddedStatusTraductor :any;
  usersAddedTraductor:any;
  successfulReportRequestTraductor:any;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor(private userService: UserService,
              public userSessionService: UserSessionService,
              private translate: TranslateService,
              private store: Store<language>) {
    this.extendedUser = this.userSessionService.loadExtendedUser();
    this.role = this.userSessionService.loadRole();
    this.keyLanguage$.subscribe(item => {
      const language:any = item
       this.translate.use(language.key);
      this.completedTraductor = this.translate.get( 'STATISTICS.COMPLETED_PAYMENTS' );
      this.numberPaymentsTraductor = this.translate.get( 'STATISTICS.NUMBER_PAYMENTS' );
      this.completedPaymentsTraductor = this.translate.get( 'STATISTICS.COMPLETED_PAYMENTS_MONETARY' );
      this.paymentsTraductor = this.translate.get( 'STATISTICS.PAYMENTS_STATE' );
      this.merchantsTraductor = this.translate.get( 'STATISTICS.MERCHANTS_ADDED' );
      this.commissinsEarnedTraductor = this.translate.get( 'STATISTICS.COMMISSINS_EARNED' );
      this.payeesAddedTraductor = this.translate.get( 'STATISTICS.PAYEES_ADDED' );
      this.payesAddedCountryTraductor = this.translate.get( 'STATISTICS.PAYEES_ADDED_COUNTRY' );
      this.payeesAddedStatusTraductor = this.translate.get( 'STATISTICS.PAYEES_ADDED_STATUS' );
      this.usersAddedTraductor = this.translate.get( 'STATISTICS.USERS_ADDED_STATUS' );
      this.successfulReportRequestTraductor =  this.translate.get( 'STATISTICS.SUCCESSFUL_REPORT_REQUEST' );
    });
   }

  ngOnInit() {
  }

  isSuperOrAdmin(): boolean {
    let value = false;
    if (this.role === ROLES.ADMIN || this.role === ROLES.SUPER_ADMIN) {
      value = true;
    }
    return value;
  }

  showTransactionReportRequestMessage() {
    const msg:any = this.successfulReportRequestTraductor.value;
    this.messageComponent.setTypeMessage(String(this.successfulReportRequestTraductor.value), 'success');
  }

}
