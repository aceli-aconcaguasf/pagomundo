import {Routes} from '@angular/router';

import {PaymentComponent} from './payment/payment.component';
import {NewPaymentComponent} from './payment/newPayment/newPayment.component';
import {ProcessPaymentComponent} from './payment/processPayment/processPayment.component';
import {NotificationComponent} from './notification/notification.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {MessageConfirmationComponent} from '../components/messageConfirmation/messageConfirmation.component';
import {GeneratedFileComponent} from './payment/generatedFile/generatedFile.component';
import {ChangeStatusComponent} from './payment/changeStatus/changeStatus.component';
import {ProfileComponent} from './profile/profile.component';
import {UserOnlyProfileViewGuard} from '../services/service.index';
import {UserComponent} from './user/user.component';
import {NewUserComponent} from './user/newUser/newUser.component';
import {MobileMessageComponent} from './mobileMessage/mobileMessage.component';
import {ChangeUserStatusComponent} from './user/changeUserStatus/changeUserStatus.component';
import {ProcessPayeeComponent} from './user/processUser/processPayee/processPayee.component';
import {ProcessMerchantComponent} from './user/processUser/processMerchant/processMerchant.component';
import {GeneratedUserFileComponent} from './user/generatedUserFile/generatedUserFile.component';
import {InvitedUserComponent} from './user/invitedUser/invitedUser.component';
import {SettingComponent} from './setting/setting.component';
import {settingRoutes} from './setting/setting.routes';
import {FundComponent} from './fund/fund.component';
import {WithdrawalComponent} from './withdrawal/withdrawal.component';
import {DashboardComponent} from './dashboard/dashboard.component';

export const pagesRoutes: Routes = [
  {path: 'mobileMessage', component: MobileMessageComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'payment', component: PaymentComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'setting', component: SettingComponent, canActivate: [UserOnlyProfileViewGuard], children: settingRoutes},
  {path: 'payment/newPayment', component: NewPaymentComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'payment/processPayment', component: ProcessPaymentComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'payment/files', component: GeneratedFileComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'payment/changePaymentStatus', component: ChangeStatusComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'notification', component: NotificationComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'fund', component: FundComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'withdrawal', component: WithdrawalComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'relatedUsers', component: UserComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'relatedUsers/newUser', component: NewUserComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'relatedUsers/invitedUser', component: InvitedUserComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'relatedUsers/changeUserStatus', component: ChangeUserStatusComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'relatedUsers/processPayees', component: ProcessPayeeComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'relatedUsers/processMerchants', component: ProcessMerchantComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'relatedUsers/generatedUserFile', component: GeneratedUserFileComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'statistics', component: StatisticsComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'messageConfirmation', component: MessageConfirmationComponent, canActivate: [UserOnlyProfileViewGuard]},
  {path: 'profile/:id', component: ProfileComponent}, {path: 'profile', component: ProfileComponent},
  {path: 'profile', component: ProfileComponent}, {path: 'profile', component: ProfileComponent},
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: '**', component: DashboardComponent},

  // { path: 'path/:routeParam', component: MyComponent },
  // { path: 'staticPath', component: ... },
  // { path: '**', component: ... },
  // { path: 'oldPath', redirectTo: '/staticPath' },
  // { path: ..., component: ..., data: { message: 'Custom' }
];

