import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';
import {ChartService} from '../../../services/chart/chart.service';
import {CARD_TYPES} from '../../../config/enums';
import {UserSessionService} from '../../../services/user/userSession.service';

export interface CountryElem {
  name: string;
  count: number;
  percentNumber: number;
  percentString: string;
}

export class StatsObject {
  count: number;
  lastMonthCount: number;
  percentNumber: number;
  percentPositiveNumber: number;

  constructor(
    count,
    lastMonthCount,
    percentNumber,
    percentPositiveNumber) {

    this.count = count;
    this.lastMonthCount = lastMonthCount;
    this.percentNumber = percentNumber;
    this.percentPositiveNumber = percentPositiveNumber;
  }
}

@Component({
  selector: 'app-dashboard-card',
  templateUrl: './dashboardCard.component.html',
  styleUrls: ['./dashboardCard.component.scss']
})
export class DashboardCardComponent implements OnInit {

  @Input() extendedUser: any;
  @Input() title: string;
  @Input() cardType: string;

  cardTypeEnum = CARD_TYPES;

  statsObject = new StatsObject(0, 0, 0, 0);
  countryArray: CountryElem[];

  month = moment().startOf('month').format('YYYY-MM');
  nextMonth = moment().add(1, 'month').format('YYYY-MM');
  monthFirstDay = moment().startOf('month').format('YYYY-MM-DD');
  monthLastDay = moment(this.nextMonth).subtract(1, 'day').format('YYYY-MM-DD');
  lastMonth = moment().subtract(1, 'month').format('YYYY-MM');

  constructor(private chartService: ChartService,
              public userSessionService: UserSessionService) { }

  ngOnInit(): void {
    this.loadCard();

  }

  loadCard() {
    switch (this.cardType) {
      case CARD_TYPES.PAYMENT_ACCEPTED:
        this.loadStatusCount();
        break;
      case CARD_TYPES.PAYMENT_MONETARY:
      case CARD_TYPES.PAYMENT_MONETARY_MERCHANT:
      case CARD_TYPES.RE_SELLER_COMMISSION:
        this.loadTotalMonetary();
        break;
      case CARD_TYPES.PAYEE_REGISTER:
        this.loadPayeeRegister();
        break;
      case CARD_TYPES.PAYEE_ACTIVE:
        this.loadPayeeActive();
        break;
      case CARD_TYPES.METHOD_CARD:
      case CARD_TYPES.METHOD_DT:
        this.loadTransactionByMethod();
        break;
      case CARD_TYPES.COUNTRY_COUNT:
        this.loadTransactionByCountry();
        break;
      default:
        break;
    }
  }

  loadStatusCount() {
    this.chartService.searchMonthlyTransactionStatus(this.lastMonth).subscribe(
      data => {
        try {
          // @ts-ignore
          const buckets = data.aggregations.filterByDate.dateCreated_date_histogram.buckets;
          buckets[this.month]
            ? this.statsObject.count = buckets[this.month].doc_count
            : this.statsObject.count = 0;
          buckets[this.lastMonth]
            ? this.statsObject.lastMonthCount = buckets[this.lastMonth].doc_count
            : this.statsObject.lastMonthCount = 0;

          this.statsObject.percentNumber = (this.statsObject.count - this.statsObject.lastMonthCount) / this.statsObject.lastMonthCount;
          this.statsObject.percentPositiveNumber = this.statsObject.percentNumber > 0
            ? this.statsObject.percentNumber : -this.statsObject.percentNumber;

        } catch (err) {
          this.statsObject = new StatsObject(0, 0, 0, 0);
          console.error('CATCH_ERROR');
          console.error(err);
        }
      });
  }

  loadTotalMonetary() {
    const methodCall = this.cardType === CARD_TYPES.RE_SELLER_COMMISSION
      ? this.chartService.searchMonthlyTransactionMonetary(this.extendedUser, this.lastMonth, true)
      : this.chartService.searchMonthlyTransactionMonetary(this.extendedUser, this.lastMonth, false);

    methodCall.subscribe(
      data => {
        try {
          const buckets = data.aggregations.filterByDate.lastUpdated_date_histogram.buckets;

          buckets[this.month]
            ? this.statsObject.count = Math.trunc(buckets[this.month].stats_field.sum) / 1000
            : this.statsObject.count = 0;
          buckets[this.lastMonth]
            ? this.statsObject.lastMonthCount = Math.trunc(buckets[this.lastMonth].stats_field.sum) / 1000
            : this.statsObject.lastMonthCount = 0;

          this.statsObject.percentNumber = (this.statsObject.count - this.statsObject.lastMonthCount) / this.statsObject.lastMonthCount;
          this.statsObject.percentPositiveNumber = this.statsObject.percentNumber > 0 ?
            this.statsObject.percentNumber : -this.statsObject.percentNumber;

        } catch (err) {
          this.statsObject = new StatsObject(0, 0, 0, 0);
          console.error('CATCH_ERROR');
          console.error(err);
        }
      });
  }

  loadPayeeRegister() {
    this.chartService.searchMonthlyPayeeCount(false, this.lastMonth).subscribe(
      data => {
        try {
          const buckets = data.aggregations.filterByDate.dateCreated_date_histogram.buckets;
          buckets[this.month]
            ? this.statsObject.count = buckets[this.month].doc_count
            : this.statsObject.count = 0;
          buckets[this.lastMonth]
            ? this.statsObject.lastMonthCount = buckets[this.lastMonth].doc_count
            : this.statsObject.lastMonthCount = 0;

          this.statsObject.percentNumber = (this.statsObject.count - this.statsObject.lastMonthCount) / this.statsObject.lastMonthCount;
          this.statsObject.percentPositiveNumber = this.statsObject.percentNumber > 0
            ? this.statsObject.percentNumber : -this.statsObject.percentNumber;

        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
      });
  }

  loadPayeeActive() {
    let callMethod;
    if (this.userSessionService.isSuperOrAdmin()) {
      callMethod = this.chartService.searchMonthlyPayeeCount(true, this.lastMonth);
    } else if (this.userSessionService.isMerchant()) {
      callMethod = this.chartService.searchMonthlyMerchantPayeeCount(this.extendedUser.user.id, this.lastMonth);
    }

    callMethod.subscribe(
      data => {
        try {
          const buckets = data.aggregations.filterByDate.dateCreated_date_histogram.buckets;
          buckets[this.month]
            ? this.statsObject.count = buckets[this.month].doc_count
            : this.statsObject.count = 0;
          buckets[this.lastMonth]
            ? this.statsObject.lastMonthCount = buckets[this.lastMonth].doc_count
            : this.statsObject.lastMonthCount = 0;

          this.statsObject.percentNumber = (this.statsObject.count - this.statsObject.lastMonthCount) / this.statsObject.lastMonthCount;
          this.statsObject.percentPositiveNumber = this.statsObject.percentNumber > 0
            ? this.statsObject.percentNumber : -this.statsObject.percentNumber;

        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
      });
  }

  loadTransactionByMethod() {
    this.chartService.searchMonthlyMethodTransaction(this.lastMonth).subscribe(
      data => {
        try {
          const buckets = data.aggregations.filterByDate.dateCreated_date_histogram.buckets;

          // CARD METHOD
          if (this.cardType === CARD_TYPES.METHOD_DT) {
            buckets[this.month] && buckets[this.month].directPayment.buckets.find(elem => elem.key === 1) // key 1 = true
              ? this.statsObject.count = buckets[this.month].directPayment.buckets.find(elem => elem.key === 1).doc_count
              : this.statsObject.count = 0;
            buckets[this.lastMonth] && buckets[this.lastMonth].directPayment.buckets.find(elem => elem.key === 1) // key 1 = true
              ? this.statsObject.lastMonthCount = buckets[this.lastMonth].directPayment.buckets.find(elem => elem.key === 1).doc_count
              : this.statsObject.lastMonthCount = 0;

            this.statsObject.percentNumber = (this.statsObject.count - this.statsObject.lastMonthCount) / this.statsObject.lastMonthCount;
            this.statsObject.percentPositiveNumber = this.statsObject.percentNumber > 0
              ? this.statsObject.percentNumber : -this.statsObject.percentNumber;

            // DIRECT PAYMENT METHOD (DIRECT TRANSFER)
          } else {
            buckets[this.month] && buckets[this.month].directPayment.buckets.find(elem => elem.key === 0) // key 0 = false
              ? this.statsObject.count = buckets[this.month].directPayment.buckets.find(elem => elem.key === 0).doc_count
              : this.statsObject.count = 0;
            buckets[this.lastMonth] && buckets[this.lastMonth].directPayment.buckets.find(elem => elem.key === 0) // key 0 = false
              ? this.statsObject.lastMonthCount = buckets[this.lastMonth].directPayment.buckets.find(elem => elem.key === 0).doc_count
              : this.statsObject.lastMonthCount = 0;

            this.statsObject.percentNumber = (this.statsObject.count - this.statsObject.lastMonthCount) / this.statsObject.lastMonthCount;
            this.statsObject.percentPositiveNumber = this.statsObject.percentNumber > 0
              ? this.statsObject.percentNumber : -this.statsObject.percentNumber;
          }

        } catch (err) {
          console.error('CATCH_ERROR');
          console.error(err);
        }
      });
  }

  loadTransactionByCountry() {
    this.countryArray = [];
    this.chartService.searchMonthlyCountryTransaction(this.monthFirstDay, this.monthLastDay).subscribe(
      data => {
        try {
          const totalCount = data.aggregations.filters.doc_count;
          const buckets = data.aggregations.filters.count.buckets;

          buckets.forEach(countryObj => {
            const country = {} as CountryElem;
            country.name = countryObj.key;
            country.count = countryObj.doc_count;
            country.percentNumber = Math.round((country.count * 100) / totalCount);
            country.percentString = `${country.percentNumber}%`;

            this.countryArray.push(country);
          });

        } catch (err) {
          this.countryArray = [];
          console.error('CATCH_ERROR');
          console.error(err);
        }
    });

  }


}
