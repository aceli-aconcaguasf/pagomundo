import {Component, AfterViewInit} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserService} from '../../services/user/user.service';
import {ExtendedUser} from '../../config/interfaces';
import {CARD_TYPES, CHART_SUB_TYPE, CHART_TYPE, ROLES} from '../../config/enums';
import {UserSessionService} from '../../services/user/userSession.service';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import * as getLanguage from '../../app.action';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit {

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  extendedUser: ExtendedUser;
  role: string;

  roles = ROLES;
  chartType = CHART_TYPE;
  chartSubType = CHART_SUB_TYPE;
  cardTypeEnum = CARD_TYPES;

  keyLanguage$:Observable<any> = this.store.select('language');
  monthlyRegisterTraductor:any;
  monthlCompletePaymentsTraductor:any;
  monthlyMonetaryTraductor:any;
  monthlyMethodWithCardTraductor:any;
  monthlyMethodTraductor:any;
  monthlyActiveTraductor:any;
  monthlyCompletedPaymentTraductor:any;
  monthlyCompletedMonetaryTraductor:any;
  monthlyCompletedComissionTraductor:any;
  monthlyMerhantVolumenTraductor:any;
  monthlyPaymentVolumeTraductor:any;
  monthlyPaymentAMountCompletedMerchantTraductor:any;
  logoImg:string;
  constructor(private userService: UserService,
              public userSessionService: UserSessionService,
              private translate: TranslateService,
              private store: Store<language>) {
    this.extendedUser = userSessionService.loadExtendedUser();
    this.role = userSessionService.loadRole();
    this.store.dispatch(new getLanguage.LanguageKey(localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" )));
  }

  ngAfterViewInit() {

  }

}
