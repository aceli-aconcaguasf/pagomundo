import {AfterViewInit, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatSort} from '@angular/material';
import {DataFinderService, UserSessionService} from 'src/app/services/service.index';
import {TooltipPosition} from '@angular/material/tooltip';
import {ROLES, STATUS, STATUSES, STATUSES_COLOR} from '../../../config/enums';
import {ExtendedUser} from '../../../config/interfaces';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboardTable.component.html',
  styleUrls: ['./dashboardTable.component.scss']
})

export class DashboardTableComponent implements AfterViewInit {

  keys = Object.keys;
  environment = environment;

  extendedUser: ExtendedUser;
  role: string;

  roles = ROLES;
  statuses = STATUSES;
  statusesColor = STATUSES_COLOR;

  uri = '/api/_search/transactions';
  query = 'status:accepted';

  // Table Variable
  totalElements = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  dataTable: any = [];
  pageSize = 10;

  positionOptions: TooltipPosition =  'above';

  errorMessage:any;

  keyLanguage$:Observable<any> = this.store.select('language');
  merchantCompanyTraductor:any;
  dateTraductor:any;
  payeesBussinesTraductor:any;
  countryTraductor:any;
  amountTraductor:any
  localCurrency:any;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private dataFinder: DataFinderService,
              public userSessionService: UserSessionService,
              private cdRef: ChangeDetectorRef,
              private translate: TranslateService,
              private store: Store<language>) {
    this.extendedUser = userSessionService.loadExtendedUser();
    this.role = userSessionService.loadRole();
    setTimeout(() => {
      this.loadTraductor();
    }, 500);
    this.keyLanguage$.subscribe(item => {
      this.loadTraductor();
  });
  }

  ngAfterViewInit() {
    this.loadTraductor();
    this.loadDataTable();
  }

  loadTraductor(){ 
      const errorMessage:any = this.translate.get( 'ERROR_MESSAGES.ERROR_GETTING_PAYMENTS' );
      this.errorMessage = errorMessage.value;
      this. merchantCompanyTraductor =  this.translate.get( 'DATA_TABLE.MERCHANT_COMPANY' );
      this. dateTraductor =  this.translate.get( 'DATA_TABLE.DATE' );
      this. payeesBussinesTraductor =  this.translate.get( 'DATA_TABLE.PAYEE_BUSINESS' );
      this. countryTraductor =  this.translate.get( 'PROFILE.COUNTRY' );
      this. amountTraductor =  this.translate.get( 'DATA_TABLE.AMOUNT' );
      this. localCurrency =  this.translate.get( 'DATA_TABLE.LOCAL_CURRENCY' );
      this.setColumnNames();
  }

  loadDataTable() {
    this.setColumnNames();
    this.query += this.setQueryRole(this.extendedUser.id);
    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          // return this.dataFinder._search(this.uri, this.query, this.sort.active,
          //   this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, 5);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.totalElements = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.dataTable = data;
      });

    this.cdRef.detectChanges();
  }

  setColumnNames() {
    switch (this.role) {
      case ROLES.SUPER_ADMIN:
      case ROLES.ADMIN:
          this.columnNames = {
            merchant: this.merchantCompanyTraductor.value, 
            payee: this.payeesBussinesTraductor.value, 
            country: this.countryTraductor.value,
            amountBeforeCommission: this.amountTraductor.value
          };
          break;
      case ROLES.RESELLER:
          this.columnNames = {
            merchant: this.merchantCompanyTraductor.value, 
            payee: this.payeesBussinesTraductor.value,
            commissionPaid: 'Commission Paid', 
            country: this.countryTraductor.value, 
            amountBeforeCommission: this.amountTraductor.value
          };
          break;
      case ROLES.MERCHANT:
          this.columnNames = {
            payee: this.payeesBussinesTraductor.value, country: this.countryTraductor.value, amountBeforeCommission: this.amountTraductor.value
          };
          break;
      case ROLES.PAYEE:
          this.columnNames = {
            merchant: this.merchantCompanyTraductor.value,
            lastUpdated: this.dateTraductor.value,
             amountLocalCurrency: this.localCurrency.value
          };
          break;
    }
    this.columns = Object.keys(this.columnNames);
  }

  setQueryRole(id: number): string {
    let query = '';
    switch (this.role) {
      case ROLES.RESELLER:
        query = ` AND reseller.id:${id}`;
        break;
      case ROLES.MERCHANT:
        query = ` AND merchant.id:${id}`;
        break;
      case ROLES.PAYEE:
        query = ` AND payee.id:${id}`;
        break;
    }
    return query;
  }

  getReSellerCommissionPaid(row): number {
    if (row.status === STATUS.ACCEPTED) {
      return (row.resellerFixedCommission + (row.amountBeforeCommission * row.resellerPercentageCommission));
    } else {
      return  (this.extendedUser.resellerFixedCommission + (row.amountBeforeCommission * this.extendedUser.resellerPercentageCommission));
    }
  }

}
