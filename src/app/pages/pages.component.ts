import {Component, OnDestroy, ViewChild} from '@angular/core';
import {UserSessionService} from '../services/user/userSession.service';
import {MessageNotificationComponent} from '../components/messageNotification/messageNotification.component';
import {NotificationService} from '../services/notification/notification.service';
import {ReportService} from '../services/report/report.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnDestroy {

  isMobile = false;
  watcher: any;

  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor(private userSessionService: UserSessionService,
              private notificationService: NotificationService,
              private reportService: ReportService) {
    if (this.userSessionService.loadIsMobile()) {
      this.isMobile = this.userSessionService.loadIsMobile();
    }
    this.initReportNotification();
  }

  ngOnDestroy(): void {
    clearTimeout(this.watcher);
  }

  initReportNotification() {
    if (this.userSessionService.haveStatus()) {
      this.watchForReportNotification();
      this.reportService.alreadyLoadNotificationReport.subscribe(value => {
        if (!value) {
          setTimeout(() => {
            this.watchForReportNotification();
          }, 30000);
        }
      });
    }
  }

  watchForReportNotification() {
    this.notificationService.getDownloadReportNotification(this.userSessionService.loadExtendedUser()).subscribe(
      data => {
        if (data.content.length > 0) {
          this.reportService.setNotificationReportValue(true);
          clearTimeout(this.watcher);
          this.messageComponent.downloadReportNotification(data.content[0]);
        } else {
          this.watcher = setTimeout(() => {
            this.watchForReportNotification();
          }, 30000);
          // OnDestroy we clear the setTimeout
          // Or when there is already a report notification load
        }
     });
  }

}
