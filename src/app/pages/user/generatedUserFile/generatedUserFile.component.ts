import {Component, AfterViewInit, ViewChild, ChangeDetectorRef, OnDestroy} from '@angular/core';
import {DataFinderService, UserService} from '../../../services/service.index';
import {merge, of as observableOf, BehaviorSubject} from 'rxjs';
import {MatPaginator, MatSort} from '@angular/material';
import {startWith, switchMap, catchError, map} from 'rxjs/operators';
import {STATUSES, STATUSES_COLOR} from '../../../config/enums';
import {MessageNotificationComponent} from '../../../components/messageNotification/messageNotification.component';
import {Location} from '@angular/common';

@Component({
  selector: 'app-generated-file',
  templateUrl: './generatedUserFile.component.html',
  styleUrls: ['./generatedUserFile.component.scss']
})
export class GeneratedUserFileComponent implements AfterViewInit, OnDestroy {

  uri = '/api/_search/extended-user-groups';
  query = '';

  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;
  alreadyRefreshing = true;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  data: any[] = [];
  pageSize = 10;

  statuses = STATUSES;
  statusesColor = STATUSES_COLOR;

  refreshTable: any;

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  errorMessage = 'Error getting files';
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor( private dataFinder: DataFinderService,
               private cdRef: ChangeDetectorRef,
               private location: Location,
               private userService: UserService) {
  }

  ngAfterViewInit() {
    this.setAutomaticRefresh();
    const variable: any = this.location.getState();
    if (variable.withErrors) {
      // this.messageComponent.setTypeMessage('Some users could not be processed', 'warning');
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.refreshTable);
    this.alreadyRefreshing = true;
  }

  loadTable() {
    this.initColumnDef();

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query,
            this.sort.active, this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => ( this.data = data));

    this.cdRef.detectChanges();
  }

  changePaginationHandler() {
    if (this.paginator.pageIndex === 0 && !this.alreadyRefreshing) {
      this.alreadyRefreshing = true;
      this.setAutomaticRefresh();
    } else if (this.paginator.pageIndex !== 0) {
      this.alreadyRefreshing = false;
      clearTimeout(this.refreshTable);
    }
  }

  initColumnDef() {
    this.columnNames = {
      action: ' ', fileName: 'File', dateCreated: 'Date', status: 'Status', country: 'Country', bank: 'Bank'
    };
    this.columns = Object.keys(this.columnNames);
  }

  getFilePath(path: string, type: string): string {
    if (path) {
      const arrayPath = path.split('/');
      switch (type) {
        case 'fileName':
        case 'downloadName':
          return arrayPath[arrayPath.length - 1];
        case 'pathLink':
          let pathReturn = '/assets/fileBucket';
          for (let i = 1; i < arrayPath.length; i++) {
            pathReturn += `/${arrayPath[i]}`;
          }
          return pathReturn;
        default:
          break;
      }
    }
  }

  setAutomaticRefresh() {
    this.loadTable();

    this.refreshTable = setTimeout(() => {
      this.setAutomaticRefresh();
    }, 10000);
    // OnDestroy or pageIndex different zero we clear the setTimeout
  }

  refresh() {
    this.ngAfterViewInit();
  }

}
