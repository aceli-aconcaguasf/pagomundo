import {ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';
import {TooltipPosition} from '@angular/material/tooltip';
import {DataFinderService, UserService, UserSessionService} from '../../services/service.index';
import {
  AssociateMerchantModalComponent, FundModalComponent,
  MessageNotificationComponent, NicknameModalComponent, ProfileModalComponent,
  RelatedUserModalComponent, StatusReasonModalComponent,
  UserModalComponent
} from '../../components/components.index';
import {MatDialog} from '@angular/material';
import {ExtendedUser} from '../../config/interfaces';
import {ROLES, STATUS} from '../../config/enums';
import {TableUserComponent} from './tableUser/tableUser.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent {

  extendedUser: ExtendedUser;
  keys = Object.keys;

  roles = ROLES;
  role: string;
  filterQuery: string;

  collapse = false;
  toggleChecked = false;
  rolePayeeFilter = false;
  roleFilterSelected: string;
  radioButtonOptions = [];

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  positionOptions: TooltipPosition =  'above';

  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  @ViewChild('TableUserComponent', {static: false}) userTable: TableUserComponent;
  changeClass:any;
  constructor(private dataFinder: DataFinderService,
              private router: Router,
              private cdRef: ChangeDetectorRef,
              private userService: UserService,
              public userSessionService: UserSessionService,
              public dialog: MatDialog,
              private translate: TranslateService) {
                this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    this.extendedUser = this.userSessionService.loadExtendedUser();
    this.role = this.userSessionService.loadRole();
    setTimeout(() => {
      this.setRadioButtonOptions();
      this.initRoleFilter();
    }, 500);
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  setRadioButtonOptions() {
    let adminTranslate:any = this.translate.get( 'TEXT.ADMIN' );
    let reSellerTranslate:any = this.translate.get( 'TEXT.RESELLER' );
    let merchantTranslate:any = this.translate.get( 'TEXT.MERCHANT' );
    let payeeTranslate:any = this.translate.get( 'TEXT.PAYEE' );

    if (this.role === ROLES.SUPER_ADMIN) {
      this.radioButtonOptions = [
        { name: adminTranslate.value, role: ROLES.ADMIN,  checked: true},
        { name: reSellerTranslate.value, role: ROLES.RESELLER,  checked: false},
        { name: merchantTranslate.value, role: ROLES.MERCHANT,  checked: false},
        { name: payeeTranslate.value,  role: ROLES.PAYEE,  checked: false},
      ];
    }
    else if (this.role === ROLES.ADMIN) {
      this.radioButtonOptions = [
        { name: reSellerTranslate.value, role: ROLES.RESELLER,  checked: false},
        { name: merchantTranslate.value, role: ROLES.MERCHANT,  checked: true},
        { name: payeeTranslate.value,  role: ROLES.PAYEE,  checked: false},
      ];
    }
  }

  initRoleFilter() {
    switch (this.role) {
      case ROLES.SUPER_ADMIN:
        this.roleFilterSelected = ROLES.ADMIN;
        break;
      case ROLES.ADMIN:
        this.roleFilterSelected = ROLES.RESELLER;
        break;
      case ROLES.RESELLER:
        this.roleFilterSelected = ROLES.MERCHANT;
        break;
      case ROLES.MERCHANT:
        this.roleFilterSelected = ROLES.PAYEE;
        break;
      case ROLES.PAYEE:
        this.roleFilterSelected = ROLES.MERCHANT;
        break;
    }
  }

  setFilterQuery(query) {
    query !== '' ? this.filterQuery = query : this.filterQuery = null;
  }

  selectRoleFilterSuperAdminView() {
    this.collapse = false;
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  handleCreateNewUserButton() {
    switch (this.role) {
      case ROLES.SUPER_ADMIN:
      case ROLES.ADMIN:
        this.roleFilterSelected === ROLES.ADMIN ?
          this.router.navigateByUrl('/relatedUsers/newUser', { state: { newUserRole: ROLES.ADMIN } }).then()
          : this.roleFilterSelected === ROLES.RESELLER ?
          this.router.navigateByUrl('/relatedUsers/newUser', { state: { newUserRole: ROLES.RESELLER } }).then()
          : this.roleFilterSelected === ROLES.MERCHANT ?
            this.router.navigateByUrl('/relatedUsers/newUser', { state: { newUserRole: ROLES.MERCHANT } }).then()
            : this.router.navigateByUrl('/relatedUsers/newUser', { state: { newUserRole: ROLES.PAYEE } }).then();
        break;
      case ROLES.MERCHANT:
        this.router.navigateByUrl('/relatedUsers/newUser', { state: { newUserRole: ROLES.PAYEE } }).then();
        break;
    }
  }

  handleCreateNewUserAsReSellerButton(type) {
    type === ROLES.MERCHANT ?
      this.router.navigateByUrl('/relatedUsers/newUser', { state: { newUserRole: ROLES.MERCHANT } }).then()
      : this.router.navigateByUrl('/relatedUsers/newUser', { state: { newUserRole: ROLES.PAYEE } }).then();
  }

  handleInvitedUserButton() {
    this.router.navigate(['/relatedUsers/invitedUser']).then();
  }

  handleChangeUserStatusButton() {
    this.router.navigate(['/relatedUsers/changeUserStatus']).then();
  }

  handleProcessUsersButton(userRole: string) {
    if (userRole === ROLES.MERCHANT) {
      this.router.navigate(['/relatedUsers/processMerchants']).then();
    } else {
      this.router.navigate(['/relatedUsers/processPayees']).then();
    }
  }

  handleClickProcessedFileButton() {
    this.router.navigate(['/relatedUsers/generatedUserFile']).then();
  }

  openRelatedUserModal(extendedUserId: number): void {
    this.dialog.open(RelatedUserModalComponent, {
      data: {
        extendedUserId,
        extendedUserRole : this.roleFilterSelected,
      },
      disableClose: true,
    });
  }

  openFundModal(extendedUser: ExtendedUser): void {

    let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.FUNDS_MODIFIED' );
    const dialogRef = this.dialog.open(FundModalComponent, {
      data: {
        extendedUser,
        type: 'FUND_MERCHANT'
      }
    });
    dialogRef.afterClosed().subscribe(balance => {
      if (balance) {
        this.userService.setFundsMerchant(extendedUser.id, balance).subscribe(
          () => {
            this.showMessage( messageTranslate.value, 'success');
            this.userTable.initTable();
          });
      }
    });
  }

  openChangeNicknameModal(extendedUserRelation): void {
    let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.NICKNAME_MODIFIED_CORRECTLY' );
    const dialogRef = this.dialog.open(NicknameModalComponent, {
      data: {extendedUserRelation}
    });
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        this.showMessage(messageTranslate.value, 'success');
        this.userTable.initTable();
      }
    });
  }

  openChangeAlias(extendedUserRelation): void {

    let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.NICKNAME_MODIFIED_CORRECTLY' );

    const dialogRef = this.dialog.open(NicknameModalComponent, {
      data: {extendedUserRelation}
    });
    dialogRef.afterClosed().subscribe(value => {
      if (value) {
        this.showMessage( messageTranslate.value, 'success');
        this.userTable.initTable();
      }
    });
  }

  openReasonModal(userObj: any): void {
    const extendedUser = userObj.extendedUser;
    const reasonType = userObj.reasonType;
    const dialogRef = this.dialog.open(UserModalComponent, {
      data: {extendedUser, reasonType}
    });
    let status = '';
    let messageTranslate:any;
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        switch (reasonType) {
          case 'Activate':
          case 'Enable':
            status = 'ACCEPTED';
            messageTranslate = this.translate.get( 'TEXT.USERS_ACCEPTED' );
            break;
          case 'Disable':
            status = 'REJECTED';
            messageTranslate = this.translate.get( 'TEXT.USERS_DISABLED' );
            break;
          case 'Delete':
            status = 'CANCELLED';
            messageTranslate = this.translate.get( 'TEXT.USERS_DELETED' );
            break;
        }
        extendedUser.status = status;


        this.userService.changeUserStatus(extendedUser).subscribe(
          () => {
            this.showMessage( messageTranslate.value, 'success');
            this.userTable.initTable();
          }
        );
      }
    });
  }

  openRelatedUserProfile(extendedUser: ExtendedUser): void {
    const dialogRef = this.dialog.open(ProfileModalComponent, {
      height: extendedUser.role === ROLES.PAYEE ? '885px' : '800px',
      width: '2000px',
      data: {extendedUser}
    });
    dialogRef.afterClosed().subscribe(() => {
      this.userTable.initTable();
    });
  }

  openAssociateMerchantModal(extendedUser: ExtendedUser): void {
    const extendedUserRelations = [];
    const dialogRef = this.dialog.open(AssociateMerchantModalComponent, {
      width: '80%',
      // height: '90%',
      height: '80%',
      disableClose: true,
      data: {extendedUser},
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result.value) {
        for (const merchant of result.merchantToAssociate) {
          const userRelation = {
            extendedUser: { id: merchant.id },
            userRelated: { id: extendedUser.user.id }
          };
          extendedUserRelations.push(userRelation);
        }

        let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.MERCHANTS_ASSOCIATED' );

        this.userService.associateMassiveMerchantToReSeller(extendedUserRelations).subscribe(() => {
          this.showMessage(`${messageTranslate.value} ${extendedUser.fullName}`, 'success');
          this.userTable.initTable();
        });
      }
    });
  }

  openRemoveAssociationConfirmationModal(extendedUserRelation): void {

    let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.REMOVE_ASSOCIATION' );
    let removeAssociationMerchant:any = this.translate.get( 'NOTIFICATION_TRADUCTIONS.REMOVE_ASSOC_COMERCIANT' );

    const dialogRef = this.dialog.open(StatusReasonModalComponent, {
      data: {
        entity: extendedUserRelation,
        title: messageTranslate.value,
        reasonType: removeAssociationMerchant.value,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.value) {
        const extendedUserRelations = [];
        const obj = {
          id: result.entity.id,
          status: STATUS.CANCELLED,
          statusReason: result.entity.statusReason,
        };
        extendedUserRelations.push(obj);

        let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SUCCESSFUL_ASSOCIATION' );

        this.userService.removeMerchantReSellerAssociation(extendedUserRelations).subscribe( () => {
          this.showMessage( messageTranslate.value, 'success');
          this.userTable.initTable();
        });
      }
    });
  }

  canSeeButton(type: string): boolean {
    let value = false;
    switch (type) {
      case 'PROCESS_PAYEE':
      case 'CHANGE_STATUS':
      case 'STATUS_USER':
        if (this.userSessionService.isSuperOrAdmin() && this.roleFilterSelected === ROLES.PAYEE) {
          value = true;
        }
        break;
      case 'INVITED_USER':
        if (this.role === ROLES.MERCHANT) {
          value = true;
        }
        break;
      case 'NEW_USER':
        switch (this.role) {
          case ROLES.SUPER_ADMIN:
          case ROLES.ADMIN:
          case ROLES.MERCHANT:
            value = true;
            break;

          default:
            value = false;
            break;
        }
        break;
      case 'PROCESS_MERCHANT':
        if (this.userSessionService.isSuperOrAdmin() && this.roleFilterSelected === ROLES.MERCHANT) {
          value = true;
        }
        break;

      case 'NEW_USER_RESELLER':
        if (this.userSessionService.isReSeller()) {
          value = true;
        }
        break;
    }
    return value;
  }
}
