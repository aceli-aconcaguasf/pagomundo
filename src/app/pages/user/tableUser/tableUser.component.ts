import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
  ViewRef
} from '@angular/core';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {MatPaginator, MatSort, TooltipPosition} from '@angular/material';
import {ExtendedUser} from '../../../config/interfaces';
import {DataFinderService, UserSessionService} from 'src/app/services/service.index';
import {ROLES, STATUS, STATUSES_COLOR, STATUSES_USER} from '../../../config/enums';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-table-user',
  templateUrl: './tableUser.component.html',
  styleUrls: ['./tableUser.component.scss']
})

export class TableUserComponent implements OnChanges {

  @Input() extendedUser: ExtendedUser;
  @Input() role: string;
  @Input() roleFilterSelected: string;
  @Input() filterQuery: string;
  @Input() menuCollapse: boolean;

  @Output() public openReasonModal = new EventEmitter<any>();
  @Output() public openFundModal = new EventEmitter<any>();
  @Output() public openChangeNicknameModal = new EventEmitter<any>();
  @Output() public openRelatedUserProfile = new EventEmitter<any>();
  @Output() public openRelatedUserModal = new EventEmitter<number>();
  @Output() public openRemoveAssociationConfirmationModal = new EventEmitter<any>();
  @Output() public openAssociateMerchantModal = new EventEmitter<any>();

  roles = ROLES;
  status = STATUS;
  statuses = STATUSES_USER;
  statusesColor = STATUSES_COLOR;
  positionOptions: TooltipPosition = 'above';

  uri = '';
  query = '';

  // Table Variable
  resultsLength = 0;
  isLoadingResults = true;
  isLoading = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  dataTable: any = [];
  pageSize = 10;
  keyLanguage$:Observable<any> = this.store.select('language');
  isComponentInit = false;
  errorMessage = 'Error getting users';
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private dataFinder: DataFinderService,
              private cdRef: ChangeDetectorRef,
              public userSessionService: UserSessionService,
              private store: Store<language>,
              private translate: TranslateService) {
                this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
                let errorMessageTranslate:any = this.translate.get( 'USERS.ERROR_GETTING_USERS' );

                this.errorMessage = errorMessageTranslate.value;
                this.keyLanguage$.subscribe(item => {
                  this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
                  const language:any = item
                  this.translate.use(language.key);
                  this.initTable();
                });
              }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.menuCollapse && this.isComponentInit) {
      return;
    } // Do not refresh table when expanding filter
    this.isComponentInit = true;
    this.initTable();
  }

  initTable() {
    this.setColumnNames(this.role);
    setTimeout(() => {
      this.loadDataTable();
    }, 600);
  }

  loadDataTable() {
    this.query = this.setQuery(this.role);

    this.query += this.filterQuery ? ` AND ${this.filterQuery}` : ''; 

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          this.isLoading = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isLoading = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
      this.dataTable = data;
    });

    if (!(this.cdRef as ViewRef).destroyed) {
      this.cdRef.detectChanges();
    }
  }

  setMatSort(): string {
    let sort;
    switch (this.role) {
      case ROLES.SUPER_ADMIN:
      case ROLES.ADMIN:
        sort = 'lastUpdated';
        break;
      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          sort = 'extendedUser.lastUpdated';
        } else {
          sort = 'id';
        }
        break;
      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        sort = 'extendedUser.lastUpdated';
        break;
    }
    return sort;
  }

  setQuery(role: string): string {
    let query = '';
    switch (role) {
      case ROLES.SUPER_ADMIN:
        this.uri = '/api/_search/extended-users';
        switch (this.roleFilterSelected) {
          case ROLES.ADMIN:
            query = `role:${ROLES.ADMIN}`;
            break;
          case ROLES.RESELLER:
            query = `role:${ROLES.RESELLER}`;
            break;
          case ROLES.MERCHANT:
            query = `role:${ROLES.MERCHANT}`;
            break;
          case ROLES.PAYEE:
            query = `role:${ROLES.PAYEE}`;
            break;
          default:
            break;
        }
        break;
      case ROLES.ADMIN:
        this.uri = '/api/_search/extended-users';
        switch (this.roleFilterSelected) {
          case ROLES.RESELLER:
            query = `role:${ROLES.RESELLER}`;
            break;
          case ROLES.MERCHANT:
            query = `role:${ROLES.MERCHANT}`;
            break;
          case ROLES.PAYEE:
            query = `role:${ROLES.PAYEE}`;
            break;
          default:
            break;
        }
        break;
      case ROLES.RESELLER:
        switch (this.roleFilterSelected) {
          case ROLES.MERCHANT:
            this.uri = '/api/_search/extended-user-relations';
            query = `userRelated.id:${this.extendedUser.user.id} AND status:${STATUS.ACCEPTED} AND extendedUser:*`;
            break;
          case ROLES.PAYEE:
            this.uri = '/api/_search/payees-related-to-reseller';
            query = `extendedUser.id:${this.extendedUser.id}`;
            break;
          default:
            break;
        }
        break;
      case ROLES.MERCHANT:
        this.uri = '/api/_search/extended-user-relations';
        query = `userRelated.id:${this.extendedUser.user.id} AND extendedUser.role:${ROLES.PAYEE}`;
        break;
      case ROLES.PAYEE:
        this.uri = '/api/_search/extended-user-relations';
        query = `userRelated.id:${this.extendedUser.user.id} AND extendedUser:*`;
        break;
      default:
        break;
    }
    return query;
  }

  setColumnNames(role) {

    let nameTranslate:any = this.translate.get( 'TEXT.NAME' );
    let companyTranslate:any = this.translate.get( 'TEXT.COMPANY' );
    let loginNameTranslate:any = this.translate.get( 'TEXT.LOGIN_NAME' );
    let emailTranslate:any = this.translate.get( 'TEXT.EMAIL' );
    let statusTranslate:any = this.translate.get( 'TEXT.STATUS' );
    let paymentMethodTranslate:any = this.translate.get( 'TEXT.PAYMENT_METHOD' );
    let paymentMethodNumberTranslate:any = this.translate.get( 'TEXT.PAYMENT_METHOD_NUMBER' );
    let countryTranslate:any = this.translate.get( 'TEXT.COUNTRY' );
    let mobileNumberTranslate:any = this.translate.get( 'TEXT.MOBILE_NUMBER' );
    let balanceTranslate:any = this.translate.get( 'TEXT.BALANCE' );
    let idNumberTranslate:any = this.translate.get( 'TEXT.ID_NUMBER' );

    switch (role) {
      case ROLES.SUPER_ADMIN:
        switch (this.roleFilterSelected) {
          case ROLES.ADMIN:
            this.columnNames = {
              fullName: nameTranslate.value, loginName: loginNameTranslate.value + '| ' + emailTranslate.value, status: statusTranslate.value, action: '',
              paymentMethod: paymentMethodTranslate.value, number: paymentMethodNumberTranslate.value
            };
            break;

          case ROLES.RESELLER:
            this.columnNames = {
              relatedUserModal: '', company: companyTranslate.value, fullName: nameTranslate.value, loginName: loginNameTranslate.value + '| ' + emailTranslate.value,
              country: countryTranslate.value, status: statusTranslate.value, mobileNumber: mobileNumberTranslate.value, balance: balanceTranslate.value, action: ''
            };
            break;

          case ROLES.MERCHANT:
            this.columnNames = {
              relatedUserModal: '', company: companyTranslate.value, fullName: nameTranslate.value, loginName: loginNameTranslate.value + '| ' + emailTranslate.value,
              country: countryTranslate.value, status: statusTranslate.value, mobileNumber: mobileNumberTranslate.value, balance: balanceTranslate.value, action: ''
            };
            break;

          case ROLES.PAYEE:
            this.columnNames = {
              relatedUserModal: '', 
              fullName: nameTranslate.value, 
              id: idNumberTranslate.value, 
              loginName: loginNameTranslate.value + '| ' + emailTranslate.value,
              country: countryTranslate.value, 
              status: statusTranslate.value,
              mobileNumber: mobileNumberTranslate.value,
              paymentMethod: paymentMethodTranslate.value,
              number: paymentMethodNumberTranslate.value, action: ''
            };
        }
        break;

      case ROLES.ADMIN:
        switch (this.roleFilterSelected) {
          case ROLES.RESELLER:
            this.columnNames = {
              relatedUserModal: '', company: companyTranslate.value, fullName: nameTranslate.value, loginName: loginNameTranslate.value + ' | ' + emailTranslate.value,
              country: countryTranslate.value, status: statusTranslate.value, mobileNumber: mobileNumberTranslate.value, balance: balanceTranslate.value, action: '',
              paymentMethod: paymentMethodTranslate.value, number: paymentMethodNumberTranslate.value
            };
            break;

          case ROLES.MERCHANT:
            this.columnNames = {
              relatedUserModal: '', company: companyTranslate.value, fullName: nameTranslate.value, loginName: loginNameTranslate.value + ' | ' + emailTranslate.value,
              country: countryTranslate.value, status: statusTranslate.value, mobileNumber: mobileNumberTranslate.value, balance: balanceTranslate.value, action: ''
            };
            break;

          case ROLES.PAYEE:
            this.columnNames = {
              relatedUserModal: '', fullName: nameTranslate.value, id: idNumberTranslate.value, loginName: loginNameTranslate.value + ' | ' + emailTranslate.value,
              country: countryTranslate.value, status: statusTranslate.value, mobileNumber: mobileNumberTranslate.value,
              paymentMethod: paymentMethodTranslate.value, number: paymentMethodNumberTranslate.value, action: ''
            };
        }
        break;

      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          this.columnNames = {
            relatedUserModal: '', company: companyTranslate.value, fullName: nameTranslate.value, loginName: loginNameTranslate.value + ' | ' + emailTranslate.value,
            country: countryTranslate.value, status: statusTranslate.value, mobileNumber: mobileNumberTranslate.value, action: ''
          };
        } else {
          this.columnNames = {
            relatedUserModal: '', fullName: nameTranslate.value, id: idNumberTranslate.value, loginName: loginNameTranslate.value + ' | ' + emailTranslate.value,
            country: countryTranslate.value, status: statusTranslate.value, mobileNumber: mobileNumberTranslate.value
          };
        }
        break;

      case ROLES.MERCHANT:
        this.columnNames = {
          fullName: nameTranslate.value, nickname: 'Nickname', id: idNumberTranslate.value, loginName: loginNameTranslate.value + ' | ' + emailTranslate.value,
          country: countryTranslate.value, status: statusTranslate.value, mobileNumber: mobileNumberTranslate.value, action: ''
        };
        break;

      case ROLES.PAYEE:
        this.columnNames = {
          company: companyTranslate.value, alias: 'Alias', status: statusTranslate.value
        };
        break;
      default:
        break;
    }
    this.columns = Object.keys(this.columnNames);
  }

  callOpenReasonModal(extendedUser: ExtendedUser, reasonType: string): void {
    const userObj = {
      extendedUser,
      reasonType,
    };
    this.openReasonModal.emit(userObj);
  }
  callRemoveAssociationConfirmationModal(extendedUserRelation): void {
    this.openRemoveAssociationConfirmationModal.emit(extendedUserRelation);
  }

  callAssociateMerchantModal(extendedUser: ExtendedUser): void {
    this.openAssociateMerchantModal.emit(extendedUser);
  }

  callOpenFundModal(extendedUser: ExtendedUser): void {
    this.openFundModal.emit(extendedUser);
  }

  callOpenChangeNicknameModal(extendedUserRelation): void {
    this.openChangeNicknameModal.emit(extendedUserRelation);
  }

  callOpenRelatedUserProfile(extendedUser: ExtendedUser): void {
    this.openRelatedUserProfile.emit(extendedUser);
  }

  callOpenRelatedUserModal(id: number): void {
    this.openRelatedUserModal.emit(id);
  }

  isSuperOrAdmin(): boolean {
    return this.userSessionService.isSuperOrAdmin();
  }

  canSeeButtonAction(row: any, type: string): boolean {
    let value = false;
    switch (type) {
      case 'FUND':
        if (this.userSessionService.isSuperOrAdmin() && row
          && this.roleFilterSelected === ROLES.MERCHANT && row.status === STATUS.ACCEPTED) {
          value = true;
        }
        break;
      case 'ACTIVATE':
          if ((row && row.status === STATUS.IN_PROCESS && this.userSessionService.isSuperOrAdmin()) ||
          (row && row.status === STATUS.CREATED && this.roleFilterSelected === ROLES.MERCHANT && this.userSessionService.isSuperOrAdmin())) {
          value = true;
        }
        break;
      case 'ENABLE':
        if (row && row.status === STATUS.REJECTED && this.userSessionService.isSuperOrAdmin()) {
          value = true;
        }
        break;
      case 'DISABLE':
        if (row && (row.status === STATUS.ACCEPTED || row.status === STATUS.IN_PROCESS) &&
          this.userSessionService.isSuperOrAdmin()) {
          value = true;
        }
        break;
      case 'DELETE':
        if (row && (row.status === STATUS.ACCEPTED || row.status === STATUS.REJECTED || row.status === STATUS.CREATED
          || row.status === STATUS.FAILED) && this.userSessionService.isSuperOrAdmin()) {
          value = true;
        }
        break;
      case 'ASSOCIATE_MERCHANT':
        if (this.userSessionService.isSuperAdmin() && this.roleFilterSelected === ROLES.RESELLER && row.status === STATUS.ACCEPTED) {
          value = true;
        }
        break;
      case 'REMOVE_ASSOCIATION':
        if (this.userSessionService.isReSeller()) {
          value = true;
        }
        break;
      case 'CHANGE_NICKNAME':
        if (this.userSessionService.isMerchant()) {
          value = true;
        }
        break;
    }
    return value;
  }

  setValueForNameColumnHandler(row): string {
    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        if (this.roleFilterSelected === ROLES.MERCHANT || this.roleFilterSelected === ROLES.RESELLER) {
          return row.fullName;
        } else {
          if (row.user && row.user.payeeAsCompany) {
            return row.company ? row.company : 'Profile not confirmed';
          } else {
            return row.fullName;
          }
        }

      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          return row.extendedUser && row.extendedUser.fullName ? row.extendedUser.fullName : '';
        } else {
          if (row.user && row.user.payeeAsCompany) {
            return row.company ? row.company : 'Profile not confirmed';
          } else {
            return row.fullName;
          }
        }

      case ROLES.MERCHANT:
        if (row.extendedUser.user.payeeAsCompany) {
          return row.extendedUser.company ? row.extendedUser.company : 'Profile not confirmed';
        } else {
          return row.extendedUser.fullName;
        }

      case ROLES.PAYEE:
        return row.extendedUser.fullName;
    }
  }

  setValueForIdNumberColumnHandler(row): string {
    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
      case ROLES.RESELLER:
        if (row.user && row.user.payeeAsCompany) {
          return row.taxId ? row.taxId : '';
        } else {
          return row.idNumber ? row.idNumber : '';
        }

      case ROLES.MERCHANT:
        if (row.extendedUser && row.extendedUser.user && row.extendedUser.user.payeeAsCompany) {
          return row.extendedUser.taxId;
        } else {
          return row.extendedUser.idNumber;
        }
    }
  }

  setValueForEmailColumnHandler(row): string {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        return row.email;

      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          return row.extendedUser && row.extendedUser.email ? row.extendedUser.email : '';
        } else {
          return row.email;
        }

      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        return row.extendedUser.email;

    }
  }

  setValueForAliasColumnHandler(row): string {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
      case ROLES.RESELLER:
      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        return row.extendedUser.alias;
    }
  }

  setValueForCountryColumnHandler(row): string {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        return row.postalCountry ? row.postalCountry.name : '';

      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          return row.extendedUser && row.extendedUser.postalCountry ? row.extendedUser.postalCountry.name : '';
        } else {
          return row.postalCountry ? row.postalCountry.name : '';
        }

      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        return row.extendedUser.postalCountry ? row.extendedUser.postalCountry.name : '';

    }
  }

  setValueForStatusColumnHandler(row): string {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        return row.status;

      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          return row.extendedUser && row.extendedUser.status ? row.extendedUser.status : '';
        } else {
          return row.status;
        }

      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        return row.extendedUser.status;

    }
  }

  setValueForStatusReasonColumnHandler(row): string {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        return row.statusReason;

      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          return row.extendedUser && row.extendedUser.statusReason ? row.extendedUser.statusReason : '';
        } else {
          return row.statusReason;
        }

      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        return row.extendedUser.statusReason;

    }
  }

  setValueForMobileNumberColumnHandler(row): string {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        return row.mobileNumber;

      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          return row.extendedUser && row.extendedUser.mobileNumber ? row.extendedUser.mobileNumber : '';
        } else {
          return row.mobileNumber;
        }

      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        return row.extendedUser.mobileNumber;

    }
  }

  setValueForOpenRelatedUserProfileHandler(row): any {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
        return row;

      case ROLES.RESELLER:
        if (this.roleFilterSelected === ROLES.MERCHANT) {
          return row.extendedUser;
        } else {
          return row;
        }

      case ROLES.MERCHANT:
        return row.extendedUser;

    }
  }

  setValueForToolTipHandler(row): string {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
      case ROLES.RESELLER:
        return row.user && row.user.payeeAsCompany ? 'Business Accounts' : 'Payee';

      case ROLES.MERCHANT:
        return row.extendedUser.user.payeeAsCompany ? 'Business Accounts' : 'Payee';

    }
  }

  setValueForIconTypeHandler(row): string {

    switch (this.role) {

      case ROLES.ADMIN:
      case ROLES.SUPER_ADMIN:
      case ROLES.RESELLER:
        return row.user && row.user.payeeAsCompany ? 'home' : 'user';

      case ROLES.MERCHANT:
        return row.extendedUser.user.payeeAsCompany ? 'home' : 'user';
    }
  }
}