import { AfterViewInit, ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { merge, of as observableOf, BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { DataFinderService, UserService, UserSessionService } from '../../../services/service.index';
import { MessageNotificationComponent } from '../../../components/components.index';
import { MatSort } from '@angular/material';
import { ExtendedUser } from '../../../config/interfaces';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-change-status-user',
  templateUrl: './changeUserStatus.component.html',
  styleUrls: ['./changeUserStatus.component.scss'],
})
export class ChangeUserStatusComponent implements AfterViewInit {
  uri = '/api/_search/extended-users';
  query = '';

  payeeCountries = [];
  countrySelected: any;
  filteredPayeeCountries: Observable<string[]> | any;
  countryForm: FormGroup;
  seeBank = false;
  banks = [];
  allBankAccounts = [];
  totalExtendedUsersSelected: ExtendedUser[] = [];
  collapse = false;

  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  data: any = [];

  checked = false;
  disabled = false;
  cardNumberError = false;
  cardNumberLettersError = false;

  statusReason = '';
  bankId = '';
  changeClass: number;
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  errorMessage = 'Error getting users';
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private dataFinder: DataFinderService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private userService: UserService,
    public userSessionService: UserSessionService,
    private translate: TranslateService) {
    this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
    this.countryForm = new FormGroup({
      countryPayee: new FormControl('', Validators.required),
    });
    this.initPayeeCountryFilter();
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss");
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;

  }

  get CountryPayee() { return this.countryForm.get('countryPayee'); }

  ngAfterViewInit() {

    let nameTranslate: any = this.translate.get('TEXT.NAME');
    let countryTranslate: any = this.translate.get('TEXT.COUNTRY');
    let loginNameTranslate: any = this.translate.get('TEXT.LOGIN_NAME');
    let emailTranslate: any = this.translate.get('TEXT.EMAIL');
    let mobileNumberTranslate: any = this.translate.get('TEXT.MOBILE_NUMBER');
    let idNumberTranslate: any = this.translate.get('TEXT.ID_NUMBER');
    let cardNumberTranslate: any = this.translate.get('PROFILE.CARD_NUMBER');
    let typeTranslate: any = this.translate.get('TEXT.TYPE');

    this.columnNames = {
      check: '', id: idNumberTranslate.value, idType: typeTranslate.value, loginName: loginNameTranslate.value + ' | ' + emailTranslate.value, fullName: nameTranslate.value,
      country: countryTranslate.value, mobileNumber: mobileNumberTranslate.value, cardNumber: cardNumberTranslate.value
    };
    this.columns = Object.keys(this.columnNames);
  }

  initPayeeCountryFilter() {
    this.filteredPayeeCountries = this.CountryPayee.valueChanges
      .pipe(
        startWith(''),
        map(value => this._payeeCountryFilter(value))
      );
  }

  private _payeeCountryFilter(value: string): string[] {
    this.seeBank = false;
    this.bankId = '';
    if (value) {
      if (value.length >= 2) {
        this.loadPayeeCountry(value);
      } else {
        this.payeeCountries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.payeeCountries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadPayeeCountry(name: string) {
    this.dataFinder.getCountriesForPayeeAndName(name).subscribe(
      data => {
        this.payeeCountries = data.content;
      });
  }

  displayFnPayeeCountry(country): string {
    return country ? country.name : country;
  }

  loadTable() {
    this.query = this.getQueryString();
    (document.getElementById('allCheck') as HTMLInputElement).checked = true;

    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, 100);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          this.totalExtendedUsersSelected = [...data.content];
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
        this.data = data;
      });
  }

  getQueryString(): string {
    let query = 'role:ROLE_PAYEE AND status:IN_PROCESS';
    query += this.bankId !== ''
      ? ' AND bank.id:' + this.bankId
      : '';
    return query;
  }

  seeRelatedUser(extendedUserId: number) {
    this.router.navigate(['/profile', extendedUserId]).then();
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  allCheckElement() {
    // allCheck
    if ((document.getElementById('allCheck') as HTMLInputElement).checked) {
      this.totalExtendedUsersSelected = [...this.data];
      for (const extendedUser of this.totalExtendedUsersSelected) {
        (document.getElementById(`${extendedUser.id}`) as HTMLInputElement).checked = true;
      }
    } else {
      for (const extendedUser of this.totalExtendedUsersSelected) {
        (document.getElementById(`${extendedUser.id}`) as HTMLInputElement).checked = false;
      }
      this.totalExtendedUsersSelected = [];
    }
  }

  addCheckElement(extendedUser: ExtendedUser) {
    if ((document.getElementById(`${extendedUser.id}`) as HTMLInputElement).checked) {
      this.totalExtendedUsersSelected.push(extendedUser);
    } else {
      const index = this.totalExtendedUsersSelected.indexOf(extendedUser);
      if (index !== -1) {
        this.totalExtendedUsersSelected.splice(index, 1);
      }
    }
    (document.getElementById('allCheck') as HTMLInputElement).checked = this.data.length === this.totalExtendedUsersSelected.length;
  }

  onChangeCountry() {
    this.banks = [];
    this.allBankAccounts = [];
    if (document.getElementById('banks')) {
      (document.getElementById('banks') as HTMLSelectElement).selectedIndex = 0;
    }
    if (this.CountryPayee.value.id) {
      this.countrySelected = this.CountryPayee.value;
      this.dataFinder.getAllBankAccounts(this.countrySelected.id).subscribe(
        accounts => {
          this.seeBank = true;
          this.allBankAccounts = accounts.content;
          const arrayBankId = [];
          for (const account of this.allBankAccounts) {
            const index = account.bank.id;
            if (arrayBankId.indexOf(index) === -1) {
              arrayBankId.push(index);
              this.dataFinder.getBanksById(index).subscribe(
                bank => {
                  if (bank.content[0]) {
                    this.banks.push(bank.content[0]);
                  }
                });
            }
          }
        });
    } else {
      this.countrySelected = null;
      this.seeBank = false;
    }
  }

  selectBank(event) {
    this.bankId = '';
    this.bankId = event.target.value;

    setTimeout(() => {
      this.loadTable();
    }, 0);
  }

  massiveChangeStatus(reasonType: string): boolean {
    let messageTranslate: any;
    this.cardNumberError = false;
    this.cardNumberLettersError = false;
    if (this.totalExtendedUsersSelected.length > 0) {
      let status = '';
      let message = '';
      const extendedUsers = [];
      switch (reasonType) {
        case 'Activate':
        case 'Enable':
          status = 'ACCEPTED';
          if (this.totalExtendedUsersSelected.length === 1) {
            messageTranslate = this.translate.get('TEXT.USERS_ACCEPTED_ONE');
          } else {
            messageTranslate = this.translate.get('TEXT.USERS_ACCEPTED');
          }
          message = messageTranslate.value;
          break;
        case 'Disable':
          status = 'REJECTED';
          if (this.totalExtendedUsersSelected.length === 1) {
            messageTranslate = this.translate.get('TEXT.USERS_DISABLED_ONE');
          } else {
            messageTranslate = this.translate.get('TEXT.USERS_DISABLED');
          }
          message = messageTranslate.value;
          break;
        case 'Delete':
          status = 'CANCELLED';
          if (this.totalExtendedUsersSelected.length === 1) {
            messageTranslate = this.translate.get('TEXT.USERS_DELETED_ONE');
          } else {
            messageTranslate = this.translate.get('TEXT.USERS_DELETED');
          }
          message = messageTranslate.value;
          break;
      }
      for (const extendedUser of this.totalExtendedUsersSelected) {
        let cardNumber = null;
        if (reasonType === 'Activate') {
          if (!extendedUser.cardNumber) { extendedUser.cardNumber = ''; }
          cardNumber = extendedUser.cardNumber.replace(/-/g, '');
          if (cardNumber.length !== 16) {
            this.cardNumberError = true;
            messageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.INVALID_CARD_NUMBERS');
            this.showMessage(messageTranslate.value, 'error');
            return false;
          } else if (!cardNumber.match('^[0-9]+$')) {
            this.cardNumberLettersError = true;
            messageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.INVALID_CARD_LETTERS');
            this.showMessage(messageTranslate.value, 'error');
            return false;
          }
        }
        const bodyExtendedUser = {
          id: extendedUser.id,
          status,
          cardNumber: extendedUser.cardNumber ? extendedUser.cardNumber.replace(/-/g, '') : null,
          statusReason: this.statusReason.length !== 0 ? this.statusReason : null,
        };
        extendedUsers.push(bodyExtendedUser);
      }
      this.userService.changeMassiveUserStatus(extendedUsers).subscribe(
        data => {
          if (data.extendedUsersWithErrors.length === 0) {
            this.showMessage(message, 'success');
          } else {
            messageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SOME_USERS_NOT_CHANGE');
            this.showMessage(messageTranslate.value, 'success');
          }
          this.loadTable();
        }
      );
    } else {
      messageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SELECT_ONE_PAYMENT');
      this.showMessage(messageTranslate.value, 'error');
    }
  }

  cleanErrorVariable() {
    this.cardNumberError = false;
    this.cardNumberLettersError = false;
  }

  setErrorStyle(row): boolean {
    if ((document.getElementById(`${row.id}`) as HTMLInputElement).checked) {

      if (row.cardNumber) {
        const cardNumber = row.cardNumber.replace(/-/g, '');
        if (cardNumber.length !== 16) {
          return true;
        } else if (!cardNumber.match('^[0-9]+$')) {
          return true;
        }
      }
      if ((this.cardNumberError || this.cardNumberLettersError) && !row.cardNumber) { return true; }
    }
  }
}