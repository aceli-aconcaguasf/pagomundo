import { Component, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import {MatDialog, MatSort, TooltipPosition} from '@angular/material';
import {DataFinderService, UserService, UserSessionService} from '../../../services/service.index';
import {merge, of as observableOf, BehaviorSubject} from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import {ExtendedUser} from '../../../config/interfaces';
import {ConfirmationModalComponent, EditExtUserModalComponent,
        MessageNotificationComponent} from '../../../components/components.index';
import {ROLES} from '../../../config/enums';
import {TranslateService} from '@ngx-translate/core';

export enum INVITED_REASON {
  INVITED = 'Invited',
  PENDING_PROFILE= 'Pending profile'
}

@Component({
  selector: 'app-invited-user',
  templateUrl: './invitedUser.component.html',
  styleUrls: ['./invitedUser.component.scss']
})
export class InvitedUserComponent implements AfterViewInit {

  query = '';

  keys = Object.keys;
  uri = '/api/_search/invited-users';
  invitedStatus = INVITED_REASON;

  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  tableData: any = [];
  extendedUser: ExtendedUser;
  positionOptions: TooltipPosition =  'above';

  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  errorMessage = 'Error getting users.';
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private userService: UserService,
              private cdRef: ChangeDetectorRef,
              public dialog: MatDialog,
              private dataFinder: DataFinderService,
              private userSessionService: UserSessionService,
              private translate: TranslateService
  ) {
    this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    this.extendedUser = this.userSessionService.loadExtendedUser();

  }

  ngAfterViewInit() {
    this.loadTable();
  }

  loadTable() {
    this.initColumnDef();
    this.setQuery();

    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active, this.sort.direction);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => {
      this.tableData = data;
    });

    this.cdRef.detectChanges();
  }

  initColumnDef() {

    let nameTranslate:any = this.translate.get( 'TEXT.NAME' );
    let loginNameTranslate:any = this.translate.get( 'TEXT.LOGIN_NAME' );
    let emailTranslate:any = this.translate.get( 'TEXT.EMAIL' );
    let statusTranslate:any = this.translate.get( 'TEXT.STATUS' );
    let statusReasonTranslate:any = this.translate.get( 'DATA_TABLE.STATUS_REASON' );
    
    this.columnNames = {
      fullName: nameTranslate.value, email: loginNameTranslate.value + ' | ' + emailTranslate.value, status: statusTranslate.value, statusReason: statusReasonTranslate.value, action: ''
    };
    this.columns = Object.keys(this.columnNames);
  }

  setQuery() {
    this.query = `role:${ROLES.PAYEE}`;
  }

  reSendMail(extendedUser) {
    let resendEmail:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.EMAIL_REDEND' );
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      disableClose: true,
      data: {title: `${resendEmail.value} ${extendedUser.firstName} ${extendedUser.lastName}`},
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userService.reSendEmail(extendedUser.email).subscribe(() => {
          this.showMessage(`${resendEmail.value}  ${extendedUser.email}`, 'success');
        });
      }
    });
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  editEmail(userRelated) {
    const dialogRef = this.dialog.open(EditExtUserModalComponent, {
      minWidth: '500px',
      disableClose: true,
      data: { extendedUser: userRelated },
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response.withValue) {

        const body = {
          currentEmail: userRelated.email,
          newEmail: response.newEmail,
        };

        this.userService.changeInvitedUserEmail(body).subscribe(() => {
          this.userService.reSendEmail(body.newEmail).subscribe(() => {
            this.showMessage(`Email modified and resent to  ${body.newEmail}`, 'success');
            setTimeout(() => this.loadTable(), 100);
          });
        });
      }
    });
  }
}