import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../../../services/user/user.service';
import { UserSessionService } from '../../../../services/user/userSession.service';
import { DataFinderService } from '../../../../services/data-finder.service';
import { AUTOCOMPLETE_TYPE, BANK_ACCOUNT_TYPES, DROWBOX_TYPE, ROLES } from '../../../../config/enums';
import { TooltipPosition } from '@angular/material';
import { noWhitespaceValidator } from '../../../../shared/functions';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-invite-payee-form',
  templateUrl: './invitePayeeForm.component.html',
  styleUrls: ['./invitePayeeForm.component.scss'],
})
export class InvitePayeeFormComponent implements OnInit {

  @Input() countryPostalId: number;
  @Input() errorValidation: boolean;
  @Input() createUserError: boolean;
  @Input() rolToCreate: string;

  @Output() subtractCount = new EventEmitter<number>();
  @Output() setShowMessage = new EventEmitter<any>();
  drownBoxType = DROWBOX_TYPE;
  payeesForm: FormGroup;
  keys = Object.keys;
  accountType = BANK_ACCOUNT_TYPES;
  selectedBankAccountType = 'AHO';
  positionOptions: TooltipPosition = 'above';
  typePayee: string;
  maxcuenta : number = 20; // MaxLength de cuenta en Perú
  maxDni: number = 8;
  minDni: number = 8;

  roles = ROLES;
  banks = [];
  cities = [];
  elements = [];
  citiesControl = new FormControl();
  filteredCities: Observable<string[]>;
  pageGetBank = 0;
  pageGetCity = 0;
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;
  public dataComplete = null;
  dataCities = [];
  public inputCityFormGroup: FormGroup;
  public inputCIty = [];
  newCityName = null;
  newCityInput = false;
  isInterbank: boolean;
  constructor(private formBuilder: FormBuilder,
    public userService: UserService,
    public userSessionService: UserSessionService,
    private dataFinder: DataFinderService,
    private cdRef: ChangeDetectorRef,
    public requestService: RequestService,
    private translate: TranslateService,
    private _formBuilder: FormBuilder) {
    this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
    this.inputCityFormGroup = this._formBuilder.group({
      inputCIty: this._formBuilder.array([])
    });
    // const user = JSON.parse(localStorage.getItem( "Pm1-4m3r1c4s-us3r" ));
  }

  ngOnInit() {
    this.initPayeesForm();
    this.addPayeeToArray();
    this.dropBoxGetCity();
  }

  get payeeArray(): FormArray {
    return this.payeesForm.get('payeeArray') as FormArray;
  }

  getPayeeAsPersonControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('payeeAsPerson') as FormControl;
  }

  getPayeeAsCompanyControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('payeeAsCompany') as FormControl;
  }

  getNameControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('name') as FormControl;
  }

  getLastNameControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('lastName') as FormControl;
  }

  getEmailControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('email') as FormControl;
  }

  getNicknameControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('nickname') as FormControl;
  }

  getWithBankInfoControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('withBankInfo') as FormControl;
  }

  getBankControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('bank') as FormControl;
  }

  getNumberControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('number') as FormControl;
  }

  getBankCityControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('bankCity') as FormControl;
  }

  getBankAccountTypeControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('bankAccountType') as FormControl;
  }

  getBankAccountTypeControlInput(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('cityNamePayee') as FormControl;
  }

  getCitiesArray(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('cityArray') as FormControl;
  }

  getBankAccountNumberControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('bankAccountNumber') as FormControl;
  }

  getClabeBankAccountNumberControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('clabeBankAccountNumber') as FormControl;
  }

  getCbuBankAccountNumberControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('cbuBankAccountNumber') as FormControl;
  }

  getBankBranchControl(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('bankBranch') as FormControl;
  }

  getNewCityBank(index): FormControl {
    const control = this.payeeArray.at(index);
    return control.get('newCityBank') as FormControl;
  }
  reset() {
    this.payeeArray.reset();
    this.myControl.setValue("");
    this.ngOnInit();
  }

  initPayeesForm() {
    this.payeesForm = this.formBuilder.group({
      payeeArray: this.formBuilder.array([])
    });
  }

  addPayeeToArray() {
    let payee: FormGroup;
    if (this.rolToCreate === ROLES.ADMIN) {
      payee = this.formBuilder.group({
        name: new FormControl('', [Validators.required, noWhitespaceValidator]),
        lastName: new FormControl('', [Validators.required, noWhitespaceValidator]),
        email: new FormControl('', [Validators.required, Validators.email]),
      });
    }
    else {
      payee = this.formBuilder.group({
        name: new FormControl({
          value: '',
          disabled: true
        }, [Validators.required, noWhitespaceValidator]),
        lastName: new FormControl({
          value: '',
          disabled: true
        }, [Validators.required, noWhitespaceValidator]),
        email: new FormControl({
          value: '',
          disabled: true
        }, [Validators.required, Validators.email]),
        payeeAsCompany: new FormControl(false),
        payeeAsPerson: new FormControl(false),
        withBankInfo: new FormControl({
          value: false,
          disabled: true
        }),
        number: new FormControl({
          value: '',
          disabled: true
        }),
        bank: new FormControl({
          value: null,
          disabled: true
        }),
        bankCity: new FormControl({
          value: null,
          disabled: true
        }),
        bankAccountType: new FormControl({
          value: this.selectedBankAccountType,
          disabled: true
        }),
        bankAccountNumber: new FormControl({
          value: '',
          disabled: true
        },Validators.pattern("^[0-9,$]*$")),
        bankBranch: new FormControl({
          value: null,
          disabled: true
        }),
        clabeBankAccountNumber: new FormControl(''),
        cbuBankAccountNumber: new FormControl(''),
        cityNamePayee: new FormControl(''),
        cityArray: new FormControl(null),
        newCityBank: new FormControl('')
      });

      if (this.userSessionService.isMerchant()) {
        payee.addControl('nickname',
          new FormControl({
            value: '',
            disabled: true
          }));
      }
    }
    this.payeeArray.push(payee);
  }

  removePayee(index: number) {
    this.payeeArray.removeAt(index);
    this.subtractCount.emit(1);
  }

  _getPageBank() {
    const uri = '/api/_search/direct-payment-banks?query=originBank.country.id:' + this.countryPostalId;
    const _this = this;
    this.dataFinder._getBank(uri, _this.pageGetBank).subscribe(
      (data: any) => {

        for (const element of data.content) {

          if (element.destinyBank) {
            if (this.banks.length !== 0) {
              let isRepeat : boolean = false;
               this.banks.find(bank => {
                if (bank.id === element.destinyBank.id) {
                  isRepeat = true;
                }
              })
              if (!isRepeat){
                this.banks.push(element.destinyBank);
              }
            } else {
              this.banks.push(element.destinyBank);
            }

          }
      }

        this.banks.sort((a, b) => {
          if (a.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
          }
          if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
          }
          return 0;
        });

        let pageCurrents = Math.ceil(data.totalElements / 100);
        if ((_this.pageGetBank + 1) <= pageCurrents) {

          _this.pageGetBank = _this.pageGetBank + 1;
          _this._getPageBank();
        }
      }
    );
  }




  private _filter(value: string): string[] {
    if (value) {
      if (value.length >= 2) {
        this.elements = [];
        for (const element of this.cities) {

          this.elements.push(element);
        }
      } else {
        this.elements = [];
      }
      const filterValue = value.toString().toLowerCase();
      this.cdRef.detectChanges();
      return this.cities.filter(elem => elem.name.toString().toLowerCase().includes(filterValue));
    }
  }


  loadBanksAndCitiesForBank() {

    const uri = '/api/_search/direct-payment-banks?query=originBank.country.id:' + this.countryPostalId;
    const _this = this;
    this.banks = [];

    this.dataFinder._getBank(uri, _this.pageGetBank).subscribe(
      (data: any) => {

        for (const element of data.content) {

          if (element.destinyBank) {
            if (this.banks.length !== 0) {
              let isRepeat : boolean = false;
               this.banks.find(bank => {
                if (bank.id === element.destinyBank.id) {
                  isRepeat = true;
                }
              })
              if (!isRepeat){
                this.banks.push(element.destinyBank);
              }
            } else {
              this.banks.push(element.destinyBank);
            }

          }
      }

        this.banks.sort((a, b) => {
          if (a.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
          }
          if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
          }
          return 0;
        });

        let pageCurrents = Math.ceil(data.totalElements / 100);
        if ((_this.pageGetBank + 1) <= pageCurrents) {

          _this.pageGetBank = _this.pageGetBank + 1;
          _this._getPageBank();
        }
      }
    );
  }

  validateUniqueEmailError(index: number): boolean {
    const emailControl = this.getEmailControl(index);
    if (emailControl && emailControl.value) {
      for (let i = 0; i < this.payeeArray.length; i++) {
        if (i !== index) {
          const email = this.getEmailControl(i);
          if (emailControl.value === email.value) {
            return true;
          }
        }
      }
    }
    return false;
  }

  validateUniqueBankAccountNumberError(index: number): boolean {
    const bankAcountControl = this.getBankAccountNumberControl(index);
    if (bankAcountControl && bankAcountControl.value) {
      for (let i = 0; i < this.payeeArray.length; i++) {
        if (i !== index) {
          const email = this.getBankAccountNumberControl(i);
          if (bankAcountControl.value === email.value) {
            return true;
          }
        }
      }
    }
    return false;
  }

  

  resetValidatorsOnCountryChange() {
    for (let i = 0; i < this.payeeArray.length; i++) {
      this.getWithBankInfoControl(i).reset(false);
      this.cleanBankFormValidator(i);
      this.updateBankFormValidator(i);
    }
  }

  handleBankValidators(index) {
    this.pageGetBank = 0;

    if (this.countryPostalId != 74) {
      if (this.getWithBankInfoControl(index).value) {
        this.enabledBankColombianInfo(index);
        this.setBankFormValidator(index);
      }
      else {
        this.disabledBankColombianInfo(index);
        this.cleanBankFormValidator(index);
      }
    }
    else {

      if (this.getWithBankInfoControl(index).value) {
        this.disabledBankColombianInfo(index);
        this.enabledBankBrazilInfo(index);
        this.setBankFormValidator(index);
      }
      else {
        this.disabledBankColombianInfo(index);
        this.cleanBankFormValidator(index);
      }
    }
  }

  enabledBankColombianInfo(index) {
    this.getNumberControl(index).enable();
    this.getBankControl(index).enable();
    this.getBankCityControl(index).enable();
    this.getBankAccountTypeControl(index).enable();
    this.getBankAccountTypeControlInput(index).enable();
    this.getBankAccountNumberControl(index).enable();
    this.getBankBranchControl(index).disable();
  }

  enabledBankBrazilInfo(index) {
    this.getNumberControl(index).enable();
    this.getBankControl(index).enable();
    this.getBankCityControl(index).disable();
    this.getBankAccountTypeControl(index).enable();
    this.getBankAccountTypeControlInput(index).enable();
    this.getBankAccountNumberControl(index).enable();
    this.getBankBranchControl(index).enable();
  }

  disabledBankColombianInfo(index) {
    this.getNumberControl(index).disable();
    this.getBankControl(index).disable();
    this.getBankCityControl(index).disable();
    this.getBankAccountTypeControl(index).disable();
    this.getBankAccountTypeControlInput(index).disable();
    this.getBankAccountNumberControl(index).disable();
    this.getBankBranchControl(index).disable();
  }

  setBankFormValidator(index) {
    this.loadBanksAndCitiesForBank();
    switch (this.countryPostalId) {
      case 1: // ID 1 = COLOMBIA
        this.getNumberControl(index).setValidators([Validators.required, noWhitespaceValidator]);
        this.getBankControl(index).setValidators(Validators.required);
        this.getBankCityControl(index).setValidators(Validators.required);
        this.getBankAccountTypeControl(index).setValidators(Validators.required);
        // this.getBankAccountTypeControlInput(index).setValidators(Validators.required);
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator]);
        break;
      case 2: // ID 2 = MEXICO
        this.getClabeBankAccountNumberControl(index).setValidators(
          [Validators.required, Validators.minLength(18), Validators.maxLength(18), Validators.pattern('^[0-9]+$')]);
        break;
      case 66: // ID 66 = ARGENTINA
        if (this.getPayeeAsCompanyControl(index).value) {
          this.getNumberControl(index).setValidators([Validators.required,
          Validators.minLength(11), Validators.maxLength(11), Validators.pattern('^[0-9]+$')
          ]);
        } else {
          this.getNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.pattern('^[0-9]+$')]);
        }
        this.getCbuBankAccountNumberControl(index).setValidators(
          [Validators.required, Validators.minLength(22), Validators.maxLength(22), Validators.pattern('^[0-9]+$')]);
        break;
      case 74: // ID 74 = BRAZIL
        if (this.getPayeeAsCompanyControl(index).value) {
          this.getNumberControl(index).setValidators([
            Validators.required,
            Validators.minLength(11),
            Validators.maxLength(14),
            Validators.pattern('^[0-9]+$'),
            noWhitespaceValidator
          ]);
          this.getBankAccountNumberControl(index).setValidators([
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(13),
            Validators.pattern('^[0-9]+$'),
            noWhitespaceValidator
          ]);
          this.getBankAccountNumberControl(index).updateValueAndValidity();
          this.getNumberControl(index).updateValueAndValidity();
        }
        else {
          this.getNumberControl(index).setValidators([
            Validators.required,
            Validators.minLength(11),
            Validators.maxLength(11),
            Validators.pattern('^[0-9]+$'),
            noWhitespaceValidator
          ]);

          this.getBankAccountNumberControl(index).setValidators([
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(13),
            Validators.pattern('^[0-9]+$'),
            noWhitespaceValidator]);
          this.getBankAccountNumberControl(index).updateValueAndValidity();
          this.getNumberControl(index).updateValueAndValidity();
        }

        this.getBankControl(index).setValidators(Validators.required);
        this.getBankAccountTypeControl(index).setValidators(Validators.required);
        this.getBankBranchControl(index).setValidators([
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(4),
          Validators.pattern('^[0-9]+$'),
          noWhitespaceValidator
        ]);
        break;
      case 77: // ID 77 = CHILE
        this.getBankControl(index).setValidators(Validators.required);
        this.getBankAccountTypeControl(index).setValidators(Validators.required);
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator]);
        break;
      case 101: // ID 101 = PERÚ
        this.getNumberControl(index).setValidators(Validators.required);
        this.getBankControl(index).setValidators(Validators.required);
        this.getBankAccountTypeControl(index).setValidators(Validators.required);
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator]);
        break;
      case 115: // ID 115 = VENEZUELA
      this.getNumberControl(index).setValidators([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(9),
        Validators.pattern('^[EeVv]{1}[0-9]{7,8}'),
        noWhitespaceValidator
      ]);
        this.getBankControl(index).setValidators(Validators.required);
        this.getBankAccountTypeControl(index).setValidators(Validators.required);
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(20), Validators.maxLength(20)]);
        break;
    }

    this.updateBankFormValidator(index);
  }

  cleanBankFormValidator(index) {
    this.getNumberControl(index).clearValidators();
    this.getBankControl(index).clearValidators();
    this.getBankCityControl(index).clearValidators();
    this.getBankAccountTypeControl(index).clearValidators();
    this.getBankAccountTypeControlInput(index).clearValidators();
    this.getBankAccountNumberControl(index).clearValidators();
    this.getClabeBankAccountNumberControl(index).clearValidators();
    this.getCbuBankAccountNumberControl(index).clearValidators();
    this.getBankBranchControl(index).clearValidators();

    this.updateBankFormValidator(index);
  }

  updateBankFormValidator(index) {
    this.getNumberControl(index).updateValueAndValidity();
    this.getBankControl(index).updateValueAndValidity();
    this.getBankCityControl(index).updateValueAndValidity();
    this.getBankAccountTypeControl(index).updateValueAndValidity();
    this.getBankAccountTypeControlInput(index).updateValueAndValidity();
    this.getBankAccountNumberControl(index).updateValueAndValidity();
    this.getClabeBankAccountNumberControl(index).updateValueAndValidity();
    this.getCbuBankAccountNumberControl(index).updateValueAndValidity();
    this.getBankBranchControl(index).updateValueAndValidity();
  }

  handleCantCreatePayeeError(dataError) {
    const array = dataError.userNotCreated;
    this.payeeArray.clear();
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < array.length; i++) {
      const payee = this.formBuilder.group({
        name: new FormControl(array[i].firstName, Validators.required),
        lastName: new FormControl(array[i].lastName, Validators.required),
        email: new FormControl(array[i].email, [Validators.required, Validators.email]),
        payeeAsCompany: new FormControl(array[i].payeeAsCompany),
        payeeAsPerson: new FormControl(!array[i].payeeAsCompany),
        withBankInfo: new FormControl(false),
      });

      if (dataError.country.id && array[i].bankAccountNumber) {
        payee.get('withBankInfo').setValue(true);
        switch (dataError.country.id) {
          case 1: // ID 1 = COLOMBIA
            payee.addControl('idNumber', new FormControl(array[i].idNumber, [Validators.required, noWhitespaceValidator]));
            payee.addControl('bank', new FormControl(array[i].directPaymentBank.id, Validators.required));
            payee.addControl('bankCity', new FormControl(array[i].directPaymentCity.id, Validators.required));
            payee.addControl('bankAccountType', new FormControl(array[i].bankAccountType, Validators.required));
            payee.addControl('cityNamePayee', new FormControl(array[i].bankAccountType, Validators.required));
            payee.addControl('bankAccountNumber', new FormControl(array[i].bankAccountNumber, Validators.required));
            payee.addControl('clabeBankAccountNumber', new FormControl(''));
            payee.addControl('idType', new FormControl(null));
            payee.addControl('cbuBankAccountNumber', new FormControl(''));
            payee.addControl('newCityBank', new FormControl(''));
            
            break;
          case 2: // ID 2 = MEXICO
            payee.addControl('idNumber', new FormControl(''));
            payee.addControl('bank', new FormControl(null));
            payee.addControl('bankCity', new FormControl(null));
            payee.addControl('bankAccountType', new FormControl(this.selectedBankAccountType));
            payee.addControl('bankAccountNumber', new FormControl(''));
            payee.addControl('clabeBankAccountNumber', new FormControl(array[i].bankAccountNumber, [Validators.required, Validators.minLength(18), Validators.maxLength(18), Validators.pattern('^[0-9]+$')]));
            payee.addControl('idType', new FormControl(null));
            payee.addControl('cbuBankAccountNumber', new FormControl(''));
            break;
          case 66: // ID 66 = ARGENTINA
            payee.addControl('idNumber', new FormControl(array[i].idNumber, [Validators.required, noWhitespaceValidator]));
            payee.addControl('bank', new FormControl(null));
            payee.addControl('bankCity', new FormControl(null));
            payee.addControl('bankAccountType', new FormControl(this.selectedBankAccountType));
            payee.addControl('bankAccountNumber', new FormControl(''));
            payee.addControl('clabeBankAccountNumber', new FormControl(''));
            payee.addControl('idType', new FormControl(array[i].directPaymentBank.id, Validators.required));
            payee.addControl('cbuBankAccountNumber', new FormControl(array[i].bankAccountNumber, [Validators.required, Validators.minLength(22), Validators.maxLength(22), Validators.pattern('^[0-9]+$')]));
            break;
          case 74: // ID 74 = BRASIL
            payee.addControl('idNumber', new FormControl(array[i].idNumber, [
              Validators.required,
              Validators.minLength(11),
              Validators.maxLength(14),
              Validators.pattern('^[0-9]+$'),
              noWhitespaceValidator
            ]));
            payee.addControl('bank', new FormControl(array[i].directPaymentBank.id, Validators.required));
            payee.addControl('bankAccountType', new FormControl(array[i].bankAccountType, Validators.required));
            payee.addControl('bankAccountNumber', new FormControl(array[i].bankAccountNumber, [
              Validators.required,
              Validators.minLength(6),
              Validators.maxLength(13),
              Validators.pattern('^[0-9]+$'),
              noWhitespaceValidator
            ]));
            payee.addControl('clabeBankAccountNumber', new FormControl(''));
            payee.addControl('idType', new FormControl(null));
            payee.addControl('cbuBankAccountNumber', new FormControl(''));
            payee.addControl(
              'bankBranch', new FormControl('', [
                Validators.required,
                Validators.minLength(4),
                Validators.maxLength(4),
                noWhitespaceValidator
              ])
            );
            break;
          case 77: // ID 77 = CHILE
            payee.addControl('bank', new FormControl(array[i].directPaymentBank.id, Validators.required));
            payee.addControl('bankAccountType', new FormControl(array[i].bankAccountType, Validators.required));
            payee.addControl('bankAccountNumber', new FormControl(array[i].bankAccountNumber, Validators.required));
            break;
          case 101: // ID 101 = PERÚ
            payee.addControl('bank', new FormControl(array[i].directPaymentBank.id, Validators.required));
            payee.addControl('bankAccountType', new FormControl(array[i].bankAccountType, Validators.required));
            payee.addControl('bankAccountNumber', new FormControl(array[i].bankAccountNumber, Validators.required));
            break;
          case 115: // ID 115 = VENEZUELA
            payee.addControl('bank', new FormControl(array[i].directPaymentBank.id, Validators.required));
            payee.addControl('bankAccountType', new FormControl(array[i].bankAccountType, Validators.required));
            payee.addControl('bankAccountNumber', new FormControl(array[i].bankAccountNumber, Validators.required));
            break;
        }
      }

      this.payeeArray.push(payee);
      let messageTranslate: any = this.translate.get('TEXT.ERROR_CREATING');

      setTimeout(() => {
        document.getElementById(`error${i}`).innerHTML = array[i].statusReason ? array[i].statusReason : messageTranslate.value + ' payee';
      }, 100);
    }
  }

  checkIfDisabled(index) {
    if (this.rolToCreate === ROLES.PAYEE) {
      if (!this.getPayeeAsPersonControl(index).value && !this.getPayeeAsCompanyControl(index).value) {

        let messageTranslate: any = this.translate.get('TEXT.PLEASE_SELECT_PAYEE_TYPE_TO_CREATE_IT');

        this.setShowMessage.emit(messageTranslate.value);
      }
    }
  }

  // type = 'COMPANY' or 'PERSON'
  enablePayeeFormHandler(index, type) {
    this.getNumberControl(index).reset();
    if (type === 'PERSON') {
      this.typePayee = 'PERSON';
      if (this.getPayeeAsPersonControl(index).value) {
        this.getPayeeAsCompanyControl(index).setValue(false);
      } else {
        this.getPayeeAsCompanyControl(index).setValue(true);
      }
    } else {
      this.typePayee = 'COMPANY';
      if (this.getPayeeAsCompanyControl(index).value) {
        this.getPayeeAsPersonControl(index).setValue(false);
      } else {
        this.getPayeeAsPersonControl(index).setValue(true);
      }
    }

    if (this.getPayeeAsPersonControl(index).value || this.getPayeeAsCompanyControl(index).value) {
      this.getNameControl(index).enable();
      this.getLastNameControl(index).enable();
      this.getEmailControl(index).enable();
      this.getWithBankInfoControl(index).enable();
      if (this.userSessionService.isMerchant()) {
        this.getNicknameControl(index).enable();
      }
    } else {

      this.getNameControl(index).disable();
      this.getLastNameControl(index).disable();
      this.getEmailControl(index).disable();
      this.getWithBankInfoControl(index).disable();
      if (this.userSessionService.isMerchant()) {
        this.getNicknameControl(index).disable();
      }
    }
  }

  validatePayeeArrayUniqueEmail(): boolean {
    let error = false;
    for (let i = 0; i < this.payeeArray.value.length; i++) {
      if (this.validateUniqueEmailError(i)) {
        error = true;
      }
    }
    return error;
  }

  validatePayeeArrayUniqueBankAccount(): boolean {
    let error = false;
    for (let i = 0; i < this.payeeArray.value.length; i++) {
      if (this.validateUniqueBankAccountNumberError(i)) {
        error = true;
      }
    }
    return error;
  }

  validateValidPayeeArray(): boolean {
    let error = true;
    for (let i = 0; i < this.payeeArray.value.length; i++) {
      if (!this.getPayeeAsPersonControl(i).value && !this.getPayeeAsCompanyControl(i).value) {
        error = false;
      }
    }
    return error;
  }

  validateValidCLABE(): boolean {
    let error = false;
    for (let i = 0; i < this.payeeArray.value.length; i++) {
      if (this.getClabeBankAccountNumberControl(i).invalid) {
        error = true;
      }
    }
    return error;
  }

  validateValidCPFCNPJ(): boolean {
    let error = false;
    for (let i = 0; i < this.payeeArray.value.length; i++) {
      if (this.getNumberControl(i).invalid) {
        error = true;
      }
    }
    return error;
  }

  validateValidCBU(): boolean {
    let error = false;
    for (let i = 0; i < this.payeeArray.value.length; i++) {
      if (this.getCbuBankAccountNumberControl(i).invalid) {
        error = true;
      }
    }
    return error;
  }

  atLeastOneWithBankInfoHandler(): boolean {
    let atLeastOne = false;
    for (let i = 0; i < this.payeeArray.length; i++) {
      if (this.getWithBankInfoControl(i).value) {
        atLeastOne = true;
      }
    }
    return atLeastOne;
  }

  cleanPayeeArrayHandler() {
    if (this.payeeArray.length > 1) {
      let hasRemove = false;
      for (let i = 0; i < this.payeeArray.value.length; i++) {
        if (!this.getPayeeAsPersonControl(i).value && !this.getPayeeAsCompanyControl(i).value) {
          this.payeeArray.removeAt(i);
          this.subtractCount.emit(1);
          hasRemove = true;
          break;
        }
      }
      if (hasRemove) { // avoid infinite recursion
        this.cleanPayeeArrayHandler();
      }
    }
  }

  validacionInput(e: any, type: string, addCHaracterSpecialc = null) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    let numeros = '';
    if (type === 'numeros') {
      numeros = '0123456789';
    } else if (type === 'letras') {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz ';
    } else {
      numeros = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    }

    if (addCHaracterSpecialc !== null) {
      numeros = numeros + addCHaracterSpecialc;
    }

    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }

  /**
  * Servicio del autocomplete de ciudad
   * @param id 
   * @param v 
   */
  getDataAutocompleteService(id, v) {
    if (this.getCitiesArray(id).value === '') {
      document.getElementById("errorMessage-"+id).style.color = "red";
      document.getElementById("cityNamePayee"+id).classList.add("errorInput");
    } else {
      document.getElementById("errorMessage-"+id).style.color = "grey";
      document.getElementById("cityNamePayee"+id).classList.remove("errorInput");
    }
      const city = this.getBankAccountTypeControlInput(id).value;
      this.getCitiesArray(id).setValue(null)
      if (city.length > 2) {
        const url = '/api/cities/citiesbycountry';
        const body = {
          subStr: city,
          countryId: 1
        };
        this.requestService.postRequest(url, body).subscribe(req => {
          const request: any = req;
          this.getCitiesArray(id).setValue(request);
          if (request.length === 0 ||this.getCitiesArray(id).value === null) {
            document.getElementById("errorMessage-"+id).style.color = "red";
            document.getElementById("cityNamePayee"+id).classList.add("errorInput");
          }
        });
      } else {
        this.getCitiesArray(id).setValue(null);
      }

  }

  dropBoxGetCity() {
      const url = '/api/cities/bycountry?idCountry=1';
      this.requestService.getRequest(url).subscribe(req => {
        const request: any = req;
        this.dataCities = request;
      });
    }

  setDataInput(data, id) {
    // document.getElementById("errorMessage-"+id).style.color = "grey";
    // document.getElementById("cityNamePayee"+id).classList.remove("errorInput");
    // this.getBankAccountTypeControlInput(id).setValue(data.name);
    // this.getCitiesArray(id).setValue(null);
    if (data.split(':')[0] == 1 && data.split(':')[1] == 0){
      this.newCityInput = true;
      this.getBankCityControl(id).setValue(0);
    } else if (data.split(':')[0]) {
      this.newCityInput = false;
      this.getBankCityControl(id).setValue(data.split(':')[0]);
    }
  }

  setNewCity( id,searchValue: string) {
    this.getNewCityBank(id).setValue(searchValue);
  }
  // Acciones al cambiar tipo de cuenta (debito / credito)
  onchangeTipoCuenta(element : any, index : any) {
    this.payeeArray.at(index).get('bankAccountNumber').setValue('');
    let banco = +this.getBankControl(index).value;
    if (element === 'AHO') { // saving acount
        if (banco === 200) { // BCP
          this.maxcuenta = 13;
          this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
        } else if ( banco === 201) { // BBVA
          this.maxcuenta = 20;
          this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
        } else if ( banco === 202) { // INTERBANK
          this.maxcuenta = 13;
          this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
        } else if ( banco === 203) { // SCOTIABANK
          this.maxcuenta = 10;
          this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
        }
    } else { // cuenta corriente
      if (banco === 200) { // BCP
        this.maxcuenta = 14;
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
      } else if ( banco === 201) { // BBVA
        this.maxcuenta = 20;
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
      } else if ( banco === 202) { // INTERBANK
        this.maxcuenta = 13;
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
      } else if ( banco === 203) { // SCOTIABANK
        this.maxcuenta = 10;
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
      }
    }
  }

  onchangeBanco(element : any, index : any) {
    this.maxcuenta = 20;
    this.isInterbank = false;
    let bancoID = element.slice(3).trim();
    this.payeeArray.at(index).get('bankAccountNumber').setValue('');
    this.payeeArray.at(index).get('number').setValue('');
    let tipoCuenta = this.getBankAccountTypeControl(index).value;
    this.minDni = 8;
    this.maxDni = 8;
    this.getNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.minDni), Validators.maxLength(this.maxDni)])
     if (tipoCuenta === 'AHO') { // saving acount
        if (bancoID === '200') { // BCP
          this.maxcuenta = 13;
          this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
        } else if (bancoID === '201') { // BBVA
          this.maxcuenta = 20;
          this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
        } else if ( bancoID === '202') { // INTERBANK
          this.isInterbank = true
          this.maxDni = 10;
          this.minDni = 6;
          this.getNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.minDni), Validators.maxLength(this.maxDni)])
          this.maxcuenta = 13;
          this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
        } else if ( bancoID === '203') { // SCOTIABANK
          this.maxcuenta = 10;
          this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
        }
    } else { // cuenta corriente
      if (bancoID === '200') { // BCP
        this.maxcuenta = 14;
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
      } else if (bancoID === '201') { // BBVA
        this.maxcuenta = 20;
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
      } else if ( bancoID === '202') { // INTERBANK
        this.isInterbank = true
        this.maxDni = 10;
        this.minDni = 6;
        this.getNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.minDni), Validators.maxLength(this.maxDni)])
        this.maxcuenta = 13;
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
      } else if ( bancoID === '203') { // SCOTIABANK
        this.maxcuenta = 10;
        this.getBankAccountNumberControl(index).setValidators([Validators.required, noWhitespaceValidator, Validators.minLength(this.maxcuenta)])
      }
    } 
  }
}
