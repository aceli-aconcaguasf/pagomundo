import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ExtendedUser } from '../../../config/interfaces';
import { MessageNotificationComponent } from '../../../components/messageNotification/messageNotification.component';
import { UserSessionService } from '../../../services/user/userSession.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ROLES, AUTOCOMPLETE_TYPE } from '../../../config/enums';
import { Location } from '@angular/common';
import { UserFormComponent } from '../../../components/forms/userForm/userForm.component';
import { InvitePayeeFormComponent } from './invitePayeeForm/invitePayeeForm.component';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-new-user',
  templateUrl: './newUser.component.html',
  styleUrls: ['./newUser.component.scss'],
})
export class NewUserComponent implements OnInit {

  extendedUser: ExtendedUser;
  role: string;
  roles = ROLES;
  autocompleteType = AUTOCOMPLETE_TYPE;
  count = 0;
  countrySelectedId = 0;

  errorValidation = false;
  createUserError = false;
  emailAlreadyExistError = false;
  userFormInvalid = false;

  userListAlreadyExistAndAssociated = [];
  userListAlreadyExistAndNotAssociated = [];

  newUserRole: string;
  merchantSelected: any;
  countrySelected: any = null;

  countryForm: FormGroup;
  merchantCompanySearchForm: FormGroup;
  changeClass: number;
  datePickerConfig: Partial<BsDatepickerConfig>;
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  keyLanguage$: Observable<any> = this.store.select('language');
  @ViewChild('MessagesComp', { static: false }) messageComponent: MessageNotificationComponent;
  @ViewChild('UserForm', { static: false }) userFormComponent: UserFormComponent;
  @ViewChild('InvitePayeeForm', { static: false }) invitePayeeFormComponent: InvitePayeeFormComponent;

  constructor(private router: Router,
    private location: Location,
    private userService: UserService,
    public userSessionService: UserSessionService,
    private store: Store<language>,
    private translate: TranslateService) {
    this.translate.use(localStorage.getItem("Pm1-4m3r1c4s-l4ngu4g3"));
    this.initDatePicker();
    this.extendedUser = this.userSessionService.loadExtendedUser();
    this.role = this.userSessionService.loadRole();
    this.initVariables();
    this.keyLanguage$.subscribe(item => {
      this.getNewUserTitle();
    });
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem("Pm1-4m3r1c4s-ch4ng3Cl4ss");
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  static setIdType(body, postalCountryId) {

    Object.defineProperty(body.users[0], 'idType', {
      value: {},
      enumerable: true
    });
    if (postalCountryId === 1) { // ID = 1 Colombia
      Object.defineProperty(body.users[0].idType, 'id', {
        value: 1,
        enumerable: true
      }); // CEDULA DE CIUDADANIA
    }
    else if (postalCountryId === 74) { // ID = 74 Brazil
      Object.defineProperty(body.users[0].idType, 'id', {
        value: 19,
        enumerable: true
      }); // RFC
    }
    else { // ID = 2 Mexico
      Object.defineProperty(body.users[0].idType, 'id', {
        value: 15,
        enumerable: true
      }); // RFC
    }
  }

  static setTaxIdType(body, residenceCountryId) {
    Object.defineProperty(body.users[0], 'idTypeTaxId', {
      value: {},
      enumerable: true
    });
    if (residenceCountryId === 1) { // ID = 1 Colombia
      Object.defineProperty(body.users[0].idTypeTaxId, 'id', {
        value: 16,
        enumerable: true
      }); // NIT
    } else { // ID = 2 Mexico
      Object.defineProperty(body.users[0].idTypeTaxId, 'id', {
        value: 15,
        enumerable: true
      }); // RFC
    }
  }

  ngOnInit(): void { }

  initVariables() {
    const variable: any = this.location.getState();
    this.setCountrySearch();
    if (typeof variable.newUserRole !== 'undefined') {
      // @ts-ignore
      this.newUserRole = variable.newUserRole;
      window.sessionStorage.setItem("NEW-USER-ACTUALY", this.newUserRole);
    } else {
      // @ts-ignore
      this.newUserRole = window.sessionStorage.getItem("NEW-USER-ACTUALY");
    }
    if (this.isSuperOrAdminOrReseller() && this.newUserRole === ROLES.PAYEE) {
      this.setMerchantSearch();
    }
  }

  setMerchantSearch() {
    this.merchantCompanySearchForm = new FormGroup({
      merchantCompany: new FormControl('', Validators.required),
    });
  }

  setCountrySearch() {
    this.countryForm = new FormGroup({
      countryPayee: new FormControl('', Validators.required),
    });
  }

  resetRelatedVariables() {

    this.countrySelected = null;
    this.countrySelectedId = 0;
    this.invitePayeeFormComponent.resetValidatorsOnCountryChange();
  }

  onChangePayeeCountry(country) {

    this.countrySelected = country;
    this.countrySelectedId = this.countrySelected.id;
  }

  onChangeMerchantCompany(merchant) {

    this.merchantSelected = merchant;
  }

  // AS SUPER_ADMIN
  createAdminUser() {
    let body;
    if (this.invitePayeeFormComponent.payeeArray.valid) {
      body = {
        usersRole: ROLES.ADMIN,
        users: [{
          email: this.invitePayeeFormComponent.payeeArray.value[0].email,
          firstName: this.invitePayeeFormComponent.payeeArray.value[0].name.trim(),
          lastName: this.invitePayeeFormComponent.payeeArray.value[0].lastName.trim(),
        }],
        country: this.countrySelected
      };
      this.createUserCallMethodHandler(body, ROLES.ADMIN);
    }
    else {
      let errorMessageTranslate: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');

      this.showMessage(errorMessageTranslate.value, 'error');
      this.errorValidation = true;
    }
  }

  // AS MERCHANT, Reseller, ADMIN, SUPER_ADMIN
  createPayeeUser() {
    this.userListAlreadyExistAndAssociated = [];
    this.createUserError = false;
    let body;
    let user;
    let errorMessageTranslate: any;

    if (this.extendedUser.role == "ROLE_MERCHANT") {
      this.merchantSelected = this.extendedUser;
    }

    if (!this.merchantSelected) {
      errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.PLEASE_SELECT_MERCHANT');
      this.showMessage(errorMessageTranslate.value, 'error');
      return false;
    }
    
    if (!this.invitePayeeFormComponent.validateValidPayeeArray()) {
      errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.PLEASE_SELECT_PAYE');
      this.showMessage(errorMessageTranslate.value, 'error');
      return false;
    }

    this.invitePayeeFormComponent.cleanPayeeArrayHandler();

    if (this.invitePayeeFormComponent.payeeArray.valid) {
      const uniqueMailError = this.invitePayeeFormComponent.validatePayeeArrayUniqueEmail();
      if (uniqueMailError) {

        errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.THERE_ARE_DUPLICATE');
        this.showMessage(errorMessageTranslate.value, 'error');
        return false;
      }

      body = {
        toWhom: {
          id: this.userSessionService.isMerchant() ? this.extendedUser.id : this.merchantSelected.id,
        },
        usersRole: ROLES.PAYEE,
        users: [],
        country: {}
      };

      if (this.invitePayeeFormComponent.atLeastOneWithBankInfoHandler()) {
        Object.defineProperty(body, 'country', {
          value: {},
          enumerable: true
        });
        Object.defineProperty(body.country, 'id', {
          value: this.countrySelected.id,
          enumerable: true
        });
      }

      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.invitePayeeFormComponent.payeeArray.value.length; i++) {
        user = {
          email: this.invitePayeeFormComponent.getEmailControl(i).value,
          firstName: this.invitePayeeFormComponent.getNameControl(i).value.trim(),
          lastName: this.invitePayeeFormComponent.getLastNameControl(i).value.trim(),
          payeeAsCompany: this.invitePayeeFormComponent.getPayeeAsCompanyControl(i).value,
        };

        if (this.userSessionService.isMerchant()) {
          Object.defineProperty(user, 'nickname', {
            value: this.invitePayeeFormComponent.getNicknameControl(i).value.trim(),
            enumerable: true
          });
        }

        if (this.invitePayeeFormComponent.getWithBankInfoControl(i).value) {

          switch (this.countrySelected.id) {

            case 1: // ID 1 = COLOMBIA
              if (this.invitePayeeFormComponent.getPayeeAsCompanyControl(i).value) {
                Object.defineProperty(user, 'taxId', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idTypeTaxId', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idTypeTaxId, 'id', {
                  value: 16,
                  enumerable: true
                }); // 16 = NIT
              }
              else {
                Object.defineProperty(user, 'idNumber', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idType', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idType, 'id', {
                  value: 1,
                  enumerable: true
                }); // 1 = CEDULA DE CIUDADANIA
              }
              Object.defineProperty(user, 'bankAccountType', {
                value: this.invitePayeeFormComponent.getBankAccountTypeControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'directPaymentBank', {
                value: {},
                enumerable: true
              });
              Object.defineProperty(user.directPaymentBank, 'id', {
                value: this.invitePayeeFormComponent.getBankControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'directPaymentCity', {
                value: {},
                enumerable: true
              });
              Object.defineProperty(user.directPaymentCity, 'id', {
                value: this.invitePayeeFormComponent.getBankCityControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user.directPaymentCity, 'id', {
                value: this.invitePayeeFormComponent.getBankCityControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'bankAccountNumber', {
                value: this.invitePayeeFormComponent.getBankAccountNumberControl(i).value.trim(),
                enumerable: true
              });
              Object.defineProperty(user, 'newCityBank', {
                value: this.invitePayeeFormComponent.getNewCityBank(i).value.trim(),
                enumerable: true
              });
              break;
            case 2: // ID 2 = MEXICO
              if (this.invitePayeeFormComponent.getPayeeAsCompanyControl(i).value) {
                Object.defineProperty(user, 'taxId', {
                  value: this.invitePayeeFormComponent.getClabeBankAccountNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idTypeTaxId', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idTypeTaxId, 'id', {
                  value: 15,
                  enumerable: true
                }); // 15 = RFC
              }
              else {

                this.invitePayeeFormComponent.payeesForm.value.payeeArray[i].number = this.invitePayeeFormComponent.getClabeBankAccountNumberControl(i).value;
                this.invitePayeeFormComponent.getNumberControl(i).setValue(this.invitePayeeFormComponent.payeesForm.value.payeeArray[i].number);
                debugger; Object.defineProperty(user, 'idNumber', {
                  value: this.invitePayeeFormComponent.getClabeBankAccountNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idType', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idType, 'id', {
                  value: 15,
                  enumerable: true
                }); // 15 = RFC
              }
              Object.defineProperty(user, 'bankAccountNumber', {
                value: this.invitePayeeFormComponent.getClabeBankAccountNumberControl(i).value.trim(),
                enumerable: true
              });
              break;
            case 66: // ID 66 = ARGENTINA
              if (this.invitePayeeFormComponent.getPayeeAsCompanyControl(i).value) {
                Object.defineProperty(user, 'taxId', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idTypeTaxId', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idTypeTaxId, 'id', {
                  value: 18,
                  enumerable: true
                }); // 18 = CUIT
              } else {
                Object.defineProperty(user, 'idNumber', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idType', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idType, 'id', {
                  value: 17,
                  enumerable: true
                }); // 17 = DNI
              }
              Object.defineProperty(user, 'bankAccountNumber', {
                value: this.invitePayeeFormComponent.getCbuBankAccountNumberControl(i).value.trim(),
                enumerable: true
              });
              break;
            case 74: // Brazil
              if (this.invitePayeeFormComponent.getPayeeAsCompanyControl(i).value) {
                Object.defineProperty(user, 'taxId', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idTypeTaxId', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idTypeTaxId, 'id', {
                  value: 20,
                  enumerable: true
                }); // 16 = NIT
                Object.defineProperty(user.idType, 'id', {
                  value: 20,
                  enumerable: true
                });
              }
              else {
                Object.defineProperty(user, 'idNumber', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idType', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idTypeTaxId, 'id', {
                  value: 19,
                  enumerable: true
                }); // 16 = NIT
                Object.defineProperty(user.idType, 'id', {
                  value: 19,
                  enumerable: true
                });
              }

              Object.defineProperty(user, 'bankAccountType', {
                value: this.invitePayeeFormComponent.getBankAccountTypeControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'directPaymentBank', {
                value: {},
                enumerable: true
              });
              Object.defineProperty(user.directPaymentBank, 'id', {
                value: this.invitePayeeFormComponent.getBankControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'directPaymentCity', {
                value: {},
                enumerable: true
              });
              Object.defineProperty(user.directPaymentCity, 'id', {
                value: this.invitePayeeFormComponent.getBankCityControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'bankAccountNumber', {
                value: this.invitePayeeFormComponent.getBankAccountNumberControl(i).value.trim(),
                enumerable: true
              });

              Object.defineProperty(user, 'bankBranch', {
                value: this.invitePayeeFormComponent.getBankBranchControl(i).value.trim(),
                enumerable: true
              });
              break;

              case 101: // Perú
              if (this.invitePayeeFormComponent.getPayeeAsCompanyControl(i).value) {
                Object.defineProperty(user, 'taxId', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idTypeTaxId', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idTypeTaxId, 'id', {
                  value: 22,
                  enumerable: true
                }); // 22 = DNI
              }
              else {
                Object.defineProperty(user, 'idNumber', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idType', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idType, 'id', {
                  value: 22,
                  enumerable: true
                }); // 22 = dni
              }

              Object.defineProperty(user, 'bankAccountType', {
                value: this.invitePayeeFormComponent.getBankAccountTypeControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'directPaymentBank', {
                value: {},
                enumerable: true
              });
              Object.defineProperty(user.directPaymentBank, 'id', {
                value: this.invitePayeeFormComponent.getBankControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'directPaymentCity', {
                value: {},
                enumerable: true
              });
              Object.defineProperty(user.directPaymentCity, 'id', {
                value: this.invitePayeeFormComponent.getBankCityControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'bankAccountNumber', {
                value: this.invitePayeeFormComponent.getBankAccountNumberControl(i).value.trim(),
                enumerable: true
              });
              break;

              case 115: // VENEZUELA
              if (this.invitePayeeFormComponent.getPayeeAsCompanyControl(i).value) {
                Object.defineProperty(user, 'taxId', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idTypeTaxId', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idTypeTaxId, 'id', {
                  value: 23,
                  enumerable: true
                }); // 23
              }
              else {
                Object.defineProperty(user, 'idNumber', {
                  value: this.invitePayeeFormComponent.getNumberControl(i).value.trim(),
                  enumerable: true
                });
                Object.defineProperty(user, 'idType', {
                  value: {},
                  enumerable: true
                });
                Object.defineProperty(user.idType, 'id', {
                  value: 23,
                  enumerable: true
                }); // 23 = dni
              }

              Object.defineProperty(user, 'bankAccountType', {
                value: this.invitePayeeFormComponent.getBankAccountTypeControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'directPaymentBank', {
                value: {},
                enumerable: true
              });
              Object.defineProperty(user.directPaymentBank, 'id', {
                value: this.invitePayeeFormComponent.getBankControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'directPaymentCity', {
                value: {},
                enumerable: true
              });
              Object.defineProperty(user.directPaymentCity, 'id', {
                value: this.invitePayeeFormComponent.getBankCityControl(i).value,
                enumerable: true
              });
              Object.defineProperty(user, 'bankAccountNumber', {
                value: this.invitePayeeFormComponent.getBankAccountNumberControl(i).value.trim(),
                enumerable: true
              });
              break;





            case 77: //CHILE
              Object.defineProperty(user.idTypeTaxId, 'id', {
                value: 21,
                enumerable: true
              }); // 21 = RUT

            Object.defineProperty(user, 'bankAccountType', {
              value: this.invitePayeeFormComponent.getBankAccountTypeControl(i).value,
              enumerable: true
            });
            Object.defineProperty(user, 'directPaymentBank', {
              value: {},
              enumerable: true
            });
            Object.defineProperty(user.directPaymentBank, 'id', {
              value: this.invitePayeeFormComponent.getBankControl(i).value,
              enumerable: true
            });
            Object.defineProperty(user, 'bankAccountNumber', {
              value: this.invitePayeeFormComponent.getBankAccountNumberControl(i).value,
              enumerable: true
            });
            break;
          }
        }

        body.country = this.countrySelected;
        if (user.bankAccountNumber !== undefined) {
          if (this.validateLenghtNumber(user)) {
            body.users = [];
            return;
          }
        }
        body.users.push(user);
      }
console.log(body);
      this.createUserCallMethodHandler(body, ROLES.PAYEE);
    }
    else {

      this.errorValidation = true;
      if (this.countrySelected.id == 74) {

        if (this.invitePayeeFormComponent.payeesForm.value["payeeArray"][0].payeeAsCompany) {

          if (this.invitePayeeFormComponent.payeesForm.value["payeeArray"][0].withBankInfo &&
          (this.invitePayeeFormComponent.payeesForm.value["payeeArray"][0].bankAccountNumber.length < 6
            || this.invitePayeeFormComponent.payeesForm.value["payeeArray"][0].bankAccountNumber.length > 13)) {
            return false;
          }
        }
        else {
          if (this.invitePayeeFormComponent.payeesForm.value["payeeArray"][0].withBankInfo &&
          (this.invitePayeeFormComponent.payeesForm.value["payeeArray"][0].bankAccountNumber.length < 6
            || this.invitePayeeFormComponent.payeesForm.value["payeeArray"][0].bankAccountNumber.length > 13)) {
            return false;
          }
        }
      }
      
      if (this.invitePayeeFormComponent.validateValidCLABE()) { // Error clae mexico
        return false;
      }

      if (this.invitePayeeFormComponent.validateValidCBU()) {// ERROR CBU ARGENTINA
        return false;
      }

      if (this.invitePayeeFormComponent.validateValidCPFCNPJ()) {
        return false;
      }

      errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_THE_REQUI');
      this.showMessage(errorMessageTranslate.value, 'error');
    }
  }

  validateLenghtNumber(obj: any) {
    let band = false;
    let message = '';

    if (this.countrySelected.id == 77 && obj.payeeAsCompany && (obj.bankAccountNumber.length > 13 || obj.bankAccountNumber.length < 2) ) {
      band = true;
      message = 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_RUT_10';
    }
    else if (this.countrySelected.id == 74 &&  obj.payeeAsCompany && obj.taxId.length !== 14) {
      band = true;
      message = 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_CPF_CNPJ_14';
    }
    else if (this.countrySelectedId === 66 &&  obj.payeeAsCompany && obj.taxId.length !== 11) { // ID = 66 Argentina
      band = true;
      message = 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_ID_NIT_11';
    }
    else if ( this.countrySelected.id == 1 && obj.payeeAsCompany && obj.taxId.length !== 14) {
      band = true;
      message = 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_NUMBER_DOCUMENT_14';
    }
    else if (this.countrySelected.id == 2 && !obj.payeeAsCompany && obj.idNumber.length !== 18) {
      band = true;
      message = 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_CPF_CNPJ_14';
    }
    if (band) {
      const errorMessageTranslate: any = this.translate.get(message);
      this.showMessage(errorMessageTranslate.value, 'error');
    }
    return band;
  }

  // CREATE MERCHANT OR Reseller
  createMerchantOrReSellerUser() {

    this.userFormInvalid = false;
    this.emailAlreadyExistError = false;
    let body;
    this.evaluateUserCountry('postal');
    this.evaluateUserCountry('residence');
    let errorMessageTranslate: any;

    if (this.userFormComponent.userForm.valid) {

      body = {
        usersRole: this.newUserRole === ROLES.RESELLER ? ROLES.RESELLER : ROLES.MERCHANT,
        toWhom: {
          id: this.extendedUser.id
        },
        users: [{
          email: this.userFormComponent.userEmail.value,
          firstName: this.userFormComponent.userFirstName1.value.trim(),
          lastName: this.userFormComponent.userLastName1.value.trim(),
          mobileNumber: this.userFormComponent.userMobileNumber.value.trim(),
          birthDate: new Date(this.userFormComponent.userBirthDate.value),
          company: this.userFormComponent.userCompany.value.trim(),
          postalAddress: `${this.userFormComponent.userPostalAddress.value.trim()} | ${this.userFormComponent.userCityPostal.value.trim()} | ${this.userFormComponent.usercpPostalAddress.value.trim()}`,
          residenceAddress: `${this.userFormComponent.userResidenceAddress.value.trim()} | ${this.userFormComponent.userCityResidence.value.trim()}  | ${this.userFormComponent.usercpResidenceAddress.value.trim()}`,
          postalCountry: {
            id: this.userFormComponent.userCountryPostal.value.id,
          },
          residenceCountry: {
            id: this.userFormComponent.userCountryResidence.value.id,
          },
          taxId: this.userFormComponent.userTaxId.value.trim(),
          idNumber: this.userFormComponent.userIdNumber.value.trim(),
        }],
        country: this.countrySelected
      };

      if ((this.userFormComponent.userCountryPostal.value.id === 1) || (this.userFormComponent.userCountryPostal.value.id === 2)) {
        NewUserComponent.setTaxIdType(body, this.userFormComponent.userCountryPostal.value.id);
      }

      if ((this.userFormComponent.userCountryResidence.value.id === 1) || (this.userFormComponent.userCountryResidence.value.id === 2)) {

        NewUserComponent.setIdType(body, this.userFormComponent.userCountryResidence.value.id);
      }

      this.createUserCallMethodHandler(body, this.newUserRole);
    } else {
      this.userFormInvalid = true;

      errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.COMPLETE_FIELDS');
      this.showMessage(errorMessageTranslate.value, 'error');
      return false;
    }
  }

  createUserCallMethodHandler(body, typeUser) {

    let errorMessageTranslate: any;

    this.userService.createMassiveUsers(body).subscribe(
      (data) => {
        if (typeUser === ROLES.ADMIN || typeUser === ROLES.PAYEE) {
          if (data.userNotCreated.length > 0) {
             errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.FOLLOWING_PAYEES');

            // this.showMessage(errorMessageTranslate.value, 'warning');
            this.showMessage(data.userNotCreated[0].statusReason, 'warning');
            this.resetValues();
            this.createUserError = true;
            // this.invitePayeeFormComponent.handleCantCreatePayeeError(data);
          }
          else if (data.userExistingAndAssociated.length === 0 && data.userExistingAndNotAssociated.length === 0) {

            let textPayeeCreated: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.PAYEES_cREATED');
            let textAdminCreated: any = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.ADMIN_CREATED');
            let textSuccess: any = this.translate.get('TEXT.SUCCESS');

            this.resetValues();
            this.resetInviteForm();
            this.showMessage(typeUser === ROLES.PAYEE ? textPayeeCreated.value : textAdminCreated.value, 'success');
          } else if (data.userExistingAndNotAssociated.length > 0 &&
            data.userExistingAndAssociated.length === 0 && data.usersCreatedAndAssociated.length === 0) {
            this.resetValues();
            this.resetInviteForm();

            errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.CANT_ASSOCIATED');
            this.showMessage(errorMessageTranslate.value, 'warning');
          } else if (data.userExistingAndAssociated[0].statusReason !== null) {
            this.resetValues();
            this.resetInviteForm();
            this.showMessage(data.userExistingAndAssociated[0].statusReason, 'error');
          } else {
            this.resetValues();
            this.resetInviteForm();
            errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.USER_CREATED_ASSOCIATED');
            this.showMessage(errorMessageTranslate.value, 'success');
          }

          // User Existing And Associated
          if (data.userExistingAndAssociated.length > 0) {
            this.userListAlreadyExistAndAssociated = data.userExistingAndAssociated;
          } else {
            this.userListAlreadyExistAndAssociated = [];
          }

          // User Existing And Not Associated
          if (data.userExistingAndNotAssociated.length > 0) {
            this.userListAlreadyExistAndNotAssociated = data.userExistingAndNotAssociated;
          } else {
            this.userListAlreadyExistAndNotAssociated = [];
          }
        }
        else {

          if (data.userNotCreated.length !== 0 || data.userNotCreated === null) {

            if (this.newUserRole === ROLES.RESELLER) {

              errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.RESELLER_NOT_CREATED');
            }
            else {

              errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.MERCHANT_COULDNT_CREATED');
            }

            this.showMessage(errorMessageTranslate.value, 'warning');
          }
          else if (data.usersCreatedAndAssociated.length > 0) {

            if (this.newUserRole === ROLES.RESELLER) {

              errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.RE_SELLER_cREATED');
            }
            else {

              errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.MERCHANT_CREATED');
            }

            this.showMessage(errorMessageTranslate.value, 'success');
            this.userFormComponent.userForm.reset();
            this.userFormComponent.ngOnInit();
            this.initVariables();
          } else if (data.userExistingAndNotAssociated.length > 0) {
            if (typeUser === ROLES.MERCHANT && this.userSessionService.isReSeller()) {
              this.emailAlreadyExistError = true;
              const existUser = data.userExistingAndNotAssociated[0];
              errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.EMAIL_ALREDY_USED');

              this.showMessage(errorMessageTranslate.value, 'error');
            }
            else {
              this.emailAlreadyExistError = true;
              const existUser = data.userExistingAndNotAssociated[0];
              errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.EMAIL_ALREDY_USED');
              this.showMessage(errorMessageTranslate.value, 'error');
            }
          } else if (data.userExistingAndAssociated > 0) {
            if (typeUser === ROLES.MERCHANT && this.userSessionService.isReSeller()) {
              this.emailAlreadyExistError = true;
              const existUser = data.userExistingAndAssociated[0];
              errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.EMAIL_ALREDY_USED');
              this.showMessage(errorMessageTranslate.value, 'error');
            } else {
              this.emailAlreadyExistError = true;
              const existUser = data.userExistingAndAssociated[0];
              errorMessageTranslate = this.translate.get('ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.EMAIL_ALREDY_USED');
              this.showMessage(errorMessageTranslate.value, 'error');
            }
          }
        }
      });
  }

  evaluateUserCountry(type: string) {
    if (type === 'postal') {
      this.userFormComponent.userCountryPostal.value.id ?
        this.userFormComponent.userCountryPostal.setErrors(null) :
        this.userFormComponent.userCountryPostal.setErrors({
          incorrect: true
        });
    } else {
      this.userFormComponent.userCountryResidence.value.id ?
        this.userFormComponent.userCountryResidence.setErrors(null) :
        this.userFormComponent.userCountryResidence.setErrors({
          incorrect: true
        });
    }
  }

  resetValues() {
    this.count = 0;
    this.errorValidation = false;
  }

  resetInviteForm() {
    this.invitePayeeFormComponent.reset();
    this.invitePayeeFormComponent.ngOnInit();
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  showMessageFromInvitedUser(message) {
    this.showMessage(message, 'error');
  }

  addUser() {
    this.errorValidation = false;
    if (this.count < 9) {
      this.invitePayeeFormComponent.addPayeeToArray();
      this.count++;
    }
  }

  getNewUserTitle(): string {

    let trans = "";//TITLES
    let errorMessageTranslate: any;
    let value;
    switch (this.newUserRole) {
      case ROLES.ADMIN:
        errorMessageTranslate = this.translate.get('TEXT.CREATE_AN_ADMIN');
        value = errorMessageTranslate.value;
        break;
      case ROLES.RESELLER:
        errorMessageTranslate = this.translate.get('TEXT.CREATE_A_RESELLER');
        value = errorMessageTranslate.value;
        break;
      case ROLES.MERCHANT:
        errorMessageTranslate = this.translate.get('TEXT.CREATE_A_MERCHANT');
        value = errorMessageTranslate.value;
        break;
      case ROLES.PAYEE:
        errorMessageTranslate = this.translate.get('TEXT.CREATE_AN_PAYEE');
        value = errorMessageTranslate.value;
        break;
    }
    return value;
  }

  canSeeContent(type: string): boolean {
    let value = false;
    switch (type) {
      case 'USER_WITHOUT_FORM':
        if (this.userSessionService.isMerchant() ||
          (this.userSessionService.isSuperOrAdmin() && this.newUserRole === ROLES.ADMIN) ||
          (this.merchantSelected && this.isSuperOrAdminOrReseller() && this.newUserRole === ROLES.PAYEE)) {
          value = true;
        }
        break;
      case 'USER_WITHOUT_FORM_HEADER':
        if (this.userSessionService.isMerchant() ||
          (this.isSuperOrAdminOrReseller() && this.newUserRole === ROLES.PAYEE)) {
          value = true;
        }
        break;
      case 'USER_WITHOUT_FORM_BODY':
        if (this.userSessionService.isMerchant() ||
          (this.userSessionService.isSuperOrAdmin() && this.newUserRole === ROLES.ADMIN) ||
          (this.isSuperOrAdminOrReseller() && this.newUserRole === ROLES.PAYEE)) {
          value = true;
        }
        break;
      case 'USER_WITH_FORM':
        if (this.isSuperOrAdminOrReseller() && (this.newUserRole === ROLES.MERCHANT || this.newUserRole === ROLES.RESELLER)) {
          value = true;
        }
        break;
      case 'MERCHANT_NAME_FORM':
        if (this.isSuperOrAdminOrReseller() && this.newUserRole === ROLES.PAYEE) {
          value = true;
        }
        break;
      case 'COUNTRY_FORM':
        if (this.userSessionService.isMerchant() ||
          (this.isSuperOrAdminOrReseller() && this.merchantSelected && this.newUserRole === ROLES.PAYEE)) {
          value = true;
        }
        break;
      default:
        break;
    }
    return value;
  }

  isSuperOrAdminOrReseller(): boolean {
    return this.userSessionService.isSuperOrAdmin() || this.userSessionService.isReSeller();
  }

  initDatePicker() {
    this.datePickerConfig = Object.assign({}, {
      dateInputFormat: 'YYYY-MM-DD',
      dateInputFormatSend: 'YYYY-MM-DD',
    });
  }

  validacionInput(e: any, type: string) {
    const key =  e.keyCode || e.which;
    const teclado = String.fromCharCode(key);
    const numeros = type === 'numeros' ? "0123456789" :
    'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789 ';
    const especiales = ['8','164','165'];
    let teclado_especial = false;

    for (let i in especiales) {
      if (key ==especiales[i]) {
        teclado_especial = true;
      }
    }

    if (numeros.indexOf(teclado) == -1 && !teclado_especial) {
      return false;
    }
  }
}
