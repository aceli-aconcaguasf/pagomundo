import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {merge, of as observableOf, BehaviorSubject, Observable} from 'rxjs';
import {DataFinderService, UserService} from '../../../../services/service.index';
import {MessageNotificationComponent} from '../../../../components/components.index';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ROLES, STATUS} from '../../../../config/enums';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-process-payment',
  templateUrl: './processPayee.component.html',
  styleUrls: ['./processPayee.component.scss'],
})
export class ProcessPayeeComponent implements AfterViewInit {
  totalUsersToProcess = [];

  banks = [];
  allBankAccounts = [];
  bankId = '';

  uri = '/api/_search/extended-users';
  query = '';

  payeeCountries = [];
  countrySelected: any;
  filteredPayeeCountries: Observable<string[]> | any;
  countryForm: FormGroup;
  seeBank = false;
  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  data: any[] = [];

  errorMessage = 'Error getting users';
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor(private router: Router,
              private dataFinder: DataFinderService,
              private userService: UserService,
              private translate: TranslateService) {
      this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    this.countryForm = new FormGroup({
      countryPayee: new FormControl('', Validators.required),
    });
    this.initPayeeCountryFilter();
  }

  get countryPayee() { return this.countryForm.get('countryPayee'); }

  ngAfterViewInit() {

    let nameTranslate:any = this.translate.get( 'TEXT.NAME' );
    let loginNameTranslate:any = this.translate.get( 'TEXT.LOGIN_NAME' );
    let emailTranslate:any = this.translate.get( 'TEXT.EMAIL' );
    let mobileNumberTranslate:any = this.translate.get( 'TEXT.MOBILE_NUMBER' );
    let idNumberTranslate:any = this.translate.get( 'TEXT.ID_NUMBER' );

    this.columnNames = {
      check: '', id: idNumberTranslate.value, fullName: nameTranslate.value, loginName: loginNameTranslate.value + ' | ' + emailTranslate.value,
       mobileNumber: mobileNumberTranslate.value
    };
    this.columns = Object.keys(this.columnNames);
  }

  initPayeeCountryFilter() {
    this.filteredPayeeCountries = this.countryPayee.valueChanges
      .pipe(
        startWith(''),
        map(value => this._payeeCountryFilter(value))
      );
  }

  private _payeeCountryFilter(value: string): string[] {
    this.seeBank = false;
    this.bankId = '';
    if (value) {
      if (value.length >= 2) {
        this.loadPayeeCountry(value);
      } else {
        this.payeeCountries = [];
      }
      const filterValue = value.toString().toLowerCase();

      if (this.payeeCountries.find(country =>
        country.name.toString().toLowerCase() === value.toString().toLowerCase())) {

        this.countryPayee.setValue(
          this.payeeCountries.find(country =>
            country.name.toString().toLowerCase() === value.toString().toLowerCase()));
        this.onChangeCountry();
      }

      return this.payeeCountries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }

  loadPayeeCountry(name: string) {
    this.dataFinder.getCountriesForPayeeAndName(name).subscribe(
      data => {
        this.payeeCountries = data.content;
      });
  }

  displayFnPayeeCountry(country): string {
    return country ? country.name : country;
  }

  allCheckElement() {
    // allCheck
    if ((document.getElementById('allCheck') as HTMLInputElement).checked) {
      this.totalUsersToProcess = [...this.data];
      for (const process of this.totalUsersToProcess) {
        (document.getElementById(process.id) as HTMLInputElement).checked = true;
      }
    } else {
      for (const process of this.totalUsersToProcess) {
        (document.getElementById(process.id) as HTMLInputElement).checked = false;
      }
      this.totalUsersToProcess = [];
    }
  }

  getQueryString(): string {
    let query = `role:${ROLES.PAYEE} AND (status:${STATUS.CREATED} OR status:${STATUS.FAILED})`;
    query += this.countrySelected.id !== ''
      ? ` AND postalCountry.id:${this.countrySelected.id}`
      : '';
    return query;
  }

  loadTable() {
    this.query = this.getQueryString();
    // If the user changes the sort order, reset back to the first page.
    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query,
            this.sort.active, this.sort.direction, 100);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          this.totalUsersToProcess = [...data.content];
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
  }

  onChangeCountry() {
    this.banks = [];
    this.allBankAccounts = [];
    if (document.getElementById('banks')) {
      (document.getElementById('banks') as HTMLSelectElement).selectedIndex = 0;
    }
    if (this.countryPayee.value.id) {
      this.countrySelected = this.countryPayee.value;
      this.dataFinder.getAllBankAccounts(this.countrySelected.id).subscribe(
        accounts => {
          this.allBankAccounts = accounts.content;
          const arrayBankId = [];
          for (const account of this.allBankAccounts) {
            const index = account.bank.id;
            if (arrayBankId.indexOf(index) === -1) {
              arrayBankId.push(index);
              this.dataFinder.getBanksById(index).subscribe(
                bank => {
                  // Recharge card doest work for STP (id = 30). STP is in MEX (id Country = 2 )
                  if (bank.content[0] && bank.content[0].id !== 30) {
                    this.banks.push(bank.content[0]);
                  }
                });
            }
          }

          this.seeBank = true;
          setTimeout(() => {
            this.loadTable();
          }, 0);
        });
    } else {
      this.countrySelected = null;
      this.seeBank = false;
    }
  }

  selectBank(event) {
    this.bankId = event.target.value;
  }

  addCheckElement(user: any) {
    if ((document.getElementById(user.id) as HTMLInputElement).checked) {
      this.totalUsersToProcess.push(user);
    } else {
      const index = this.totalUsersToProcess.indexOf(user);
      if (index !== -1) {
        this.totalUsersToProcess.splice(index, 1);
      }
    }
    (document.getElementById('allCheck') as HTMLInputElement).checked = this.data.length === this.totalUsersToProcess.length;
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  processUsers() {
    if (this.totalUsersToProcess.length <= 0) {

      let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SELECT_AT_LEAST' );      
      this.showMessage( messageTranslate.value, 'error');
    } else {
      const extendedUsersIDs = [];
      for (const elem of this.totalUsersToProcess) {
        const id = {
          id: elem.id
        };
        extendedUsersIDs.push(id);
      }

      this.userService.processUser(+this.bankId, extendedUsersIDs).subscribe(
        data => {
          if (data.extendedUsersWithErrors.length === 0) {
            this.router.navigate(['/relatedUsers/generatedUserFile']).then();
          } else {
            this.router.navigateByUrl('/relatedUsers/generatedUserFile', { state: { withErrors: true } }).then();

          }
        });
    }
  }
}