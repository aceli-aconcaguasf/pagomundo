import {AfterViewInit, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {merge, of as observableOf, BehaviorSubject} from 'rxjs';
import {DataFinderService, UserService} from '../../../../services/service.index';
import {MessageNotificationComponent} from '../../../../components/components.index';
import {ROLES, STATUS} from '../../../../config/enums';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-process-payment',
  templateUrl: './processMerchant.component.html',
  styleUrls: ['./processMerchant.component.scss'],
})
export class ProcessMerchantComponent implements AfterViewInit {
  totalUsersToProcess = [];

  uri = '/api/_search/extended-users';
  query = `role:${ROLES.MERCHANT} AND (status:${STATUS.CREATED} OR status:${STATUS.FAILED})`;

  resultsLength = 0;
  isLoadingResults = true;
  isErrorOccurred = false;

  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  data: any[] = [];
  errorMessage = 'Error getting users';
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  changeClass:number;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;

  constructor(private router: Router,
              private cdRef: ChangeDetectorRef,
              private dataFinder: DataFinderService,
              private userService: UserService,
              private translate: TranslateService) {
                this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
                let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
                this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  ngAfterViewInit() {
    this.setColumnsName();
    this.loadTable();
  }

  setColumnsName() {
    let nameTranslate:any = this.translate.get( 'TEXT.NAME' );
    let companyTranslate:any = this.translate.get( 'TEXT.COMPANY' );
    let loginNameTranslate:any = this.translate.get( 'TEXT.LOGIN_NAME' );
    let emailTranslate:any = this.translate.get( 'TEXT.EMAIL' );
    let mobileNumberTranslate:any = this.translate.get( 'TEXT.MOBILE_NUMBER' );
    let idNumberTranslate:any = this.translate.get( 'TEXT.ID_NUMBER' );

    this.columnNames = {
      check: '', company: companyTranslate.value, taxId: 'TAX ID', fullName: nameTranslate.value,
      loginName: loginNameTranslate.value + ' | ' + emailTranslate.value, id: idNumberTranslate.value, mobileNumber: mobileNumberTranslate.value
    };
    this.columns = Object.keys(this.columnNames);
  }

  allCheckElement() {
    // allCheck
    if ((document.getElementById('allCheck') as HTMLInputElement).checked) {
      this.totalUsersToProcess = [...this.data];
      for (const process of this.totalUsersToProcess) {
        (document.getElementById(process.id) as HTMLInputElement).checked = true;
      }
    } else {
      for (const process of this.totalUsersToProcess) {
        (document.getElementById(process.id) as HTMLInputElement).checked = false;
      }
      this.totalUsersToProcess = [];
    }
  }

  loadTable() {
    // If the user changes the sort order, reset back to the first page.
    merge(this.sort.sortChange)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          console.log(this.uri);
          console.log(this.query);
          console.log(this.sort.active);
          console.log(this.sort.direction);
          return this.dataFinder._search(this.uri, this.query,
            this.sort.active, this.sort.direction, 100);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          this.totalUsersToProcess = [...data.content];
         
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);

    this.cdRef.detectChanges();
  }

  addCheckElement(user: any) {
    if ((document.getElementById(user.id) as HTMLInputElement).checked) {
      this.totalUsersToProcess.push(user);
    } else {
      const index = this.totalUsersToProcess.indexOf(user);
      if (index !== -1) {
        this.totalUsersToProcess.splice(index, 1);
      }
    }
    (document.getElementById('allCheck') as HTMLInputElement).checked = this.data.length === this.totalUsersToProcess.length;
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  processUsers() {
    let selectAtLeastOneMerchant:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.SELECT_LEAST_MERCHANT' );
    let merchantProcced:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.MERCHANTS_PROCESSED' );
    if (this.totalUsersToProcess.length <= 0) {
      this.showMessage(selectAtLeastOneMerchant.value, 'error');
    } else {
      const merchantsToProcess = [];
      for (const elem of this.totalUsersToProcess) {
        const id = {
          id: elem.id,
          status: STATUS.IN_PROCESS
        };
        merchantsToProcess.push(id);
      }

      this.userService.changeMassiveUserStatus(merchantsToProcess).subscribe(() => {
        this.showMessage(merchantProcced.value, 'success');
        this.loadTable();
      });

    }
  }

}
