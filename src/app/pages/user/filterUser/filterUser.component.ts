import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {map, startWith} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {DataFinderService} from '../../../services/data-finder.service';
import {ROLES, STATUSES_USER} from '../../../config/enums';

@Component({
  selector: 'app-filter-user',
  templateUrl: './filterUser.component.html',
  styleUrls: ['./filterUser.component.scss']
})

export class FilterUserComponent implements OnChanges {

  @Input() role: string;
  @Input() roleFilterSelected: string;

  @Output() public applyFilterToTable = new EventEmitter<string>();

  keys = Object.keys;
  roles = ROLES;
  statuses = STATUSES_USER;

  showApplyFilter = false;
  collapse = false;

  countryForm: FormGroup;
  countries = [];
  countrySelected: any;
  filteredPayeeCountries: Observable<string[]> | any;

  nameInput = '';
  nicknameInput = '';
  companyInput = '';
  idNumberInput = '';
  emailInput = '';
  aliasInput = '';
  statusId = '';
  filterName = '';
  nameFilter = null;
  onlyPayeeCompany = false;
  checked = false;
  disabled = false;

  userName = [];
  nicknameArr = [];
  AliasArr = [];
  nameStatus = [];
  idNumberArr = [];
  emailArr = [];
  companyArr = [];
  countryFilter = [];
  onlyPayeeCompanyArr = [];
  namePayee = [];
  namePayeeCompany = [];
  changeClass:number;
  mssgEmpty = false;
  constructor(private dataFinder: DataFinderService) {
    this.countryForm = new FormGroup({
      country: new FormControl('', Validators.required),
    });
    this.initCountryFilter();
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  get CountryFilter() {
    return this.countryForm.get('country');
  }

  ngOnChanges(changes: SimpleChanges) {
    this.collapse = false;
    this.resetVariable();
    this.cleanFilter();
    this.setFilterQuery();
  }

  initCountryFilter() {
    this.filteredPayeeCountries = this.CountryFilter.valueChanges
      .pipe(
        startWith(''),
        map(value => this._countryFilter(value))
      );
  }

  loadCountry(name: string) {
    this.dataFinder.getCountriesForName(name).subscribe(data => {
      this.countries = data.content;
    });
  }

  displayFnPayeeCountry(country): string {
    return country ? country.name : country;
  }

  onChangeCountry() {
    if (this.CountryFilter.value.id) {
      this.countrySelected = this.CountryFilter.value;
    } else {
      this.countrySelected = null;
    }
  }

  applyFilter() {
    this.mssgEmpty = false;
    switch (this.filterName) {

      case 'country':
        if (this.countrySelected) {
          let notDuplicated = true;
          for (const country of this.countryFilter) {
            if (country.id === this.countrySelected.id) {
              notDuplicated = false;
            }
          }
          if (notDuplicated) {
            this.countryFilter.push(this.countrySelected);
          }
        } else {
          this.mssgEmpty = true;
          return;
        }
        break;

      case 'status':
        if (this.statusId !== '') {
          const indexStatus = this.nameStatus.indexOf(this.statusId);
          if (indexStatus === -1) {
            this.nameStatus.push(this.statusId);
          }
        } else{ 
          this.mssgEmpty = true;
          this.nameStatus = [];
        }
        break;

      case 'payeeCompany':
        if (this.onlyPayeeCompanyArr.length !== 1) {
          const obj = {
            value: this.onlyPayeeCompany,
            text: this.onlyPayeeCompany ? 'Business Accounts' : 'Payee'
          };
          this.onlyPayeeCompanyArr.push(obj);
        }
        break;

      case 'name':
        if (this.nameInput === '') {
          this.mssgEmpty = true;
          return;
        }
        switch (this.nameFilter) {
          case 'payeeCompany':
            const indexPayeeCompany = this.namePayeeCompany.indexOf(this.nameInput.trim());
            if (indexPayeeCompany === -1) {
              this.namePayeeCompany.push(this.nameInput.trim());
            }
            break;

          case 'payee':
            const indexPayee = this.namePayee.indexOf(this.nameInput);
            if (indexPayee === -1) {
              this.namePayee.push(this.nameInput);
            }
            break;

          default:
            const indexName = this.userName.indexOf(this.nameInput.trim());
            if (indexName === -1) {
              this.userName.push(this.nameInput.trim());
            }
            break;
        }
        break;

      case 'company':
        if (this.companyInput === '') {
          this.mssgEmpty = true;
          return;
        }
        const indexCompany = this.companyArr.indexOf(this.companyInput);
        if (indexCompany === -1) {
          this.companyArr.push(this.companyInput);
        }
        break;

      case 'nickname':
        if (this.nicknameInput === '') {
          this.mssgEmpty = true;
          return;
        }
        const indexNickname = this.nicknameArr.indexOf(this.nicknameInput);
        if (indexNickname === -1) {
          this.nicknameArr.push(this.nicknameInput);
        }
        break;

      case 'idNumber':
        if (this.idNumberInput === '') {
          this.mssgEmpty = true;
          return;
        }
        const indexIdNumber = this.idNumberArr.indexOf(this.idNumberInput);
        if (indexIdNumber === -1) {
          this.idNumberArr.push(this.idNumberInput);
        }
        break;

      case 'email':
        if (this.emailInput === '') {
          this.mssgEmpty = true;
          return;
        }
        const indexEmail = this.emailArr.indexOf(this.emailInput.trim());
        if (indexEmail === -1) {
          this.emailArr.push(this.emailInput.trim());
        }
        break;

        case 'alias':
          if (this.aliasInput === '') {
            this.mssgEmpty = true;
            return;
          }
          const indexAlias = this.AliasArr.indexOf(this.aliasInput.trim());
          if (indexAlias === -1) {
            this.AliasArr.push(this.aliasInput.trim());
          }
          break;

      default:
        break;
    }

    this.setFilterQuery();
    this.resetVariable();
  }

  setFilterQuery() {
    let query = '';

    // COUNTRY
    if (this.countryFilter.length > 0) {
      if (this.countryFilter.length === 1) {
        query += this.isSuperOrAdmin()
          ? `postalCountry.id:${this.countryFilter[0].id}`
          : `extendedUser.postalCountry.id:${this.countryFilter[0].id}`;
      } else {
        for (let index = 0; index < this.countryFilter.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( postalCountry.id:${this.countryFilter[index].id} OR `
              : `( extendedUser.postalCountry.id:${this.countryFilter[index].id} OR `;
          } else {
            if (this.countryFilter.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `postalCountry.id:${this.countryFilter[index].id} OR `
                : `extendedUser.postalCountry.id:${this.countryFilter[index].id} OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `postalCountry.id:${this.countryFilter[index].id} )`
                : `extendedUser.postalCountry.id:${this.countryFilter[index].id} )`;
            }
          }
        }
      }
    }
    // END COUNTRY

    // COMPANY
    if (this.companyArr.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.companyArr.length === 1) {
        query += this.isSuperOrAdmin()
          ? `company:*${this.companyArr[0]}*`
          : `extendedUser.company:*${this.companyArr[0]}*`;
      } else {
        for (let index = 0; index < this.companyArr.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( company:*${this.companyArr[index]}* OR `
              : `( extendedUser.company:*${this.companyArr[index]}* OR `;
          } else {
            if (this.companyArr.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `company:*${this.companyArr[index]}* OR `
                : `extendedUser.company:*${this.companyArr[index]}* OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `company:*${this.companyArr[index]}* )`
                : `extendedUser.company:*${this.companyArr[index]}* )`;
            }
          }
        }
      }
    }
    // END COMPANY

    // NICKNAME
    if (this.nicknameArr.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.nicknameArr.length === 1) {
        query += `nickname:*${this.nicknameArr[0]}*`;
      } else {
        for (let index = 0; index < this.nicknameArr.length; index++) {
          if (index === 0) {
            query += `( nickname:*${this.nicknameArr[index]}* OR `;
          } else {
            if (this.nicknameArr.length - 1 !== index) {
              query += `nickname:*${this.nicknameArr[index]}* OR `;
            } else {
              query += `nickname:*${this.nicknameArr[index]}* )`;
            }
          }
        }
      }
    }
    // END NICKNAME

    // MERCHANT NAME
    if (this.userName.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.userName.length === 1) {
        query += this.isSuperOrAdmin()
          ? `fullName:*${this.userName[0]}*`
          : `extendedUser.fullName:*${this.userName[0]}*`;
      } else {
        for (let index = 0; index < this.userName.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( fullName:*${this.userName[index]}* OR `
              : `( extendedUser.fullName:*${this.userName[index]}* OR `;
          } else {
            if (this.userName.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `fullName:*${this.userName[index]}* OR `
                : `extendedUser.fullName:*${this.userName[index]}* OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `fullName:*${this.userName[index]}* )`
                : `extendedUser.fullName:*${this.userName[index]}* )`;
            }
          }
        }
      }
    }
    // END MERCHANT NAME

    // NAME PAYEE
    if (this.namePayee.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.namePayee.length === 1) {
        query += this.isSuperOrAdmin()
          ? `fullName:*${this.namePayee[0]}*`
          : `extendedUser.fullName:*${this.namePayee[0]}*`;
      } else {
        for (let index = 0; index < this.namePayee.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( fullName:*${this.namePayee[index]}* OR `
              : `( extendedUser.fullName:*${this.namePayee[index]}* OR `;
          } else {
            if (this.namePayee.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `fullName:*${this.namePayee[index]}* OR `
                : `extendedUser.fullName:*${this.namePayee[index]}* OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `fullName:*${this.namePayee[index]}* )`
                : `extendedUser.fullName:*${this.namePayee[index]}* )`;
            }
          }
        }
      }
    }
    // END NAME PAYEE

    // NAME PAYEE COMPANY
    if (this.namePayeeCompany.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.namePayeeCompany.length === 1) {
        query += this.isSuperOrAdmin()
          ? `company:*${this.namePayeeCompany[0]}*`
          : `extendedUser.company:*${this.namePayeeCompany[0]}*`;
      } else {
        for (let index = 0; index < this.namePayeeCompany.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( company:*${this.namePayeeCompany[index]}* OR `
              : `( extendedUser.company:*${this.namePayeeCompany[index]}* OR `;
          } else {
            if (this.namePayeeCompany.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `company:*${this.namePayeeCompany[index]}* OR `
                : `extendedUser.company:*${this.namePayeeCompany[index]}* OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `company:*${this.namePayeeCompany[index]}* )`
                : `extendedUser.company:*${this.namePayeeCompany[index]}* )`;
            }
          }
        }
      }
    }
    // END NAME PAYEE COMPANY

    // WHICH PAYEE SEE
    if (this.onlyPayeeCompanyArr.length === 1) {
      if (query !== '') {
        query += ' AND ';
      }
      query += this.isSuperOrAdmin()
        ? `user.payeeAsCompany:${this.onlyPayeeCompanyArr[0].value}`
        : `extendedUser.user.payeeAsCompany:${this.onlyPayeeCompanyArr[0].value}`;
    }
    // END WHICH PAYEE SEE

    // STATUS
    if (this.nameStatus.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.nameStatus.length === 1) {
        query += this.isSuperOrAdmin()
          ? `status:${this.nameStatus[0]}`
          : `extendedUser.status:${this.nameStatus[0]}`;
      } else {
        for (let index = 0; index < this.nameStatus.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( status:${this.nameStatus[index]} OR `
              : `( extendedUser.status:${this.nameStatus[index]} OR `;
          } else {
            if (this.nameStatus.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `status:${this.nameStatus[index]} OR `
                : `extendedUser.status:${this.nameStatus[index]} OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `status:${this.nameStatus[index]} )`
                : `extendedUser.status:${this.nameStatus[index]} )`;
            }
          }
        }
      }
    }
    // END STATUS

    // ID NUMBER AND TAX ID
    if (this.idNumberArr.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.idNumberArr.length === 1) {
        query += this.isSuperOrAdmin()
          ? `( ( idNumber:*${this.idNumberArr[0]}* AND user.payeeAsCompany:false )`
          : `( ( extendedUser.idNumber:*${this.idNumberArr[0]}* AND extendedUser.user.payeeAsCompany:false )`;
      } else {
        for (let index = 0; index < this.idNumberArr.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( ( ( idNumber:*${this.idNumberArr[index]}* AND user.payeeAsCompany:false ) OR `
              : `( ( ( extendedUser.idNumber:*${this.idNumberArr[index]}* AND extendedUser.user.payeeAsCompany:false ) OR `;
          } else {
            if (this.idNumberArr.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `( idNumber:*${this.idNumberArr[index]}* AND user.payeeAsCompany:false ) OR `
                : `( extendedUser.idNumber:*${this.idNumberArr[index]}* AND extendedUser.user.payeeAsCompany:false ) OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `( idNumber:*${this.idNumberArr[index]}* AND user.payeeAsCompany:false ) )`
                : `( extendedUser.idNumber:*${this.idNumberArr[index]}* AND extendedUser.user.payeeAsCompany:false ) )`;
            }
          }
        }
      }
    }

    if (this.idNumberArr.length > 0) {
      if (query !== '') {
        query += ' OR ';
      }
      if (this.idNumberArr.length === 1) {
        query += this.isSuperOrAdmin()
          ? `( taxId:*${this.idNumberArr[0]}* AND user.payeeAsCompany:true )`
          : `( extendedUser.taxId:*${this.idNumberArr[0]}* AND extendedUser.user.payeeAsCompany:true )`;
      } else {
        for (let index = 0; index < this.idNumberArr.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( ( taxId:*${this.idNumberArr[index]}* AND user.payeeAsCompany:true ) OR `
              : `( ( extendedUser.taxId:*${this.idNumberArr[index]}* AND extendedUser.user.payeeAsCompany:true ) OR `;
          } else {
            if (this.idNumberArr.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `( taxId:*${this.idNumberArr[index]}* AND user.payeeAsCompany:true ) OR `
                : `( extendedUser.taxId:*${this.idNumberArr[index]}* AND extendedUser.user.payeeAsCompany:true ) OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `( taxId:*${this.idNumberArr[index]}* AND user.payeeAsCompany:true ) )`
                : `( extendedUser.taxId:*${this.idNumberArr[index]}* AND extendedUser.user.payeeAsCompany:true ) )`;
            }
          }
        }
      }
      query += ' )';
    }
    // END ID NUMBER ADN TAX ID

    // EMAIL
    if (this.emailArr.length > 0) {
      if (query !== '') {
        query += ' AND ';
      }
      if (this.emailArr.length === 1) {
        query += this.isSuperOrAdmin()
          ? `email:${this.emailArr[0]}`
          : `extendedUser.email:${this.emailArr[0]}`;
      } else {
        for (let index = 0; index < this.emailArr.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( email:${this.emailArr[index]} OR `
              : `( extendedUser.email:${this.emailArr[index]} OR `;
          } else {
            if (this.emailArr.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `email:${this.emailArr[index]} OR `
                : `extendedUser.email:${this.emailArr[index]} OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `email:${this.emailArr[index]} )`
                : `extendedUser.email:${this.emailArr[index]} )`;
            }
          }
        }
      }
    }
    // END EMAIL

    // ALIAS
    if (this.AliasArr.length > 0) {
      if (query !== '') { query += ' AND '; }
      if (this.AliasArr.length === 1) {
        query += this.isSuperOrAdmin()
          ? `alias:*${this.AliasArr[0]}*`
          : `extendedUser.alias:*${this.AliasArr[0]}*`;
      } else {
        for (let index = 0; index < this.AliasArr.length; index++) {
          if (index === 0) {
            query += this.isSuperOrAdmin()
              ? `( alias:*${this.AliasArr[index]}* OR `
              : `( extendedUser.alias:*${this.AliasArr[index]}* OR `;
          } else {
            if (this.AliasArr.length - 1 !== index) {
              query += this.isSuperOrAdmin()
                ? `alias:*${this.AliasArr[index]}* OR `
                : `extendedUser.alias:*${this.AliasArr[index]}* OR `;
            } else {
              query += this.isSuperOrAdmin()
                ? `alias:*${this.AliasArr[index]}* )`
                : `extendedUser.alias:*${this.AliasArr[index]}* )`;
            }
          }
        }
      }
    }
    // END ALIAS

    this.applyFilterToTable.emit(query);
  }

  addFilter(event: any) {
    this.mssgEmpty = false;
    this.filterName = event.target.value;
    this.showApplyFilter = true;
    event.target.selectedIndex = 0;
  }

  resetFilter(typeName: string) {
    switch (typeName) {
      case 'country':
        this.countryFilter = [];
        break;
      case 'company':
        this.companyArr = [];
        break;
      case 'name':
        this.userName = [];
        break;
      case 'nickname':
        this.nicknameArr = [];
        break;
        case 'Alias':
          this.AliasArr = [];
          break;
      case 'seeOnly':
        this.onlyPayeeCompanyArr = [];
        this.onlyPayeeCompany = false;
        break;
      case 'payee':
        this.namePayee = [];
        break;
      case 'payeeCompany':
        this.namePayeeCompany = [];
        break;
      case 'status':
        this.nameStatus = [];
        break;
      case 'idNumber':
        this.idNumberArr = [];
        break;
      case 'email':
        this.emailArr = [];
        break;
      case 'alias':
        this.AliasArr = [];
        break;
      default:
        break;
    }
    this.setFilterQuery();
  }

  deleteFilter(typeName: string, name: any) {
    switch (typeName) {
      case 'userName':
        const indexName = this.userName.indexOf(name);
        this.userName.splice(indexName, 1);
        break;
      case 'nickname':
        const indexNickname = this.nicknameArr.indexOf(name);
        this.nicknameArr.splice(indexNickname, 1);
        break;
      case 'Alias':
        const indexAlias = this.AliasArr.indexOf(name);
        this.AliasArr.splice(indexAlias, 1);
        break;
      case 'payee':
        const indexPayee = this.namePayee.indexOf(name);
        this.namePayee.splice(indexPayee, 1);
        break;
      case 'payeeCompany':
        const indexPayeeCompany = this.namePayeeCompany.indexOf(name);
        this.namePayeeCompany.splice(indexPayeeCompany, 1);
        break;
      case 'company':
        const indexCompany = this.companyArr.indexOf(name);
        this.companyArr.splice(indexCompany, 1);
        break;
      case 'country':
        const indexCountry = this.countryFilter.indexOf(name);
        this.countryFilter.splice(indexCountry, 1);
        break;
      case 'status':
        const indexStatus = this.nameStatus.indexOf(name);
        this.nameStatus.splice(indexStatus, 1);
        break;
      case 'idNumber':
        const indexIdNumber = this.idNumberArr.indexOf(name);
        this.idNumberArr.splice(indexIdNumber, 1);
        break;
      case 'email':
        const indexEmail = this.emailArr.indexOf(name);
        this.emailArr.splice(indexEmail, 1);
        break;
      default:
        break;
    }
    this.setFilterQuery();
  }

  cleanFilter() {
    this.userName = [];
    this.namePayee = [];
    this.namePayeeCompany = [];
    this.nameStatus = [];
    this.countryFilter = [];
    this.nicknameArr = [];
    this.idNumberArr = [];
    this.companyArr = [];
    this.emailArr = [];
    this.AliasArr = [];
    this.onlyPayeeCompanyArr = [];
    this.onlyPayeeCompany = false;
    this.setFilterQuery();
  }

  resetVariable() {
    this.filterName = '';
    this.nameFilter = null;
    this.statusId = '';
    this.nameInput = '';
    this.nicknameInput = '';
    this.companyInput = '';
    this.idNumberInput = '';
    this.emailInput = '';
    this.aliasInput = '';
    this.countrySelected = null;
    this.CountryFilter.reset();
    this.mssgEmpty = false;
    this.showApplyFilter = false;
  }

  isSuperOrAdmin(): boolean {
    return this.role === ROLES.SUPER_ADMIN || this.role === ROLES.ADMIN;
  }

  handleClickFilterToggle() {
    this.initCountryFilter();
    this.resetVariable();
    this.collapse = !this.collapse;
  }

  canNotSeeFilter(filter: string): boolean {
    let value = true;
    switch (filter) {
      case 'country':
      case 'status':
        if (this.role === ROLES.SUPER_ADMIN && this.roleFilterSelected === ROLES.ADMIN) {
          value = false;
        }
        if (this.role === ROLES.PAYEE) {
  
          value = false;
        }
        break;
      case 'Alias':
          if (this.role === ROLES.SUPER_ADMIN && this.roleFilterSelected === ROLES.ADMIN) {
            value = false;
          }
          break;
      case 'Name':
          if (this.role === ROLES.PAYEE) {
  
            value = false;
          }
          break;

      case 'Email':
            if (this.role === ROLES.PAYEE) {
    
              value = false;
            }
            break;
      case 'company':
        if (this.role === ROLES.MERCHANT ||
          (this.isSuperOrAdmin() && this.roleFilterSelected === ROLES.PAYEE) ||
          (this.role === ROLES.SUPER_ADMIN && this.roleFilterSelected === ROLES.ADMIN)) {
          value = false;
        }
        break;
      case 'idNumber':
      case 'payeeCompany':
        if (this.role === ROLES.PAYEE || this.role === ROLES.RESELLER ||
          (this.isSuperOrAdmin() &&
            (this.roleFilterSelected === ROLES.MERCHANT || this.roleFilterSelected === ROLES.RESELLER
              || this.roleFilterSelected === ROLES.ADMIN))) {
          value = false;
        }
        break;
      case 'nickname':
        if (this.role !== ROLES.MERCHANT) {
          value = false;
        }
        break;
      case 'name':
      case 'email':
        if (this.role === ROLES.PAYEE) {
          value = false;
        }
    }
    return value;
  }

  private _countryFilter(value: string): string[] {
    if (value) {
      if (value.length >= 2) {
        this.loadCountry(value);
      } else {
        this.countries = [];
      }
      const filterValue = value.toString().toLowerCase();
      return this.countries.filter(country => country.name.toString().toLowerCase().includes(filterValue));
    }
  }
}