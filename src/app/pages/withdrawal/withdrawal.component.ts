import {Component, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {MessageNotificationComponent} from '../../components/messageNotification/messageNotification.component';
import {UserService, UserSessionService} from '../../services/service.index';
import {ExtendedUser} from '../../config/interfaces';
import {FundModalComponent} from '../../components/fundModal/fundModal.component';
import {ROLES, STATUS} from '../../config/enums';
import {NoteModalComponent} from '../../components/noteModal/noteModal.component';
import {FormControl} from '@angular/forms';
import {TableWithdrawalComponent} from './tableWithdrawal/tableWithdrawal.component';


export enum STATUSES_WITHDRAWAL_FILTER {
  ACCEPTED = 'Accepted',
  REJECTED = 'Rejected',
}

@Component({
  selector: 'app-fund',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.scss']
})
export class WithdrawalComponent {

  extendedUser: ExtendedUser;
  role: string;
  roles = ROLES;
  queryFilter: string;
  status = STATUS;

  selectedIndex = new FormControl(0);
  collapse = false;

  // FILTERS

  keys = Object.keys;
  statuses = STATUSES_WITHDRAWAL_FILTER;
  showApplyFilter = false;
  nameInput = '';
  filterName = '';
  statusId = '';
  nameReSeller = [];
  nameStatus = [];
  accountBalance: any;
  lenguaje : string = window.localStorage['Pm1-4m3r1c4s-l4ngu4g3'];

  errorMessage = 'Error getting funds history';
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;
  changeClass:number;
  @ViewChild('TableWithdrawal', {static: false}) tableWithdrawal: TableWithdrawalComponent;
  @ViewChild('TableWithdrawalHistorical', {static: false}) tableWithdrawalHistorical: TableWithdrawalComponent;

  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private userService: UserService,
              public userSessionService: UserSessionService,
              public dialog: MatDialog) {

    this.extendedUser = this.userSessionService.loadExtendedUser();
    this.role = this.userSessionService.loadRole();
    this.accountBalance = this.userService.accountBalance;
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  openWithdrawalModal(): void {
    this.lenguaje = window.localStorage['Pm1-4m3r1c4s-l4ngu4g3'];
    if (this.extendedUser.status !== STATUS.ACCEPTED) {
      this.showMessage('In disabled state you cannot request withdrawals', 'warning');
      return;
    } else if (this.accountBalance.value < 500 ) {
      if (this.lenguaje === 'es'){
        this.showMessage('El balance de cuenta debe ser mayor a $500', 'warning');
      } else {
        this.showMessage('Account balance must be greater than $500', 'warning');
      } return;
    } else {

      // the modal is the same as funds
      const dialogRef = this.dialog.open(FundModalComponent, {
        data: {
          extendedUser: this.extendedUser,
          type: 'REQUEST_WITHDRAWAL',
        },
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(response => {
        if (response.value) {
          this.userService.requestWithdrawals(response.amount).subscribe(
            () => {
              this.showMessage('Withdrawal request sent', 'success');
              this.tableWithdrawal.loadTable();
            }
          );
        }
      });
    }
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  openActionWithdrawalModal(withdrawalObject): void {
    /*
    * withdrawalObject = {withdrawal, reasonType}
    * reasonType:
    *  Cancel
    *  Accept
    *  Reject
    */
    const dialogRef = this.dialog.open(NoteModalComponent, {
      data: {
        type: 'withdrawal_reason',
        reasonType: withdrawalObject.reasonType
      },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      let actionMsg;
      if (result.value) {
        const updateWithdrawal = {
          id: withdrawalObject.withdrawal.id,
          reason: result.reason
        };

        switch (withdrawalObject.reasonType) {
          case 'Accept':
            Object.defineProperty(updateWithdrawal, 'status', {value: this.status.ACCEPTED, enumerable: true});
            actionMsg = 'accepted';
            break;
          case 'Cancel':
            Object.defineProperty(updateWithdrawal, 'status', {value: this.status.CANCELLED, enumerable: true});
            actionMsg = 'cancelled';
            break;
          case 'Reject':
            Object.defineProperty(updateWithdrawal, 'status', {value: this.status.REJECTED, enumerable: true});
            actionMsg = 'rejected';
            break;
        }

        // same service as funds
        this.userService.updateFunds(updateWithdrawal).subscribe(
          () => {
            this.showMessage(`Withdrawal ${actionMsg}`, 'success');
            this.tableWithdrawal.loadTable();
            if (this.userSessionService.isSuperAdmin()) {
              this.tableWithdrawalHistorical.loadTable();
            }
        });
      }
    });
  }

  changeTab(index) {
    this.selectedIndex.setValue(index);
    this.collapse = false;
    this.cleanFilter();
  }

  addFilter(event: any) {
    this.filterName = event.target.value;
    this.showApplyFilter = true;
    event.target.selectedIndex = 0;
  }

  handleClickFilterToggle() {
    this.resetVariable();
    this.collapse = !this.collapse;
  }

  resetVariable() {
    this.filterName = '';
    this.statusId = '';
    this.nameInput = '';
    this.showApplyFilter = false;
  }

  cleanFilter() {
    this.nameStatus = [];
    this.nameReSeller = [];
    this.queryFilter = this.getQueryFilterString();
  }

  resetFilter(typeName: string) {
    switch (typeName) {
      case 'reseller':
        this.nameReSeller = [];
        break;
      case 'status':
        this.nameStatus = [];
        break;
      default:
        break;
    }
    this.queryFilter = this.getQueryFilterString();
  }

  deleteFilter(typeName: string, name: any) {
    switch (typeName) {
      case 'reseller':
        const indexReSeller = this.nameReSeller.indexOf(name);
        this.nameReSeller.splice(indexReSeller, 1);
        break;
      case 'status':
        const indexStatus = this.nameStatus.indexOf(name);
        this.nameStatus.splice(indexStatus, 1);
        break;
      default:
        break;
    }
    this.queryFilter = this.getQueryFilterString();
  }

  applyFilter() {
    switch (this.filterName) {
      case 'status':
        if (this.statusId !== '') {
          const indexStatus = this.nameStatus.indexOf(this.statusId);
          if (indexStatus === -1) {
            this.nameStatus.push(this.statusId);
          }
        }
        break;
      case 'name':
        const indexReSeller = this.nameReSeller.indexOf(this.nameInput);
        if (indexReSeller === -1) {
          this.nameReSeller.push(this.nameInput);
        }
        break;
      default:
        break;
    }
    this.resetVariable();
    this.queryFilter = this.getQueryFilterString();
  }

  getQueryFilterString(): string {
    let queryFilter = '';

    // Reseller NAME
    if (this.nameReSeller.length > 0) {
      if (this.nameReSeller.length === 1) {
        queryFilter += `extendedUser.fullName:${this.nameReSeller[0]}`;
      } else {
        for (let index = 0; index < this.nameReSeller.length; index++) {
          if (index === 0) {
            queryFilter += `( extendedUser.fullName:${this.nameReSeller[index]} OR `;
          } else {
            if (this.nameReSeller.length - 1 !== index) {
              queryFilter += `extendedUser.fullName:${this.nameReSeller[index]} OR `;
            } else {
              queryFilter += `extendedUser.fullName:${this.nameReSeller[index]} )`;
            }
          }
        }
      }
    }
    // END NAME

    // STATUS
    if (this.nameStatus.length > 0) {
      if (queryFilter !== '') {
        queryFilter += ' AND ';
      }
      if (this.nameStatus.length === 1) {
        queryFilter += `status:${this.nameStatus[0]}`;
      } else {
        for (let index = 0; index < this.nameStatus.length; index++) {
          if (index === 0) {
            queryFilter += `( status:${this.nameStatus[index]} OR `;
          } else {
            if (this.nameStatus.length - 1 !== index) {
              queryFilter += `status:${this.nameStatus[index]} OR `;
            } else {
              queryFilter += `status:${this.nameStatus[index]} )`;
            }
          }
        }
      }
    }
    // END STATUS

    return queryFilter;
  }

}
