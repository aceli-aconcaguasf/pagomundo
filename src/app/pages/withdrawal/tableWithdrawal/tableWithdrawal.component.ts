import {ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {merge, Observable, of as observableOf} from 'rxjs';
import {MatDialog, MatPaginator, MatSort, TooltipPosition} from '@angular/material';
import {DataFinderService, UserService, UserSessionService} from '../../../services/service.index';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {ExtendedUser} from '../../../config/interfaces';
import {MONEY_MOVEMENT, ROLES, STATUS, STATUSES_COLOR, STATUSES_WITHDRAWAL} from '../../../config/enums';
import {environment} from '../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { language } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';


@Component({
  selector: 'app-table-withdrawal',
  templateUrl: './tableWithdrawal.component.html',
  styleUrls: ['./tableWithdrawal.component.scss']
})
export class TableWithdrawalComponent implements OnChanges {

  @Input() extendedUser: ExtendedUser;
  @Input() role: string;
  @Input() filter: string;
  @Input() historical: boolean;

  @Output() public openActionWithdrawalModal = new EventEmitter<any>();

  keys = Object.keys;
  environment = environment;
  uri = '/api/_search/extended-user-funds';
  query = '';
  positionOptions: TooltipPosition = 'above';

  data: any[] = [];
  columns: string[] = [];
  columnNames: { [key: string]: string } = {};
  roles = ROLES;
  status = STATUS;
  statuses = STATUSES_WITHDRAWAL;
  statusesColor = STATUSES_COLOR;
  keyLanguage$: Observable<any> = this.store.select('language');
  resultsLength = 0;
  pageSize = 10;
  isLoadingResults = true;
  isErrorOccurred = false;

  errorMessage = 'Error getting funds history';

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private userService: UserService,
              public userSessionService: UserSessionService,
              public dialog: MatDialog,
              private cdRef: ChangeDetectorRef,
              private dataFinder: DataFinderService,
              private translate: TranslateService,
              private store: Store<language>) {
              this.keyLanguage$.subscribe(item => {
                const language: any = item
                this.translate.use(language.key);
                this.setColumnsName();
              });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initTable();
  }

  initTable() {
    this.setColumnsName();
    setTimeout(() => {
      this.userSessionService.isSuperAdmin() && this.historical ? this.pageSize = 8 : this.pageSize = 10;
      this.loadTable();
    }, 0);
  }

  setColumnsName() {
    let date:any = this.translate.get('DATA_TABLE.DATE');
    let satus:any = this.translate.get('DATA_TABLE.STATUS');
    let amount:any = this.translate.get('DATA_TABLE.AMOUNT');
    let status_reason:any = this.translate.get('DATA_TABLE.STATUS_REASON');
    let reSeller:any = this.translate.get('TEXT.RESELLER');
    if (this.userSessionService.isSuperAdmin()) {
      if (this.historical) {
        this.columnNames = {
          dateCreated: date.value, status: satus.value, amount: amount.value, reseller: reSeller.value, statusReason: status_reason.value,
        };
      } else {
        this.columnNames = {
          dateCreated: date.value, status: satus.value, amount: amount.value, reseller: reSeller.value, statusReason: status_reason.value, action: ''
        };
      }
    } else {
      this.columnNames = {
        lastUpdated: date.value, status: satus.value, amount: amount.value, statusReason: status_reason.value, action: ''
      };
    }

    this.columns = Object.keys(this.columnNames);
  }

  loadTable() {
    this.query = this.setQuery();

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.paginator.pageIndex = 0;

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dataFinder._search(this.uri, this.query, this.sort.active,
            this.sort.direction, this.paginator.pageSize, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isErrorOccurred = false;
          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isErrorOccurred = true;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
    this.cdRef.detectChanges();
  }

  setQuery(): string {
    let query = '';
    this.userSessionService.isReSeller() ?
      query = `extendedUser.id:${this.extendedUser.id} AND category:REQUESTED AND type:${MONEY_MOVEMENT.WITHDRAWAL} `
      : this.historical ?
        query = `(status:${STATUS.ACCEPTED} OR status:${STATUS.REJECTED}) AND category:REQUESTED AND type:${MONEY_MOVEMENT.WITHDRAWAL} `
        : query = `status:${STATUS.CREATED} AND category:REQUESTED AND type:${MONEY_MOVEMENT.WITHDRAWAL} `;

    if (this.filter && this.historical) {
      query += ` AND ${this.filter}`;
    }

    return query;
  }

  handleActionWithdrawal(withdrawal: any, reasonType: string): void {
    /*
    * reasonType:
    *  Cancel
    *  Accept
    *  Reject
    */
    const result = {
      withdrawal,
      reasonType,
    };
    this.openActionWithdrawalModal.emit(result);
  }

}
