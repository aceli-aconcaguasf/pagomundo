import {Component, ViewChild} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {LoaderService, NotificationService, UserService, UserSessionService} from 'src/app/services/service.index';
import {TooltipPosition} from '@angular/material/tooltip';
import {MatPaginator} from '@angular/material/paginator';
import {
  ConfirmationModalComponent,
  IssueStatusChangeModalComponent, IssueStatusReasonModalComponent,
  MessageNotificationComponent,
  NoteModalComponent,
  RecipientNotificationModalComponent
} from 'src/app/components/components.index';
import {MatDialog, MatTabGroup} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ExtendedUser, RecipientData} from '../../config/interfaces';
import {GROUP_RECEIVER, STATUS, ROLES} from '../../config/enums';
import {TableComponent, FilterComponent} from '../../shared/shared.index';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-issue',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  providers: [LoaderService]
})
export class NotificationComponent {

  @ViewChild('Filter', {static: false}) filterComponent: FilterComponent;
  @ViewChild('TableNotificationA', {static: false}) tableComponentA: TableComponent;
  @ViewChild('TableNotificationB', {static: false}) tableComponentB: TableComponent;
  @ViewChild('MessagesComp', {static: false}) messageComponent: MessageNotificationComponent;
  @ViewChild('TabGroup', { static: false }) tabGroup: MatTabGroup;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  extendedUser: any;
  role: string;
  roles = ROLES;

  positionOptions: TooltipPosition =  'above';
  menuCollapse: BehaviorSubject<boolean> = this.userService.menuCollapse;

  createNotification = false;
  isIssue = false;
  isReceiver = false;
  collapse = false;
  isNotificationHTML = false;

  recipientList = [];
  shortRecipientList = [];
  roleRecipientList: string;
  roleAllUserSelected: string;
  relatedUserExtended: string;
  receiverGroup: string;
  seeAllRelatedOfUser = false;
  seeAllUser = false;
  notificationFormInvalid = false;
  recipientExtendedUser: ExtendedUser;
  notificationSelected: any;

  notificationForm: FormGroup;
  selectedIndex = new FormControl(0);

  moreQuantityRecipients = 0;
  totalPagesGetRecipients = 0;
  recipientArrayGetRecipients = [];

  seeAllList = true;
  showSpinner = false;
  isComponentInit = false;
  showToLegend = false;
  filterQuery: string;
  changeClass:number;

  issueSubject: BehaviorSubject<boolean> = this.notificationService.issueSubject;

  constructor(private userService: UserService,
              public userSessionService: UserSessionService,
              private notificationService: NotificationService,
              private loaderService: LoaderService,
              public dialog: MatDialog,
              private translate: TranslateService) {

                this.translate.use( localStorage.getItem( "Pm1-4m3r1c4s-l4ngu4g3" ) );
    this.extendedUser = userSessionService.loadExtendedUser();
    this.role = userSessionService.loadRole();
    this.loadNotification();
    this.isComponentInit = true;
    this.subscribeNewNotification();
    setTimeout(() => {
      this.setIsReceiverValue(this.tabGroup);
    }, 0);
    let Pm14m3r1c4sCh4ng3Cl4ss = window.localStorage.getItem( "Pm1-4m3r1c4s-ch4ng3Cl4ss" );
    this.changeClass = Pm14m3r1c4sCh4ng3Cl4ss == "1" ? 1 : 0;
  }

  get subjectNotification() { return this.notificationForm.get('subject'); }
  get descriptionNotification() { return this.notificationForm.get('description'); }
  // get noteNotification() { return this.notificationForm.get('note'); }

  subscribeNewNotification() {
    if (this.isComponentInit) {
      this.issueSubject.subscribe(value => {
        if (value === this.isIssue) {
          if (this.tableComponentA) { this.tableComponentA.initTable(); }
          if (this.tableComponentB) { this.tableComponentB.initTable(); }
        }
        this.isIssue = value;
        this.getNotificationUnread(this.isIssue ? 'ISSUE' : 'MESSAGE');
      });
    }

  }

  newNotificationButtonHandler() {
    this.createNotification = true;
    this.cleanNotificationData();
    this.initNotificationForm();
  }

  cancelSendNotificationButtonHandler() {
    this.resetNotification();
    this.loadNotification();
  }

  openRecipientNotificationModal() {
    const dialogRef = this.dialog.open(RecipientNotificationModalComponent, {
      width: '75%',
      height: '90%',
      disableClose: true,
      data: {
        list: this.recipientList,
        roleRecipientList: this.roleRecipientList,
        receiverGroupType: this.receiverGroup,
        extendedUserData: this.recipientExtendedUser,
      },
    });
    dialogRef.afterClosed().subscribe((recipientData: RecipientData) => {
      if (recipientData.withValue) {
        this.cleanNotificationData();
        this.receiverGroup = recipientData.groupReceivers;
        this.loadRecipientData(recipientData);
      }
    });
  }

  loadRecipientData(recipientData) {
    this.showSpinner = false;
    switch (recipientData.groupReceivers) {
      case GROUP_RECEIVER.ALL_ADMINS:
        this.roleAllUserSelected = 'administrators';
        this.setSeeMessageLegend('ALL_USER');
        break;
      case GROUP_RECEIVER.ALL_RESELLERS:
        this.roleAllUserSelected = 'Resellers';
        this.setSeeMessageLegend('ALL_USER');
        break;
      case GROUP_RECEIVER.ALL_MERCHANTS:
        this.roleAllUserSelected = 'merchants';
        this.setSeeMessageLegend('ALL_USER');
        break;
      case GROUP_RECEIVER.ALL_PAYEES:
        this.roleAllUserSelected = 'payees';
        this.setSeeMessageLegend('ALL_USER');
        break;
      case GROUP_RECEIVER.ALL_MERCHANTS_FROM_RESELLER:
        this.setSeeMessageLegend('MERCHANT_FROM_RESELLER');
        this.recipientExtendedUser = recipientData.user;
        break;
      case GROUP_RECEIVER.ALL_PAYEES_FROM_MERCHANT:
        this.setSeeMessageLegend('PAYEE_FROM_MERCHANT');
        this.recipientExtendedUser = recipientData.user;
        break;
      case GROUP_RECEIVER.ALL_MERCHANTS_FROM_PAYEE:
        this.setSeeMessageLegend('MERCHANT_FROM_PAYEE');
        this.recipientExtendedUser = recipientData.user;
        break;
      case GROUP_RECEIVER.RECEIVERS:
        this.recipientList = recipientData.recipients;
        this.loadShortRecipientList();
        this.roleRecipientList = recipientData.roleRecipientList;
        this.setSeeMessageLegend('RECEIVERS_LIST');
        break;
    }
  }

  loadShortRecipientList() {
    this.shortRecipientList = [];
    if (this.recipientList.length > 5) {
      for (let i = 0; i < 5; i++) {
        this.shortRecipientList.push(this.recipientList[i]);
      }
      this.moreQuantityRecipients = this.recipientList.length - 5;
      this.seeAllList = false;
    } else {
      this.moreQuantityRecipients = 0;
      this.seeAllList = true;
    }
  }

  removeElementFromList(recipient) {
    const index = this.recipientList.indexOf(recipient);
    if (index !== -1) {
      this.recipientList.splice(index, 1);
    }
    this.loadShortRecipientList();
  }

  seeAllListHandle(value) {
    if (this.recipientList.length > 5) {
      this.seeAllList = value;
    }
  }

  cleanNotificationData() {
    this.seeAllRelatedOfUser = false;
    this.seeAllUser = false;
    this.receiverGroup = '';
    this.roleAllUserSelected = '';
    this.recipientExtendedUser = null;
    this.notificationSelected = null;
    this.recipientList = [];
    this.shortRecipientList = [];
    this.notificationFormInvalid = false;
    this.loadShortRecipientList();
  }

  resetNotification() {
    this.cleanNotificationData();
    this.createNotification = false;
    this.notificationForm.reset({value: ''});
  }

  sendNotificationButtonHandler(type) {
    this.notificationFormInvalid = false;

    if ( type === 'ISSUE') {
      this.subjectNotification.setValidators(Validators.required);
    } else {
      this.subjectNotification.clearValidators();
    }
    this.subjectNotification.updateValueAndValidity();

    if (this.notificationForm.valid) {
      let notificationReceiverBody;
      if (type === 'ISSUE') {
        notificationReceiverBody = {
          notification: {
            subject: this.subjectNotification.value,
            description: this.descriptionNotification.value,
            subCategory: 'ISSUE_CREATED',
          },
        };
      } else {

        let receivers = [];
        let extendedUserId;
        if (this.recipientExtendedUser) {
          extendedUserId = {
            id: this.recipientExtendedUser.id,
          };
        } else {
          extendedUserId = null;
        }
        if (this.recipientList.length > 0) {
          this.recipientList.forEach((recipient) => {
            const elem = {
              id: +recipient.id
            };
            receivers.push(elem);
          });
        } else {
          receivers = null;
        }

        notificationReceiverBody = {
          notification: {
            subject: this.subjectNotification.value ? this.subjectNotification.value : '(No Subject)',
            description: this.descriptionNotification.value,
            subCategory: 'MESSAGE',
            receiverGroup: this.receiverGroup,
            extendedUser: extendedUserId,
          },
          receivers
        };
      }

      let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.NOTIFICACION' );

      this.notificationService.createNotificationReceiver(notificationReceiverBody).subscribe(
        () => {
          this.showMessage( messageTranslate.value, 'success');
          this.refreshTables();
        }
      );
    } else {
      this.notificationFormInvalid = true;
    }
  }

  setSeeMessageLegend(type: string) {
    switch (type) {
      case 'ALL_USER':
        this.seeAllUser = true;
        this.seeAllRelatedOfUser = false;
        break;
      case 'PAYEE_FROM_MERCHANT':
        this.relatedUserExtended = 'payees';
        this.seeAllRelatedOfUser = true;
        this.seeAllUser = false;
        break;
      case 'MERCHANT_FROM_RESELLER':
      case 'MERCHANT_FROM_PAYEE':
        this.relatedUserExtended = 'merchants';
        this.seeAllRelatedOfUser = true;
        this.seeAllUser = false;
        break;
      case 'RECEIVERS_LIST':
        this.seeAllRelatedOfUser = false;
        this.seeAllUser = false;
        break;
    }
  }

  selectNotificationTypeFilter(event) {
    setTimeout(() => {
      this.setIsReceiverValue(this.tabGroup);
    }, 0);
    this.isIssue = event.checked;
    this.createNotification = false;
    this.loadNotification();
    this.getNotificationUnread(this.isIssue ? 'ISSUE' : 'MESSAGE');
  }

  showTabs(type): boolean {
    const sent = 'sent';
    const received = 'received';
    let value = true;
    if (this.isIssue && this.userSessionService.isSuperOrAdmin() && type === sent) {
      value = false;
    }
    if (this.isIssue && !this.userSessionService.isSuperOrAdmin() && type === received) {
      value = false;
    }
    return value;
  }

  initNotificationForm(notificationReceiver?) {
    if (notificationReceiver && notificationReceiver.notification.subCategory === 'DOWNLOADED_FILE') {
      this.isNotificationHTML = true;

      let bodyText = notificationReceiver.description;
      const regards = bodyText.slice(bodyText.indexOf('xlsx') + 4);
      bodyText = bodyText.replace(bodyText.slice(bodyText.indexOf('...')), '');
      bodyText.trim();

      const arrayPath = notificationReceiver.notification.description.split('/');

      setTimeout(() => {
        const description = document.getElementById('notificationHTML');
        description.innerHTML = '';

        const linkContainer = document.createElement('div');
        linkContainer.className = 'report-link';

        const p1 = document.createElement('p');
        const p2 = document.createElement('p');
        p1.innerText = bodyText;
        p2.innerText = regards;

        let messageTranslate:any = this.translate.get( 'TEXT.DOWNLOAD_REPORT' );

        const a = document.createElement('a');
        a.className = 'simple';
        a.download = arrayPath[2] ? arrayPath[2].slice(0, arrayPath[2].indexOf('.xlsx')) : '';
        a.href =  arrayPath[2] ? `/assets/fileBucket/downloadedStatisticsReport/${arrayPath[2]}` : '';
        a.target = '_blank';
        a.innerText = messageTranslate.value;
        linkContainer.append(a);

        description.append(p1);
        description.append(linkContainer);
        description.append(p2);
      }, 100);

      this.notificationForm = new FormGroup({
        subject: new FormControl(notificationReceiver ? notificationReceiver.subject : ''),
        description: new FormControl('', Validators.required),
        note: new FormControl(notificationReceiver ? notificationReceiver.notification.notes : ''),
      });

    } else {
      this.isNotificationHTML = false;
      this.notificationForm = new FormGroup({
        subject: new FormControl(notificationReceiver ? notificationReceiver.subject : ''),
        description: new FormControl(notificationReceiver ? notificationReceiver.description : '', Validators.required),
        note: new FormControl(notificationReceiver ? notificationReceiver.notification.notes : ''),
      });
    }
  }

  loadNotification(notification?, toValue?: boolean) {
    if ( toValue !== undefined) { this.showToLegend = toValue; }
    this.totalPagesGetRecipients = 0;
    this.recipientArrayGetRecipients = [];
    this.createNotification = false;
    this.cleanNotificationData();
    this.initNotificationForm(notification);
    if (notification) {
      this.showSpinner = true;
      this.readNotification(notification);
      this.notificationSelected = notification;
      if (this.showToLegend) {
        this.loadReceivedNotification(notification);
      } else {
        this.loadSentNotification(notification);
      }
    }
  }

  loadReceivedNotification(notification) {
    const recipientNotification = {} as RecipientData;
    recipientNotification.groupReceivers = notification.notification.receiverGroup;
    if (notification.notification.receiverGroup === GROUP_RECEIVER.RECEIVERS) {
      this.notificationService.getAllNotificationReceiverOfMessage(notification.notification.id).subscribe(
        data => {
          this.totalPagesGetRecipients = data.totalPages;
          data.content.forEach(elem => this.recipientArrayGetRecipients.push(elem.receiver));
          if (this.totalPagesGetRecipients > 1) {
            this.getNotificationReceiverOfMessage(notification.notification.id, recipientNotification, 1);
          } else {
            recipientNotification.recipients = this.recipientArrayGetRecipients;
            this.loadRecipientData(recipientNotification);
          }
        }
      );
    } else if (notification.notification.receiverGroup === GROUP_RECEIVER.ALL_MERCHANTS_FROM_RESELLER
      || notification.notification.receiverGroup === GROUP_RECEIVER.ALL_MERCHANTS_FROM_PAYEE
      || notification.notification.receiverGroup === GROUP_RECEIVER.ALL_PAYEES_FROM_MERCHANT) {
      recipientNotification.user = notification.notification.extendedUser;
      this.loadRecipientData(recipientNotification);
    } else {
      this.loadRecipientData(recipientNotification);
    }
  }

  loadSentNotification(notification) {
    const recipientNotification = {} as RecipientData;
    recipientNotification.groupReceivers = GROUP_RECEIVER.RECEIVERS;
    this.notificationService.getAllNotificationSentOfMessage(notification.notification.id).subscribe(
      data => {
        data.content.forEach(elem => this.recipientArrayGetRecipients.push(elem.sender));
        recipientNotification.recipients = this.recipientArrayGetRecipients;
        this.loadRecipientData(recipientNotification);
      }
    );
  }

  getNotificationReceiverOfMessage(id, notification, numPage) {
    this.notificationService.getAllNotificationReceiverOfMessage(id, numPage).subscribe(
      data => {
        data.content.forEach(elem => this.recipientArrayGetRecipients.push(elem.receiver));
        if (numPage === this.totalPagesGetRecipients) {
          notification.recipients = this.recipientArrayGetRecipients;
          this.loadRecipientData(notification);
        } else {
          this.getNotificationReceiverOfMessage(id, notification, numPage + 1);
        }
      }
    );
  }

  changeTab(index) {
    this.selectedIndex.setValue(index);
    this.isReceiver = index !== 1;
    this.resetNotification();
  }

  setIsReceiverValue(tabGroup: MatTabGroup) {
    switch (this.role) {
      case ROLES.SUPER_ADMIN:
        this.isReceiver = this.isIssue;
        break;
      case ROLES.ADMIN:
        this.isReceiver = (tabGroup._tabs.length === 2 && tabGroup.selectedIndex === 0) || this.isIssue;
        break;
      case ROLES.RESELLER:
      case ROLES.MERCHANT:
      case ROLES.PAYEE:
        this.isReceiver = tabGroup.selectedIndex === 0 && !this.isIssue;
        break;
    }
  }

  showMessage(message: string, type: string) {
    this.messageComponent.setTypeMessage(message, type);
  }

  updateNote(): void {
    if (this.userSessionService.isSuperOrAdmin() && this.notificationSelected && this.canEditNotes(this.notificationSelected)) {
      const dialogRef = this.dialog.open(NoteModalComponent, {
        disableClose: true,
        data: {
          type: 'notification_note',
          note: this.notificationSelected.notification.notes,
          notificationIssue: this.notificationSelected.notification
        }
      });

      dialogRef.afterClosed().subscribe(newNote => {
        if (newNote) {
          const notificationUpdate = {
            id: this.notificationSelected.notification.id,
            notes: newNote,
          };
          this.notificationService.updateNotification(notificationUpdate).subscribe(
            (notification) => {
              this.notificationSelected.notification = notification;
              this.loaderService.show();
              setTimeout(() => {
                if (this.tableComponentA) { this.tableComponentA.initTable(); }
                if (this.tableComponentB) { this.tableComponentB.initTable(); }
                this.loaderService.hide();

                let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.ISSUE_NOTE' );

                this.showMessage( messageTranslate.value, 'success');
                this.loadNotification(this.notificationSelected);
              }, 3000);
          });
        } else {
          this.loadNotification(this.notificationSelected);
        }
      });
    }
  }

  canSendNotification(): boolean {
    if (this.createNotification && !this.isIssue && this.recipientList.length > 0) {
      return true;
    } else if (this.createNotification && (this.seeAllRelatedOfUser || this.seeAllUser)) {
      return true;
    } else { return this.createNotification && this.isIssue; }
  }

  canSeeNotificationAction(actionType): boolean {
    if (this.userSessionService.isSuperOrAdmin() &&
        this.notificationSelected.notification.status === STATUS.CREATED && actionType === 'accept') {
      return true;
    } else if (this.userSessionService.isSuperOrAdmin() &&
        this.notificationSelected.notification.status === STATUS.IN_PROCESS &&
        (actionType === 'solve' || actionType === 'reject') &&
        (this.notificationSelected.notification.responsible && this.notificationSelected.notification.responsible.id)
        === this.extendedUser.id) {
      return true;
    } else if ((this.userSessionService.isSuperAdmin() || (this.userSessionService.isSuperOrAdmin() &&
              (this.notificationSelected.notification.responsible && this.notificationSelected.notification.responsible.id)
              === this.extendedUser.id)) &&
              this.notificationSelected.notification.status === STATUS.IN_PROCESS && actionType === 'release') {
      return true;
    } else if (!this.userSessionService.isSuperOrAdmin() &&
        this.notificationSelected.notification.status === STATUS.CREATED && actionType === 'cancel') {
      return true;
    }
  }

  notificationActionHandler(actionType) {
    if (this.notificationSelected) {
      let status;
      let statusReason;
      switch (actionType) {
        case 'solve':
        case 'reject':
        case 'cancel':
          status = actionType === 'solve' ? STATUS.ACCEPTED
                 : actionType === 'reject' ? STATUS.REJECTED : STATUS.CANCELLED;
          const dialogRef = this.dialog.open(IssueStatusReasonModalComponent, {
            disableClose: true,
            data: {
              notification: this.notificationSelected.notification,
              reasonType: actionType === 'solve' ? 'Solve' : actionType === 'reject' ? 'Reject' : 'Cancel',
            },
          });
          dialogRef.afterClosed().subscribe((update) => {
            if (update) {
              statusReason = this.notificationSelected.notification.statusReason;
              let notificationUpdateA = {};
              if (actionType === 'reject') {
                let newNote;
                this.notificationSelected.notification.notes ?
                  newNote = `${this.notificationSelected.notification.notes} \n\n${statusReason}` :
                  newNote = `${statusReason}`;
                notificationUpdateA = {
                  id: this.notificationSelected.notification.id,
                  status,
                  statusReason,
                  notes: newNote,
                };
              } else {
                notificationUpdateA = {
                  id: this.notificationSelected.notification.id,
                  status,
                  statusReason,
                };
              }
              this.updateNotificationServiceCaller(notificationUpdateA);
            }
          });
          break;
        case 'accept':
        case 'release':
          status = actionType === 'accept' ? STATUS.IN_PROCESS : STATUS.CREATED;
          const notificationUpdateB = {
            id: this.notificationSelected.notification.id,
            status,
          };
          this.updateNotificationServiceCaller(notificationUpdateB);
          break;
      }
    }
  }

  updateNotificationServiceCaller(notificationUpdate) {
    this.notificationService.updateNotification(notificationUpdate).subscribe(
      () => {
        this.loaderService.show();
        setTimeout(() => {
          this.refreshTables();
          this.loaderService.hide();

          let messageTranslate:any = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.ISSUE_UPDATE' );

          this.showMessage( messageTranslate.value, 'success');
        }, 3000);
      }
    );
  }

  refreshTables() {
    if (this.tableComponentA) { this.tableComponentA.initTable(); }
    if (this.tableComponentB) { this.tableComponentB.initTable(); }
    this.resetNotification();
  }

  openIssueStatusChangeModal(notification) {
    this.dialog.open(IssueStatusChangeModalComponent, {
      maxWidth: '85%',
      minWidth: '70%',
      maxHeight: '80%',
      disableClose: true,
      data: { notification: notification.notification },
    });
  }

  addFilterToTable(query) {
    this.filterQuery = query ? query : '';
  }

  setCollapse(value) {
    this.collapse = value;
  }

  deleteMessageNotification(notification) {

    let messageTranslate:any;
    messageTranslate = this.translate.get( 'TEXT.DELETE_NOTIFICATION' );

    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      disableClose: true,
      data: {
        title: messageTranslate.value
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const notificationUpdate = {
          id: notification.id,
          status: STATUS.CANCELLED
        };
        this.notificationService.updateNotificationReceiver(notificationUpdate).subscribe(
          () => {
            messageTranslate = this.translate.get( 'ERRORS_WARNINGS_SUCCESS_NOTIFICATION_CARDS.MESSAGE_DELETED' );
            this.showMessage(messageTranslate.value, 'success');
            this.refreshTables();
          });
      }
    });
  }

  readNotification(notification) {
    if (notification.status === STATUS.CREATED || notification.status === STATUS.IN_PROCESS) {
      notification.status = STATUS.ACCEPTED;
      const notificationUpdate = {
        id: notification.id,
        status: STATUS.ACCEPTED,
      };
      this.notificationService.updateNotificationReceiverWithoutLoader(notificationUpdate).subscribe();
    }
  }

  getNotificationUnread(type) {
    this.notificationService.changeNewNotificationValue(type, false);
    const notificationMethod = type === 'ISSUE' ?
      this.notificationService.getAllCreatedIssueOfUser(this.extendedUser)
      : this.notificationService.getAllCreatedMessageOfUser(this.extendedUser);

    notificationMethod.subscribe(
      data => {
        if (data.content.length > 0) {
          this.changeNotificationUnreadToInProcess(type, data.content);
        }
      }
    );
  }

  changeNotificationUnreadToInProcess(type, notifications) {
    const cant = notifications.length;
    let numberIteration = 0;
    notifications.forEach(notification => {
      const notificationUpdate = {
        id: notification.id,
        status: STATUS.IN_PROCESS,
      };
      this.notificationService.updateNotificationReceiverWithoutLoader(notificationUpdate).subscribe(
        () => {
          numberIteration++;
          if (cant === numberIteration) {
            this.getNotificationUnread(type);
          }
        }
      );
    });
  }

  canEditNotes(notificationReceived): boolean {
    let value = false;
    if ((notificationReceived.notification.status === STATUS.IN_PROCESS ||
      notificationReceived.notification.status === STATUS.CREATED)) {
      value = true;
    }
    return value;
  }
}
