import {NgModule} from '@angular/core';

// COMPONENTS
import {
  NoteModalComponent,
  FundModalComponent,
  RelatedUserModalComponent,
  UserModalComponent,
  ChangePasswordModalComponent,
  ProfileModalComponent,
  CommissionModalComponent,
  RecipientNotificationModalComponent,
  IssueStatusChangeModalComponent,
  IssueStatusReasonModalComponent,
  ConfirmationModalComponent,
  CardNumberModalComponent,
  AssociateMerchantModalComponent,
  EditExtUserModalComponent,
  StatusReasonModalComponent,
  NicknameModalComponent,
  PaymentCardFormComponent
} from '../components/components.index';

// MODULES
import {ComponentsModule} from '../components/components.module';
import {SharedModule} from '../shared/shared.module';
import {MatPasswordStrengthModule} from '@angular-material-extensions/password-strength';
import {PipeModule} from '../pipes/pipe.module';
import {FeatherModule} from 'angular-feather';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';

// PAGES
import {PaymentComponent} from './payment/payment.component';
import {NewPaymentComponent} from './payment/newPayment/newPayment.component';
import {ProcessPaymentComponent} from './payment/processPayment/processPayment.component';
import {NotificationComponent} from './notification/notification.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {GeneratedFileComponent} from './payment/generatedFile/generatedFile.component';
import {ChangeStatusComponent} from './payment/changeStatus/changeStatus.component';
import {ProfileComponent} from './profile/profile.component';
import {UserComponent} from './user/user.component';
import {TableUserComponent} from './user/tableUser/tableUser.component';
import {FilterUserComponent} from './user/filterUser/filterUser.component';
import {NewUserComponent} from './user/newUser/newUser.component';
import {MobileMessageComponent} from './mobileMessage/mobileMessage.component';
import {ChangeUserStatusComponent} from './user/changeUserStatus/changeUserStatus.component';
import {ProcessPayeeComponent} from './user/processUser/processPayee/processPayee.component';
import {ProcessMerchantComponent} from './user/processUser/processMerchant/processMerchant.component';
import {GeneratedUserFileComponent} from './user/generatedUserFile/generatedUserFile.component';
import {InvitedUserComponent} from './user/invitedUser/invitedUser.component';
import {SettingComponent} from './setting/setting.component';
import {UserSettingComponent} from './setting/userSetting/userSetting.component';
import {TableUserSettingComponent} from './setting/userSetting/tableUserSetting/tableUserSetting.component';
import {BankSettingComponent} from './setting/bankSetting/bankSetting.component';
import {FundComponent} from './fund/fund.component';
import {TableFundComponent} from './fund/tableFund/tableFund.component';
import {InvitePayeeFormComponent} from './user/newUser/invitePayeeForm/invitePayeeForm.component';
import {WithdrawalComponent} from './withdrawal/withdrawal.component';
import {TableWithdrawalComponent} from './withdrawal/tableWithdrawal/tableWithdrawal.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardCardComponent} from './dashboard/dashboardCard/dashboardCard.component';
import {DashboardTableComponent} from './dashboard/dashboardTable/dashboardTable.component';

import {HTTP_INTERCEPTORS, HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

@NgModule({
  declarations: [
    PaymentComponent,
    NewPaymentComponent,
    ProcessPaymentComponent,
    NotificationComponent,
    StatisticsComponent,
    GeneratedFileComponent,
    ChangeStatusComponent,
    ProfileComponent,
    UserComponent,
    TableUserComponent,
    FilterUserComponent,
    NewUserComponent,
    MobileMessageComponent,
    ChangeUserStatusComponent,
    ProcessMerchantComponent,
    ProcessPayeeComponent,
    GeneratedUserFileComponent,
    InvitedUserComponent,
    SettingComponent,
    UserSettingComponent,
    BankSettingComponent,
    TableUserSettingComponent,
    FundComponent,
    TableFundComponent,
    InvitePayeeFormComponent,
    WithdrawalComponent,
    TableWithdrawalComponent,
    DashboardComponent,
    DashboardCardComponent,
    DashboardTableComponent
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    FeatherModule,
    MatPasswordStrengthModule.forRoot(),
    PipeModule,
    BsDatepickerModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    })
  ],
  exports: [
    ComponentsModule,
    SharedModule,
    PaymentComponent,
    NewPaymentComponent,
    ProcessPaymentComponent,
    NotificationComponent,
    StatisticsComponent,
    ProfileComponent,
    UserComponent,
    TableUserComponent,
    FilterUserComponent,
    NewUserComponent,
    MobileMessageComponent,
    ChangeUserStatusComponent,
    ProcessMerchantComponent,
    ProcessPayeeComponent,
    GeneratedUserFileComponent,
    InvitedUserComponent,
    SettingComponent,
    UserSettingComponent,
    BankSettingComponent,
    TableUserSettingComponent,
    FundComponent,
    TableFundComponent,
    InvitePayeeFormComponent,
    WithdrawalComponent,
    TableWithdrawalComponent,
    DashboardComponent,
    DashboardCardComponent,
    DashboardTableComponent,
  ],
  providers: [],
  entryComponents: [
    NoteModalComponent,
    UserModalComponent,
    RelatedUserModalComponent,
    FundModalComponent,
    ChangePasswordModalComponent,
    ProfileModalComponent,
    CommissionModalComponent,
    RecipientNotificationModalComponent,
    IssueStatusChangeModalComponent,
    IssueStatusReasonModalComponent,
    ConfirmationModalComponent,
    CardNumberModalComponent,
    EditExtUserModalComponent,
    AssociateMerchantModalComponent,
    StatusReasonModalComponent,
    NicknameModalComponent,
    PaymentCardFormComponent
  ],
})
export class PagesModule {
}
