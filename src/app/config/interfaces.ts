export interface FirstLoginData {
  id: number;
  login: string;
  firstName: string;
  lastName: string;
  email: string;
  activated: boolean;
}

export interface User {
  activated: boolean;
  authorities: string[];
  createdBy: string;
  createdDate: Date;
  email: string;
  firstName: string;
  id: number;
  imageUrl: string;
  langKey: string;
  lastModifiedBy: string;
  lastModifiedDate: Date;
  lastName: string;
  login: string;
  merchant: string;
  resetDate: Date;
  payeeAsCompany: boolean;
}

export interface ExtendedUser {
  user: {
    id: number;
    login: string;
    lastName: string;
    firstName: string;
  };
  id: number;
  balance: number;
  status: string;
  lastName1: string;
  lastName2: string;
  firstName1: string;
  firstName2: string;
  fullName: string;
  idNumber: number;
  confirmedProfile: boolean;
  taxId: string;
  gender: string;
  maritalStatus: string;
  residenceAddress: string;
  postalAddress: string;
  cpPostalAddress: string;
  phoneNumber: string;
  mobileNumber: string;
  birthDate: Date;
  email: string;
  company: string;
  imageIdUrl: string;
  imageAddressUrl: string;
  role: string;
  statusReason: string;
  cardNumber: string;
  resellerFixedCommission: number;
  resellerPercentageCommission: number;
  bankCommission: number;
  fxCommission: number;
  rechargeCost: number;
  useMerchantCommission: boolean;
  bankAccountType: string;
  bankAccountNumber: string;
  useDirectPayment: boolean;
  canChangePaymentMethod: boolean;
  bankBranch:string;
  directPaymentBank: {
    id: number;
    name: string;
  };
  directPaymentCity: {
    id: number;
    name: string;
  };
  idType: {
    id: number;
    name: string;
  };
  idTypeTaxId: {
    id: number;
    name: string;
  };
  postalCity: {
    id: number,
    name: string,
    country: {
      id: number,
      name: string
    }
  };
  residenceCity: {
    id: number,
    name: string,
    country: {
      id: number,
      name: string
    }
  };
  postalCountry: {
    id: number,
    name: string,
  };
  residenceCountry: {
    id: number,
    name: string,
  };
}

export interface Payee {
  id: number;
  fullName: string;
  nickname: string;
  amount: number;
  localCurrency: number;
  description: string;
  country: any;
  idNumber: string;
  extendedUserId: number;
  isSelected: boolean;
  payeeAsCompany: boolean;
}

export interface RecipientData {
  groupReceivers: string;
  user: ExtendedUser;
  withValue: boolean;
  recipients: any[];
  roleRecipientList: string;
}
