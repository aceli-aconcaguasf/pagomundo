#!/bin/bash
usage() { colorEcho r "Usage: $0 -v <version number {X.Y.Z}> [-e <environment (default: sandbox)>] [-h]" 1>&2; }
usageAndExit() {  usage;   exit 1; }

colorEcho() {
  local code="\033["
  case "$1" in
  black | bk) color="${code}0;30m" ;;
  red | r) color="${code}1;31m" ;;
  green | g) color="${code}1;32m" ;;
  yellow | y) color="${code}1;33m" ;;
  blue | b) color="${code}1;34m" ;;
  purple | p) color="${code}1;35m" ;;
  cyan | c) color="${code}1;36m" ;;
  gray | gr) color="${code}0;37m" ;;
  *) local text="$1" ;;
  esac
  [ -z "$text" ] && local text="$color${*:2}${code}0m"
  echo -e "$text"
}

while getopts ":h:e:v:p:m:" o; do
  case "$o" in
  h) usageAndExit ;;
  e) ENVIRONMENT=${OPTARG,,} ;;
  v) VERSION=${OPTARG,,} ;;
  *) colorEcho r "" ;;
  esac
done
shift $((OPTIND - 1))

if [[ -z "${ENVIRONMENT}" ]]; then ENVIRONMENT="sandbox"; fi
if [[ -z "${VERSION}" ]]; then usageAndExit; fi

PORT="[managed by nginx]"
case "${ENVIRONMENT}" in
prod)
  API_URL='http://test-pagomundoapi.aconcaguasf.com.ar'
  ;;
sandbox)
  API_URL='http://test-pagomundoapi.aconcaguasf.com.ar'
  ;;
qa)
  API_URL='http://test-pagomundoapi.aconcaguasf.com.ar'
  PORT=80
  ;;
*) usageAndExit ;;
esac

APPLICATION_NAME="${ENVIRONMENT}-pmi-frontend"
NETWORK_NAME="${ENVIRONMENT}-pmi-net"
IMAGE_NAME="${APPLICATION_NAME}"
TAG_NAME="${VERSION}.${ENVIRONMENT}"

IMAGE_NAME="${IMAGE_NAME}:${VERSION}"
BUCKETPATH='/home/aconcagua/fileBucket/'

sed -i "s|APIBASEURL|${API_URL}|g" src/environments/environment.prod.ts

tput reset
echo
usage
echo
echo
colorEcho p "--> ENVIRONMENT:"
colorEcho r "    ${ENVIRONMENT}"

echo
colorEcho p "${ENVIRONMENT} Application:"
colorEcho b "    Name = ${APPLICATION_NAME}"
colorEcho b "    Version = ${VERSION}"
colorEcho b "    Git tag = ${TAG_NAME}"
colorEcho b "    Bucketpath = ${BUCKETPATH}"
colorEcho r "    API_URL = ${API_URL}"

echo
colorEcho p "${ENVIRONMENT} Docker:"
colorEcho b "    Image = ${IMAGE_NAME}"
colorEcho b "    Network name = ${NETWORK_NAME}"
colorEcho b "    Port = ${PORT}"

echo
head -10 src/environments/environment.prod.ts

echo
read -r -p "--> Press enter to start deploying ${IMAGE_NAME} or CTRL+C to abort"

echo
colorEcho p "--> Removing dist & node_modules folders..."
rm -rf dist/
rm -rf node_modules/

echo
colorEcho p "--> npm install..."
npm install

echo
colorEcho p "--> ng build..."
ng build --prod;


colorEcho p "--> Building docker application..."
colorEcho b "    ${IMAGE_NAME}"
docker build -t "${IMAGE_NAME}" .
colorEcho g "--> Application built! <--"
echo

colorEcho p "--> Renaming docker application..."
echo "docker ps -qa -f name=${APPLICATION_NAME}"
if [ "$(docker ps -qa -f name="$APPLICATION_NAME")" ]; then
  colorEcho b "    Docker image '${APPLICATION_NAME}' exists."
  if [ "$(docker container inspect -f '{{.State.Running}}' ${APPLICATION_NAME})" == "true" ]; then
    DOCKER_STOP="docker stop ${APPLICATION_NAME}"
    ($DOCKER_STOP)
    colorEcho b "    Docker '${APPLICATION_NAME}' stopped."
  fi

  APPLICATION_RENAME=$APPLICATION_NAME-$(date +%Y.%m.%d.%H.%M)
  DOCKER_RENAME="docker rename ${APPLICATION_NAME} ${APPLICATION_RENAME}"
  ($DOCKER_RENAME)
  colorEcho b "    Docker '${APPLICATION_NAME}' renamed to ${APPLICATION_RENAME}."
else
  colorEcho b "    Docker '${APPLICATION_NAME}' doesn't exist."
fi

if [[ ${PORT} != "[managed by nginx]" ]]; then APPLICATION_PORT=" -p 8080:${PORT}"; else APPLICATION_PORT=""; fi
DOCKER_DEPLOY="docker run --name ${APPLICATION_NAME} --restart unless-stopped --network ${NETWORK_NAME}${APPLICATION_PORT} -e API_URL=${API_URL} -v ${BUCKETPATH}:/usr/local/apache2/htdocs/assets/fileBucket -d ${IMAGE_NAME}"
colorEcho p "--> Deploying docker..."
($DOCKER_DEPLOY)

echo
colorEcho g "--> ${ENVIRONMENT} application deployed! <--"
echo

colorEcho p "--> Creating git tag..."
colorEcho b "    ${TAG_NAME}"
git tag "${TAG_NAME}"
git push origin --tags

echo
colorEcho g "--> Application deployed successfully! <--"
echo
